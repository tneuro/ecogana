<# : Select.bat
@echo off
set /P envName=What is your desired environment name?:

setlocal
set "psCommand="(new-object -COM 'Shell.Application')^
.BrowseForFolder(0,'Please choose the Anaconda3 folder.',0,0).self.path""
for /f "usebackq delims=" %%I in (`powershell %psCommand%`) do set "folder=%%I"
setlocal enabledelayedexpansion
endlocal

rem echo %folder%

set PY_EXE=%folder%\envs\%envName%\python.exe
rem echo %PY_EXE%
set PY_SHELL="%PY_EXE% "%%1" %%*"
rem echo %PY_SHELL%

reg add HKCR\py_auto_file\shell\open\command /t REG_SZ /d %PY_SHELL% /f
reg add HKCR\Applications\python.exe\shell\open\command /t REG_SZ /d %PY_SHELL% /f

rem also this?
rem assoc .py=Python.File
rem ftype Python.File=%PY_SHELL%
