General Python Setup and ECoGAna Installation
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install_windows
   install_general
   gabilan_user_setup


Other Good Ideas
----------------

Matplotlib Style Settings
~~~~~~~~~~~~~~~~~~~~~~~~~

A file called ``matplotlibrc`` is shipped with the matplotlib `matplotlib`_ plotting library. Here is a snippet from the file provided in the new 2.0 release describing where to find it and where local modifications can be saved

::

    # This is a sample matplotlib configuration file - you can find a copy
    # of it on your system in
    # site-packages/matplotlib/mpl-data/matplotlibrc.  If you edit it
    # there, please note that it will be overwritten in your next install.
    # If you want to keep a permanent local copy that will not be
    # overwritten, place it in HOME/.matplotlib/matplotlibrc (unix/linux
    # like systems) and C:\Documents and Settings\yourname\.matplotlib
    # (win32 systems).


Previous versions had a couple of lousy default settings.  Recommended changes:

* ``backend : Qt4Agg``
* ``backend.qt4 : PySide``
* ``figure.facecolor : white``
* ``figure.dpi : 100`` (can also be set dynamically when saving bitmaps)
* ``image.interpolation : nearest`` (to give MATLAB-style ``imagesc`` plots)

Dynamic style changes
^^^^^^^^^^^^^^^^^^^^^

Also see the section for fonts and text sizes.  `seaborn`_, which is installed during the Anaconda directions, offers capabilities to dynamically change style settings under different contexts.

Animations & video
^^^^^^^^^^^^^^^^^^

Matplotlib has nice capabilities for animation/video writing. I have only used `ffmpeg`_, which is well supported by matplotlib. If you install this encoding software, check these settings

* ``animation.writer : ffmpeg`` 
* ``animation.codec : libx264`` 



.. _matplotlib: http://matplotlib.org/
.. _ffmpeg: https://ffmpeg.org/
.. _seaborn: http://seaborn.pydata.org/
