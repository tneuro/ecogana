Quick Setup (non-WIndows command line)
======================================

Anaconda and Git
----------------

Notes for non-windows installation. Many of these steps are done through a bash-or-similar command line.

#. Download and Install `Anaconda <https://www.continuum.io/downloads>`_ for Python 2.7 (`Miniconda <https://conda.io/miniconda.html>`_ suggested for lightweight install)
#. Start command prompt
#. Update conda version
    
   a. ``$ conda update conda``

#. Using :download:`packages.txt <../../packages.txt>`, create new environment (e.g. "snowflakes")

   a. ``$ conda create --name snowflakes python=2 --file packages.txt -y``

#. Go into new environment
      
   a. ``$ activate snowflakes``

#. Any other packages can be searched/installed individually using conda

   a. ``$ conda search xyz``
   a. ``$ conda install xyz``

#. In particular, these packages might need to be installed by hand (?)

   a. ``conda install -c menpo opencv -y``

#. Get `bitbucket <http://bitbucket.org>`_ account, get links to clone repositories
#. (**RECOMMENDED**) set up an `SSH key`_  and link it with your bitbucket account
#. Get `git <https://git-scm.com/downloads>`_ software.
#. At this stage, this :download:`getFiles.sh <../../getFiles.sh>`
   script may avail you, otherwise proceed with the manual git cloning.
#. Do some Git clones. The bitbucket URLs are displayed here in SSH mode. To use HTTPS mode, change to ``https://username@bitbucket.org/tneuro/repo-name.git``

   a. ``$ git clone git@bitbucket.org:tneuro/ecoglib.git``
   b. ``$ git clone git@bitbucket.org:tneuro/ecogana.git``
   c. ``$ git clone https://github.com/miketrumpis/nitime.git``
   d. ``$ git clone https://github.com/miketrumpis/npTDMS.git``
   e. ``$ cd npTDMS``
   f. ``$ git checkout hdf5_objects``


#. Add folders to path, see lines for .bash_profile or .profile below

::

    LIBPATH=$HOME/ecog_libraries
    export PYTHONPATH=$PYTHONPATH:$LIBPATH/ecoglib:$LIBPATH/ecogana:$LIBPATH/npTDMS:$LIBPATH/nitime
    PFPATH=$HOME/ana_notebooks
    export PYTHONPATH=$PYTHONPATH:$PFPATH
    export PATH=$PATH:$LIBPATH/ecogana/cmdline:$LIBPATH/ecoglib/cmdline
 
These are also recommended in .bash_profile or .profile::

    export QT_API=pyside
    export ETS_TOOLKIT=qt4

Configuration
-------------

`ecogana` needs to be configured to use various data paths.  Follow direction in this section, :ref:`user_config_info`.


.. _SSH KEY: https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html
