Bootstrapping A User On Gabilan
===============================

First make a new user

.. code-block:: bash

    $ sudo adduser --gecos "" <username>
    $ sudo usermod -G jhub_users <username>

Connecting remotely
-------------------

Jupyterhub
``````````

First, make a "notebooks" directory to play nicely with jupyterhub.

.. code-block:: bash

    $ mkdir ~/notebooks

And don't forget to add the user to the whitelisted users in `/etc/jupyterhub/jupyterhub_config.py`. You can run a fully-furnished analysis environment (via a Docker container) through Jupyterhub using notebooks. If notebooks satisify your needs, you can skip everything else in this document. Check out :ref:`jupyterhubdetails` for more info about how your gabilan account is linked to jupyterhub.

SSH
```

SSH (ssh keys are secure and convenient, but not required). For windows, Putty and Pageant are available. Use -A to forward your local ssh agent authentication. Use -Y to forward X11 (for graphics -- requires installing an X11 client).

.. code-block:: bash

    $ ssh [-A][-Y] username@gabilan2.egr.duke.edu

X2Go
````

`X2Go <https://wiki.x2go.org/doku.php/download:start>`_ is a open-source implementation of NX that provides compressed graphics transfer (much faster than ssh -Y).

Teamviewer
``````````

Teamviewer is also hosted on gabilan.


pyenv
-----

Boostrapping Python versions in pyenv requires basic C libraries and compiler, which are available on Gabilan any most linux boxes.

.. code-block:: bash

    $ git clone https://github.com/pyenv/pyenv.git ~/.pyenv
    $ echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
    $ echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
    $ echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bashrc
    $ exec "$SHELL"


pyenv virtualenv
````````````````

.. code-block:: bash

    $ git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
    $ echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc
    $ exec "$SHELL"


Python setup
------------

This installs a Python2 and a Python3 (takes a minute), and then sets up one virtual environment for ecog analysis. Pyenv has tab-completion in its subcommands, to hint at the command you want.

.. code-block:: bash

    $ pyenv install 2.7.13
    $ pyenv install 3.6.4
    $ pyenv virtualenv 2.7.13 ecog_py2
    $ pyenv activate ecog_py2

Some install sequences are ticklish -- do this order (under the activated environment). The "mayavi" package requires an "X11" connection, which is present when logged in at the physical screen or when logged in through SSH with X forwarding. I.e. `ssh -Y user@gabilan2.egr.duke.edu`.

.. code-block:: bash

    $ pip install numpy
    $ pip install /srv/jupyterhub/ecog_libraries/third_party/PySide-1.2.4-cp27-cp27mu-linux_x86_64.whl
    $ pip install vtk
    $ pip install pip install -r /srv/jupyterhub/ecog_libraries/ecoglib/requirements.txt
    $ pip install pip install -r /srv/jupyterhub/ecog_libraries/ecogana/requirements.txt

Now install "local" code in -e mode for "develop mode". When the code in the path is changed (e.g. through git pull) then those changes will be tracked in the Python install.

.. code-block:: bash

    $ pip install -e /srv/jupyterhub/ecog_libraries/ecoglib/
    $ pip install -e /srv/jupyterhub/ecog_libraries/ecogana/

Optional, install the LFP scroller GUI

.. code-block:: bash

    $ pip install -e /srv/jupyterhub/ecog_libraries/lfp_scroller

ECOG analysis setup
-------------------

Make a file called `~/.mjt_exp_conf.txt` (that is under the user home directory). This is an example using actual mount-points for gabilan.


.. code-block:: json

   [globals]
   local_exp = ~/recordings
   memory_limit = 16e9
   network_exp = /mnt/nas-media/animal data
   user_sessions = ~/recording-sessions
   clear_temp_converted = True
   filter_highpass = 2
   stash_path = ~/cached_results
   channel_mask = /mnt/nas-media/animal data/channel_mask_db/

One option for setting up recording session configs is to copy them from the jupyterhub-served location

.. code-block:: bash

    $ rsync -aHv /srv/jupyterhub/recording-sessions .

Also make empty directories for the other paths. Optionally, the local experiment path can be blank and the cached results path can point to the main location `/srv/jupyterhub/cached_results/`.

.. code-block:: bash

    $ mkdir ~/recordings
    $ mkdir ~/cached_results



