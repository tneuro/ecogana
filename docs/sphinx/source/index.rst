.. ECoGAna documentation master file, created by
   sphinx-quickstart on Wed Jan 18 10:20:29 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. |date| date::
.. |time| date:: %H:%M
	  
ECoGAna's documentation
=======================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install_stub
   setup_notes
   usage
   jupyterhub
   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Misc
====

Documents rebuilt |date| at |time|.
