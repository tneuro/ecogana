Setup for Windows
=================

I (Mike) do not use/understand Windows. I have spent 0.001% of my
coding hours solving Windows problems. The procedure here installs Python
and setup environment variables (e.g. %PATH% and friends) so that the
Python based lab software can be used through the Windows `cmd.exe`
terminal. In some cases [#f1]_, `.py` files can be "double-clicked" to
run after some registry hacks.

These instructions are pretty seamless for Windows 8 and earlier, and I am aware of a Windows 10
setup that basically works, but not perfectly. Good luck!

Set up your Python 2.7 system
-----------------------------

This code base is developed under Python 2.7. The instruction on this
page refer to using `Anaconda <https://www.continuum.io/downloads>`__ as
a Python environment. Anaconda (or `Miniconda <https://conda.io/miniconda.html>`_ for bare-bones installation) is recommended because it provides:

Package management
~~~~~~~~~~~~~~~~~~

This is helpful because the software ecosystem of Python relies on
multiple inter-connected open source projects. Package management "by
hand" (i.e. downloading, building, and installing code) should be
avoided as far as possible. Anaconda provides package management on the
command line with the ``conda`` command.

Optional: Environment management
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Often one wants to switch between Python systems, to en/disable
different code versions or to switch betweeen 2.7 and 3.4 systems. In
Python, "virtual environments" are typically used to accomplish these
switches. Anaconda provides virtual environment support through the
``activate`` command.

Anaconda and Git Install
~~~~~~~~~~~~~~~~~~~~~~~~

1. Download and install **Anaconda/Miniconda for Python 2.7** from the
   following link in the suggested location (remember where you installed it):

   https://www.continuum.io/downloads
   https://conda.io/miniconda.html

2. Download and install Git from the following link in the suggested
   location with suggested settings:

   https://git-scm.com/downloads

3. Obtain a Bitbucket account and get permission to access the analysis
   code

4. (**RECOMMENDED**) set up an `SSH key`_  and link it with your bitbucket account

Anaconda setup, package management, and Git repository setup
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A collection of scripts will set up a Python environment with the
correct software. The batch file will download and install several
python packages listed in ``packages_win.txt``, and then clone four Git
repositories. For Windows 8 or earlier, registry entries *should be* set up correctly to
run Python scripts from the command line, or (if no arguments are
required) from double-click. For details about registry stuff and file
associations, see the section :ref:`registry_hacks`. 

If Anaconda environments are used, the Windows command path can be modified
during environment activation (see section
:ref:`activate_hooks`). If you are not using environments, or want to permanently set the path to
include tools from ecogana/ecoglib, skip the instructions in
:ref:`activate_hooks` and consider adding this line to the batch file:

::

    setx PATH "%~dp0ecogana\cmdline;%~dp0ecoglib\cmdline;

1. Create a folder where you want to save the analysis code 

   a. **Avoid using spaces anywhere in this path** or else Windows
      will not find command-line programs
   b. ``C:\Users\Joe The Plumber\My Superbad Ana Folder`` **bad!!**
   c. ``C:\Users\JTP\analysis_code`` **good!!**

2. Download and move these files to the folder you created

   a. :download:`packages_win.txt <../../packages_win.txt>`,
   b. :download:`Setup.bat <../../Setup.bat>`, 
   c. :download:`getFiles.sh <../../getFiles.sh>` 

3. Run the Setup.bat file as Administrator
4. Follow instructions on command prompt, including

   a. Enter an Anaconda environment name
   b. Enter bitbucket user info, to clone repositories
   c. Use a pop-up window to point to the Anaconda install path

.. _registry_hacks:

Registry setup and py-file association
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Note: these hacks have worked successfully on Windows 8 and
earlier. I have had some trouble on Windows 10 because perhaps the
registry keys don't look the same anymore.*

These details should be handled magically, but here are the details in
case of problems. (MT set up a system recently where `reg add ...` and
`setx PATH ...` both failed from the Setup.bat script.. wtf)

In the Windows command line, the OS must decode what
to do when one types `script-name.py --opt1 --opt2 arg`. The
instructions for decoding such syntax are in the registry under (at
least) these two keys:

::
    
    HKCR\py_auto_file\shell\open\command
    HKCR\Applications\python.exe\shell\open\command

The value for these keys needs to be set to the equivalent of

::

    C:\path\to\python.exe "%1% %*

Where the path-to-python is **the python.exe found in the virtual environment
created in the previous step**. The code above enables the .py file (`%1%` to
be run with python, and all extra words on the command line (`%*`) are
passed through.

These instructions may also be used (but are redundant?)

::

    assoc .py=Python.File
    ftype Python.File=%PY_SHELL%

.. _activate_hooks:

Windows command-line path for calling python programs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(Paraphrased per directions `here <http://conda.pydata.org/docs/using/envs.html#windows>`_)

**Skip this section if environments are not used.**

Assuming repositories were cloned under the %HOMEPATH% directory. Adjust
as necessary to your correct path.

::

    cd C:\Users\jsmith\Anaconda3\envs\name_of_your_env
    mkdir .\etc\conda\activate.d
    mkdir .\etc\conda\deactivate.d
    type NUL > .\etc\conda\activate.d\env_vars.bat
    type NUL > .\etc\conda\deactivate.d\env_vars.bat

Now add these lines to ``activate.d\env_vars.bat``:

::

    set PATH_PREV=%PATH%
    set PATH=%PATH%;%HOMEPATH%\PythonAnalysis\ecogana\cmdline;%HOMEPATH%\PythonAnalysis\ecoglib\cmdline

And these lines to ``deactivate.d\env_vars.bat``:

::

    set PATH=%PATH_PREV
    set PATH_PREV=

Now after activating your Anaconda environment in a windows command
terminal, programs (such as config\_tool.py and froemke\_ana.py) can be
called on the command line.

.. _config_tool:

Configuration of ecogana behavior
---------------------------------

Map the network drive to this computer if this hasn't been done already.

There is a GUI control for setting up the global variables for the
software (explanations of variables are found in the tool). For more
detail, see :ref:`user_config_info`.

::

    ecogana/cmdline/config_tool.py

On Windows, one should be able to "double-click" that script to run the
program. If the default program has not been set, then first set the
``Anaconda\env\environment-name\python.exe`` program as the default.

If that still does not work:

1. Open Windows command line
2. Enter 'activate DesiredName'
3. 'cd' to the path (if command path was not set correctly)
4. Enter 'python config\_tool.py'

.. _SSH KEY: https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html

.. rubric:: Footnotes

.. [#f1] Python files can be executable scripts and/or modules. Most
	 useful "scripts" take command-line arguments. But scripts that
	 require no arguments can, in principle, be run with "double-click". See http://faculty.washington.edu/rjl/uwamath583s11/sphinx/notes/html/python_scripts_modules.html
