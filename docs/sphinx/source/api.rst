.. _api_link:

ECoGAna Package Reference
=========================

Auto-documented API for the ECoGAna set of code.

For the purpose of consolidation, the "ecoglib" package is depicted
as a subpackage of ecogana. Accordingly, any documented code reading
`ecogana.ecoglib.xyz` would in reality read `ecoglib.xyz`.

.. toctree::
   :maxdepth: 1

   apidocs/ecogana
    

