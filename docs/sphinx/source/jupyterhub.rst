.. _jupyterhubdetails:

Computing Via Jupyterhub
========================

The Viventi Lab notebook server is switching to Jupyterhub, a proper web service with indepedent workspaces on a user-by-user basis.

Prior single-user notebook server:

* **Pro**: simple setup & maintenance
* **Pro**: curated Python environment
* **Con**: notebooks must be used one at a time
* **Con**: notebook edits are global

Jupyterhub service:

* **Pro**: each login uses an independent Docker "container"
* **Pro**: curated Python environment
* **Pro**: mixture of global and user-level files
* **Pro**: can choose different containers at login (future)
* **Con**: complex configuration and interaction between gabilan "host" and the service container

Jupyterhub is (per `docs <https://jupyterhub.readthedocs.io/en/stable/>`_):

Three subsystems make up JupyterHub:

* a multi-user Hub (tornado process)
* a configurable http proxy (node-http-proxy)
* multiple single-user Jupyter notebook servers (Python/IPython/tornado) 

JupyterHub performs the following functions:

* The Hub launches a proxy
* The proxy forwards all requests to the Hub by default
* The Hub handles user login and spawns single-user servers on demand
* The Hub configures the proxy to forward URL prefixes to the single-user notebook servers

Using Jupyterhub
----------------

You will need

* A system login on new gabilan (`adduser --gecos "" your-name`)
* To allow write-access to certain file areas, add your user to the "jhub_users" group (`usermod -G jhub_users your-name`)
* **Important** create a directory named "work" under your user directory (`mkdir ~/notebooks`). Failing to do this will prevent you from being able to create new files in your user directory.

When a user authenticates at the Jupyterhub "hub" (`<https://gabilan2.egr.duke.edu:8000>`_ **this service is not yet running full-time**), the "spawner" module spins up a Docker container that runs a curated Python installation. The container is run as a user that is a clone of the host's (gabilan's) authenticated user. 

Notebook
~~~~~~~~

The Notebook server will look very similar to the old single-user server (it is literally a single-user server run from the Docker container). 

.. figure:: images/container_notebook.png
   :scale: 40%
   :alt: Container notebook server
   :align: center
   
   Screenshot of the notebook for a user without any personal files.

The directories shown in the image are shared across all users in the `jhub_users` group. Otherwise, the contents of `~/notebooks` (none in this example) are present here. Multiple kernels are available. Python 2/3 are installed, as is the R language (not shown). Jupyter also supports matlab interaction.

New buttons are also present on the top-right:

* Quit: stops the container service and logs out
* Logout: logs out of the server & hub, but leaves the docker container running in the background. Logging back into the hub within a few hours will attach to the same container service, but note that the container will eventually be closed if idle for 5 hours (see next section).
* Control Panel: for regular users, there are commands to stop & restart the server

**Note** that neither the container environment nor the container user have any inherent connection to the host. We selectively make some connections to the persistent host+user through file management tricks.

Workflow
~~~~~~~~

More code should live in the user space going forward. Unlike the previous server where all notebooks were global, user-space work does not clobber shared work, is individually maintained, and should allow for more analysis novelty. 

The workflow vision is TBD, but something along these lines:

* Test notebooks will be simple tests and demos of the code base
* "Reference code" should be a very stripped down version of the existing notebooks, including basic pre-processing (compile_clicks, distill_sessions, ...) and result plots
* Other sets of existing notebooks will be at the user or shared level, as appropriate
* Major sub-projects should branch off the reference code (i.e. use it as a template) and live in the user-space before graduating to shared or reference code.
* The results cache area is shared, but you are encouraged to store results separately from the existing stores (i.e. parallel subdirectories).

Bulk File Access
~~~~~~~~~~~~~~~~

These options are known to work for connecting to gabilan for file transfers:

* **scp** (`secure copy <https://kb.iu.edu/d/agye>`_)
* `Filezilla <https://filezilla-project.org/>`_
* AFP (apple share protocol): from finder, punch command-k and use afp:://gabilan2.egr.duke.edu

Windows Samba or CIFS volumes would in principle work too, when set up.

Container Lifecycle
~~~~~~~~~~~~~~~~~~~

While the jupyterhub service is active, a user may log in and trigger the creation of a notebook environment where the following steps take place.

#. Temporary user-level configuation files are created (or swapped from existing files)
#. Lab git repositories are updated (git pull)
#. A prepared Docker container is launched as the logged-in user, with various host volumes mounted
#. Lab repositories are installed to the container system
#. A single-user notebook server is started and hosted by the container
#. When the user quits the server, temporary user-level configuration files are destroyed (and existing ones swapped back in)

**Save your work before walking away!** Containers idle for 5 hours are closed for memory management.

Container-host Interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~

The user's directory is mounted as a Docker volume in such a way as to provide some crossover between the host and the container. In particular, config files appear to be respected in the following ways:

Notebook config
+++++++++++++++

Files in `~/.jupyter` affect how the notebooks act. This means that I can turn off bracket and quote auto-completes with the following lines in `~/.jupyter/nbconfig/notebook.json`:

.. code-block:: json

    {
      "CodeCell": {
        "cm_config": {
          "autoCloseBrackets": false
        }
      }
    }

pyenv
+++++

Customizations in `~/.bashrc` and `~/.profile` affect terminals launched through the notebook. This produces some interesting interactions when using `pyenv <https://github.com/pyenv/pyenv>`_ to manage Python virtual environments. That is, pyenv appears to over-ride the notebook terminal just as any other bash instance. If you have pyenv set up, then this behavior is probably what you want. If you do not have pyenv set up, then your Python environment will fall back on the container, which is based on `miniconda <https://conda.io/miniconda.html>`_ with Python 3.6. You can use a Python 2.7 environment by typing `source activate python2-kernel` at the prompt.  

Since pyenv doesn't seem to affect how the notebook server runs, I'll leave the bash files without swapping out.

Other configs
+++++++++++++

Presumably plotting customizations in `~/.config/matplotlib/matplotlibrc` are respected by the notebooks.

Setup Details
-------------

Jupyterhub config files and the Docker image source are found in this repository: `<https://bitbucket.org/tneuro/jupyterhub_spawner/src/master/>`_

Installation
~~~~~~~~~~~~

Installed on Ubuntu Bionic, based on basic `docs <https://jupyterhub.readthedocs.io/en/stable/quickstart.html>`_.
(This is all run as sudo or root)

* sudo apt-get install npm nodejs-legacy
* Jupyterhub (installed in Ubuntu `python3 -m pip install jupyterhub`)

  + Updating: `python3 -m pip install -U pip jupyterhub`

* npm install -g configurable-http-proxy

We use DockerSpawner to launch containers at log-in. So notebook servers are actually run in a Docker container. 

* Docker CE installed via instructions `here <https://docs.docker.com/install/linux/docker-ce/ubuntu/>`_ for Ubuntu Bionic.
* dockerspawner: `python3 -m pip install dockerspawner`

Self-signed SSL certificate per `instructions <https://jupyter-notebook.readthedocs.io/en/latest/public_server.html#using-ssl-for-encrypted-communication>`_.

Cookie:

.. code-block:: bash

    openssl rand -hex 32 > /srv/jupyterhub/secrets/jupyterhub_cookie_secret

Run as 

.. code-block:: bash

    sudo jupyterhub -f /etc/jupyterhub/jupyterhub_config.py

jupyterhub_config.py
~~~~~~~~~~~~~~~~~~~~

* PAM authenticator with whitelisted users

.. code-block:: python

    c.JupyterHub.authenticator_class = 'jupyterhub.auth.PAMAuthenticator'
    c.Authenticator.whitelist = {'miket', 'ashley', 'brinnae', 'hillmj', 'kumar', 'iakov', 'kay', 'katrina'}
    c.Authenticator.admin_users = {'miket'}


* SingleUserSpawner variant of DockerSpawner, with tweak to allow "jhub_users"

.. code-block:: python

    class SystemGroupSpawner(SystemUserSpawner):

        # local unix groups a user might be a member of
        groups = ['jhub_users']

        def get_env(self):
            env = super().get_env()
            # don't set USER env, which SystemUserSpawner uses.
            env.pop('USER',  None)
            # set notebook UID
            env['NB_UID'] = self.user_id
            for groupname in self.groups:
                group = grp.getgrnam(groupname)
                # find the first group in our group list that the user is a member of,
                # and set the group id
                if self.user.name in group.gr_mem:
                    env['NB_GID'] = group.gr_gid
            return env

    # Select our custom Spawner
    c.JupyterHub.spawner_class = SystemGroupSpawner
    # must start container as root in order for docker-stacks to set up NB_UID / NB_GID correctly
    c.SystemGroupSpawner.extra_create_kwargs = {'user': 'root'}

    # This line should be redundant with above,
    # but there's a bug in docker-stacks assuming $UID is the user id
    # BUG: https://github.com/jupyter/docker-stacks/pull/420
    c.SystemGroupSpawner.environment = {'UID': '0'}

    ## The class to use for spawning single-user servers.
    c.DockerSpawner.image = 'labserver'
    c.DockerSpawner.remove_containers = True
    c.DockerSpawner.remove = True
    c.DockerSpawner.notebook_dir = '/home/{username}/notebooks'

* Details

  - This spawner changes the username (and UID) to the login user, and importantly the GID to match the `jhub_users` GID
  - Docker mounts the `/home/{username}` directory on the host to the container
  - The UID trick gives the container user the correct user-level file permissions to the home directory
  - The GID trick gives the container user the correct group-level file permissions to some other mounted volumes

* Mounted volumes

.. code-block:: python

    c.DockerSpawner.volumes = {'/mnt/nas-media': '/mnt/nasmedia'}
    c.DockerSpawner.volumes[os.path.join(content_root, 'recording-sessions')] = '/home/{username}/notebooks/sessions'
    c.DockerSpawner.volumes[os.path.join(content_root, 'ana_scripts')] = '/home/{username}/notebooks/reference_notebooks'
    c.DockerSpawner.volumes[os.path.join(content_root, 'cached_results')] = '/home/{username}/notebooks/cached_results'
    c.DockerSpawner.volumes[os.path.join(content_root, 'test_notebooks')] = '/home/{username}/notebooks/test_notebooks'
    c.DockerSpawner.volumes[os.path.join(content_root, 'signal_diagnostics')] = '/home/{username}/notebooks/signal_diagnostics'
    c.DockerSpawner.volumes[os.path.join(content_root, 'shared_temp')] = '/home/{username}/notebooks/shared_temp'
    c.DockerSpawner.volumes[os.path.join(content_root, 'ecog_libraries')] = '/opt/ecog_libraries'

* Details

  - NAS media drive (permissions inherited from CIFS mount)
  - recording session files (jhub_users)
  - basic "reference" notebooks (jhub_users)
  - test notebooks (read-only)
  - results caches (jhub-users)
  - signal diagnostic scripts (jhub-users)
  - shared work area (jhub-users)
  - ecog python libraries (jhub-users -- mounted to Docker image, but not in the served "notebooks" directory)

There are also pre-start and post-close hooks that trigger before starting a new container and after closing a container. We do the following pre-start hooks:

.. code-block:: python

    c.Spawner.pre_spawn_hook = up_hooks
    c.Spawner.post_stop_hook = down_hooks

* Swap out (and backup) existing global RC files for container-friendly ones: `.mjt_exp_conf.txt, .bashrc, .profile`
* git-pull from the viventi lab code repositories (under `/srv/jupyterhub/ecog_libraries`)

On closing of containers, we restore any swapped-out files.

Dockerfile
~~~~~~~~~~

Based off of the `jupyterhub/minimal-notebook` image. Build sequence is scripted in `build.sh`.

* uses miniconda setup (installed in `/opt/conda`)
* python3 based
* launches a jupyterhub in single-user mode that the main hub's proxy attaches to

There is a kernel corresponding to a python3 conda environment with future versions of `ecogdata` and `ecoglib`. There is also a legacy python2 conda environment with prior `ecogana` and `ecoglib` libraries.

* pip install requirements under this environment

File Management
~~~~~~~~~~~~~~~

Some important content is served from directories in `/srv/jupyterhub/`. Most have group ownership of `jhub_users`. We set up the directory permissions to keep group ownership the same as the group ownership of the parent directory. Here are the steps do change the defaults for an new (empty) directory:

.. code-block:: bash

    chmod g+s directory
    setfacl -d -m g::rwx directory

Now default file permissions for new files/directories inherit group +rw access. To apply these settings for pre-populated directories (e.g. recording-sessions), do these steps:

.. code-block:: bash

    find recording-sessions -type d -print0 | xargs -0 sudo chmod g+s
    find recording-sessions -type d -print0 | xargs -0 sudo setfacl -d -m g::rwx recording-sessions
    find recording-sessions -type f -print0 | xargs -0 sudo chmod g+w

This applies the previous settings on all subdirectories (`-type d`) and then sets group-writeable permissions on all files (`-type f`). The `-print0` and `xargs -0` are there to handle file names with spaces.

To check that you didn't miss anything:

.. code-block:: bash

    find recording-sessions -not -perm /g=w

