Getting started on an ECoGAna "environment"
===========================================

.. _user_config_info:

User configuration file 
------------------------

Some information--mostly filesystem paths--are needed by multiple
components in ecoglib and ecogana. This config file should be found in
``~/.mjt_exp_conf.txt`` (where ``~/`` refers to the user directory in
Unix). This location can be mysterious in Windows, so the
``config_tool.py`` is recommended. This is found in
``ecogana/cmdline/config_tool.py`` and should also be found on the
command path. 

.. figure:: images/config_tool.png
   :scale: 50%
   :alt: confg_tool.py GUI
   :align: center
   
   Image of ``config_tool.py`` window

The config file is **mainly used to define paths**

+ Recording file paths:

   -  Two recording paths defined: **local** and **network**
   -  Under "auto" loading, if a recording is not found on local path,
      it will be searched for in the network path.
   -  The network path is only available if mounted in the file system!

+ Other paths:

   - ``user_sessions``: session config path -- see
     :ref:`session_config_info` below
   - ``channel_mask``: channel mask database path
   -  ``stash_path`` : this is used by many analysis scripts as a
      centralized path to stick caches of computational results. 

+ Unused (?)

   -  ``local_proj`` : path that was used by a handful of tools. The
      idea is to allow the analysis path hierarchy to be decoupled
      (good idea!) from the libraries under version control.

My config settings:

.. code::

  [globals]
  local_exp = /Users/mike/experiment_data/
  memory_limit = 6e9
  local_proj = /Users/mike/work/exp_ana/
  channel_mask = /Volumes/media/animal data/channel_mask_db
  network_exp = /Volumes/media/animal data/
  user_sessions = /Users/mike/work/exp_ana/session_conf
  clear_temp_converted = True
  filter_highpass = 2
  stash_path = /Users/mike/work/cached_results/

.. _session_config_info:

Session configuration files
---------------------------

The experiment session config files are used to assist loading and interpreting
recordings (see :ref:`auto_loading` below). There is no longer a
central repository of config files in ecogana, you must maintain a
local directory with the following structure

::

    group_a/
        config_a1.txt
	config_a2.txt
	resource.txt
    group_b/
        config_b1.txt

In code, sessions are referrenced as ``'group_a/config_a1'`` (always with the
forward slash, even on Windows). Spaces
and strange characters in path names are untested and generally should
be avoided. Addition resources are commonly linked to by the config
files. They should be available either in the session path, or in the
same directory as the config file itself (e.g. ``resource.txt``).

Here is an example named ``froemke/2015-08-11_conf.txt`` that
describes several recordings made during auditory stimulation.

::

    [session]
    exp_path: %(local_exp)s/2015-08-11_Froemke_Lab 
    nwk_path: %(network_exp)s/2015-08-11 Froemke Lab _08112015
    electrode: psv_61_15row
    headstage: mux6
    [DEFAULT]
    bnc : 1
    tone_onset: 0.2
    tone_width: 0.05
    tones_tab: freq_0.5oct_13stim_rot.txt
    amps_tab: 70

    [initial placement]
    [initial_placement2]
    [homecage_anesthetized]

These config files are written to be parsed by
`ConfigParser <https://docs.python.org/2/library/configparser.html>`__
from the Python standard library. There are ``[sections]`` and there are
``parameters : value`` pairs. Two sections should always be defined:

``[session]`` -- contains info regarding recording, including

-  path(s) to find the recordings: ``exp_path`` and/or ``nwk_path``

   + Paths can be absolute, but in this case notice that they
     reference variables set in the "global" config file. This makes
     config files portable between machines.

-  ``headstage`` name -- this will switch which loading method to use
-  ``electrode`` name -- this will be used with
   :py:mod:`ecogana.devices.electrode_pinouts` to find the correct channel map
- In the case of active electrodes, ``daq`` is also expected

``[DEFAULT]`` -- info regarding experiment setup. The examples here are

-  ``bnc``: in this case, stim audio was recorded on BNC chan 1 (counting
   from zero)
-  tone shape parameters (``tone_onset`` and ``tone_width``)
-  ``tones tab``: this points to a text table defining the pitch sequence
-  ``amps tab``: in this case, it is a single entry (otherwise it can be a
   sequence ``10, 20, 30``, or another table file ``amps.txt``)

Recordings with external stimulation
++++++++++++++++++++++++++++++++++++

Use subsequent named sections (like ``[initial_placement]``) to
indicate the path name of recordings that occurred during
external stimulation stimulated. Named recordings trigger the
generation of an object that describes the timing and conditions of
the experiment (see 
:py:class:`ecogana.expconfig.exp_descr.base_exp.StimulatedExperiment`
description). That object gets attached to the "Bunch" container that
holds the loaded dataset as a ``.exp`` attribute.

In the parsed config file, each of these subsections inherit all the
parameters from ``[session]`` and ``[DEFAULT]``. **Note** that *any*
of the previously defined fields (notably ``tones_tab`` or
``amps_tab`` can be over-ridden within each named section, such as

::

    [homecage_anesthetized]
    tones_tab : alternate-table.txt
    electrode : other-electrode-name


Other config file tricks
++++++++++++++++++++++++

Many of the parameters/values in a config are literally translated to
arguments for the underlying data loading method (see
:ref:`data_loading`). Here are some examples.

*Switch the default row-multiplexing ordering in an active electrode*

::

    [session]
    exp_path: /%(local_exp)s/2016-04-22_active
    nwk_path: /%(network_exp)s/Viventi 4-22-2016
    headstage: zif26 to 2x uhdmi
    daq: 2t-as daq v1
    bnc: 10, 11
    electrode: actv_64
    row_order = 0, 1, 2, 3, 4, 5, 6, 7

*Set resample rate to 2 kS/s and store the downsampled raw data in the
local experiment path*

::

    [session]
    exp_path: %(local_exp)s/2016-07-19_panasonic
    nwk_path: %(network_exp)s/Viventi 2016-07-19 Acute
    headstage: oephys
    trigger_idx: 0
    electrode: psv_61_intan
    useFs: 2000
    store_path: %(exp_path)s
    rec_num = 100

**Also note** that any of these parameters can also be set on a
per-recording basis in the corresponding named section.

.. _data_loading:

Loading recording files
-----------------------

Loading methods for recording data is split betweeen recording
systems. Generally, there is a loading module in
:py:mod:`ecogana.devices.load` for each kind of "headstage".
(Headstage refers to the name used in the session config file, which
isn't entirely accurate for, e.g., the "oephys" designation).

From most robust to least robust, these system will work.

1. National Instruments TDMS with multiplexed channels (but see below
   about converting TDMS to HDF5). :py:mod:`ecogana.devices.load.mux`
   and :py:mod:`ecogana.devices.load.active_electrodes` use these
   files. The .mat files from the MATLAB TDMS conversion should work
   interchangeably with HDF5 (i.e. they should literally be HDF5
   files) with a slightly different container.

2. Open-Ephys, implemented in :py:mod:`ecogana.devices.load.open_ephys`

3. Blackrock .ns5 and .ns6 files (these have to be converted to HDF5
   first, and I only know of matlab-based tools to read the .nsX
   files). See the "extra_tools" directory in the ecogana repository.

Loading methods within each of the hardware specific modules have the
general signature

.. code-block:: python

    load_foo(path_to_recs, name_of_rec, electrode, *other_args, **options)

where there are different device-specific flags. 

.. _auto_loading:

Automatic loading
+++++++++++++++++

The experiment config files described in :ref:`session_config_info` go
part of the way towards hiding the complexity of the device-specific
options and presenting a standardized interface. Typical auto-loading
looks like this

.. code-block:: python

    from ecogana.devices.data_util import load_experiment_auto
    data = load_experiment_auto(session, recording, bandpass=(2, 200))

There is also a "manual" loading interface that is still generalized,
but delegates action based on the recording equipment. In this case
literal paths, headstage name, and electrode name must be specified at
a minimum. Depending on the load routine, other `"positional"
arguments <https://docs.python.org/2/glossary.html>`_ might be
required. This method *will not* generate an experiment description,
since it does not refer to session config files at all.

.. code-block:: python

    from ecogana.devices.data_util import load_experiment_manual
    load_experiment_manual(
        exp_path, test, headstage, electrode, *other_load_args,
	**load_kwargs
	)

Data handling: HDF5
-------------------

Most of the data input-output is done using the HDF5 file structure,
interfaced through PyTables (``tables`` package). The file extension
convention here is "``*.h5``\ ". This file structure is compatible with
recent versions of MATLAB and is convenient for working with large
arrays.

TDMS
++++

Reading National Instruments TDMS is *slow*. I convert to HDF5 right off
the bat using ``tdms_to_hdf5.py`` (part of ``ecoglib``).

::

    usage: tdms_to_hdf5.py [-h] [-p PERMUTATION] [-m] [-z COMPRESSION] [-b]
                           tdms_file [tdms_file ...] h5_file [h5_file ...]

    Convert TDMS to HDF5
    
    positional arguments:
      tdms_file             path to the TDMS file
      h5_file               name of the HDF5 file to create
    
    optional arguments:
      -h, --help            show this help message and exit
      -p PERMUTATION, --permutation PERMUTATION
                            file with table of channel permutations
      -m, --memmap          use disk mapping for large files
      -z COMPRESSION, --compression COMPRESSION
                            use zlib level # compression in HDF5
      -b, --batch           Batch process all matching files


Example usage in batch mode:

::

    $ tdms_to_hdf5.py -b /path/to/tdms/*.tdms /path/to/h5-destination/

**Currently**, due to a quirk, TDMS files are automatically converted
to HDF5 *if they are found on the network path*, but not if they are
on the local path.

Other data handling
+++++++++++++++++++

Many of the .h5 files are designed to wrap ``Bunch`` objects, defined in
:py:mod:`ecogana.ecoglib.util`. A "Bunch" is the conventional name for a simple
container object that is a dictionary, but acts like a "dot-accessible"
container. They are a bit like the ``struct`` in MATLAB, and I use them
as such in place of more feature-full objects.

There are two methods that stash and load Bunches using HDF5:
``save_bunch`` and ``load_bunch`` defined in :py:mod:`ecogana.ecoglib.data.h5utils`.
They are not exhaustively tested for all data combinations, but they
have been extensively *used* for common numerical data.

Pickle Gotcha
+++++++++++++

Anything that is not "naturally" supported by HDF5 (e.g. arrays and
other kinds of sequences and numbers) is handled by the Python
`pickle <https://docs.python.org/2/library/pickle.html>`__. The pickled
string is just saved in a field called ``_b_pickle``, and un-pickling is
attempted upon loading.

What I've just discovered is a "stale" pickle. The pickle is normally
used for objects that are more complex than just sequences. However, the
ability to re-create an object is lost if the object definition (i.e.
the code) has changed, or even if the package-path of the code has
changed.

In some cases it may be possible to use git to create a window on the
previous code. Using the git SHA tag of a commit with the correct
source:

::

    $ git stash

    $ git checkout [commit-hash]

    # in python
    >>> from ecoglib.data.h5utils import load_bunch
    >>> b = load_bunch('stale_bunch.h5', '/')
    # inspect the object, quit python

    $ git checkout master

    $ git stash pop

It's unclear to me how to save an "upgraded" pickle. Something to
consider moving forward is to use this idea as a mixin:
http://code.activestate.com/recipes/521901-upgradable-pickles/


Using multiprocessing and shared memory
---------------------------------------

There are two conflicts that I'm aware of when it comes to my hack for
multiprocessing with shared memory.

1. One is well known, and it has to do with calling into certain
   numerical libraries while in a "forked" process. This is a pretty
   serious drawback, but it doesn't affect things like timeseries
   filtering and FFTs. This is an issue using Mac and the BLAS
   implementation found in "vecLib"
2. The second conflict is a bit murky, and seems to arise from the use
   of certain Enthought libraries (possibly from an interaction with the
   Qt application thread?).

The good news (for some) is that my mode of using Python appears to
mask the second conflict. Specifically, both of the following usages of
python *never* experience random segfaults while doing multiprocessing:

-  ``ipython --pylab``: starting up with the separate GUI thread
-  ``ipython notebook`` while also using the ``%matplotlib inline``
   feature

To be clear, code that combines multiprocessing and Enthought imports
*does* cause crashes under the following usage styles:

-  ``python some-mp-script.py`` (calling the script non-interactively)
-  ``ipython`` with ``run some-mp-script.py`` (running the script from
   an ipython session *without* a GUI thread)


