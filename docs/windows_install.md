# Setup for Windows

## Set up your Python 2.7 system

This code base is developed under Python 2.7. The instruction on this page refer to using [Anaconda](https://www.continuum.io/downloads) as a Python environment. Anaconda is recommended because it provides:

### Package management

This is helpful because the software ecosystem of Python relies on multiple inter-connected open source projects. Package management "by hand" (i.e. downloading, building, and installing code) should be avoided as far as possible. Anaconda provides package management on the command line with the `conda` command.

### Environment management

Often one wants to switch between Python systems, to en/disable different code versions or to switch betweeen 2.7 and 3.4 systems. In Python, "virtual environments" are typically used to accomplish these switches. Anaconda provides virtual environment support through the `activate` command.

### Anaconda environment setup and package management

Python projects link to other on other non-core projects for specific
functionality. the `conda` program provides a reasonably simple
package manager.

1. Download packages.txt
1. Start command prompt in Administrator (or Terminal on Mac OS)
1. Navigate to where packages.txt was downloaded
1. Update conda version

		conda update conda

1. Create new environment named "DesiredName" (name is arbitrary)

		conda create --name DesiredName python=2 --file packages.txt

1. Go into new environment--**be sure to do any installation and work within this environment**

		activate DesiredName

Miscellaneous packages and updates must now be performed:

		conda install mayavi=4.4.0=np19py27_0
		conda install -c https://conda.anaconda.org/menpo opencv
		conda install numpy=1.10.1=py27_0

### QT GUI framework

QT is the preferred GUI framework behind the Python GUI features used here.

* Archived QT4 AP for Win/MacI: https://download.qt.io/archive/qt/4.8/4.8.6/

## Setup lab-specific code libraries

### Bitbucket/Github repositories

The code repositories are managed with the "[Git](https://git-scm.com/)" software. [Bitbucket](https://bitbucket.org/) and [Github](https://github.com/) are code repository hosts that offer similar features. On top of literally hosting the remote copy of the repository, they both offer features to coordinate asynchronous development of code by multiple parties.

Bitbucket is used for the active development, due to the unlimited private repositories it gives to academic users.

1. Get bitbucket account to be given access to private repositories
1. Download Git: "[https://git-scm.com/downloads](https://git-scm.com/downloads)"
1. Alternatively, download other git software like "[Sourcetree](https://www.sourcetreeapp.com/)"

Get Git, open **git-bash** (or whatever client). Make a new directory for
the code repositories to live on your computer. The following commands
apply literally to the git-bash command-line. In a GUI client, find the "clone"
button and use the URLs given below:

    git clone https://github.com/miketrumpis/nitime.git
    git clone https://github.com/miketrumpis/npTDMS.git
	git clone git@bitbucket.org:tneuro/ecogana.git
	git clone git@bitbucket.org:tneuro/ecoglib.git

Either in the GUI client or on the command-line (commands below),
checkout the "hdf5_objects" branch of the npTDMS project. In a GUI
client, you might find this located in the "remote" rather than the
local clone.

    cd npTDMS
    git checkout hdf5_objects

## Finishing setup of Python system environment

### Windows 7

1. From the desktop, right click the Computer icon.
1. Choose Properties from the context menu.
1. Click the Advanced system settings link.
1. Click Environment Variables. ...

### Later Windows Versions (?)

Start with control panel -> search "edit the system environment variables" -> Environment Variables -> New

### Add variables
Now **add** an environment variable called PYTHONPATH. It is a
semi-colon separated list of the git repository paths just cloned.

Varialbe name:

    PYTHONPATH
    
Variable value:

    path\to\ecogana;path\to\ecoglib;path\to\npTDMS;path\to\nitime

**Add** these new variables:

    QT_API=pyside
    ETS_TOOLKIT=qt4

### Associating Anaconda Python with .py files in Windows Explorer

**Edit** the PYTHONPATH variable and add the new variables:

    path\to\Anaconda3\envs\DesiredName\
    path\to\Anaconda3\pkgs\

Use the Anaconda\envs\DesiredName\python.exe program to open
.py files

### Windows command-line path for calling python programs

**TODO**

On Mac and *NIX, some python scripts can be called as if they are
executable programs. From any working path, you can type
"some-program.py" and that program will run. **I don't know how to do
this on WIndows!**


## Configuration of ecogana behavior

There is a GUI control for setting up the global variables for the
software (explanations of variables are found in the tool). 

    ecogana/cmdline/config_tool.py

On Windows, one should be able to "double-click" that script to run
the program. If that does not work, then go back to the WIndows
command-line that has an "activated" Anaconda environment. Navigate to
the path above and run the program:

    python config_tool.py
