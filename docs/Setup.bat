<# : Select.bat
@echo off
cd %~dp0
set /P envName=What is your desired environment name?:

rem This section sets up an Anaconda environment and installs packages
conda create --name %envName% python=2 --file "%~dp0\packages_win.txt" -y
call activate %envName%
conda install -c menpo opencv -y

rem This section clones git repositories (using a "sh" shell script in git-bash)
set "variable=%~dp0"
set "variable=%variable:\=/%"
"%ProgramFiles%\Git\bin\sh.exe" -l -i -c "%variable%getFiles.sh"
setx QT_API "pyside
setx ETS_TOOLKIT "qt4

rem Launch a little pop-up window to set the "folder" variable
rem to point to the Anaconda installation path
setlocal
set "psCommand="(new-object -COM 'Shell.Application')^
.BrowseForFolder(0,'Please choose the Anaconda2 folder.',0,0).self.path""
for /f "usebackq delims=" %%I in (`powershell %psCommand%`) do set "folder=%%I"
setlocal enabledelayedexpansion
endlocal

rem Set up windows shell to launch the environment python with arguments preserved
set PY_EXE=%folder%\envs\%envName%\python.exe
rem echo %PY_EXE%

rem This will look like "C:\path\to\python.exe "%1% %*"
set PY_SHELL="%PY_EXE% "%%1" %%*"
rem echo %PY_SHELL%

reg add HKCR\py_auto_file\shell\open\command /t REG_SZ /d %PY_SHELL% /f
reg add HKCR\Applications\python.exe\shell\open\command /t REG_SZ /d %PY_SHELL% /f

rem also this?
assoc .py=Python.File
ftype Python.File=%PY_SHELL%

rem Now set PYTHONPATH to include the local git clones, as well as
rem the Anaconda environment paths (is this redundant with "activate"??)
setx PYTHONPATH "%~dp0ecogana\;%~dp0ecoglib\;%~dp0npTDMS\;%~dp0nitime\;%folder%\envs\%envName%\;%folder%\pkgs\;

pause
