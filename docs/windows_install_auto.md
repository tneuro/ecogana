# Setup for Windows

## Set up your Python 2.7 system

This code base is developed under Python 2.7. The instruction on this page refer to using [Anaconda](https://www.continuum.io/downloads) as a Python environment. Anaconda is recommended because it provides:

### Package management

This is helpful because the software ecosystem of Python relies on multiple inter-connected open source projects. Package management "by hand" (i.e. downloading, building, and installing code) should be avoided as far as possible. Anaconda provides package management on the command line with the `conda` command.

### Environment management

Often one wants to switch between Python systems, to en/disable different code versions or to switch betweeen 2.7 and 3.4 systems. In Python, "virtual environments" are typically used to accomplish these switches. Anaconda provides virtual environment support through the `activate` command.

### Anaconda and Git Install

1. Download and install Anaconda for Python 2.7 from the following link in the suggested location (remember where you installed it):

    https://www.continuum.io/downloads
    
1.  Download and install Git from the following link in the suggested location with suggested settings:

    https://git-scm.com/downloads
    
1. Obtain a Bitbucket account and get permission to access the analysis code

### Anaconda environment setup, package management, and Git repository setup

1. Create a folder where you want to save the analysis code
1. Download packages.txt, Setup.bat, getFiles.sh and move the files to the folder you created
1. Run the Setup.bat file as Administrator
1. Follow instructions on command prompt

The script will create a new environment of a name of your choice and then install the appropriate
packages from the text file of the appropriate versions in order for the analysis code to properly
work. The script will then prompt you for your bitbucket account name such that it can obtain the
analysis code from the repository. It will also ask for your password in order to clone in the
code. Finally, it will ask you to point out where the Anaconda3 folder was installed such that
it can set environment variables to appropriate values like the paths to the code and environement and
the backend method for the analysis code.

### Windows command-line path for calling python programs

*Per directions [here](http://conda.pydata.org/docs/using/envs.html#windows)*

Assuming repositories were cloned under the %HOMEPATH%\PythonAnalysis directory. Adjust as necessary to your correct path.

```
cd C:\Users\jsmith\Anaconda3\envs\name_of_your_env
mkdir .\etc\conda\activate.d
mkdir .\etc\conda\deactivate.d
type NUL > .\etc\conda\activate.d\env_vars.bat
type NUL > .\etc\conda\deactivate.d\env_vars.bat
```

Now add these lines to `activate.d\env_vars.bat`:

```
set PATH_PREV=%PATH%
set PATH=%PATH%;%HOMEPATH%\PythonAnalysis\ecogana\cmdline;%HOMEPATH%\PythonAnalysis\ecoglib\cmdline
```

And these lines to `deactivate.d\env_vars.bat`:

```
set PATH=%PATH_PREV
set PATH_PREV=
```

Now after activating your Anaconda environment in a windows command terminal, programs (such as config_tool.py and froemke_ana.py) can be called on the command line.

## Configuration of ecogana behavior

Map the network drive to this computer if this hasn't been done already.

There is a GUI control for setting up the global variables for the
software (explanations of variables are found in the tool). 

    ecogana/cmdline/config_tool.py

On Windows, one should be able to "double-click" that script to run
the program. If the default program has not been set, then first set 
the Anaconda\envs\DesiredName\python.exe program as the default. 

If that still does not work:

1. Open Windows command line
1. Enter 'activate DesiredName'
1. 'cd' to the path
1. Enter 'python config_tool.py'