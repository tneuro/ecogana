=============================================
 EcogAna: Analysis Code From The Viventi Lab
=============================================

.. attention::
   This repository is obsolete, replaced by `ecogdata <https://github.com/miketrumpis/ecogdata>`_, `ecoglib <https://bitbucket.org/tneuro/ecoglib/>`_, and `ecogtopus <https://bitbucket.org/tneuro/ecogtopus/>`_.

