import os
import os.path as osp
from traits.api import HasTraits, Str, Enum, List, Instance, Button, \
     Float, Bool, File, Code, Directory
from traitsui.api import View, Item, UItem, Handler, EnumEditor, \
     Group, VGroup, HGroup, CheckListEditor, spring
from ecogana.expconfig import *

session_folders = sorted(session_groups()) #get from froemke.experiment_conf

import inspect
from ecoglib.util import Bunch

from .load.util import convert_tdms
from .data_util import _loading, DataSet, parse_param, params_table, \
     load_experiment_manual, load_experiment_auto
from .electrode_pinouts import electrode_maps

params_tooltips = {
    # common args
    'exp_path' : 'Path to folder where data is located',
    'test' : 'Specific test for session',
    'electrode' : 'Electrode used during experiment',
    # (mostly) common kwargs
    'bandpass' : '"Low Freq, High Freq" for bandpass filtering',
    'notches' : 'Notch frequencies',
    'trigger' : 'trigger channels',
    'snip_transient' : 'How much transient to snip',
    'units' : 'Final units of data (e.g. V, mV, uV)',
    'save' : 'Save or nah',
    'bnc' : 'Comma seperated list of bnc channels to attach to dataset',
}   

class ErrorView(HasTraits):
    error_msg = Str('Lol')
    
    def __init__(self,msg):
        self.error_msg = msg
    
    view = View(
        VGroup(
            spring,
            HGroup(
                Item(name='error_msg',style='readonly',width=200,height=50),
                show_labels=False
            ),
            spring,
        ),
        title='Error',
        resizable=True
    )
    
class Spec(HasTraits):
    pass

class DataSetSetup(HasTraits):
    session = Str
    recording = Str
    
    save_dir = Directory(os.getcwd())
    plots_list = List(Str,['PSD plots','Site RMS','Column RMS',
                           'Channel outliers','Corr graph',
                           '2D spatial corr','1D spatial corr'])
    plot_checklist = List( editor = CheckListEditor(
                                values = ['PSD plots','Site RMS','Column RMS',
                                          'Channel outliers','Corr graph',
                                          '2D spatial corr','1D spatial corr'],
                                cols = 2))
    sig_keys = List()
    
    scroll_len = Float(2.0)
    picker_sets_mask = Bool(True)
    
    gen_plots_button = Button('Generate Plots')
    gen_scroll_button = Button('Scroller')
    gen_chan_picker = Button('Launch Channel Picker')
    
    ## depending on manual or auto load:
    # manual: data directory, headstage, electrode
    # auto: session, test
    def __init__(self, load_method, con_args, *psargs, **kwargs):
        if load_method == 'manual':
            dset = load_experiment_manual(
                con_args['exp_path'], con_args['test'], 
                con_args['headstage'], con_args['electrode'], 
                *psargs, **kwargs
                )
            self.data = DataSet.load_data(dset)

        if load_method == 'auto':
            dset = load_experiment_auto(
                con_args['session'], con_args['recording'], **kwargs
                )
            self.data = DataSet.load_data(dset)
        
        self.plot_checklist = self.plots_list
        self.session = self.data.session
        self.recording = self.data.test
        
        a = inspect.getargspec(DataSet.signal_check)
        args = a.args
        vals = a.defaults
        n_pos_arg = len(args) - len(vals)
        self.sig_keys = args[n_pos_arg+2:]
        sig_vals = list(vals[2:])
        
        for name, val in zip(self.sig_keys, sig_vals):
            self.add_trait( name, Str(val) )
        
    @staticmethod
    def load_manual(exp_path, test, headstage, electrode, *psargs, **kwargs):
        load_args = dict(exp_path=exp_path, test=test,
                         headstage=headstage, electrode=electrode)
        a = DataSetSetup('manual', load_args, *psargs, **kwargs)
        return a
    @staticmethod
    def load_auto(session, recording, *psargs, **kwargs):
        load_args = dict(session=session, recording=recording)
        a = DataSetSetup('auto', load_args, *psargs, **kwargs)
        return a
        
    def _gen_plots_button_fired(self):
        def _float_or_seq(x):
            if not x or x == '()':
                return ()
            try:
                return float(x)
            except ValueError:
                return map(float, x.split(','))
            
        # bsize_sec=2, electrode_pitch=0.406, notches=(), hi_f=100, lo_f=10
        t = {'bsize_sec': float, 'electrode_pitch': float,
             'hi_f': float, 'lo_f': float, 'notches': _float_or_seq}
        kwarg = dict()
        for n in self.sig_keys:
            conv = t.get(n, str)
            kwarg[n] = conv(getattr(self, n))
        ## kwarg = [ (n, getattr(self, n)) for n in self.sig_keys ]
        ## kwarg = dict( kwarg )
        
        self.data.signal_check(save_dir=self.save_dir,
                               checklist=self.plot_checklist,
                               **kwarg)
    
    def _gen_scroll_button_fired(self):
        if not self.data.dset.bandpass:
            err = ErrorView('May want to reload data with bandpass settings')
            err.configure_traits()
        self.data.view_scroll(self.scroll_len)

    def _gen_chan_picker_fired(self):
        self.data.channel_picker(self.scroll_len, 
                                 set_mask=self.picker_sets_mask)
    
    def default_traits_view(self):
        names = self.sig_keys
        tips = [params_tooltips.get(n, 'No Help Available') for n in names]
        kw_args_items = [Item(name=n, style='simple', tooltip=tip)
                         for n, tip in zip(names, tips)]
        key_group = VGroup(*kw_args_items, show_labels=True)
        # Panel for signal statistics / diagnostics
        sig_check_group = VGroup(
                    HGroup(
                        Item(name='session', label='Session', style='readonly'),
                        '22'
                    ),
                    HGroup(Item(name='recording', label='Recording'), '22'),
                    Item(name='save_dir', label='Save Dir.'),
                    Group(key_group, label='Plot Settings'),
                    VGroup(
                        Group(Item(name='plot_checklist', style='custom'),
                              show_labels=False),
                        label='Choose Plots'
                    ),
                    HGroup(
                        spring,
                        Item(name='gen_plots_button'), 
                        spring, show_labels=False
                    ),
        )
        # Panel for channel scroller
        scroll_group = VGroup(
                    Group(
                        Item(name='scroll_len', label='Scroll Length'),
                        label = 'Scroll Settings'
                    ),
                    HGroup(
                        spring,
                        Group(UItem(name='gen_scroll_button')),
                        spring
                    ),
                    show_labels=False
        )
        # Panel for channel picker (type of scroller)
        picker_group = VGroup(
                    Group(
                        Item(name='scroll_len', label='Scroll Length'),
                        Item(name='picker_sets_mask', label='Set Mask'),
                        label = 'Scroll Settings'
                    ),
                    HGroup(
                        spring,
                        Group(UItem(name='gen_chan_picker')),
                        spring
                    ),
                    show_labels=False
        )
        
        return View(
            Group(picker_group, label='Channel Picker'),
            Group(sig_check_group, label='Signal Check'),
            Group(scroll_group, label='Data Scroller'),
            title='Data Options',
            resizable=True,
        )
    
class ManualHandler(Handler):
    def object_headstageMenu_changed(self,info):
        info.object.HStageMenu = GenericMenu(hstage=info.object.cur_hstage)
        info.object.HStageMenu.createKeyArgs()
        if 'headstage' in info.object.HStageMenu.psNames:
            hs = info.object.headstageMenu
            info.object.HStageMenu.headstage = hs

class GenericMenu(Spec):
    hstage = Str()
    psNames = List()
    keyNames = List()

    def createKeyArgs(self):
        a = inspect.getargspec(_loading[self.hstage])
        args = a.args
        vals = a.defaults
        n_pos_args = len(args) - len(vals)
        self.keyNames = args[n_pos_args:]
        key_args = map(str, vals)
        # skip the 1st three positional args:
        # * path
        # * test/recording
        # * electrode designation
        # These are handled by the other interface.
        self.psNames = args[3:n_pos_args]
        ps_args = [0] * len(self.psNames)

        # add these same names as Traits on self
        for name, val in zip(self.psNames, ps_args):
            self.add_trait( name, Str(val) )
        for name, val in zip(self.keyNames, key_args):
            self.add_trait( name, Str(val) )

    def get_parsed_variables(self, all_kwarg=False):
        kwarg = [ (n, parse_param(n, getattr(self, n), params_table))
                   for n in self.keyNames ]
        kwarg = dict( kwarg )
        psarg = tuple( [parse_param(n, getattr(self, n), params_table)
                        for n in self.psNames] )
        if all_kwarg:
            kwarg.update( dict( zip(self.psNames, psarg) ) )
            return kwarg
        args = Bunch()
        args.kwarg = kwarg
        args.psarg = psarg
        return args


    def default_traits_view(self):
        
        self.createKeyArgs()
        # make positional args item group
        names = self.psNames
        tips = [params_tooltips.get(n, 'No Help Available') for n in names]
        ps_args_items = [Item(name=n, style='simple', tooltip=tip)
                         for n, tip in zip(names, tips)]
        # make keyword args item group
        names = self.keyNames
        tips = [params_tooltips.get(n, 'No Help Available') for n in names]
        kw_args_items = [Item(name=n, style='simple', tooltip=tip)
                         for n, tip in zip(names, tips)]
        pos_group = VGroup(*ps_args_items, 
                           visible_when = 'len(psNames)>0',
                           show_labels=True)
        key_group = VGroup(*kw_args_items, 
                           show_labels=True)
        
        return  View(
            Group(pos_group,label='Positional Arguments'),
            Group(key_group,label='Keyword Arguments'),
        )


class ManualLoad(Spec):
    data_dir = File
    headstageMenu = Enum(sorted(_loading.keys()))
    headChoice = Bool(False)
    headstageCust = Str()
    electrodeMenu = Enum(sorted(electrode_maps.keys()))
    electrodeCust = Str()
    electrodeChoice = Bool(False)
    HStageMenu = Instance(Spec)
    load_button = Button('Load Data')
    
    def _load_button_fired(self):
        a = self.HStageMenu.get_parsed_variables()
        if not a:
            a = Bunch(psarg=(), kwarg=dict())

        if len(self.headstageCust):
            hs = self.headstageCust
        else:
            hs = self.headstageMenu

        if len(self.electrodeCust):
            elec = self.electrodeCust
        else:
            elec = self.electrodeMenu

        exp_path, test = os.path.split(self.data_dir)
        if self.cur_hstage == 'oephys':
            # need to split another level down to get the "test" directory name
            exp_path, test = os.path.split(exp_path)
        test, ext = os.path.splitext(test)
        if ext == '.tdms':
            cfn = convert_tdms(test, '', exp_path, clear=True)
        else:
            cfn = None
        
        d = DataSetSetup.load_manual(
            exp_path, test, hs, elec, *a.psarg, **a.kwarg
            )
        d.configure_traits()
        if cfn is not None:
            cfn()
        return d

    @property
    def cur_hstage(self):
        return self.headstageCust if self.headChoice else self.headstageMenu
    
    select_group = Group(
        VGroup(
            Item(name='data_dir',style='simple', label='Data Dir:'),
            HGroup(
                Item(name='headstageMenu', label='Headstage',
                     visible_when='not headChoice'),
                Item(name='headstageCust', label='Headstage',
                     visible_when='headChoice'),
                spring,
                Item(name='headChoice',label='Other heastage'),
                '22'
            ),
            HGroup(
                Item(name='electrodeMenu',label='Electrode', 
                     visible_when='not electrodeChoice'),
                Item(name='electrodeCust', label='Electrode', 
                     visible_when='electrodeChoice'),
                spring,
                Item(name='electrodeChoice', label='Other electrode'),
                '22'
                )
        ),
    )
    
    hstage_group = Group(
        Item(name = 'HStageMenu',style='custom'),
        show_labels=False,
    )
    
    view = View(
        VGroup(
            select_group,
            VGroup(hstage_group),
            HGroup(spring,Item(name = 'load_button'),spring,show_labels = False),
            '5'
        ),
        handler = ManualHandler,
    )

class AutoHandler(Handler):
    session = Instance(Bunch)

    def _change_hstage_menu(self,info):
        info.object.HStageMenu = GenericMenu(hstage=info.object.cur_hstage)
        
        cfg = session_conf(info.object.session)
        test_info = cfg.session
        test_info.update(cfg.get(info.object.cur_run, {}))

        # must keep these values as string types (not parsed types)
        hs = info.object.HStageMenu
        a = dict( [ (n, getattr(hs, n)) for n in hs.keyNames ] )

        # dict1.viewkeys() & dict2.viewkeys() -> set(intersecting keys)
        a.update((k, test_info[k]) for k in a.viewkeys() & test_info.viewkeys())
        for n in hs.keyNames:
            setattr(hs, n, str(a[n]))

        a = dict( [ (n, getattr(hs, n)) for n in hs.psNames ] )
        a.update((k, test_info[k] ) for k in a.viewkeys() & test_info.viewkeys())
        for n in hs.psNames:
            setattr(hs, n, str(a[n]))
        
    def _change_session_txt(self,info):
        try:
            p = find_conf(info.object.session)
            info.object.session_txt = open(p).read()
        except:
            info.object.session_txt = ''

    def _session_scan(self):
        if not 'tone_tab' in self.session:
            self.session.tone_tab = 'none'

    def object_session_grp_changed(self,info):
        s_group = available_sessions(info.object.session_grp)
        info.object.session_list = [s.split('/')[1] for s in s_group]
        info.object.session_disp = info.object.session_list[0]
        info.object.session =  info.object.session_grp + '/' + \
          info.object.session_disp
        info.object.cur_run = ""
    
    def object_session_disp_changed(self,info):
        info.object.session =  info.object.session_grp + '/' + \
          info.object.session_disp
        info.object.cur_run = ""
    
    def object_selected_run_changed(self,info):
        if info.object.selected_run:
            session = info.object.session
            ind = info.object.avail_runs_list.index(info.object.selected_run)
            
            # get all keyword args and values from previously selected run
            prev_run = None
            if info.object.cur_run:
                prev_run = info.object.cur_run
                prev_menu = info.object.HStageMenu
                prev_keys = dict( [ (n, getattr(prev_menu, n)) for n in prev_menu.keyNames ] )
            
                
            info.object.cur_run = info.object.runs[ind]
            conf = session_conf(session)
            default_test = self.session
            default_test.update(conf[info.object.runs[ind]])
            hstage = default_test.headstage

            info.object.cur_hstage = hstage
            
            # get keywords args that have () as value, update from previous run
            self._change_hstage_menu(info)
            if prev_run:
                hs = info.object.HStageMenu

                empty_keys = dict( [ (n, getattr(hs, n)) for n in hs.keyNames if getattr(hs, n) == '()'] )
                empty_keys.update(prev_keys)
                for n in empty_keys.keys():
                    setattr(hs, n, str(empty_keys[n]))
            
            

    def object_session_changed(self,info):
        session = info.object.session
        si = session_info(session)
        conf = session_conf(session)
        self.session = conf.session
        self._session_scan()
        session_items = sorted([s for s in sorted(conf.sections) 
                         if s not in ('session', 'tonotopy')])
        info.object.runs = session_items
        run_info = list()
        loc_path = si.get('exp_path', '')
        nwk_path = si.get('nwk_path', '')
        if os.name == 'nt': #if os is windows
            if si.get('exp_path') and si.get('exp_path','')[0] =='/':
                loc_path = si.get('exp_path','')[1:]
            if si.get('nwk_path') and si.get('nwk_path','')[0] =='/':
                nwk_path = si.get('nwk_path','')[1:]
        for run in session_items:
            amp_tab = conf[run].get('amp_tab', '<no stim>')
            try:
                # only convert to check "type"
                amp_tab = int(amp_tab)
                amp_tab = '{0} dB SPL'.format(amp_tab)
            except ValueError:
                pass
                
            if osp.exists(osp.join(loc_path, run+'.h5')):
                pth = ''
                ftype = 'h5'
            elif osp.exists(osp.join(loc_path, run+'.tdms')):
                pth = ''
                ftype = 'tdms'
            elif osp.exists(osp.join(nwk_path, run+'.h5')):
                pth = 'nwk'
                ftype = 'h5'
            elif osp.exists(osp.join(nwk_path, run+'.tdms')):
                pth = 'nwk'
                ftype = 'tdms'
            else:
                pth = 'offline'
                ftype = ''

            run_str = ', '.join( filter(None, (run, amp_tab, ftype, pth)) )
            run_info.append(run_str)
            
        info.object.avail_runs_list = run_info
        hstage = self.session.headstage
        info.object.cur_hstage = hstage
        self._change_hstage_menu(info)
        self._change_session_txt(info)
        
class AutomaticLoad(Spec):
    session_grp = Enum(session_folders[0], session_folders)
    session_list = List(Str,[])
    session_disp = Str()
    session = Str()
    runs = List(Str)
    cur_run = Str()
    avail_runs_list = List(Str,[])
    selected_run = Str
    session_txt = Code
    show_session_txt = Bool
    cur_hstage = Str()
    HStageMenu = Instance(Spec)
    load_button = Button('Load Data')
    
    def _load_button_fired(self):
        
        if not self.cur_run:
            err = ErrorView('Click on a run first!')
            err.configure_traits()
        else:
            kw = self.HStageMenu.get_parsed_variables(all_kwarg=True)
            if not kw:
                kw = dict()
                
            cfg = session_conf(self.session)
            test_info = cfg.session
            test_info.update(cfg.get(self.cur_run,{}))
            
            d = DataSetSetup.load_auto(self.session, self.cur_run, **kw)
            d.configure_traits();
            return d
        
    hstage_group = Group(
        Item(name = 'HStageMenu',style='custom',height=300),
        show_labels=False
    )
    traits_view = View(
        HGroup(
            VGroup(
                Item(name='session_grp',label='Session Group'),
                Item(name='session_disp',label='Session',
                    editor= EnumEditor(name='object.session_list',mode='list'),
                ),
            ),
            spring,
            Item(name='show_session_txt', label='Show session config')
        ),
        HGroup(
            VGroup(
                '9',
                VGroup(
                    Group(
                        Item(name='selected_run',style='custom',
                            editor= EnumEditor(name='object.avail_runs_list', 
                                                mode='list'),
                            width=300
                        ),
                        show_labels=False
                    ),
                    label='Session Runs'
                ),
                '8'
            ),
            VGroup(
                Group(
                    Item(name='session_txt', style='readonly',
                        width=300
                    ),
                    show_labels=False
                ),
                label='Session Config',
                visible_when='show_session_txt',
            ),
        ),
        Item('_'),
        hstage_group,
        HGroup(spring,Item(name = 'load_button'),spring,show_labels = False),
        '5',
        handler = AutoHandler,
    )
class LoadTypeHandler(Handler):
    def object_loadtype_changed(self,info):
        if(info.object.loadtype == 'Automatic'):
            info.object.loadMenu = AutomaticLoad()
        else:
            info.object.loadMenu = ManualLoad()
class DataLoadSetup(HasTraits):

    loadtype = Enum('Manual', 'Automatic')
    loadMenu = Instance(Spec)
    
    initial_group = VGroup(
        HGroup(
            '5',
            Item(name='loadtype', label='Parameter Loading'),
        ),
        label = 'Initial Setup',
    )
    
    load_group = VGroup(
        HGroup(
            '5',
            Item(name = 'loadMenu',style='custom'),
            '5',
            show_labels=False
        ),
        label = 'Loading Info',
    )
    
    view = View(
        VGroup(
            initial_group,
            load_group
        ),
        title='Data Select',
        handler=LoadTypeHandler,
        resizable=True
    )



if __name__=='__main__':
    ast = DataLoadSetup()
    ast.configure_traits()

