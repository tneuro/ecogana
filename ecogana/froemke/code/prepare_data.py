from ecogana.expconfig import locate_old_session
from ecogana.devices.data_util import load_experiment_auto
    
def load_experiment(session, test, clean_temp=None, **load_kwargs):
    print 'Use ecogana.devices.data_util.load_experiment_auto()'

    if len( session.split('/') ) < 2:
        session = locate_old_session(session)
    return load_experiment_auto(session, test, **load_kwargs)
