import os, sys
import os.path as osp
import cPickle
import tables
import numpy as np
import gc

import ecoglib.util as ut
from ecogana.expconfig.exp_descr import join_experiments
from sandbox.array_split import shared_ndarray

from ecogana.froemke.experiment_conf import *
from ecogana.froemke.code.prepare_data import load_experiment

import ecogana.devices.data_util as du

def append_datasets(session, tests, **load_kwargs):
    """
    Append multiple data sets end-to-end to form a single set.
    If StimulatedExperiments are associated with these sets,
    then also join those experiments to reflect the union of all
    conditions presented.
    """

    print 'Use ecogana.devices.data_util.append_datasets'
    return du.append_datasets(session, tests, **load_kwargs)

def interleave_datasets(
        session, tests, connections, alignments=(), **load_kwargs
        ):
    """
    Join two or more datasets that are sampled on distinct sites.
    If StimulatedExperiments are associated, they should have
    identical conditions. In this case, the data will be aligned
    by trigger times. Otherwise a sequence of sample shifts must
    be provided to correctly align the sets.
    """
    pass
    

    
def load_pickle(path, test, snip_transients=False):
    dset = cPickle.load(open(osp.join(path, test+'_proc.pk')))
    if not snip_transients:
        return dset
    ft = dset.exp.time_stamps[0] if dset.exp else 2000
    snip_len = min(2000, ft)
    dset.data = dset.data[:,snip_len:]
    if dset.exp:
        dset.exp.time_stamps -= snip_len
    return dset

def join_datasets(all_sets, popdata=True, rasterize=True, shared_mem=True):
    print 'Use ecogana.devices.data_util.join_datasets'
    return du.join_datasets(
        all_sets, popdata=popdata, rasterize=rasterize, shared_mem=shared_mem
        )
