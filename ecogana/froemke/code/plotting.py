from __future__ import division
import numpy as np
import matplotlib.pyplot as pp
from matplotlib.collections import PolyCollection, LineCollection
from matplotlib.colors import LogNorm
import ecogana.anacode.tile_images as tile_images

def table_of_tones(tx, chan_avg, yl=(), title=''):
    f = pp.figure()
    n_plot = chan_avg.shape[0]
    if not yl:
        yl = (chan_avg.min(), chan_avg.max())
    for n, avgs in enumerate(chan_avg):
        pp.subplot(n_plot, 1, n+1)
        pp.plot(tx, avgs.T)
        pp.ylim(yl)
        if n < n_plot - 1:
            pp.xticks([])
        if n == 0 and title:
            pp.title(title)
            
def table_of_traces(
        tx, chan_avg, tones, amps, yl=(), title='', 
        tm=None, calib_unit='V'
        ):
    tones, amps = map(np.atleast_1d, (tones, amps))
    n_conds = len(tones) * len(amps)
    fig, axs = tile_images.quick_tiles(
        n_conds, nrow=len(amps), tilesize=(0.75, 0.5),
        title=title
        )
    cond_lookup = np.arange(n_conds).reshape(len(tones), len(amps)).T
    # flip upside down
    cond_lookup = cond_lookup[::-1,:].ravel()
    amps = amps[::-1]
    if not yl:
        mn = chan_avg.min(); mx = chan_avg.max()
        yl = (mn, mx)
    
    for n, ax in enumerate(axs):
        ax.plot(tx, chan_avg[cond_lookup[n],:].T)
        if (n // len(tones)) == (len(amps)-1):
            t0 = 0.5 * (tx[-1] + tx[0])
            tone = tones[ n - (len(amps)-1)*len(tones) ]/1000.0
            ax.text(
                t0, yl[0], '%2.1f'%tone, 
                va='top', ha='center', fontsize=8
                )
        if (n % len(tones)) == 0:
            y0 = 0.5 * (yl[0] + yl[1])
            delta = (tx[-1] - tx[0])/10.0
            ax.text(
                tx[0]-delta, y0, '%d'%amps[n//len(tones)],
                va='center', ha='right', fontsize=8
                    )
        ax.axis('off')
        ax.set_ylim(yl)
        if tm is not None:
            ax.axvline(tm, color='r', linestyle='--')

    fig.subplots_adjust( 
        bottom = 0.075,
        right = max(0.85, 1 - 1/float(fig.get_figwidth()))
        )
    
    if title:
        fig.subplots_adjust(top = 0.9)
        ## fig.text(0.5, 0.9, title, ha='center', va='baseline')

    calib = tile_images.calibration_axes(axs[-1], calib_unit=calib_unit)
    
    return fig

def plot_stacked_epochs(
        tx, chan_samps, labels, title='', tm=None, calib_unit='V'
        ):

    # plots n groups of stacks, each stack is m[n] traces tall
    # * chan_samps is iterable with length-n
    # * each item in chan_samps is converted to a LineCollection

    len_tx = tx[-1] - tx[0]
    tx_pad = 0.1 * len_tx

    n = len(chan_samps)
    m_mx = max( [len(s) for s in chan_samps] )
    
    fig = pp.figure(figsize = (1.5*n + 0.5, 6))
    ax = fig.add_subplot(111)

    all_line_groups = list()
    all_offsets = np.zeros(n)
    for ni, samps in enumerate(chan_samps):
        line_group = list()
        all_offsets[ni] = np.median( samps.ptp(axis=1) )
        for timeseries in samps:
            line = list( zip(tx+ni*(len_tx+tx_pad), timeseries) )
            line_group.append(line)
        all_line_groups.append(line_group)

    offset = np.median(all_offsets)
    for ni, line_group in enumerate(all_line_groups):
        lc = LineCollection(
            line_group, offsets=(0, offset), colors='k', linewidths=0.5
            )
        ax.add_collection(lc)
        if tm is not None:
            ax.axvline(tm + ni * (len_tx + tx_pad), color='r', ls='--')

        # put label at center of this stack
        x0 = tx[0] + ni*(len_tx+tx_pad)
        x1 = x0 + len_tx
        xc = 0.5 * (x0 + x1)
        ax.text( 
            xc, -2.5*offset, labels[ni], 
            transform=ax.transData, ha='center' 
            )
            
    ax.set_xlim( tx[0] - tx_pad, n * (len_tx + tx_pad) + tx[0] )
    ax.set_ylim( -offset, m_mx*offset )
    ax.axis('off')
    ax.set_title(title)
    # leave 1 inch for scale
    w = fig.get_figwidth()
    fig.subplots_adjust(left=0.01, right= max(0.85, 1 - 1/float(w)))

    calib_ax = tile_images.calibration_axes(
        ax, y_scale=2*offset, calib_unit=calib_unit
        )
    
    return fig
    
def stacked_epochs(
        tx, chan_samps, tones, amps, breaks, clim=(), title='', tm=None,
        stacked='tones', cmap=pp.cm.jet, clabel=r"$\mu V$", bcolor='white'
        ):
    # chan_samps is (n_tones, n_amps, n_trials, n_pts)
    shape = list(chan_samps.shape)
    if len(shape) < 4:
        if stacked == 'tones':
            shape.insert(1, 1)
        else:
            shape.insert(0, 1)
        chan_samps.shape = shape
    
    tones, amps = map(np.atleast_1d, (tones, amps))
    nframe = len(tones) if stacked == 'amps' else len(amps)
    ## fig, axs = tile_images.quick_tiles(
    ##     nframe, nrow=1, tilesize=(4.0, 4.5), title=title,
    ##     calib='right'
    ##     )
    fig, axs = tile_images.quick_tiles(
        nframe, nrow=1, tilesize=(2.0, 4.5), title=title,
        calib='right'
        )

    if not clim:
        mn = chan_samps.min(); mx = chan_samps.max()
        clim = (mn, mx)

    tone_labels = ['%1.1f k'%(t/1000,) for t in tones]
    amp_labels = ['%02d dB'%a for a in amps]
    if stacked=='tones':
        chan_samps = np.rollaxis(chan_samps, 1)
        x_labels = amp_labels
        y_labels = tone_labels
    else:
        x_labels = tone_labels
        y_labels = amp_labels

    n_stacked = chan_samps.shape[1] * chan_samps.shape[2]
    if tx[-1] - tx[0] < 5:
        t0, tf = tx[0] * 1000, tx[-1] * 1000
    else:
        t0, tf = tx[0], tx[-1]
    extent = [t0, tf, 0, n_stacked-1]
        
    for n, ax in enumerate(axs):
        img = chan_samps[n].copy().reshape(-1, shape[-1])
        ax.imshow(img, extent=extent, clim=clim, cmap=cmap)
        ax.axis('auto')
        for b in breaks:
            ax.axhline(b, color=bcolor, linestyle='--', linewidth=1.5)

        ax.set_ylim(0, n_stacked-1)
        if n==0:
            ticks = 0.5 * (np.r_[0, breaks] + np.r_[breaks, n_stacked])
            #print ticks
            ax.set_yticks(ticks)
            ax.set_yticklabels( y_labels )
        else:
            ax.yaxis.set_visible(False)
        if len(axs) > 3:
            ax.tick_params(labelsize=8)
        else:
            ax.tick_params(labelsize=10)

        ax.set_xlabel(x_labels[n])
        #ax.xaxis.set_label_position('top')
            
        if tm is not None:
            ax.axvline(tm, color='k', linestyle='--', linewidth=1)
        if t0 < 0 and tf > 0:
            ax.axvline(0, color=(.5, .5, .5), linestyle='--')
        ax.set_xlim(t0, tf)

    figwid = fig.get_figwidth()
    fig.subplots_adjust(
        top=0.925, 
        left=0.5 / figwid, 
        bottom=0.1, 
        wspace=0.05
        )
            
    bbox = axs[-1].get_position()
    # situate new axes in 2nd quarter of the region to the 
    # right of the last plotted axes
    edge = 1 - 1 / figwid #0.8
    x1 = 11*edge/12 + 1.0/12
    xw = (1-edge)/6.
    fig.subplots_adjust(right=edge)
    cax = fig.add_axes([x1, bbox.y0, xw, bbox.y1-bbox.y0])
    cbar = pp.colorbar(ax.images[0], cax=cax)
    cax.tick_params(labelsize=10)
    cbar.set_label(clabel)
    return fig


def table_of_epochs(
        tx, chan_samps, tones, amps, yl=(), title='', 
        tm=None, label=r'$\mu V$'
        ):
    # here chan_samps is (ncond, nsamp, npt)
    tones, amps = map(np.atleast_1d, (tones, amps))
    n_conds = len(tones) * len(amps)
    fig, axs = tile_images.quick_tiles(
        n_conds, nrow=len(amps), tilesize=(10, 6),
        title=title, calib='right'
        )
    cond_lookup = np.arange(n_conds).reshape(len(tones), len(amps)).T
    # flip upside down
    cond_lookup = cond_lookup[::-1,:].ravel()
    amps = amps[::-1]

    rgba = chan_samps.ndim == 4
    if not rgba:
        plot_samps = chan_samps * 1e6
    else:
        plot_samps = chan_samps

    if not yl:
        mn = plot_samps.min(); mx = plot_samps.max()
        yl = (mn, mx)
    else:
        yl = map(lambda x: x*1e6, yl)
    
    for n, ax in enumerate(axs):
        ax.imshow(
            plot_samps[n], clim=yl,
            extent=[tx[0], tx[-1], 1, chan_samps.shape[1]]
            )
        ax.axis('auto')
        if (n // len(tones)) == (len(amps)-1):
            t0 = 0.5 * (tx[-1] + tx[0])
            tone = tones[ n - (len(amps)-1)*len(tones) ]/1000.0
            ax.text(
                t0, yl[0], '%2.1f'%tone, 
                va='top', ha='center', fontsize=8
                )
        if (n % len(tones)) == 0:
            y0 = 0.5 * chan_samps.shape[1]
            delta = (tx[-1] - tx[0])/10.0
            ax.text(
                tx[0]-delta, y0, '%d'%amps[n//len(tones)],
                va='center', ha='right', fontsize=8
                    )
            if amps[n//len(tones)] > amps.min():
                ax.xaxis.set_visible(False)
            ax.tick_params(labelsize=8)
        else:
            ax.axis('off')
        ax.set_ylim(1, chan_samps.shape[1])
        if tm is not None:
            ax.axvline(tm, color='k', linestyle='--', linewidth=1)
        ax.axvline(0, color=(.5, .5, .5), linestyle='--')

    bbox = axs[-1].get_position()
    # situate new axes in 2nd quarter of the region to the 
    # right of the last plotted axes
    edge = 0.85
    x1 = 3*edge/4 + 1.0/4
    xw = (1-edge)/4.
    fig.subplots_adjust(right=edge)
    cax = fig.add_axes([x1, bbox.y0, xw, len(amps)*yw])
    cbar = pp.colorbar(ax.images[0], cax=cax)
    cax.tick_params(labelsize=10)
    cbar.set_label(label)
            
    if title:
        fig.subplots_adjust(top=0.9)
    return fig


def resp_matrix(tx, chan_avg, tones, amps, clim=(), title=''):
    chan_pwr = np.sqrt(np.mean(chan_avg[:,tx>0]**2, axis=-1))
    #chan_pwr -= chan_pwr.mean()

    if tx[0] < 0:
        norm = np.std(chan_avg[:,tx<0])
        chan_pwr /= norm
    
    chan_pwr.shape = (len(tones), len(amps))
    fig = pp.figure()
    if not clim:
        clim = chan_pwr.min(), chan_pwr.max()
    pp.imshow(chan_pwr.T, origin='upper', clim=clim)
    ax = pp.gca()
    ax.set_xticks(np.arange(0, len(tones), 3))
    ax.set_xticklabels(tones[::3])
    ax.set_yticks(np.arange(len(amps)))
    ax.set_yticklabels(amps)
    cbar = pp.colorbar(orientation='horizontal')
    if tx[0] < 0:
        cbar.set_label('x Baseline Pwr')
    else:
        cbar.set_label('RMS pwr')
    if title:
        ax.set_title(title)
    return fig

def pk_matrix(chan_avg, tones, amps, clim=(), title=''):
    mx_matrix = np.max(chan_avg, -1).reshape(len(tones), len(amps))
    mx_matrix -= mx_matrix.mean()
    mx_matrix *= 1e6
    clim = map(lambda x: x*1e6, clim)
    fig = pp.figure()
    if not clim:
        clim = mx_matrix.min(), mx_matrix.max()
    pp.imshow(mx_matrix.T, origin='upper', clim=clim)
    ax = pp.gca()
    ax.set_xticks(np.arange(0, len(tones), 3))
    ax.set_xticklabels(tones[::3])
    ax.set_yticks(np.arange(len(amps)))
    ax.set_yticklabels(amps)
    cbar = pp.colorbar(orientation='horizontal')
    cbar.set_label('Pk Voltage $\mu$V')
    if title:
        ax.set_title(title)
    return fig


def trial_coding_matrix(btab1, btab2, btab3, title=''):
    # btab{1,2,3} correspond to 
    # 1) pre-trigger responses
    # 2) post-trigger responses
    # 3) post-stim responses
    
    xor_colors = pp.cm.brg(np.linspace(0, 1, 3))
    or_colors = pp.cm.spring(np.linspace(0, 1, 3))
    and_colors = np.array(pp.cm.rainbow(0))
    import matplotlib as mpl
    new_cmap = mpl.colors.ListedColormap(
        np.r_[xor_colors, or_colors, and_colors[None,:]]
        )

    n = 0
    color_tab = np.ones(btab1.shape) * np.nan
    # xor
    color_tab[btab1 & ~(btab2 | btab3)] = 0
    color_tab[btab2 & ~(btab1 | btab3)] = 1
    color_tab[btab3 & ~(btab1 | btab2)] = 2
    # or
    color_tab[(btab1 & btab2) & ~btab3] = 3
    color_tab[(btab1 & btab3) & ~btab2] = 4
    color_tab[(btab2 & btab3) & ~btab1] = 5
    # and
    color_tab[btab1 & btab2 & btab3] = 6
    
    f = pp.figure(figsize=(5.5, 6)); 
    pp.subplot(211)
    pp.imshow(color_tab, cmap=new_cmap, clim=(0,6))
    #pp.xlabel('conditions'); pp.ylabel('trial reps')
    cbar = pp.colorbar()
    cbar.set_ticks(range(7))
    cbar.set_ticklabels(
        ('pre-trig', 'post-trig', 'post-stim',
         'pre-trig & post-trig', 'pre-trig & post-stim', 
         'post-trig & post-stim', 'all response')
         )
    if title:
        pp.title(title)


    ax = pp.subplot(212)
    _, bins, patches = ax.hist(
        color_tab.ravel(), bins=np.r_[-0.5, np.arange(7)+0.5]
        )
    for n, p in enumerate(patches):
        p.set_facecolor(new_cmap(float(n)/6))

    ax.set_xticks([])
    ## ax.set_xticks(range(6))
    ## ax.set_xticklabels(        
    ##     ('pre-trig', 'post-trig', 'post-stim',
    ##      'pre-trig & post-trig', 'pre-trig & post-stim', 
    ##      'post-trig & post-stim', 'all response'),
    ##      rotation='vertical'
    ##     )
    ax.set_xlim(-0.5, 6.5)
    f.tight_layout()
    return f

def array_tuning(curves, basis, variations, chan_map, title='', yl=()):
    # curves is shaped (n_variation, n_basis)
    # basis and variations are sequences of labels
    fig, axs, _ = tile_images.tiled_axes(
        chan_map.geometry, chan_map,
        title=title, calib='right', tilesize=(1.5, 1)
        )

    nvar = len(variations)
    p_colors = pp.cm.winter(np.linspace(0, 1, nvar))
    if not yl:
        yl = curves.min(), curves.max()
    ii, _ = chan_map.to_mat()
    for n in xrange(len(axs)):
        ax = axs[n]
        ax.set_color_cycle(p_colors)
        ax.plot(curves[n].T, linewidth=2)
        ax.set_ylim(yl)
        ax.yaxis.set_visible(False)
        ax.set_xticks(range(0, len(basis)))
        ax.set_xticklabels([])
        ## if ii[n] == chan_map.geometry[0] - 1:
        ##     ax.tick_params(labelsize=8)
        ##     ax.set_xticklabels(basis)

    wid = fig.get_figwidth()
    ht = fig.get_figheight()
    fig.subplots_adjust(
        top = 1 - 0.5 / ht,
        left = 0.5 / wid,
        wspace = 0.5 / wid,
        hspace = 0.5 / ht
        )
    
            
    bbox = axs[-1].get_position()
    # situate new axes in 2nd quarter of the region to the 
    # right of the last plotted axes
    edge = 1 - 1 / wid #0.8
    x1 = 11*edge/12 + 1.0/12
    xw = (1-edge)/6.
    fig.subplots_adjust(right=edge)

    text_spacing = 0.05
    # so 0.5 = 0.5 * (mn_txt + mx_txt) = 0.5 * (2*mn_txt + (nvar-1)*spacing)
    mn_txt = (1 - (nvar-1)*text_spacing)/2.0
    for n, v in enumerate(variations):
        fig.text(
            x1, mn_txt + n*text_spacing, v, color=p_colors[n],
            fontsize=16
            )
    
    return fig
    
def draw_tuning(tones, amps, tuning):
    """ beta2 in ps / km
        C is chirp
        z is an array of z positions """
    ## T = np.linspace(-10, 10, 100)
    ## sx = T.size
    ## sy = z.size

    ## T = np.tile(T, (sy, 1))
    ## z = np.tile(z, (sx, 1)).T

    ## U = 1 / np.sqrt(1 - 1j*beta2*z * (1 + 1j * C)) * np.exp(- 0.5 * (1 + 1j * C) * T * T / (1 - 1j*beta2*z*(1 + 1j*C)))

    fig = pp.figure()
    ax = fig.add_subplot(1,1,1, projection='3d')

    verts = []
    lines = []
    x = np.arange(len(tones))
    for amp_tuning in tuning:
        mn = amp_tuning.min()
        v1 = (0, mn)
        vf = (len(tones)-1, mn)
        verts.append( [v1] + zip(x, amp_tuning) + [vf] )
        lines.append( np.c_[x, amp_tuning] )
        #verts.append(zip(T[i, :], U[i, :]))

    poly = PolyCollection(
        verts, facecolors=(1,1,1,1), edgecolors='none', closed=False
        )
    colors = pp.cm.winter(np.linspace(0, 1, len(amps)))
    lines = LineCollection(
        lines, linewidth=2, colors=colors, zorder=10
        )

    #ax.add_collection3d(poly, zs=amps, zdir='y')
    ax.add_collection3d(lines, zs=amps, zdir='y')
    ax.set_xlim3d(0, len(tones))
    ax.set_ylim3d(np.nanmin(amps), np.nanmax(amps))
    ax.set_zlim3d(np.nanmin(tuning), np.nanmax(tuning))
    xl = ax.get_xlim(); yl = ax.get_ylim(); zl = ax.get_zlim()
    ax.plot( [xl[0], xl[0], xl[1], xl[1], xl[0]],
             [yl[0], yl[1], yl[1], yl[0], yl[0]],
             [zl[0], zl[0], zl[0], zl[0], zl[0]],
             zdir='z', color='k' )
    ax.plot( [xl[0], xl[0], xl[1], xl[1], xl[0]],
             [yl[0], yl[1], yl[1], yl[0], yl[0]],
             [zl[1], zl[1], zl[1], zl[1], zl[1]],
             zdir='z', color='k' )
    ax.plot( [xl[0], xl[0]], [yl[0], yl[0]], [zl[0], zl[1]], color='k' )
    ax.plot( [xl[0], xl[0]], [yl[1], yl[1]], [zl[0], zl[1]], color='k' )
    ax.plot( [xl[1], xl[1]], [yl[0], yl[0]], [zl[0], zl[1]], color='k' )
    ax.plot( [xl[1], xl[1]], [yl[1], yl[1]], [zl[0], zl[1]], color='k' )
    
    return fig
