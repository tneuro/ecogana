"""

Signal Characteristics
======================

"""

import os.path as p
import numpy as np
import os.path as osp
from glob import glob
import copy

from ecoglib.filt.time import filter_array, notch_all
from ecoglib.vis.script_plotting import ScriptPlotterMaker

import ecogana.anacode.signal_testing as st
from ecogana.froemke.experiment_conf import session_conf
from ecogana.froemke.code.prepare_data import load_experiment


## *** session *** (magic string, leave it!)
session = "2014-09-23"
## *** recording *** (magic string, leave it!)
recording = 'test_036'

clobber = True
spm = ScriptPlotterMaker(
    session, 'signal_power', www=True, 
    formats=('png','pdf'),
    dpi=300
    )

bsize_sec = 2
# hard coded for Froemke tests??
electrode_pitch = 0.406

notches = () #(60, 120, 180, 240)

conf = session_conf(session)
sconf = conf.session

if isinstance(recording, str) and recording=='all':
    recording = filter(lambda s: s != 'session', conf.sections)

if not isinstance(recording, (list, tuple)):
    recording = (recording,)

for rec_spec in recording:
    figname = rec_spec
    
    existing_plots = glob( 
        osp.join(
            osp.join(osp.join(session, 'signal_power'), 'pdf'), figname+'*'
            )
        )
    if not clobber and len( existing_plots ):
        continue

    rconf = sconf.deepcopy()
    rconf.update(conf.get(rec_spec, {}))
    headstage = rconf.headstage
    
    bandpass = (2, -1)

    dset = load_experiment(
        session, rec_spec, bandpass=bandpass, 
        snip_transient=True, save=False, notches=notches
        )

    chan_map = dset.chan_map
    Fs = dset.Fs
    if 'ground_chans' in dset:
        data = np.row_stack( (dset.data, dset.ground_chans) )
        d_chans = np.arange(dset.data.shape[0])
        g_chans = np.arange(dset.ground_chans.shape[0]) + dset.data.shape[0]
    else:
        data = dset.data
        d_chans = np.arange(dset.data.shape[0])
        g_chans = ()

    with spm.new_plotter() as splot:
        fig_all, fig_avg = st.plot_avg_psds(
            data, d_chans, g_chans, 'Channel spectra: %s'%headstage, 
            bsize_sec=bsize_sec, Fs=Fs, units=dset.units
            )
        xlim = min(1000, Fs/2)
        fig_all.axes[0].set_xlim(0, xlim)
        fig_avg.axes[0].set_xlim(0, xlim)
        splot.savefig(
            fig_all, figname+'_all_spectra', 
            rst_text='Spectral power density computed and averaged over %f '\
            'sec blocks.'%bsize_sec
            )
        splot.savefig(
            fig_avg, figname+'_avg_spectra',
            rst_text='Average spectra with standard deviation margins. '\
            'Outliers fall beyond 3 times the interquartile range.'
            )

        fig = st.plot_rms_array(
            data, d_chans, chan_map, '%s RMS power: electrode array'%headstage,
            Fs=Fs, color_lims=True, units=dset.units
            )
        splot.savefig(
            fig, figname+'_array',
            rst_text='RMS signal power in electrode array layout'
            )

        fig = st.plot_rms_array(
            data, d_chans, chan_map, '%s RMS power: electrode array'%headstage,
            Fs=Fs, color_lims=False, units=dset.units
            )
        splot.savefig(
            fig, figname+'_array_fr',
            rst_text='RMS signal power in electrode array layout (full range)'
            )

        if headstage.startswith('mux'):
            fig = st.plot_mux_columns(
                data, d_chans, g_chans, 'RMS power: MUX columns', Fs=Fs,
                color_lims=False, units=dset.units
                )
            splot.savefig(
                fig, figname+'_columns_fr',
                rst_text='RMS signal power in DAQ columns (full range)'
                )
        ### The following analyses require bandpass filtering
        dset.data = filter_array(
            dset.data, inplace=False, 
            design_kwargs=dict(lo=2, hi=200, Fs=dset.Fs)
            )
        if not notches:
            notch_all(dset.data, dset.Fs, nmax=360, nwid=3.0)
        
        fig, mask = st.plot_channel_mask(
            dset.data, dset.chan_map, '%s %s'%(headstage, rec_spec), 
            units=dset.units
            )
        splot.savefig(
            fig, figname+'_bad_channels',
            rst_text='Channel mask applied on outliers in RMS power'
            )

        fig = st.plot_site_corr_new(
            data[:len(d_chans)], chan_map, 'Channel correlation table'
            )
        splot.savefig(
            fig, figname+'_corrcoef',
            rst_text='Correlation matrix averaged over %.1f sec blocks'%bsize_sec
            )

        fig_cent, fig_hex = st.plot_centered_rxx(
             dset.data, d_chans[mask], chan_map.subset( mask.nonzero()[0] ), 
            ' '.join([headstage, rec_spec]), Fs=Fs, pitch=electrode_pitch
            )
        splot.savefig(
            fig_cent, figname+'_centered_corr', 
            rst_text='Cross-correlation averaged over %.1f sec blocks, '\
            'arranged by 2D distance, and centered relative to each site. '\
            'Marginal plots show average horizontal '\
            'and vertical correlations.'%bsize_sec
            )
        splot.savefig(
            fig_hex, figname+'_correlation_density',
            rst_text= 'Density plot shows sample of correlation values at each '\
            'unique distance combination on the array.'
            )

        # Note -- this plot *does not* exclude outliers
        emphasis = 'rows+cols,allnabes' if headstage.startswith('mux') else ''
        fig = st.scatter_correlations(
            dset.data, d_chans, chan_map, mask,
            'Correlation groups by distance: %s'%headstage, highlight=emphasis,
            pitch=electrode_pitch
            )
        splot.savefig(
            fig, figname+'_correlation_scatters',
            rst_text='Pairwise cross-correlation is plotted against the '\
            'corresponding distance of the electrode pair. In the top panel '\
            'sites that share rows and columns in MUX geometry are '\
            'highlighted. In the bottom panel, these groups are restricted to '\
            'sites that share neighboring row and column indexes.'
            )

