import os
import os.path as osp
import numpy as np
import matplotlib.cm as cmaps
import matplotlib.pyplot as pp
if __name__=='__main__':
    from matplotlib.figure import Figure
    pyplot_figure = pp.figure
    pp.figure = Figure
from ecoglib.util import Bunch

from ecogana.froemke.code.exp_view import TonotopyAnalysis, \
     TonotopyAnalysisSetup, TonotopicMap, TonotopicFrequencyAnalysis
from ecogana.expconfig import params, new_SafeConfigParser
from ecogana.froemke.experiment_conf import session_info, session_conf

from traits.api import HasTraits, Str, Enum, List, Instance, Button, Int, \
     Float, Array, Enum, Bool, on_trait_change, Range, File, Tuple, \
     Property, property_depends_on
from traitsui.api import View, Item, UItem, Handler, EnumEditor, \
     TextEditor, SetEditor, Include, Group, VGroup, HGroup, \
     CheckListEditor, CustomEditor, spring, ListEditor, RangeEditor

from ecoglib.vis.gui_tools import SavesFigure, ArrayMap, EvokedPlot

def auto_labels(exp):
    labels = dict()
    # xxx: this should be proper, but in mixed experiment configs
    # there are default values for extra conditions
    for name in exp.event_names:
        table = np.unique(getattr(exp, name))
        if name.lower().find('carrier_tone') >= 0:
            label = ['{0:1.1f} k'.format(t/1000) for t in table]
        elif name.lower().find('am_freq') >= 0:
            label = ['{0} Hz'.format(t) for t in table]
        elif name.lower().find('amp') >= 0:
            label = ['{0:2d} dB'.format(int(t)) for t in table]
        elif name.lower().find('mod_freq') >= 0:
            label = ['{0} Hz'.format(t) for t in table]
        elif name.lower().find('am_depth') >= 0:
            label = ['{0}%'.format(100 * t) for t in table]
        else:
            label = ['{0}'.format(t) for t in table]

        labels[name] = label
    return labels


def auto_colors(exp):
    _def_colors = dict(
        am_depth='rainbow',
        mod_freq='terrain',
        carrier_tone='gnuplot',
        tone='cubehelix',
        tones='cubehelix',
        amp='copper'
        )
    return dict( [(t, _def_colors.get(t, 'Set1')) for t in exp.enum_tables] )

import ecogana.anacode.anatool.modules as mods
import ecogana.anacode.anatool.base as base

class SanesAnalysis(base.Analysis):

    modules = List(
        (mods.ChannelAnalysis, mods.AverageAnalysis,
         mods.ArrayMaps, mods.FrequencyAnalysis,
         mods.Spectrogram)
        )

    def __init__(self, dataset, **traits):
        exp = dataset.exp
        tab_names = exp.event_names
        tab_labels = auto_labels(exp)
        colors = auto_colors(exp)
        colors.update( traits.pop('condition_colors', {}) )
        # bypass TonotopyAnalysis instantiation -- might
        # think about making a middle-ground tonotopy class
        super(SanesAnalysis, self).__init__(
            dataset, condition_labels=tab_labels, 
            condition_colors=colors, **traits
            )
    
    def _attach_array_plot(self):
        labels = self.condition_labels[self.exp.enum_tables[0]]
        tone_score = self.ep_score()
        while tone_score.ndim > 2:
            tone_score = tone_score.mean(axis=-1)

        ttl_str = self.dataset.name
        cm = self._colors[ self.exp.enum_tables[0] ]
        self.array_plot = TonotopicMap(
            tone_score, self.dataset.chan_map, labels, ttl_str,
            desaturate=False, figsize=(4.67, 4.0), cmap=cm
            )

    # Custom tweak to the EP plotting, which plots groups of lines per tone
    @on_trait_change('selected_site')
    def _change_ep_plot(self):
        lines = self.ep_plot.static_artists[:]
        self.ep_plot.remove_static_artist(lines)
        # this is effectively removing any pk_scatter, so mark it as gone
        self._pk_scatter = None

        if self.selected_site < 0:
            # go back to grand averages
            plt_avg = self.ep_average(reshape=False)
            while plt_avg.ndim > 2:
                plt_avg = plt_avg.mean(1)
            colors = self._colors['allchan']
        else:
            plt_avg = self.ep_average(reshape=True)[self.selected_site]
            plt_avg.shape = (plt_avg.shape[0], -1, plt_avg.shape[-1])
            colors = self._colors.values()[0]
            
        tx = self._timebase()
        new_lines = self.ep_plot.create_fn_image(
            plt_avg.T, t=tx, color=colors, lw=0.5
            )
        
        self.ep_plot.add_static_artist(new_lines)
        mn = plt_avg.min()
        mx = plt_avg.max()
        #ymin = 10 * (np.abs(mn)//10 + 1) * np.sign(mn)
        #ymax = 10 * (np.abs(mx)//10 + 1) * np.sign(mx)
        ymin, ymax = mn, mx
        self.ep_plot.ylim = (ymin, ymax)
        self.ep_plot.xlim = self.epoch_start, self.epoch_end
        self._toggle_peaks()
        self.ep_plot.draw()


def available_sessions():
    from glob import glob
    from ecogana.froemke.experiment_conf import available_sessions
    sessions = available_sessions()
    sessions = filter(lambda s: s.find('sanes')>=0, sessions)
    if not len(sessions):
        return ['None']
    return sessions

class SanesSessionHandler(Handler):
    runs = List(Str)
    available_runs = List(Str)
    session = Instance(Bunch)
    
    def _session_scan(self):
        if not 'tone_tab' in self.session:
            self.session.tone_tab = 'none'

    def object_session_changed(self, info):
        # want to set the '.values' attribute of the info.object thing
        info.object.proc_runs = list()
        session = info.object.session
        si = session_info(session)
        sconf = session_conf(session)
        self.session = sconf.session
        self._session_scan()
        session_items = [s for s in sorted(sconf.sections) 
                         if s not in ('session', 'tonotopy')]

        run_info = list()
        loc_path = si.get('exp_path', '')
        nwk_path = si.get('nwk_path', '')
        for run in session_items:
            exp_type = sconf[run].get('exp_type', '<no exp>')
                
            if osp.exists(osp.join(loc_path, run+'.h5')):
                pth = ''
                ftype = 'h5'
            elif osp.exists(osp.join(loc_path, run+'.tdms')):
                pth = ''
                ftype = 'tdms'
            elif osp.exists(osp.join(nwk_path, run+'.h5')):
                pth = 'nwk'
                ftype = 'h5'
            elif osp.exists(osp.join(nwk_path, run+'.tdms')):
                pth = 'nwk'
                ftype = 'tdms'
            else:
                pth = 'offline'
                ftype = ''

            run_str = ', '.join( filter(None, (run, exp_type, ftype, pth)) )
            run_info.append(run_str)
        
        self.available_runs = run_info

    def object_proc_runs_changed(self, info):
        self.available_runs = sorted(self.available_runs)


class SanesAnalysisSetup(TonotopyAnalysisSetup):
    """
    Loads the same kinds of files in the same ways,
    just describes the sessions differently

    """
    
    session = Enum(available_sessions()[0], available_sessions())
    ana_class = SanesAnalysis

    def default_traits_view(self):
        v = super(SanesAnalysisSetup, self).default_traits_view()
        v.handler = SanesSessionHandler
        # hack!
        v.content.content[0].content[0].content.pop()
        return v
