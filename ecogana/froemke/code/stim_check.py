import numpy as np
import matplotlib.pyplot as pp
import matplotlib as mpl
import ecoglib.trigger_fun as tfun
import sandbox.mtm_spectrogram as mtm_spec
from ecoglib.vis.script_plotting import ScriptPlotterMaker

import nitime.algorithms as ntalg

import ecogana.froemke.code.join_data as join_data
from ecogana.froemke.experiment_conf import session_info

def stim_plots(session, test_nums, audio_chan=0):
    tests = ['test_%03d'%t if isinstance(t, int) else t for t in test_nums]

    dset = join_data.append_datasets(
        session, tests, save=False
        )
    exp_path = session_info(session)['exp_path']


    audio = dset.bnc
    if audio.ndim > 1:
        audio = audio[audio_chan]

    audio_trig = dset.exp[:]
    samp_change = len(audio) / dset.data.shape[-1]
    audio_trig.time_stamps *= samp_change

    Fs = dset.Fs * samp_change
    print Fs
    check_audio_labels(audio, audio_trig, Fs, name=dset.name, path=exp_path)

def check_audio_labels(audio, audio_exp, Fs, isi=0.5, name='', path='.'):
    
    spm = ScriptPlotterMaker(
        '.', name+'_stimcheck', formats=('pdf',),
        dpi=300
        )

    inter_trig_pts = int(isi * Fs)
    stim_epochs = tfun.extract_epochs(
        audio, audio_exp, 
        pre=0.1*Fs, 
        post=inter_trig_pts
        )
    stim_tx = np.linspace(
        -100, 1000*inter_trig_pts/Fs, stim_epochs.shape[-1]
        )
    stim_epochs = stim_epochs[0]

    conditions, cond_tables = audio_exp.enumerate_conditions()
    tones = cond_tables.tones; amps = cond_tables.amps
    utones = np.unique(tones)
    uamps = np.unique(amps)
    tone_labels = ['%1.1f k'%(t/1000,) for t in utones]
    # quick look at DC offset bizniz

    pre_level = np.mean(stim_epochs[:, stim_tx<0], axis=1)
    post_level = np.mean(stim_epochs[:, stim_tx>0], axis=1)
    dc_offset = post_level - pre_level

    with spm.new_plotter() as splot:
        pp.figure()
        pp.hist(dc_offset, bins=20)
        pp.gca().xaxis.set_major_formatter(
            mpl.ticker.FormatStrFormatter('%.1e')
            )
        splot.savefig(pp.gcf(), 'dc_shifts')


    # check that the tones are where they're supposed to be

    with spm.new_plotter() as splot:
        for tone in utones:
            epochs = stim_epochs[audio_exp.tones==tone]
            # if sorted on the expected amplitude level,
            # then the plotted traces should be grouped by hue
            ix = np.argsort(audio_exp.amps[ audio_exp.tones==tone ])
            epochs = epochs[ix]
            
            t0 = audio_exp.stim_props.tone_onset - 0.05
            t1 = audio_exp.stim_props.tone_onset + \
              audio_exp.stim_props.tone_width + 0.05
            epochs = epochs[:, (stim_tx>t0*1000) & (stim_tx<t1*1000)]

            f0 = tone if tone < Fs/2 else abs(Fs-tone)
            while f0 > Fs/2:
                f0 = abs(Fs - f0)
            
            pp.figure(figsize=(8,8));
            pp.subplot(311)
            pp.gca().set_color_cycle(
                list(pp.cm.jet(np.linspace(0,1,epochs.shape[0])))
                )
            #sub_tx = stim_tx[(stim_tx>t0*1000) & (stim_tx<t1*1000)]
            dmod, ix, _ = mtm_spec.mtm_complex_demodulate(
                epochs, 4, adaptive=False, samp_factor=16
                )

            f0_idx = int( round( ((dmod.shape[1] - 1) * 2 / Fs) * f0 ) )
            
            #hbt = signal.hilbert(epochs)
            #pp.plot(sub_tx, np.abs(hbt).T)
            pp.plot(
                ix / Fs + t0,
                np.abs(dmod[:, f0_idx, :]).T
                )
            pp.title('Stim power envelope')
            pp.xlabel('time from trigger (sec)')
            
            fx, psds, _ = ntalg.multi_taper_psd(
                epochs, NW=2, Fs=Fs, jackknife=False, adaptive=False
                )

            pp.subplot(312)
            pp.gca().set_color_cycle(
                list(pp.cm.jet(np.linspace(0,1,epochs.shape[0])))
                )
            pp.plot(fx, psds.T)
            if tone > Fs/2:
                pp.title('stimuli at triggers labeled %.1f KHz (aliased)'%(
                    tone/1000,
                    ))
                f0 = abs(Fs - tone)
                while f0 > Fs/2:
                    f0 = abs(Fs - f0)
            else:
                pp.title('stimuli at triggers labeled %.1f KHz'%(tone/1000,))
                f0 = tone
            pp.subplot(313)
            pp.gca().set_color_cycle(
                list(pp.cm.jet(np.linspace(0,1,epochs.shape[0])))
                )
            #pp.plot(fx, psds.T)
            pp.semilogy(fx, psds.T)
            pp.xlim( max(0, f0-2000), min(Fs/2, f0+2000) )
            pp.gca().axvline(f0, color='c', linestyle='--', linewidth=3)
            pp.gcf().subplots_adjust(hspace=0.45)
            splot.savefig(pp.gcf(), 'stim_spectra_%1.1f_k'%(tone/1000,))

    # find the amplitude-tone profile for the maximum amp
    
    tone_amp_curve = np.zeros_like(utones)
    epochs = stim_epochs[audio_exp.amps==uamps.max()]
    tones = audio_exp.tones[audio_exp.amps==uamps.max()]
    t0 = audio_exp.stim_props.tone_onset - 0.02
    t1 = t0 + audio_exp.stim_props.tone_width + 0.02
    epochs = epochs[:, (stim_tx>t0*1000) & (stim_tx<t1*1000)]

    fx, psds, _ = ntalg.multi_taper_psd(
        epochs, NW=2, Fs=Fs, jackknife=False, adaptive=False
        )
    tone_amp_curve = [ psds[tones==t].max(axis=1).mean() for t in utones ]
    with spm.new_plotter() as splot:
        f = pp.figure(figsize=(12,6))
        pp.plot( np.log(utones), tone_amp_curve, 'b-' )
        pp.plot( np.log(utones), tone_amp_curve, 'b.' )
        pp.xticks( np.log(utones), tone_labels )
        pp.xlabel('tone')
        pp.ylabel(r'$V^{2}/Hz$')
        pp.title('Tone calibration curve at %d dB SPL'%uamps.max())
        splot.savefig(f, 'calibration_curve')

        txt = 'calibration points:\n'
        txt = txt + '\n'.join( ['%s : %1.2e V^2/Hz'%(t, a) 
                          for (t, a) in zip(tone_labels, tone_amp_curve)] )
        print txt
        tfig = pp.figure(figsize=(4,4))
        tfig.text(.5, .5, txt, ha='center', va='center')
        splot.savefig(tfig, 'calibration_points')
    
