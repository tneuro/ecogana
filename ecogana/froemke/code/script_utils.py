import numpy as np
import ecoglib.numutil as nut
import ecoglib.trigger_fun as tfun
import sandbox.array_split as array_split
    
def randomize_phase(x, xf=None):
    if xf is None:
        xf = np.fft.fft(x)
    nfft = xf.shape[-1]
    xf_mag = np.abs(xf)
    shape = x.shape[:-1] + (nfft/2,)
    xf_ssphs = 2*np.pi*(np.random.rand(*shape) - 0.5)
    xf_phs = np.c_[
        np.zeros(x.shape[:-1]+(1,)), xf_ssphs, -xf_ssphs[...,:-1][...,::-1]
        ]
    xrand = np.fft.ifft( xf_mag * np.exp(1j*xf_phs) ).real
    return xrand

@array_split.split_at()
def gen_dephased_samples(x, num, scoring_fn, tmax=None, randseed=None):
    # This function not fully featured and likely incompatible
    if randseed is None:
        import time
        randseed = int(time.time())
    np.random.seed(randseed)
    samps = np.zeros( (x.shape[0], num) )
    xf = np.fft.fft(x)
    for n in xrange(num):
        xrand = randomize_phase(x)
        if tmax:
            xrand = xrand[...,:tmax]
        samps[:, n] = scoring_fn( xrand.mean(axis=1) )
        #samps[:, n] = np.ptp( xrand.mean(axis=1), axis=-1 )
    return samps

@array_split.split_at()
def gen_permuted_samples(
        x, nsamp, exptab, ana_win, scoring_fn,
        single_trial=False, maxstat=True, randseed=None,
        replacement=False, **kwargs
        ):
    # This method returns a sample of scores under the null hypothesis
    # of sites recording spontaneous activity that is independent of
    # the stimulation. The sample can be generated from single-trial
    # scores, or from the score of average false-responses.
    #
    # In single-trial mode, scores are taken from each response in
    # a falsely grouped set of responses. An estimator for a 
    # "representative" single-trial response may be calculated by
    # these scores.
    # Otherwise, a score is taken from the average response of 
    # falsely grouped sets.
    #
    # 
    #
    # when split, we're working with a fraction of rows of x
    if randseed is None:
        import time
        randseed = int(time.time())
    np.random.seed(randseed)
    conds, _ = exptab.enumerate_conditions()
    ncond = len(np.unique(conds))
    nrep = len(conds) / ncond
    if single_trial:
        samps_shape = (x.shape[0], ncond, nrep, nsamp)
    else:
        samps_shape = (x.shape[0], ncond, nsamp)

    # allow for the possibility of a simple pass-thru,
    # which will return epochs with randomized phase or labels
    if scoring_fn is None:
        scoring_fn = lambda x: x
        samps_shape = samps_shape + xscore.shape[-1]

    samps = np.zeros( samps_shape )
        
    pre, post = ana_win
    for n in xrange(nsamp):
        if replacement:
            # sample with replacement from the set of trials
            rand_trials = np.random.randint(
                0, high=nrep*ncond, size=nrep*ncond
                )
            rand_trials.shape = (ncond, nrep)
        else:
            # shuffle exactly the condition set
            np.random.shuffle(conds)
            rand_trials = np.row_stack(
                [np.where(conds==c)[0] for c in xrange(ncond)]
                )
        for c in xrange(ncond):
            epochs = tfun.extract_epochs(
                x, exptab, selected=rand_trials[c], pre=pre, post=post
                )
            if single_trial:
                samps[:,c,:,n] = scoring_fn( epochs )
            else:
                samps[:,c,n] = scoring_fn( epochs.mean(axis=1) )
    return samps
    

def false_scored_means(
        data, audexp, scoring_fn, scoring_win, 
        nsurr=200, method='permute', replacement=False, randseed=None
        ):
    # scoring win needs to be specified in pts
    conditions, cond_tables = audexp.enumerate_conditions()
    tones = cond_tables.tones; amps = cond_tables.amps
    utones = np.unique(tones)
    uamps = np.unique(amps)
    ncond = len(tones)

    
    pre, post = scoring_win
    tmax = pre+post
    if method=='phase':
        # change pre + post to be a power of 2, keep tmax the same
        d = nut.nextpow2(post + pre)
        post = -pre + d

    false_scores = np.zeros( (data.shape[0], ncond, nsurr) )

    if method=='phase':
        for c in xrange(ncond):
            epochs = tfun.extract_epochs(
                data, audexp, conditions==c+1,
                pre=pre, post=post
                )

            false_scores[:,c,:] = gen_dephased_samples(
                epochs, nsurr, scoring_fn, tmax=tmax
                )
    elif method=='permute':
        for n in xrange(len(uamps)):
            subexp = audexp.subexp( audexp.amps==uamps[n] )
            samples = gen_permuted_samples(
                data, nsurr, subexp, (pre, post), scoring_fn,
                replacement=replacement, randseed=randseed
                )
            false_scores[:, n::len(uamps), :] = samples

    return false_scores

def fra_thresholds(true_scores, false_scores, plot=False, nstdev=3):
    # find active sites with RF variance rule, 
    # use false RFs to estimate a threshold
    #
    # This method seems formally similar to an F test

    # false score should be shaped (nchan, ntone, namp, nsamps)
    (nchan, ntone, namp, nsamps) = false_scores.shape
    ncond = ntone * namp
    false_scores = false_scores.transpose(0, 2, 1, 3).copy()
    false_scores.shape = (nchan, namp, -1)
    # 1st average over samples and tones to get population stats
    mu = np.mean(false_scores, axis=-1)
    sig = np.std(false_scores, axis=-1)
    
    true_zscore = (true_scores - mu[:,None,:]) / sig[:,None,:]
    false_zscores = (false_scores - mu[:,:,None]) / sig[:,:,None]
    false_zscores.shape = (nchan, ncond, -1)
    false_vars = np.var(false_zscores, axis=1)
    var_thresh = np.mean(false_vars, axis=-1) + \
      nstdev*np.std(false_vars, axis=-1)
    true_var = np.var(true_zscore.reshape(nchan, -1), axis=1)
    active_sites = np.where(true_var > var_thresh)[0]

    if plot:
        import matplotlib.pyplot as pp
        pp.figure()
        pp.boxplot(false_vars.T)
        pp.scatter(np.arange(1,nchan+1), true_var, color='g')
        pp.scatter(np.arange(1,nchan+1), var_thresh, color='r')
    return active_sites, true_zscore
        
def ordered_tone_epochs(tones, amps, a, group_sizes=False):
    idx = [np.where( (tones==t) & (amps==a) )[0] for t in np.unique(tones)]
    sizes = [len(i) for i in idx]
    if group_sizes:
        return np.concatenate(idx), sizes
    return np.concatenate(idx)

def ordered_amp_epochs(tones, amps, t, group_sizes=False):
    idx = [np.where( (tones==t) & (amps==a) )[0] for a in np.unique(amps)]
    sizes = [len(i) for i in idx]
    if group_sizes:
        return np.concatenate(idx), sizes
    return np.concatenate(idx)

