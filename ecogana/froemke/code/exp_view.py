import matplotlib.pyplot as pp
from matplotlib import cm
if __name__=='__main__':
    from matplotlib.figure import Figure
    pyplot_figure = pp.figure
    pp.figure = Figure


import os.path as osp

import numpy as np
from scipy.ndimage import gaussian_filter1d
import scipy.stats.distributions as dists

from traits.api import Str, List, Instance, Button, Int, \
     Float, Enum, Bool, on_trait_change, Code
from traitsui.api import View, Item, UItem, Handler, SetEditor, Group, VGroup, HGroup, spring

from ecogana.froemke.experiment_conf import *
from ecogana.froemke.code.join_data import append_datasets
from ecoglib.util import Bunch, mkdir_p, equalize_groups
from ecoglib.channel_map import CoordinateChannelMap

from matplotlib.figure import Figure
import ecogana.froemke.code.plotting as plt
import ecogana.anacode.colormaps as colormaps
from ecogana.anacode.plot_util import desaturated_map
from ecogana.anacode.ep_scoring import active_sites, nroots_average, shuffled_nroots_average

from ecogana.anacode.anatool.session_manager import AnalysisSetup
from ecogana.anacode.anatool.base import Analysis
from ecogana.anacode.anatool.modules import *
from ecogana.anacode.anatool.filters_module import Filters

import ecogana.anacode.tile_images as tile_images
from ecogana.expconfig.exp_descr import ordered_epochs
import ecoglib.vis.traitsui_bridge as tb
from ecoglib.vis.gui_tools import SavesFigure, ArrayMap

class TonotopicMap(ArrayMap):

    selected_site = Int(-1)
    
    def __init__(
            self, site_scores, chan_map, tone_labels, title, 
            figsize=(), desaturate=(), cmap=pp.cm.rainbow,
            **plot_kwargs
            ):
        if not figsize:
            figsize = (3.5, 3)
        fig = Figure(figsize=figsize)
        self._coord_map = isinstance(chan_map, CoordinateChannelMap)
        # do an auto colorbar for coordinate maps (not ideal, but the desaturated colormap drawing is buried pretty
        # deep)
        if self._coord_map:
            cbar = True
            arr_ax = fig.add_subplot(111)
        else:
            cbar = False
            arr_ax = fig.add_axes([0.05, 0.05, .75, .75])
            # don't need to hold onto this axes
            _ = fig.add_axes([0.8, 0.05, .125, .75])
            fig.text(0.5, 0.9, title, ha='center', va='center')

        # make a provisional map object, redraw if desaturation is used
        super(TonotopicMap, self).__init__(chan_map, vec=site_scores, ax=arr_ax, cbar=cbar, labels=tone_labels,
                                           clim=(0, len(tone_labels) - 1), cmap=cmap, **plot_kwargs)
        if self._coord_map:
            self.ax.set_title(title)

        self._tone_labels = tone_labels

        if len(desaturate):
            print 'UPDATING PROVISIONAL MAP'
            self.update_map(site_scores, cmap=cmap, desaturate=desaturate, **plot_kwargs)


    def _update_scatter_map(self, scores, cmap=pp.cm.rainbow, desaturate=()):
        scatter = self.ax.collections[-1]
        colors = cmap(scatter.norm(scores))
        if len(desaturate):
            colors[:, -1] = desaturate
        scatter.set_color(colors)


    def _setup_grid_map(self, scores, cmap=pp.cm.rainbow, desaturate=()):
        n_tones = len(self._tone_labels)
        scores = self.chan_map.embed(scores)
        if len(desaturate):
            desaturate = self.chan_map.embed(desaturate)
        else:
            desaturate = 1
        self.fig.axes[0].images = []
        self.fig.axes[1].images = []
        fig = desaturated_map(
            scores, desaturate, cmap, 
            drange=(0, n_tones-1),
            labels=self._tone_labels,
            fig=self.fig
            )
        geo = self.chan_map.geometry
        self.ax.set_xlim(-0.5, geo[1]-0.5)
        self.ax.set_ylim(geo[0]-0.5, -0.5)
        return fig


    def update_map(self, site_scores, **kwargs):
        if self._coord_map:
            self._update_scatter_map(site_scores, **kwargs)
        else:
            self._setup_grid_map(site_scores, **kwargs)
        try:
            self.fig.canvas.draw()
        except AttributeError:
            pass


class TonotopicChannelAnalysis(ChannelAnalysis):
    
    # launch single-trial plots
    plot_all_trials = Button('Plot All Trials')

    def _plot_all_trials_fired(self):
        save_str = 'chan_%02d_all_trials'%self.selected_site
        fig = self.all_trials_heatmap()
        fig.canvas = None
        return SavesFigure.live_fig(
            fig, sfile=save_str, spath=self.spath
            )
    
    def all_trials_heatmap(self, clim=(), cmap=None):
        Fs = self.dataset.Fs
        i, j = self.dataset.chan_map.subset([self.selected_site]).to_mat()
        plt_str = 'Chan %02d, Site (%d, %d) all trials'%(
            self.selected_site, i[0], j[0]
            )
        
        conditions, cond_tables = self.exp.enumerate_conditions()
        uconds = np.unique(conditions)
        n_reps = [np.sum(conditions==c) for c in uconds]
        mx_rep = max(n_reps)
        rep_groups = np.array(n_reps).reshape(len(self.tones), len(self.amps))

        all_epochs = self.ana.epochs(chan=self.selected_site).squeeze()
        mn, mx = np.percentile(all_epochs.ravel(), (1e-2, 100-1e-2))
        n_pts = all_epochs.shape[-1]
        tx = np.arange(n_pts) / Fs + self.epoch_start
        
        epochs = np.empty( (len(self.tones), len(self.amps), mx_rep, n_pts) )
        epochs.fill(np.nan)

        for a in xrange(len(self.amps)):
            idx, g = ordered_epochs(
                self.exp, [('amps', self.amps[a])], group_sizes=True
                )
            g = np.r_[0, np.cumsum(g)]
            amp_epochs = np.take(all_epochs, idx, axis=0)
            for t in xrange(len(self.tones)):
                epochs[ t, a, :rep_groups[t,a], : ] = amp_epochs[g[t]:g[t+1]]

        if not len(clim):
            clim = (mn, mx)
        if not cmap:
            if mn < 0 and mx > 0:
                cmap = colormaps.diverging_cm(mn, mx) 
            elif mn < 0 and mx < 0:
                cmap = colormaps.blend_palette(
                    ['blue', 'white'], n_colors=cm.jet.N, as_cmap=True
                    )
            else:
                cmap = colormaps.blend_palette(
                    ['white', 'red'], n_colors=cm.jet.N, as_cmap=True
                    )

        fig = plt.stacked_epochs(
                tx, epochs, self.tones, self.amps, 
                np.arange(mx_rep, len(self.tones)*mx_rep, mx_rep), 
                tm=0.0, stacked='tones', bcolor=(.4, .4, .4),
                cmap=cmap, clim=clim,
                title=plt_str, clabel=self.ana._nice_unit
                )
        return fig

    def default_traits_view(self):
        v = super(TonotopicChannelAnalysis, self).default_traits_view()
        new_button = UItem(
            'plot_all_trials', enabled_when='trials_as_heatmaps==True'
            )
        vgroup = v.content.content[0]
        hgroup = vgroup.content[0]
        hgroup.content.insert(0, new_button)
        return v        

from ecogana.anacode.anatool.modules import AverageAnalysis
    
class TonotopicAverageAnalysis(AverageAnalysis):
    """
    This type modifies the labels of the default Average response plots.
    
    """
    
    def _plot_site_table_b_fired(self):
        site = self.selected_site
        i, j = self.dataset.chan_map.subset([site]).to_mat()
        title = 'AEPs Chan %02d, Site (%d, %d)'%(site, i[0], j[0])
        parent = super(TonotopicAverageAnalysis, self)
        return parent._plot_site_table_b_fired(
            tabs=('amps', 'tones'), title=title
            )
    
    def _plot_array_avg_b_fired(self):
        title = 'Mean AEP +/- %s, %s @ %s'%(
            self.y_err, self.tones_label, self.amps_value
            )
        parent = super(TonotopicAverageAnalysis, self)
        return parent._plot_array_avg_b_fired(title=title)


from ecogana.anacode.anatool.modules import ArrayMaps
class TonotopicArrayMaps(ArrayMaps):
    """
    Makes array maps of response magnitude and delay, and FRAs.
    """

    p_delay_maps = Button('Plot Delay')
    ep_fra = Button('EP Score FRA')
    z_fra = Button('Z-score FRA')
    smooth = Bool(True)

    def _p_delay_maps_fired(self):
        fig = self.delay_maps()
        sfile = 'delay_maps_%s'%self.amps_label
        fig.canvas = None
        return SavesFigure.live_fig(
            fig, sfile=sfile, spath=self.spath
            )

    def delay_maps(self):
        if self._coord_map:
            print 'Delay map not implemented for coordinate maps'
            return
        #avg = self.ana.ep_average()[:, :, self._amp_idx]
        p2p, npk, ppk = self.ana.peak_to_peak(points=True, reshape=True)
        if p2p.ndim > 2:
            npk = npk[:, :, self._amps_idx[0]]
            ppk = ppk[:, :, self._amps_idx[0]]
            p2p = p2p[:, :, self._amps_idx[0]]

        #delay_maps = 0.5 * (ppk.T + npk.T)
        delay_maps = ppk.T
        mn_delay = delay_maps.min(axis=1)
        #delay_maps = delay_maps - mn_delay[:,None]
        delay_maps = self.dataset.chan_map.embed(delay_maps, axis=1)

        cmap = pp.cm._generate_cmap('winter', 128)
        cmap.set_bad( (1,1,1,1) )
        mx_delay = 0.015 * self.dataset.Fs

        md_mn = np.median(mn_delay)
        norm = pp.Normalize(md_mn, md_mn+mx_delay, clip=True)
        no_response, pk_response = np.percentile(p2p.ravel(), [10, 98])
        sat_norm = pp.Normalize(no_response, pk_response, clip=True)
        mn_alpha = 0.
        #mn_alpha = 0.05
        
        delay_colors = cmap( norm(delay_maps), bytes=True )
        sat_values = self.dataset.chan_map.embed( sat_norm(p2p.T), axis=1 )
        delay_colors[..., -1] = (sat_values*(1-mn_alpha) + mn_alpha) * 255
        fig, axs = tile_images.quick_tiles(
            len(self.tones), nrow=1, 
            title='Activation Order @ %s'%self.amps_label, calib='bottom'
            )
        for n in xrange(len(self.tones)):
            tone = self.tones[n]
            ax = axs[n]
            ax.imshow(delay_colors[n], origin='upper')
            ax.axis('image'); ax.axis('off')
            ax.text(
                0.5, 1.025, self.tones_labels[n], 
                va='bottom', ha='center',
                transform=ax.transAxes
                )

        fig.texts[0].set_position((0.5, 0.9))
        fig.subplots_adjust(
            top = 1, bottom = 0, left = 0.015, right = 0.985, hspace = 0.2
            )
        cax = fig.add_axes([.25, .2, .5, .1])
        graded_color_map = cmap(
            np.tile(np.linspace(0,1,200), (20, 1)), bytes=True
            )
        saturation = ( 255 * np.linspace(1, 0, 20, endpoint=False)**2 ).astype('B')
        graded_color_map[:,:,-1] = saturation[:,None]
        mn_time = md_mn / self.dataset.Fs + self.epoch_start
        mx_time = (md_mn + mx_delay) / self.dataset.Fs + self.epoch_start
        cax.imshow(
            graded_color_map, extent=[mn_time*1000, mx_time*1000, 0, 1]
            )
        cax.set_aspect('auto')
        cax.xaxis.set_tick_params(labelsize=10)
        cax.yaxis.set_visible(False)
        cax.set_xlabel('Waveform latency (msec)')
        #splot.savefig(fig, 'waveform_delays')
        return fig

    def smooth_fra(self, scores, sigma=1):
        scores = gaussian_filter1d(scores, sigma, axis=-1)
        scores = gaussian_filter1d(scores, sigma, axis=-2)
        return scores
    
    def _ep_fra_fired(self):
        fig = self.ep_fra_plot()
        sfile = '%s_FRAs'%self.ana.scoring
        fig.canvas = None
        return SavesFigure.live_fig(
            fig, sfile=sfile, spath=self.spath
            )
        
    def ep_fra_plot(self):
        avg_score = self.ana.ep_score(reshape=True)
        if self.smooth:
            avg_score = self.smooth_fra(avg_score)
        amn, amx = np.percentile(avg_score.ravel(), [2, 98])
        fig = tile_images.tile_images(
            avg_score, p=self.dataset.chan_map, fill_empty=False,
            border_axes=True,
            title='%s Score RFs Over Array'%self.ana.scoring,
            clabel=r'%s score %s'%(self.ana.scoring, self.ana._nice_unit),
            cmap=pp.cm.BuPu, clim=(amn, amx),
            x_labels=self.labels('tones'), y_labels=self.labels('amps')
            )
        return fig

    def _z_fra_fired(self):
        fig = self.z_fra_plot()
        sfile = '{0}_zFRAs'.format(self.ana.scoring)
        fig.canvas = None
        return SavesFigure.live_fig(
            fig, sfile=sfile, spath=self.spath
            )
    
    def z_fra_plot(self):
        if self._coord_map:
            print 'FRA plot not implemented for coordinate maps'
            return
        avg_score = self.ana.rank_sum_z(reshape=True)
        if self.smooth:
            avg_score = self.smooth_fra(avg_score)
        amn, amx = np.percentile(avg_score.ravel(), [2, 98])
        fig = tile_images.tile_images(
            avg_score, p=self.dataset.chan_map, fill_empty=False,
            border_axes=True,
            title='%s Z-score RFs Over Array'%self.ana.scoring,
            clabel='{0} zscore'.format(self.ana.scoring),
            cmap=pp.cm.BuPu, clim=(amn, amx),
            x_labels=self.labels('tones'), y_labels=self.labels('amps')
            )
        axs = fig.axes
        n_tones = len(self.values('tones'))
        n_amps = len(self.values('amps'))
        yy, xx = np.mgrid[:n_amps, :n_tones]
        if not self.smooth:
            avg_score = self.smooth_fra(avg_score)
        for n, p in enumerate(self.ana.dataset.chan_map.as_row_major()):
            ax = axs[p]
            signif = dists.norm.isf(0.05)
            ax.contour(
                xx, yy, avg_score[n].T, [signif], 
                colors='#4BAB3F', linewidths=2.5
                )
        return fig

    def default_traits_view(self):
        traits_view = super(TonotopicArrayMaps, self).default_traits_view()
        vgroup = traits_view.content.content[0].content[1]
        vgroup.content.append(UItem('p_delay_maps', enabled_when='not _coord_map'))
        # add another VGroup to the parent HGgroup
        new_group = VGroup(
            UItem('ep_fra'), UItem('z_fra'), 
            Item('smooth', label='Smooth FRAs'),
            label='FRAs', enabled_when='not _coord_map'
            )
        hgroup = traits_view.content.content[0]
        hgroup.content.append(new_group)
        return traits_view

import ecoglib.vis.ani as ani
    
class ArrayAnimator(AnalysisModule):

    name = 'Animator'

    fps = Float(20.0)
    repeat = Bool(False)
    play = Button('Play Frames')
    vid = Button('Make Vid')

    traces = Enum( '3x3 nabe', ('channel', '3x3 nabe', 'all') )

    def __init__(self, *args, **kwargs):
        super(ArrayAnimator, self).__init__(*args, **kwargs)
        self._coord_map = isinstance(self.ana.dataset.chan_map, CoordinateChannelMap)
    
    def _play_fired(self):
        fig, func, n_frames = self.make_frames()
        anim = ani._animation.FuncAnimation(
            fig, func, frames=n_frames,
            interval=1000.0/self.fps, blit=False
            )
        anim.repeat = self.repeat
        fig.canvas.show()
        return anim

    def _vid_fired(self):
        fig, func, n_frames = self.make_frames()
        cname = 'all' if self.traces=='all' else str(self.selected_site)
        vname = 'array_%s_%s_chan_%s'%(
            self.tones_label.replace(' ', ''), self.amps_label.replace(' ', ''),
            cname
            )
        vname = osp.join(self.spath, vname)
        mkdir_p(self.spath)
        ani.write_anim(
            vname, fig, func, n_frames,
            title='Auditory evoked potential:\n %s @ %s'%(
                self.tones_label, self.amps_label
                ),
                fps=self.fps, quicktime=True
                )

    def make_frames(self):
        tidx = self._tones_idx[0]
        aidx = self._amps_idx[0]
        avg = self.ana.ep_average()[:, tidx, aidx].copy()
        chan_map = self.dataset.chan_map
        Fs = self.dataset.Fs
        avg_frames = chan_map.embed(avg.T, axis=1)
        #clim = (avg.min(), avg.max())
        #cmap = pp.cm.PuBuGn
        mn = np.nanmin(avg); mx = np.nanmax(avg)
        clim = ( min(mn, -mx), max(-mn, mx) )
        cmap = pp.cm.Spectral_r

        tx = 1000.0 * (np.arange(avg.shape[-1]) / Fs + self.ana.epoch_start)

        if self.traces == 'channel':
            trace = avg[self.selected_site].reshape(len(tx), 1)
        elif self.traces == '3x3 nabe':
            geo = chan_map.geometry
            i, j = chan_map.subset([self.selected_site]).to_mat()
            i = i[0]; j = j[0]
            row_nabes = [i-1] * 3 + [i] * 3 + [i+1] * 3
            col_nabes = [j-1, j, j+1] * 3
            grid_nabes = zip(row_nabes, col_nabes)
            chan_nabes = list()
            for ij in grid_nabes:
                try:
                    c = chan_map.lookup(*ij)
                    chan_nabes.append(c)
                except ValueError:
                    pass            
            trace = avg[chan_nabes].T
        else:
            trace = avg.T

        fig, func = ani.dynamic_frames_and_series(
            avg_frames, trace, tx=tx, 
            xlabel='msec', ylabel=self.ana._nice_unit, 
            imshow_kw=dict(origin='upper', cmap=cmap, clim=clim), 
            line_props=dict(color='b', lw=0.5),
            title='Auditory evoked potential:\n %s @ %s'%(
                self.tones_label, self.amps_label
                )
            )
        c = tb.FigureCanvas(fig)
        return fig, func, avg_frames.shape[0]
        ## fig.canvas = None
        ## return SavesFigure.live_fig(fig, spath=self.spath)

    def default_traits_view(self):
        traits_view = View(
            HGroup(
                VGroup(
                    Item('traces', label='Plotted Chans'),
                    Item('_tones_idx', style='simple', label='tone'),
                    Item('_amps_idx', style='simple', label='amp')
                    ),
                VGroup(
                    Item('fps', label='FPS', style='simple'),
                    Item('play', show_label=False),
                    Item('vid', show_label=False)
                    ),
                VGroup(
                    Item('repeat', label='Repeat Anim?'),
                    Item('epoch_start', label='t_start', width=4),
                    Item('epoch_end', label='t_stop', width=4)
                    ),
                enabled_when='not _coord_map'
                ),
            resizable=True, title='.name'
        )
        return traits_view

class TonotopicFrequencyAnalysis(FrequencyAnalysis):
    """
    This type can update the tonotopic map based on modulation scores.
    """

    swap_tonotopy = Button('Reset tonotopy')

    def _swap_tonotopy_fired(self):

        fx, pss, pnn = self._filled_conditions()
        n_lo = fx.searchsorted(self.p_lo)
        n_hi = len(fx) if self.p_hi < 0 else fx.searchsorted(self.p_hi)
        sub_band = fx[ n_lo:n_hi ]

        pss_pwr = np.nansum(pss[..., n_lo:n_hi], axis=-1)
        pnn_pwr = np.nansum(pnn[..., n_lo:n_hi], axis=-1)
        
        ratio = pss_pwr / pnn_pwr
        if not self.time_average:
            # average over reps
            ratio = np.nanmean( ratio, axis=-1 )

        # send the multiple-dimension score off to the parent to remap
        self.ana.change_map(tone_score=ratio)

    def default_traits_view(self):
        v = super(TonotopicFrequencyAnalysis, self).default_traits_view()
        button = UItem('swap_tonotopy')
        v.content.content[0].content[-1].content.append(button)
        return v

class TonotopyAnalysis(Analysis):

    desaturate = Bool(True)
    replot_map = Button('Update Tonotopy')

    modules = List( 
        (TonotopicChannelAnalysis, 
         TonotopicAverageAnalysis, 
         TonotopicArrayMaps, 
         ArrayAnimator,
         Spectrogram,
         TonotopicFrequencyAnalysis,
         StimDecoding,
         Filters)
        )

    def __init__(
            self, dataset, condition_labels=None, condition_colors=None,
            split_over=('amps',), **traits
            ):
        exp = dataset.exp
        tones = np.unique(exp.tones)
        amps = np.unique(exp.amps)

        tone_labels = ['%1.1f kHz'%(t/1000,) for t in tones]
        amp_labels = ['%02d dB SPL'%(a,) for a in amps]

        if condition_labels is None:
            condition_labels = dict()
        if condition_colors is None:
            condition_colors = dict()
        
        condition_labels.setdefault('tones', tone_labels)
        condition_labels.setdefault('amps', amp_labels)
        
        condition_colors.setdefault('tones', pp.cm.rainbow)
        condition_colors.setdefault(
            'amps', 
            colormaps.cubehelix_palette(
                len(amp_labels), start=.1, rot=2, dark=.4
                )
            )
        self._continuous_p_mapping = True
        super(TonotopyAnalysis, self).__init__(
            dataset, 
            condition_labels=condition_labels,
            split_over=split_over, condition_colors=condition_colors,
            **traits
            )

    @staticmethod
    def from_runs(
            session, runs, bandpass, save=False, snip_transient=False,
            **traits
            ):
        # runs is a sequence of recording names, e.g. 
        # ('test_001', 'test_002', 'test_003')
        dataset = append_datasets(
            session, runs, bandpass=bandpass, 
            save=save, snip_transient=False
            )
        return Analysis(dataset, **traits)

    def _epoch_window(self, samps=True, subresponse=False):
        tone_onset = self.exp.stim_props.tone_onset
        if subresponse:
            pre = -(tone_onset + self.peak_min)
            post = tone_onset + self.peak_max
        else:
            pre = -(tone_onset + self.epoch_start)
            post = tone_onset + self.epoch_end
        if samps:
            len = np.round( (pre + post) * self.dataset.Fs )
            pre = np.sign(pre) * np.round( self.dataset.Fs * np.abs(pre) )
            #post = np.sign(post) * np.round( self.dataset.Fs * np.abs(post) )
            post = len - pre
        return pre, post

    def _false_epoch_window(self, samps=True):
        tone_onset = self.exp.stim_props.tone_onset
        # false epoch offset will be a number like -200 ms (-0.2)
        pre = -(tone_onset + self.false_epoch_offset)
        post = -pre + (self.epoch_end - self.epoch_start)
        if samps:
            len = np.round( (pre + post) * self.dataset.Fs )
            pre = np.sign(pre) * np.round( self.dataset.Fs * np.abs(pre) )
            #post = np.sign(post) * np.round( self.dataset.Fs * np.abs(post) )
            post = len - pre
        return pre, post

    def _shuffled_tonotopic_confidence(self):
        """
        Make an empirical p-value for the tuning curve center of mass
        by comparing with many permutations the tuning curve.
        """
        scores = self.ep_score(single_trials=not self.score_avg_ep)
        # shuffling avg score
        while scores.ndim > 2:
            scores = np.nanmean(scores, axis=-1)
        p = shuffled_nroots_average(scores)
        return p

    def _attach_array_plot(self):
        "Make initial tonotopic map"
        
        tone_labels = self.condition_labels['tones']
        avg_score = self.ep_score(single_trials=not self.score_avg_ep)
        while avg_score.ndim > 2:
            avg_score = np.nanmean(avg_score, axis=-1)
        avg_score, _ = nroots_average(avg_score)
        if self.desaturate:
            p = self._shuffled_tonotopic_confidence()
            if self._continuous_p_mapping:
                desat = 1 - p
            else:
                desat = np.ones_like(p)
                desat[ (p > 0.05) & (p < 0.10) ] = .75
                desat[ (p > 0.10) & (p < 0.15) ] = 0.5
                desat[ (p > 0.15) & (p < 0.5) ] = 0.25
                desat[ p > 0.5 ] = 0
        else:
            desat = ()
        ttl_str = self.dataset.name + ' Tonotopy'
        cm = self._colors['tones']
        self.array_plot = TonotopicMap(
            avg_score, self.dataset.chan_map, tone_labels, ttl_str,
            desaturate=desat, figsize=(4.67, 4.0), cmap=cm
            )

    # Custom tweak to the EP plotting, which plots groups of lines per tone
    @on_trait_change('selected_site')
    def _change_ep_plot(self):
        lines = self.ep_plot.static_artists[:]
        self.ep_plot.remove_static_artist(lines)
        # this is effectively removing any pk_scatter, so mark it as gone
        self._pk_scatter = None

        if self.selected_site < 0:
            # go back to grand averages
            plt_avg = np.nanmean(self.ep_average(reshape=False), axis=1).T
            colors = self._colors['allchan']
        else:
            plt_avg = self.ep_average(reshape=True)[self.selected_site]
            plt_avg = plt_avg.transpose()
            colors = self._colors['tones']

        tx = np.arange(len(plt_avg)) / self.dataset.Fs + self.epoch_start
        new_lines = self.ep_plot.create_fn_image(
            plt_avg, t=tx, color=colors, lw=0.5
            )
        
        self.ep_plot.add_static_artist(new_lines)
        mn = np.nanmin(plt_avg)
        mx = np.nanmax(plt_avg)
        #ymin = 10 * (np.abs(mn)//10 + 1) * np.sign(mn)
        #ymax = 10 * (np.abs(mx)//10 + 1) * np.sign(mx)
        ymin, ymax = mn, mx
        self.ep_plot.ylim = (ymin, ymax)
        self.ep_plot.xlim = self.epoch_start, self.epoch_end
        self._toggle_peaks()
        self.ep_plot.draw()

    @on_trait_change('desaturate')
    def _change_map_saturation(self):
        self._replot_map_fired()

    def _replot_map_fired(self):
        self.change_map(tone_score=())

    def change_map(self, tone_score=(), **extra):
        """
        Remap the array based on a given tone_score, or the
        current tone_score. If multiple axes of scores are given,
        then average down to the "principle" condition (i.e. the
        primary condition in the enumeration table)
        """
        if self.desaturate:
            p = self._shuffled_tonotopic_confidence()
            if self._continuous_p_mapping:
                desat = 1 - p
            else:
                desat = np.ones_like(p)
                desat[ (p > 0.05) & (p < 0.10) ] = .75
                desat[ (p > 0.10) & (p < 0.15) ] = 0.5
                desat[ (p > 0.15) & (p < 0.5) ] = 0.25
                desat[ p > 0.5 ] = 0
            extra['desaturate'] = desat
        else:
            # have to actually reset this b/c of weird kwarg caching
            extra['desaturate'] = ()

        super(TonotopyAnalysis, self).change_map(score=tone_score, **extra)

    def rank_sum_z(self, n_boot=100, pcrit=1e-4, reshape=True):
        key = (self.scoring, self.peak_min, 
               self.peak_max, n_boot,
               self.false_epoch_offset)
        def score_fn(evoked):
            # what's different here is that we'll compute z-scores only
            # within each amplitude, since different runs tend to 
            # have different background levels
            order, groups = ordered_epochs(self.exp, (), group_sizes=True)
            #evoked = equalize_groups(evoked, groups, axis=1, reshape=False)
            evoked = self.score_fn(evoked)
            evoked = equalize_groups(evoked, groups, axis=1)
            n_tones, n_amps = self.exp.rolled_conditions_shape()
            evoked.shape = (len(evoked), n_tones, n_amps, evoked.shape[-1])

            # need to get these in correct order, at least for
            # mahalanobis distance
            baseline = self.epochs(baseline=True, arranged=True)[0]
            baseline = self.score_fn(baseline)
            # now put these back in experiment order for
            # the masking iteration
            
            split_survived = list()
            split_zscores = list()
            for n, m in enumerate(self.exp.iterate_for('amps')):
                survived, zscores = active_sites(
                    baseline[:,m[order]].copy(), evoked[:,:,n,:].copy(), 
                    pcrit=pcrit, n_boot=n_boot
                    )
                split_survived.append(survived)
                split_zscores.append(zscores)

            shp = evoked.shape[:-1]
            zscores = np.array(split_zscores).transpose(1,2,0)
            return zscores.reshape( len(zscores), -1 )
        return self._get_cached_score(
            score_fn, key, self._rank_sum_z_cache, 
            reshape=reshape, single_trials=True
            )

    def best_frequency(self, center_of_mass=True, single_trials=True):
        "Find the stimulated tone frequency with the highest response score."
        scores = self.ep_score(single_trials=single_trials)
        while scores.ndim > 2:
            scores = np.nanmean(scores, axis=-1)
        if center_of_mass:
            bf, _ = nroots_average(scores)
        else:
            bf = np.argmax(scores, axis=-1)
        return bf

    def default_traits_view(self):
        v = super(TonotopyAnalysis, self).default_traits_view()
        sub_map = v.content.content[0].content[-3]
        sub_map.content.insert(5, Item('desaturate', label='Desat Map'))
        return v

"""
Based on the session selection, offer a list of runs to analyze
"""

# XXX -- these Handler types could be implemented differently on a
#        per-headstage level (rather than as it is done here).
sessions = sorted(available_sessions())

class FroemkeSessionHandler(Handler):
    runs = List(Str)
    available_runs = List(Str)
    session = Instance(Bunch)
    tone_tab = Str
    
    def _session_scan(self):
        if not 'tone_tab' in self.session:
            self.session.tone_tab = 'none'

    def object_session_changed(self, info):
        # want to set the '.values' attribute of the info.object thing
        info.object.proc_runs = list()
        session = info.object.session
        si = session_info(session)
        sconf = session_conf(session)
        self.session = sconf.session
        self._session_scan()
        #self.tone_tab = self.session.tone_tab
        info.object.tones = self.session.tone_tab
        session_items = [s for s in sorted(sconf.sections) 
                         if s not in ('session', 'tonotopy')]

        run_info = list()
        loc_path = si.get('exp_path', '')
        nwk_path = si.get('nwk_path', '')
        for run in session_items:
            amp_tab = sconf[run].get('amp_tab', '<no stim>')
            try:
                # only convert to check "type"
                amp_tab = int(amp_tab)
                amp_tab = '{0} dB SPL'.format(amp_tab)
            except ValueError:
                pass
                
            if osp.exists(osp.join(loc_path, run+'.h5')):
                pth = ''
                ftype = 'h5'
            elif osp.exists(osp.join(loc_path, run+'.tdms')):
                pth = ''
                ftype = 'tdms'
            elif osp.exists(osp.join(nwk_path, run+'.h5')):
                pth = 'nwk'
                ftype = 'h5'
            elif osp.exists(osp.join(nwk_path, run+'.tdms')):
                pth = 'nwk'
                ftype = 'tdms'
            else:
                pth = 'offline'
                ftype = ''

            run_str = ', '.join( filter(None, (run, amp_tab, ftype, pth)) )
            run_info.append(run_str)
            
            
        
        #self.available_runs = session_items
        self.available_runs = run_info
        
        #info.object.proc_runs = self.runs

    def object_proc_runs_changed(self, info):
        self.available_runs = sorted(self.available_runs)


class TonotopyAnalysisSetup(AnalysisSetup):
    session = Enum(sessions[0], sessions)

    session_txt = Code
    show_session_txt = Bool
    
    lowpass_env = Bool(False)
    
    tones = Str
    desat = Bool(True)

    ana_class = TonotopyAnalysis

    def _get_dataset_params(self):
        p = super(TonotopyAnalysisSetup, self)._get_dataset_params()
        args = p[:2]
        kwargs, post_proc = p[2:]
        post_proc['lowpass_env'] = self.lowpass_env
        return args + (kwargs, post_proc)
    
    def ana_opts(self):
        return dict(desaturate=self.desat)

    @on_trait_change('show_session_txt, session')
    def _load_session_text(self):
        if self.show_session_txt:
            try:
                p = find_conf(self.session)
                self.session_txt = open(p).read()
            except:
                self.session_txt = ''
                self.show_session_txt = False
        else:
            self.session_txt = ''

    def default_traits_view(self):
        view = View(
            HGroup(
                Item(name='session'),
                Item(name='tones', style='readonly', label='Tones table' ),
                Item(name='show_session_txt', label='Show session config')
                    ),
            Item('_'),
            Item( name='proc_runs', show_label=False,
                  editor=SetEditor(
                     name='handler.available_runs',
                     left_column_title='session runs',
                     right_column_title='to process'
                     ) ),
            Item('_'),
            HGroup(
                VGroup(
                    Item('proc_bandpass', style='simple', label='Bandpass Edges'),
                    Item(
                        'proc_notches', style='simple', label='Notch freqs',
                        help='comma-separated list of notches'
                        )
                    ),
                spring,
                VGroup(
                    Item('units', width=3),
                    HGroup(
                        VGroup(
                            Item('save', label='Saving'),
                            Item('lowpass_env', label='LP Envelope'),
                            Item('integrate_amps', label='Integrate')
                            ),
                        VGroup(
                            Item('auto_prune', label='Auto Reject Chans'),
                            Item('manu_prune', label='Manually Prune Chans'),
                            Item('car', label='Com Avg Reref'),
                            Item('normalize', label='Normalize')
                            )
                        )
                    ),
                spring,
                VGroup(
                    Item('desat', label='Desat Tono Plot?'),
                    Item(name='proc', label='Tonotopy'),
                    Item('_'),
                    HGroup(
                        Item('scroll_len', width=3, label='Page sz'),
                        Item('scroll')
                        )
                    )
                ),
            Group(
                Item(
                    'session_txt', style='readonly', label='Config',
                    enabled_when='show_session_txt'
                    ),
                visible_when='show_session_txt'
                ),
            title='Analysis Helper',
            handler=FroemkeSessionHandler,
            resizable=True
            )
        return view
        

    
if __name__=='__main__':
    from ecogana.devices.data_util import load_experiment_auto
    d = load_experiment_auto('viventi/2019-04-30', '2019-04-30_18-16-55_031', bandpass=(2, 100))
    # d = load_experiment_auto('viventi/2018-02-12_acute', '2018-02-12_13-10-34_006')
    ana = TonotopyAnalysis(d)
    ana.configure_traits()

    # ast = TonotopyAnalysisSetup()
    # ast.edit_traits()
    # ast.configure_traits()

