import numpy as np
from matplotlib.colors import LinearSegmentedColormap, BoundaryNorm
import matplotlib.cm as cm
import scipy.stats as stats
import itertools
import seaborn_lite as sns
sns.reset_orig()

def stars(p):
    if p > 0.05:
        return 'ns'
    if p > 0.01:
        return '*'
    if p > 0.001:
        return '**'
    return '***'

"""
The rayleigh test is adapted from the following.

utils_directional_stats.py

Created by Loic Matthey on 2013-09-08.
Copyright (c) 2013 Gatsby Unit. All rights reserved.
"""

def rayleigh_test(r, n):
    """
    Rayleigh test of uniformity. 

    Parameters
    ----------

    r : length of **average** vector from samples around a plane

    n : number of samples

    Returns
    -------

    z, p : Rayleigh statistic and p-value
    
    """
    z = r**2 * n
    p = np.exp(np.sqrt(1. + 4*n + 4*(n**2. - (r*n)**2)) - 1. - 2.*n)
    return z, p


def ranksums(x, y, x_sum=False):
    """
    This is a copy of scipy's scipy.stats.ranksums fn. It exposes the
    actual sum of rank's of sample x.
    """
    x,y = map(np.asarray, (x, y))
    n1 = len(x)
    n2 = len(y)
    alldata = np.concatenate((x,y))
    # all the nans get ranked as 0, which does not contribute to the sums
    ranked = stats.mstats.rankdata( np.ma.masked_invalid(alldata) )
    x = ranked[:n1]
    y = ranked[n1:]
    s = np.sum(x,axis=0)
    expected = n1*(n1+n2+1) / 2.0
    z = (s - expected) / np.sqrt(n1*n2*(n1+n2+1)/12.0)
    prob = 2 * stats.distributions.norm.sf(abs(z))
    if x_sum:
        return s, prob
    return z, prob

def ranksum_matrix(
        x, plot=True, levels=[0.05], cmap='gray', ax=None, **plot_kws
        ):
    n_samples = len(x)
    p_matrix = np.zeros( (n_samples, n_samples) )
    for i,j in itertools.combinations(range(n_samples), 2):
        z, p = stats.ranksums(x[i], x[j])
        p_matrix[i,j] = p
        p_matrix[j,i] = p

    cmap = cm.cmap_d.get(cmap, cmap)
        
    if plot:
        from seaborn import heatmap
        if not ax:
            import matplotlib.pyplot as pp
            f = pp.figure()
            ax = f.add_subplot()
        else:
            f = ax.figure
        levels = np.r_[0, levels, 1]
        clrs = sns.blend_palette( 
            [cmap(0), cmap(255)], n_colors=len(levels)
            )
        cmap = LinearSegmentedColormap.from_list(
            'rankcolors', zip(levels, clrs), N=len(levels)
            )
        norm = BoundaryNorm(
            levels, cmap.N
            )
        ## norm = pp.Normalize(0, max(levels))
        #ax = sns.symmatplot(p_matrix, norm=norm, cmap=cmap, ax=ax, **plot_kws)
        ax = heatmap(p_matrix, norm=norm, cmap=cmap, ax=ax, **plot_kws)
        #ax.images[0].set_clim(0, 1)
        ax.set_title('Ranksums matrix')
        return p_matrix, f
    else:
        return p_matrix

def test_matrix(
        x, s_fun, y=(), plot=True, levels=[0.05], 
        cmap='gray', ax=None, s_kws=dict(), **plot_kws
        ):
    # plot_kws include: names, ...
    n_samples_x = len(x)
    n_samples_y = max(len(y), len(x))
    p_matrix = np.zeros( (n_samples_x, n_samples_y) )
    if len(y):
        ij_iter = itertools.product(range(len(x)), range(len(y)))
    else:
        ij_iter = itertools.combinations(range(len(x)), 2)
    for i,j in ij_iter:
        if len(y):
            args = x[i], y[j]
        else:
            args = x[i], x[j]
        z, p = s_fun(*args, **s_kws)
        #z, p = stats.ranksums(x[i], x[j])
        p_matrix[i,j] = p
        if not len(y):
            p_matrix[j,i] = p

    if plot:
        if not ax:
            import matplotlib.pyplot as pp
            f = pp.figure()
            ax = f.add_subplot(111)
        else:
            f = ax.figure
        levels = np.r_[0, levels, 1]
        clrs = sns.blend_palette( 
            [cmap(0), cmap(255)], n_colors=len(levels)
            )
        cmap = LinearSegmentedColormap.from_list(
            'rankcolors', zip(levels, clrs), N=len(levels)
            )
        norm = BoundaryNorm(
            levels, cmap.N
            )
        ## norm = pp.Normalize(0, max(levels))
        p_title = plot_kws.pop('p_title', 'Test matrix')
        if len(y):
            ax.imshow(p_matrix, norm=norm, cmap=cmap)
            if 'names' in plot_kws:
                ax.set_xticklabels(plot_kws['names'], rotation=90)
                ax.set_yticklabels(plot_kws['names'])
        else:
            from seaborn import heatmap
            ## ax = sns.symmatplot(
            ##     p_matrix, norm=norm, cmap=cmap, ax=ax, **plot_kws
            ##     )
            ax = heatmap(p_matrix, norm=norm, cmap=cmap, ax=ax, **plot_kws)
        ax.images[0].set_clim(0, 1)
        ax.set_title(p_title)
        return p_matrix, f
    else:
        return p_matrix

def control_false_discovery(p, alpha, mask=False):
    """
    Find a p-value that sets the expected value of false discovery rate (FDR)
    at alpha. The false discovery rate is the proportion of "discoveries" 
    (rejections of the null hypothesis) that occur in error. In this sense,
    false discovery rate is distinct from false alarm rate, which is the
    marginal frequency of false rejections of the null hypothesis.

    Parameters
    ----------

    p : sequence of p-values from a hypothesis test
    alpha : level of FDR (e.g. 0.1)
    mask : bool, by default return the critical p-value. If mask is True, then
    return a len(p) mask of rejected/accepted nulls

    """
    
    m = len(p)
    ps = np.sort(p)
    h1 = ps <= np.linspace(0, alpha, m, endpoint=False)
    if not h1.any():
        # No discoveries
        return h1 if mask else 0
    pc = ps[ h1 ].max()
    if mask:
        return p <= pc
    return pc

## def plot_ranksums(
##         p_matrix, levels=[0.05], cmap='Blues_r', labels=(), ax=None
##         ):

##     if ax:
##         f = ax.figure()
##     else:
##         f = pp.figure()
##         ax = f.add_subplot(111)

##     levels = np.r_[0, levels, 1]
##     if isinstance(cmap, str):
##         cmap = pp.cm.cmap_d[cmap]
##     cmap.set_bad('white')
##     #p_matrix[np.tril_indices(len(p_matrix), k=-1)] = 1
##     plt = ax.imshow(
##         p_matrix, norm=pp.cm.colors.BoundaryNorm(levels, cmap.N),
##         cmap=cmap
##         )
##     ax.set_xticks(np.arange(len(p_matrix)))
##     ax.set_yticks(np.arange(len(p_matrix)))
##     ax.xaxis.tick_top()
##     if len(labels):
##         ax.set_xticklabels(labels)
##         ax.set_yticklabels(labels)
##     cbar = pp.colorbar(plt)
##     cbar.set_ticklabels( ['<{0:.3f}'.format(p) for p in levels] )
##     ax.set_ylim(ax.get_ylim()[::-1])
##     sns.despine(bottom=True, top=False, right=True)
##     return f
        
def hotelling_t2(X, Y, pvar=1, norm_scale=False):
    # X and Y are (n, t1) and (m, t2) samples of multivariate RVs.
    # if pvar == 1, then t1 == t2
    # if pvar < 1, then project X and Y to common dimensionality t that
    # explains a minimum of (pvar)% of the variance in both datasets

    nx, tx = X.shape
    ny, ty = Y.shape

    if pvar == 1 and tx != ty:
        raise ValueError('dimensionality must be matched for full RVs')

    xb = X.mean(0)
    yb = Y.mean(0)
    
    if pvar < 1:
        Ux, Sx, Vtx = np.linalg.svd(X - xb, 0)
        Uy, Sy, Vty = np.linalg.svd(Y - yb, 0)

        var_x = np.cumsum(Sx**2) / np.sum(Sx**2)
        var_y = np.cumsum(Sy**2) / np.sum(Sy**2)

        t = max( var_x.searchsorted(pvar), var_y.searchsorted(pvar) )

        if norm_scale:
            X = Ux[:, :t] + Vtx[:t].dot(xb) / Sx[:t]
            Y = Uy[:, :t] + Vty[:t].dot(yb) / Sy[:t]
        else:
            X = Ux[:, :t] * Sx[:t] + Vtx[:t].dot(xb)
            Y = Uy[:, :t] * Sy[:t] + Vty[:t].dot(yb)
    t = X.shape[1]
    xb = X.mean(0)
    yb = Y.mean(0)
    W = (X - xb).T.dot(X - xb) + (Y - yb).T.dot(Y - yb)
    W /= (nx + ny - 2)
    t2 = (nx*ny)/(nx + ny) * (xb-yb).dot( np.linalg.solve(W, xb-yb) )
    fd = stats.distributions.f(t, nx+ny-1-t)
    
    return t2, fd.sf((nx+ny-t-1.0)/(nx+ny-2)/t*t2)
    
    
def jkm_test(X, Y, pvar=.98, norm_scale=False, n_perm=1000):
    # X and Y are (n, t1) and (m, t2) samples of multivariate RVs.
    # if pvar == 1, then t1 == t2
    # if pvar < 1, then project X and Y to common dimensionality t that
    # explains a minimum of (pvar)% of the variance in both datasets

    """
    Multivariate multidistance tests for high-dimensional low sample size 
    case-control studies
    DOI: 10.1002/sim.6418

    Multivariate tests based on interpoint distances with application 
    to magnetic resonance imaging
    doi: 10.1177/0962280214529104 
    
    Marco Marozzi
    """
    
    nx, tx = X.shape
    ny, ty = Y.shape

    if pvar == 1 and tx != ty:
        raise ValueError('dimensionality must be matched for full RVs')

    xb = X.mean(0)
    yb = Y.mean(0)
    
    if pvar < 1:
        Ux, Sx, Vtx = np.linalg.svd(X - xb, 0)
        Uy, Sy, Vty = np.linalg.svd(Y - yb, 0)

        var_x = np.cumsum(Sx**2) / np.sum(Sx**2)
        var_y = np.cumsum(Sy**2) / np.sum(Sy**2)

        t = max( var_x.searchsorted(pvar), var_y.searchsorted(pvar) )

        if norm_scale:
            X = Ux[:, :t] + Vtx[:t].dot(xb) / Sx[:t]
            Y = Uy[:, :t] + Vty[:t].dot(yb) / Sy[:t]
        else:
            X = Ux[:, :t] * Sx[:t] + Vtx[:t].dot(xb)
            Y = Uy[:, :t] * Sy[:t] + Vty[:t].dot(yb)

    t = X.shape[1]
    xi = np.random.randint(0, nx)
    #xi = X[xi]
    #X = np.row_stack( (X[:i], X[i+1:]) )
    Z = np.row_stack( (X, Y) )
    #print Z[xi].shape
    Zd = np.sum((Z-Z[xi])**2, axis=1) ** 0.5
    #print Zd.shape
    Zr = stats.rankdata(Zd)
    r1 = Zr[-ny:].sum()
    #rs, p1 = ranksums(Zd[:nx-1], Zd[nx:])
    #return rs, p1, stats.rankdata(Zd)
    pb = 0.0
    ix = np.arange(nx+ny)
    for i in xrange(n_perm):
        Z = Z[np.random.permutation(ix)]
        Zd = np.sum((Z-Z[xi])**2, axis=1) ** 0.5
        #_, p2 = ranksums(Zd[:nx-1], Zd[nx:])
        r2 = stats.rankdata(Zd)[-ny:].sum()
        if r2 > r1:
            pb += 1.0
    return Zr, pb/n_perm
        
