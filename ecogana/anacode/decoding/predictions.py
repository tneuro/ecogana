"""
Manipulations of decoder prediction vectors & confusion matrices.
"""

import numpy as np
import matplotlib as mpl
import scipy.stats.distributions as dists
from ecogana.anacode.stat_tests import ranksums
from sklearn import metrics
from ..seaborn_lite import blend_palette

from ecoglib.numutil import density_normalize

__all__ = ['cmat_to_densities', 'beta_ci', 'confusion_mat_accuracy',
           'confusion_matrix_error', 'confusion_matrix_mi',
           'kl_div', 'decoding_distance', 'confusion_to_prediction',
           'resampled_confusion', 'confusion_matrix_plot',
           'auc_matrix', 'auc_matrix_plot']

def cmat_to_densities(cmat):
    """Convert a confusion matrix to joint and marginal densities.

    Parameters
    ----------
    cmat
        A matrix H_i,j of counts of prediction (j) given input (i)
    
    Returns
    -------
    p_x : ndarray
        Marginal PMF of X (over classes)
    p_y : ndarray
        Marginal PMF of Y (over predictions)
    p_xy : ndarray
        Joint PMF
    
    """

    cmat = cmat.astype('d')
    x = cmat.sum(1)
    p_x = density_normalize(x)
    # normalize the rows of cmat into conditional PMFs
    p_y_c_x = density_normalize(cmat, raxis=0)
    # covert to joint PMF
    p_xy = p_y_c_x * p_x[:,None]
    # marginal PMF of Y
    p_y = p_xy.sum(0)
    return p_x, p_y, p_xy

def beta_ci(success_rate, attempts, alpha=95.):
    """Estimate Beta confidence intervals for prediction accuracy.

    Use the conjugate-prior Beta-Binomial model for S decoder sucesses
    in T trials. With the uniform prior of Beta(1,1) on accuracy pi,
    the posterior estimate of accuracy is distributed Beta(1+S, 1+T-S).

    Parameters
    ----------
    success_rate
        Scalar or vector of accuracy point estimates.
    attempts
        Scalar or vector of trials.

    Returns
    -------
    ci
        Beta confidence interval (at alpha level) for each input.
    
    """
    success_rate = np.atleast_1d(success_rate)
    attempts = np.atleast_1d(attempts)
    aa = 1 + success_rate * attempts
    bb = 1 + (1 - success_rate) * attempts
    ci = list()
    for a, b in zip(aa, bb):
        ci_lo = dists.beta.ppf( (50 - alpha/2)/100., a, b )
        ci_hi = dists.beta.ppf( (50 + alpha/2)/100., a, b )
        ci.append( (ci_lo, ci_hi) )
    return np.array(ci).squeeze()

def confusion_mat_accuracy(cmats):
    """Computes accuracy for a number of confusion matrices."""
    acc = np.zeros( len(cmats) )
    for n, cm in enumerate(cmats):
        acc[n] = cm.trace() / float( cm.sum() )
    return acc

def confusion_matrix_error(cmat):
    """Computes error distribution for a confusion matrix.

    Create a count of misclassifications made at steps of 0, 1, ..., K-1
    for K classes.
    """
    m = cmat.shape[0]
    mass = np.zeros(2*m-1, 'd')
    def _center(r, i):
        c_row = np.zeros_like(mass)
        c_row[ m - 1 - i : 2*m - i - 1 ] = r
        return c_row
    for i, row in enumerate(cmat):
        mass += _center(row, i)
    
    mass[m:] += mass[:m-1][::-1]
    return mass[m-1:]

def confusion_matrix_mi(cmat):
    """Compute the mutual information of a confusion matrix.

    A confusion matrix is proportional to a conditional probability
    table of response given input. Compute the mutual information
    between response and input given this estimate of the joint
    probability distribution.

    MI(X,Y) = sum_{x,y} p(x,y) log2( p(x,y) / p(x) / p(y) )

    Returns
    -------
    MI : float
        The mutual information in bits

    """

    p_x, p_y, p_xy = cmat_to_densities(cmat)
    ln_p = np.ma.masked_invalid( np.log2( p_xy / p_x[:,None] / p_y ) )
    return np.sum( p_xy * ln_p )
    
def kl_div(cmat):
    """Compute the Kullback-Leibler divergence of response from input.

    For input S,
    D(S | R) = sum_i p_s(i) log2( p_s(i)/p_r(i) )

    """
    # 
    stim_count = cmat.sum(1)
    ps = stim_count / cmat.sum()
    # calculate p(r) as sum_s p(r, s)
    # which is found by p(r | s) * p(s) (p(r | s) is the confus. matrix)
    p_rs = cmat / stim_count[:,None] * ps[:,None]
    pr = p_rs.sum(0)

    p_s, p_r, p_rs = cmat_to_densities(cmat)
    
    ratio = ps / pr
    ratio[ ~np.isfinite(ratio) ] = 1
    return np.sum( ps * np.log2(ratio) )

def decoding_distance(cmat, moment=1, err=None):
    """Calculate the average magnitude of prediction error.
    
    Assuming that the "distance" from class i to class j is
    proportional to |i - j|, then compute decoding error in 
    terms of the average decode distance (lower is better).
    
    This method calculates the n'th central moment.

    Parameters
    ----------
    cmat
        Confusion matrix

    Returns
    -------
    error (unitless)
    
    """
    cmat = cmat.astype('d')

    if err is None:
        # use the binary error e(r_j ; S=s_i) = | i - j |
        c_idx = np.arange(len(cmat))
        err = np.abs( c_idx[:,None] - c_idx )
        
    err1 = (cmat * err).sum() / cmat.sum()
    if moment < 2:
        return err1
    #err = (err - err1) ** moment
    #errn = (cmat * err).sum() / cmat.sum()
    errn = (cmat * err**moment).sum() / cmat.sum()
    return errn - err1**2

def confusion_to_prediction(cmat):
    """Convert confusion matrix to predictions & classes.

    Returns
    -------
    predictions
    classes
    
    """
    pred = []
    classes = []
    # read the rows of the conf matrix:
    # Each entry in order gives the # of times that class j
    # was chosen, while the total row count gives the # of times
    # that class i was presented
    for ci, row in enumerate(cmat):
        classes.extend( [ci] * int(row.sum()) )
        for cj, n in enumerate(row.astype('i')):
            pred.extend( [cj] * n )
    predictions = np.array(pred)
    classes = np.array(classes)
    return predictions, classes

def resampled_confusion(predictions, classes, n_samps=1000, normalized=False):
    """Bootstrap resampling of a confusion matrix.

    Bootstrap replicates of the confusion matrix are computed based on
    the preciction vector and the true class vector. Resampling is
    performed within samples corresponding to each class. If a
    confusion matrix is given, equivalent prediction/class vectors are
    created with the correct number of prediction for class j given 
    class i.

    Parameters
    ----------
    predictions : (n_trial,)
        The decoder output. Alternately, the confusion matrix.
    classes : (n_trial,)
        The class IDs of the samples (or None if a confusion matrix 
        is given)
    
    """

    if predictions.ndim == 2 and classes is None:
        predictions, classes = confusion_to_prediction(predictions)

    u_class = np.unique(classes)

    o_pred = [predictions[classes==c] for c in u_class]
    o_clas = [classes[classes==c] for c in u_class]

    # Build a resamping matrix. For each bootstrap sample, index
    # with replacement into n_samp(c) samples for c in classes
    rs = [ [np.random.randint(0, len(c), len(c)) for c in o_clas]
           for i in xrange(n_samps) ]

    cmats = list()
    for samps in rs:
        cls = np.concatenate( [ c[s] for (c, s) in zip(o_clas, samps) ] )
        pred = np.concatenate( [ p[s] for (p, s) in zip(o_pred, samps) ] )
        cmats.append( metrics.confusion_matrix(cls, pred).astype('d') )

    if normalized:
        print 'Warning: normalizing confusion matrices'
        for cm in cmats:
            cm /= cm.sum(1)[:,None]
    return cmats

def confusion_matrix_plot(cmat, title, class_labels, dummy_cond=-1, ax=None):
    """Plots a confusion matrix"""
    if ax is None:
        import matplotlib.pyplot as pp
        f = pp.figure(figsize=(6,6));
        ax = pp.subplot(111)
    else:
        f = ax.figure
    if cmat.ndim > 2:
        cmat = cmat.mean(0)
    if np.trace(cmat) > len(cmat):
        cmat = cmat / cmat.sum(1)[:,None]
    im = ax.imshow(
        cmat, clim=(0,1), cmap='Blues', origin='lower'
        )
    #ax = pp.gca()
    xticks = ax.get_xticks().clip(0, cmat.shape[1]-1).astype('i')
    if len(xticks) < len(class_labels) and dummy_cond >= 0:
        xticks = np.r_[xticks, len(class_labels)-1]
        ax.set_xticks(xticks)
    ax.set_xticklabels([class_labels[i] for i in xticks], rotation=45)
    yticks = ax.get_yticks().clip(0, cmat.shape[0]-1).astype('i')
    if len(yticks) < len(class_labels) and dummy_cond >= 0:
        yticks = np.r_[xticks, len(class_labels)-1]
        ax.set_yticks(yticks)
    ax.set_yticklabels([class_labels[i] for i in xticks])
    cbar = f.colorbar(im, use_gridspec=True, ax=ax)
    cbar.set_label('fraction labeled')
    ax.set_xlabel('predicted label')
    ax.set_ylabel('true label')
    accuracy = '(mean accuracy: {0:.2f}%)'.format( 
        cmat.diagonal().mean() * 100 
        )
    title = (title + '\n' + accuracy) if title else accuracy
    ax.set_title(title)#, fontsize=16)
    ## f.tight_layout()
    ## f.canvas.draw()
    ## bb1 = ax.get_position()
    ## bb2 = cbar.ax.get_position()
    ## bb2.y0 = bb1.y0
    ## bb2.y1 = bb1.y1
    ## cbar.ax.set_position(bb2)
    return f

def auc_matrix_plot(auc_mat, title, class_labels, ax=None):
    """Plots a multi-class area-under-the-curve matrix."""
    n_class = len(auc_mat)
    mean_auc = (auc_mat + auc_mat.T).sum() / 2.0 / n_class / (n_class-1)
    auc_mat.flat[::n_class+1] = np.nan
    if ax is None:
        import matplotlib.pyplot as pp
        f = pp.figure(figsize=(6,6));
        ax = pp.subplot(111)
    else:
        f = ax.figure
    lightred = '#FF3D3D'
    gmap = blend_palette(['black', 'white'], n_colors=256, as_cmap=True)
    gmap.set_bad(mpl.colors.hex2color(lightred) + (0.5,))
    im = ax.imshow(
        auc_mat, clim=(0,1), cmap=gmap, origin='lower'
        )
    #ax = pp.gca()
    xticks = ax.get_xticks().clip(0, auc_mat.shape[1]-1).astype('i')
    ax.set_xticklabels([class_labels[i] for i in xticks], rotation=45)
    yticks = ax.get_yticks().clip(0, auc_mat.shape[0]-1).astype('i')
    ax.set_yticklabels([class_labels[i] for i in yticks])
    cbar = f.colorbar(im, use_gridspec=True, ax=ax)
    cbar.set_label('area-under-curve (AUC)')
    accuracy = '(multiclass AUC: {0:.2f})'.format( mean_auc )
    title = (title + '\n' + accuracy) if title else accuracy
    ax.set_title(title) #, fontsize=16)
    ## f.tight_layout()
    ## f.canvas.draw()
    ## bb1 = ax.get_position()
    ## bb2 = cbar.ax.get_position()
    ## bb2.y0 = bb1.y0
    ## bb2.y1 = bb1.y1
    ## cbar.ax.set_position(bb2)

    return f

def auc_matrix(class_scores):
    """Multiclass area-under-the-curve matrix.

    Class_scores is shaped (K, K, M).
    The first axis enumerates the true class labels.
    The second axis enumerates the classifier scoring for each label.
    The last axis holds the scores for each group of class-i points
    estimated under class-j.

    The auc matrix has entires above the diagonal (j > i)
    that reflect the i'th classifier's discriminability between classes i & j
    
    Below the diagonal (j < i) reflects the opposite scenario:
    the j'th classifier's ability to discriminate between classes i & j

    Returns
    -------
    auc_mat : (K, K)
    
    """
    
    K = len(class_scores)
    auc_mat = np.zeros( (K, K) )
    for i in xrange(K):
        for j in xrange(i+1,K):
            # find discriminability of classifier i between i and j

            # classifier i scores of points in class-j
            f_i = class_scores[i,i]
            nf = len(f_i)
            # classifier i scores of points in class-i
            g_i = class_scores[j,i]
            ng = len(g_i)
            rank_sum = float( ranksums( f_i, g_i, x_sum=True )[0] )
            auc_mat[i,j] = (rank_sum - nf*(nf+1)/2.0) / (nf*ng)

            # find discriminability of classifier j between j and i

            # classifier j scores of points in class-j
            f_i = class_scores[j,j]
            nf = len(f_i)
            # classifier j scores of points in class-i
            g_i = class_scores[i,j]
            ng = len(g_i)
            rank_sum = float( ranksums( f_i, g_i, x_sum=True )[0] )
            auc_mat[j,i] = (rank_sum - nf*(nf+1)/2.0) / (nf*ng)
    return auc_mat

