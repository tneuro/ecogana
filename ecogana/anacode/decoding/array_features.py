"""
Create features from array timeseries / recordings
"""

import numpy as np
from itertools import product
from sklearn.base import BaseEstimator, TransformerMixin

from ecoglib.util import equalize_groups
from ecoglib.numutil import nextpow2
from ecoglib.filt.time import common_average_regression

from ecogana.anacode.signal_testing import safe_avg_power, bad_channel_mask
import ecogana.anacode.ep_scoring as ep_scoring

import ecoglib.estimation.multitaper as mspec

__all__ = ['prepare_samples', 'combo_lock_permutation', 
           'create_pseudo_trials', 'MakePseudoTrials']

def prepare_samples(
        ana_obj, conditions,
        mask=(), prune_chans=False, prune_responses=False,
        cmask_kws = dict(), avg_norm = 1, tf_kws = dict(),
        average_reference=False,
        average_regression=False
        ):
    """Assemble a feature matrix from an Analysis object.

    Parameters
    ----------
    ana_obj : Analysis
        Neural responses are queried based on the current peak_min and
        peak_max settings of the Analysis object
    conditions : dict
        A dictionary of experimental conditions to fix
        (i.e. conditions[c] = val) and conditions to let vary
        (i.e. conditions[c] = 'all'). This will determine the response
        vectors that are returned from ana_obj.
    
    Returns
    -------
    epochs : (n_trials, n_features)
        Feature matrix based on Analysis responses.
    classes : (n_trials,) 
        Class ID per trial (enumerated)
    class_labels
        Label of nth class.
    c_mask
        Channel mask used
    
    """
    
    dset = ana_obj.dataset
    chan_map = dset.chan_map[:]
    if len(mask):
        c_mask = np.asarray(mask).astype('?')
    else:
        c_mask = np.ones( len(dset.data), dtype='?' )
    if prune_chans:
        pwr = safe_avg_power(dset.data, int(dset.Fs))
        mask = bad_channel_mask(np.log(pwr), **cmask_kws)

        active_sites = np.where(mask)[0]
        chan_map = chan_map.subset(active_sites)
        c_mask[~mask] = False

    fixed_conditions = [(k, v) for k, v in conditions.items() if v != 'all']
    float_conditions = [k for k, v in conditions.items() if v == 'all']

    any_module = ana_obj.modules[0]
    float_labels = [any_module.labels(k) for k in float_conditions]
    if len(float_labels) > 1:
        class_labels = [ ', '.join(p) for p in product(*float_labels) ]
    else:
        class_labels = float_labels[0][:]

    if not len(fixed_conditions):
        # apparently everything is requested, in which case
        # arranged should be True to return epochs in a
        # condition-ordered sequence
        fixed_conditions = True
    epochs, groups = ana_obj.epochs(
        arranged=fixed_conditions, subresponse=True, mask_outliers=False
        )
    ### Define labels and feature transformation pipelines
    classes = np.concatenate( 
        [np.repeat(i, g) for i, g in enumerate(groups)] 
        )

    ### Prune non-responding sites
    if prune_responses:
        ana_obj.false_epoch_offset = -0.400
        ## ana_obj.scoring = 'mahal'
        ## f_epochs = ana_obj.epochs(baseline=True)[:, :np.sum(groups), :]
        ## ana_obj.peak_min = ana_obj.epoch_start 
        ## ana_obj.peak_max = ana_obj.epoch_end

        ## p_scores = ana_obj.score_fn(r=epochs).reshape(
        ##     len(epochs), len(class_labels), -1
        ##     )
        ## f_scores = ana_obj.score_fn(r=f_epochs)
        ## mask, scores = ep_scoring.active_sites(
        ##     f_scores[c_mask], p_scores[c_mask], n_boot=40, pcrit=1e-4
        ##     )

        ana_obj.scoring = 'rms'
        #z_scores = ep_scoring.avg_resp_zscore(ana_obj)
        #ep_avgs = ana_obj.ep_average(reshape=False)
        baseline = ana_obj.epochs(baseline=True, subresponse=True)
        ep_ = equalize_groups(epochs, groups, axis=1)
        z_scores = ep_scoring.avg_resp_zscore(ep_, baseline, n_samps=100)
        if prune_responses is True:
            thresh = 3
        else:
            thresh = prune_responses
        mask = z_scores.reshape( len(epochs), -1 ).max(-1) > thresh
        
        sub_mask = c_mask[c_mask]
        sub_mask[~mask] = False
        c_mask[c_mask] = sub_mask
        
    epochs = epochs[c_mask]

    # Common average regression / re-referencing
    if average_regression:
        for e in xrange(epochs.shape[1]):
            common_average_regression(epochs[:,e,:])
    if average_reference:
        mu = epochs.mean(0)
        epochs -= mu
    
    if avg_norm > 1:
        epochs = create_pseudo_trials(epochs, groups, avg_norm)
    if len(tf_kws):
        n = tf_kws['n']
        lag = tf_kws['lag']
        pl = (n - lag)/float(n)
        nw = tf_kws['nw']
        nfft = nextpow2(n) if np.log2(n) != int(np.log2(n)) else n
        highpass = tf_kws.get('highpass', 0)
        s = epochs.shape
        t, f, tf = mspec.mtm_spectrogram_basic(
            epochs.reshape(s[0]*s[1], s[2]), n, pl=pl, NW=nw,
            NFFT=nfft, adaptive=False, jackknife=False
            )
        lowpass = tf_kws.get('lowpass', f[-1])
        tf = tf[:, (f > highpass) & (f <= lowpass), :]
        epochs = tf.reshape(s[0], s[1], -1)        
    epochs = epochs.transpose(1, 0, 2).copy()
    epochs.shape = ( len(epochs), -1 )
    return epochs, classes, class_labels, c_mask

# ------------------- MAYBE NEVER USED ------------------- #
def combo_lock_permutation(x, n_col):
    """Create permutations of x using a combination lock strategy.

    Spin the dial "x" n_col - 1 times, but never use the same rotation
    twice. Of course n_col < len(x)

    Returns
    -------
    y : shape ( len(x), n_col )
        n_col unique rotations of x
        
    """
    y = np.zeros( (len(x), n_col), dtype=x.dtype )
    y[:, 0] = x
    spins = set()
    for c in xrange(1, n_col):
        r = np.random.randint(1, len(x))
        while r in spins:
            r = np.random.randint(1, len(x))
        spins.add(r)
        y[:, c] = np.roll(x, r)
    return y

def create_pseudo_trials(epochs, groups, n_choose, mix_weight=0.25):
    # for every response in epochs, choose (n_choose-1) other responses at
    # random with which to average
    k = 0
    p_epochs = list()
    nc = epochs.shape[0]
    nt = epochs.shape[-1]

    w = np.zeros( n_choose )
    w[1:] = mix_weight / (n_choose - 1)
    w[0] = 1 - mix_weight
    for g in groups:
        group_responses = epochs[:, k:k+g]
        samps = combo_lock_permutation( np.arange(g), n_choose )
        group_samps = group_responses[:, samps.ravel()]
        group_samps.shape = (nc, g, n_choose, nt)
        #p_epochs.append( group_samps.mean(-2) )
        # use weighting
        p_epochs.append( ( group_samps * w[None, None, :, None] ).sum(axis=2) )
        k += g
    return np.concatenate(p_epochs, axis=1)

class MakePseudoTrials(BaseEstimator, TransformerMixin):

    def __init__(self, n_choose=2, **kwargs):
        self.n_choose = n_choose
    
    def fit_transform(self, X, y=None, n_choose=-1):
        # replace every sample in X with the mean of n_choose
        # randomly drawn samples with the same label

        if y is None:
            raise ValueError('label map must be provided')
        u_labels = np.unique(y)
        label_map = dict([ (lab, np.where(y==lab)[0]) for lab in u_labels ])
        
        ## # X comes in as n_samples, n_features
        ## # where n_features is n_chan * n_pt
        ## X = X.reshape( len(X), n_chan, -1 ).transpose(1, 0, 2)

        Xt = np.empty_like(X)
        n_choose = self.n_choose if n_choose < 0 else n_choose
        for xxt, yy in zip(Xt, y):
            samps = np.random.choice(label_map[yy], n_choose, replace=True)
            #xxt[:] = X[samps].mean(0)
            xxt[:] = np.median(X[samps], axis=0)
        return Xt

    def transform(self, X):
        return X

# -------------------------------------------------------- #
