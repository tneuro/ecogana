from nose.tools import assert_true, assert_equal
 
from numpy.testing import assert_almost_equal, assert_array_equal
import numpy as np

from ecogana.anacode.decoding import *

def test_confusion_matrix_mi():

    cm = np.diag( np.random.randint(10, 20, size=10).astype('d') )

    px = cm.sum(1)
    px /= px.sum()

    Hx = -( px * np.log2(px) ).sum()

    assert_almost_equal( confusion_matrix_mi(cm.astype('i')), Hx )

    cm = np.eye(8)
    for i in xrange(8):
        cm[i, (i+1)%8] = 1
    # X has 3 bits of entropy (uniform over 8 groups)
    # H(X | Y) has 1 bit of entropy (half and half that X==Y)
    # I(X ; Y) should be 2
        
    assert_almost_equal( confusion_matrix_mi(cm), 2 )

    # do normalized matrix
    assert_almost_equal( confusion_matrix_mi( cm / cm.sum() ), 2 )

def test_cmat_to_densities():

    # test uniform p_x is recovered
    p_y_c_x = [ np.random.permutation(20) for i in xrange(20) ]
    p_y_c_x = np.array(p_y_c_x)
    p_x = np.ones(20) / 20.0
    
    p_x2, p_y, p_xy_ = cmat_to_densities( p_y_c_x )

    assert_almost_equal(p_x, p_x2)

    # test independent p_y is recovered
    p_x = np.random.randint(1, 10, size=15)
    p_y = np.random.randint(1, 10, size=15)

    p_y_c_x = np.array([ x * p_y for x in p_x ])

    p_x2, p_y2, p_xy = cmat_to_densities( p_y_c_x )
    assert_almost_equal(p_x2, (p_x / float(p_x.sum())))
    assert_almost_equal(p_y2, (p_y / float(p_y.sum())))
    

def test_foo():
    from ecogana.anacode.decoding import confusion_to_prediction
    from sklearn.metrics import confusion_matrix
    
    cm = np.diag( np.random.randint(10, 20, size=10).astype('d') )
    cm += np.random.randint(2, 15, size=(10, 10))
    pred, true = confusion_to_prediction(cm)

    cm2 = confusion_matrix(true, pred)

    assert_array_equal(cm, cm2)

