from collections import OrderedDict
from pkg_resources import parse_version

import numpy as np
from scipy.stats.distributions import beta as beta_dist
from scipy.interpolate import UnivariateSpline

from tqdm import tqdm

from sklearn.preprocessing import label_binarize
import sklearn.metrics as metrics
from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA
try:
    from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
except ImportError:
    from sklearn.lda import LDA
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import confusion_matrix

from .predictions import auc_matrix, decoding_distance
     ## (smoothed_cross_validation_curve, prepare_samples, vPCA, roc_curve,
     ##  confusion_matrix_plot, decoding_distance, auc_matrix, auc_matrix_plot,
     ##  plot_roc_bunch, plot_roc)


from md5 import md5

from ecoglib.util import Bunch, equalize_groups

from .post_hoc import combine_roc_curves, plot_roc

__all__ = ['smoothed_cross_validation_curve',
           'roc_curve', 'plot_roc_bunch', 'vPCA', 'Decoder']

def smoothed_cross_validation_curve(
        p, rcv, min_score=-1, ret_all=False, beta=True, convex=True,
        **kwargs
        ):
    """Pick out the optimum point from a cross-validated grid-search.

    Interpolate grid-search results weighted by cross-validation
    variance. Return the arg-maximum/minimum for the curve.

    Parameters
    ----------
    p
        Parametric values evaluated by cross-validated prediction
    rcv 
        len(p) x len(cv) prediction results in [0, 1]

    Returns
    -------
    p_opt
        Interpolated arg-min/max of the grid search.
    
    Note
    ----
    This method estimates parameters of a beta distribution prior
    on the "true" prediction rate that is sampled in the cross-validation.
    A smoothing spline is fit to the first moment ordinates, using
    the beta variance as (inverse) weight.
    
    """

    i = np.argsort(p)
    p = p[i]
    rcv = rcv[i]
    y = list()
    w = list()
    ab = list()
    for b_samps in rcv:
        if beta:
            b_samps[ b_samps==1 ] = 0.999
            b_samps[ b_samps==0 ] = 0.001
            b_samps = b_samps[ (b_samps > 0) & (b_samps < 1) ]
            if len(np.unique(b_samps)) == 1:
                # if all samps are equal, beta fit will fail,
                # so get the mode and a very small variance
                m1 = b_samps[0]
                var = 1e-3
                # fake alpha and beta:
                b = 100
                a = (m1 * (beta - 2) + 1) / (1 - m1)
            else:
                a, b = beta_dist.fit(b_samps, floc=0, fscale=1)[:2]
                m1 = beta_dist.moment(1, a, b)
                m2 = beta_dist.moment(2, a, b)
                var = m2 - m1**2
            y.append(m1)
            w.append( 1 / np.sqrt(var) )
            ab.append((a,b))
        else:
            var = b_samps.var()
            w.append( 1 / np.sqrt(var) )
            y.append( b_samps.mean() )
    y, w = map(np.array, (y, w))
    if min_score < 0:
        # auto-set
        min_score = np.percentile(y, 50)
    # good idea to not try to interpolate into areas with shitty scores
    m = (y > min_score) if convex else (y < min_score)
    y = y[m]
    p = p[m]
    w = w[m]
    k = kwargs.pop('k', m.sum()-1)
    k = min(m.sum()-1, k)
    spl = UnivariateSpline(p, y, w=w, k=k, **kwargs)
    ip = np.linspace(p[0], p[-1], 5000)
    if ret_all:
        return (ip, spl(ip)), (p, y, w), (np.array(ab)[m] if beta else None)
    else:
        return ip[ spl(ip).argmax() ] if convex else ip[ spl(ip).argmin() ]

def roc_curve(
        clf, samples, classes,
        cv_gen=None, fitting=False, combine_cv=True, combine_classes=False,
        plot=True, **pkwargs
        ):
    """Complicated classifier training.

    Appears to train and test a classifier with cross-validation. At
    the same time, one-versus-rest ROC curves are computed based on
    the predictions made at available decision variable thresholds.

    ~combine_cv and ~combine_classes
      - returns dict with n_fold, each with n_class ROCs
      - roc.part_0.cls_0.{}, roc.part_0.cls_1.{}, ..., roc.part_m.cls_n.{}
      - plots n_fold figures

    ~combine_cv and combine_classes
      - returns dict with n_fold average ROCs
      - roc.part_0.{}, roc.part_1.{}, ...
      - plots n_fold average ROCs in one figure

    combine_cv and ~combine_classes
      - returns dict with n_class average ROCs
      - roc.cls_0.{}, ..., roc.cls_n.{}
      - plots n_class ROCs in one figure

    combine_cv and combine_classes
      - return average ROC (but average over all ROCs, and not stagewise)
      - roc.{}
      - plots average ROC

    """

    
    if cv_gen is None:
        cv_gen = [ [ np.arange(len(samples)), np.arange(len(samples)) ] ]
        combine_cv = True
    else:
        # a cross-validation generator implies fitting
        fitting = True

    uclasses = np.unique(classes)
    cls_keys = ['c'+str(i) for i in uclasses.astype('i')]
    if len(uclasses) == 2:
        cls_keys = cls_keys[:1]

    rocs = Bunch()
    c_binary = label_binarize(classes, classes=uclasses)
    c_scored = np.zeros(c_binary.shape, dtype='d')
    for i, (trn, tst) in enumerate(cv_gen):
        if fitting:
            clf.fit(samples[trn], classes[trn])

        try:
            p = clf.decision_function(samples[tst])
        except AttributeError:
            p = clf.predict_log_proba(samples[tst])
        # if binary, then buffer axes
        if p.ndim == 1:
            p = p[:,None]
        c_scored[tst] = p

        if not combine_cv:
            roc_p = Bunch()
            for c, cls in enumerate(cls_keys):
                roc_c = metrics.roc_curve( c_binary[tst][:, c], p[:,c] )
                roc_p[cls] = Bunch(fpr=roc_c[0], tpr=roc_c[1], thresh=roc_c[2])

            # at this point we could combine over classes
            if combine_classes:
                lists = zip( 
                    *[ (c.fpr, c.tpr, c.thresh) for c in roc_p.values()] 
                    )
                avg_roc = combine_roc_curves( *lists )
                roc_p = Bunch(
                    fpr=avg_roc[0], tpr=avg_roc[1], 
                    fpr_err=avg_roc[2], tpr_err=avg_roc[3],
                    thresh=avg_roc[4]
                    )
            rocs['part_{0}'.format(i)] = roc_p

    if combine_cv:
        # compute ROCs on the entire prediction matrix
        rocs = Bunch()
        for c, cls in enumerate(cls_keys):
            roc_c = metrics.roc_curve( c_binary[:, c], c_scored[:, c] )
            rocs[cls] = Bunch(fpr=roc_c[0], tpr=roc_c[1], thresh=roc_c[2])
        if combine_classes:
            # zip up *all* fpr, tpr, and thresh
            all_rocs = zip( 
                *[ (c.fpr, c.tpr, c.thresh) for c in rocs.values()]
                )
            combined = combine_roc_curves( *all_rocs )
            rocs = Bunch(
                fpr = combined[0], tpr = combined[1], 
                fpr_err = combined[2], tpr_err = combined[3], 
                thresh = combined[4]
                )

    rocs['cls_score'] = c_scored.squeeze()
    if plot:
        return rocs, plot_roc_bunch(rocs, **pkwargs)
    return rocs

def plot_roc_bunch(rocs, **pkwargs):
    """Makes plots based on the Bunch returned by roc_curve."""
    
    combine_cv = 'part_0' not in rocs
    if combine_cv:
        combine_classes = 'fpr' in rocs
    else:
        combine_classes = 'fpr' in rocs.part_0
        
    if combine_cv:
        # single figure
        if combine_classes:
            all_fpr, all_tpr = rocs.fpr, rocs.tpr
            pkwargs['fpr_err'] = rocs.fpr_err
            pkwargs['tpr_err'] = rocs.tpr_err
        else:
            # zip all sub-dicts that have the key 'fpr'
            cls_keys = [k for k in rocs.keys() if hasattr(rocs[k], 'fpr')]
            cls_keys = sorted(cls_keys)
            if not len(pkwargs.get('cls_labels', ())):
                pkwargs['cls_labels'] = cls_keys
            all_fpr, all_tpr = zip( *[ ( rocs[c].fpr, rocs[c].tpr) 
                                       for c in cls_keys ] )
        f = plot_roc(all_fpr, all_tpr, **pkwargs)
    else:
        # multiple figures -- per partition, same instructions as above
        f = list()
        ttl = pkwargs.pop('title', '')
        for p in filter(lambda s: s.startswith('part_'), rocs.keys()):
            p_roc = rocs[p]
            if combine_classes:
                all_fpr, all_tpr = p_roc.fpr, p_roc.tpr
                pkwargs['fpr_err'] = p_roc.fpr_err
                pkwargs['tpr_err'] = p_roc.tpr_err
            else:
                cls_keys = [k for k in p_roc.keys() if 'fpr' in p_roc[k]]
                cls_keys = sorted(cls_keys)
                if not len(pkwargs.get('cls_labels', ())):
                    pkwargs['cls_labels'] = cls_keys
                all_fpr, all_tpr = zip( *[ ( p_roc[c].fpr, p_roc[c].tpr) 
                                           for c in cls_keys ] )

            title = ': '.join( ['CV {0}'.format(p), ttl] )
            fig = plot_roc(all_fpr, all_tpr, title=title, **pkwargs)
            f.append(fig)
    return f

class vPCA(PCA):
    """Drop-in replacement of PCA efficient for iteratively adding PCs.
    """
    
    #_isfit = False
    _x_hash = ''

    def __init__(self, n_components=None, copy=True, whiten=False):

        # force svd_solver to be full (but drop it if version < 0.18)
        kwargs = dict(svd_solver='full',
                      n_components=n_components,
                      copy=copy, whiten=whiten)
        import sklearn
        sk_version = sklearn.__version__
        if parse_version(sk_version) < parse_version('0.18.1'):
            kwargs.pop('svd_solver')
        super(vPCA, self).__init__(**kwargs)
    
    def _fit(self, X):
        x_hash = md5(X).hexdigest() if X is not None else self._x_hash
        # if we haven't seen this array, then do new SVD 
        if self._x_hash != x_hash:
            self._svd = super(vPCA, self)._fit(X)
            self._x_hash = x_hash
            return self._svd
        
        # otherwise, just update the components
        (U, S, V) = self._svd
        n_samples = len(S)
        n_features = V.shape[1]
        explained_variance_ = (S ** 2) / n_samples
        explained_variance_ratio_ = (explained_variance_ /
                                     explained_variance_.sum())

        n_components = self.n_components_
        if n_components < n_features:
            self.noise_variance_ = explained_variance_[n_components:].mean()
        else:
            self.noise_variance_ = 0.
        
        self.components_ = V[:n_components]
        self.explained_variance_ = explained_variance_[:n_components]
        self.explained_variance_ratio_ = explained_variance_ratio_[:n_components]
        self.n_components_ = n_components
        return self._svd

    def fit_transform(self, X, y=None):
        """Fit the model with X and apply the dimensionality reduction on X.
        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            Training data, where n_samples is the number of samples
            and n_features is the number of features.
        Returns
        -------
        X_new : array-like, shape (n_samples, n_components)
        """
        U, S, V = self._fit(X)
        # need to copy this instead of getting a view,
        # otherwise the subsequent scaling is *permanent*
        U = U[:, :self.n_components_].copy()

        if self.whiten:
            # X_new = X * V / S * sqrt(n_samples) = U * sqrt(n_samples)
            U *= np.sqrt(X.shape[0])
        else:
            # X_new = X * V = U * S * V^T * V = U * S
            U *= S[:self.n_components_]

        return U

    def reset_comps(self, n_components):
        if not self._x_hash:
            # silently ignore command?
            return
        self.n_components_ = n_components
        self.n_components = n_components
        return self.fit(None)


class Decoder(object):
    "A stand-alone decoding object"

    def __init__(self, classifier='svdlda', n_folds=6):
        """
        Parameters
        ----------
        classifier : str or callable
            The classifier attribute can refer to one of the built-in classifiers.
            Otherwise it can be a callable that returns a Pipeline object set
            with given keyword parameters.

        n_folds : int
            Number of non-overlapping folds to use in cross-validation.

        """

        self.classifier = classifier
        self.n_folds = n_folds

    def _new_svdlda_classifier(self, n_components=None):
        if not n_components:
            n_components=0.999
        return [ ('scale', StandardScaler()),
                 ('compress', vPCA(n_components=n_components)),
                 ('lda_clf', LDA()) ]

    def _new_svdksvc_classifier(self, n_components=None):
        if not n_components:
            n_components=0.999
        return [ ('scale', StandardScaler()),
                 ('compress', vPCA(n_components=n_components)),
                 ('svc_clf', SVC(kernel='rbf', decision_function_shape='ovr')) ]

    def _new_svdlsvc_classifier(self, n_components=None):
        if not n_components:
            n_components=0.999
        return [ ('scale', StandardScaler()),
                 ('compress', vPCA(n_components=n_components)),
                 ('svc_clf', SVC(kernel='linear', decision_function_shape='ovr')) ]

    def _new_svdlogres_classifier(self, n_components=None):
        if not n_components:
            n_components=0.999
        return [ ('scale', StandardScaler()),
                 ('compress', vPCA(n_components=n_components)),
                 ('lr_clf', LogisticRegression(penalty='l1', multi_class='ovr')) ]

    def _new_svdgnb_classifier(self, n_components=None):
        if not n_components:
            n_components=0.999
        return [ ('scale', StandardScaler()),
                 ('compress', vPCA(n_components=n_components)),
                 ('gnb_clf', GaussianNB()) ]

    def _tf_props(self):
        return OrderedDict()

    def new_classifier(self, **opts):
        if isinstance(self.classifier, str):
            cls_steps = eval('self._new_{0}_classifier'.format(self.classifier))
            return Pipeline(steps=cls_steps(**opts))
        else:
            cls_fn = self.classifier(**opts)
            #cls_fn.set_params(**opts)
            return cls_fn

    def train_compression_step(
            self, samples, classes, plot=False, density=10, 
            weight='accuracy', break_early=True, cv_gen=None,
            verbose=False, return_variance=False, **interp_kws
            ):
        " minimizes structural risk? check this terminology "

        if cv_gen is None:
            skf = StratifiedKFold(n_splits=self.n_folds)
            cv_gen = skf.split(samples, classes)
        cv_gen = list(cv_gen)
        first_fold = cv_gen[0]
        max_comps = min(len(first_fold[0]), samples.shape[1])
        num_comps = np.arange(1, max_comps//density)*density
        num_comps = num_comps[ num_comps < max_comps ]
        ## cv_gen = StratifiedShuffleSplit(classes, n_iter=10, test_size=0.2)
        cv_scores = np.zeros((len(num_comps), len(cv_gen)))
        var_pct = np.zeros((len(num_comps), len(cv_gen)))
        best_fn = min if weight.lower() == 'error' else max
        if weight.lower() == 'error':
            comp_fn = lambda x, y: x > y
        else:
            comp_fn = lambda x, y: x < y


        def _status(fold):
            fmt = 'fold {0}: best {1}: {2} ({3} components)'
            if fold < 0 or fold > len(cv_gen)-1:
                return ''
            b_val = best_fn(*cv_scores[:, fold])
            i = np.where(cv_scores[:, fold] == b_val)[0][0]
            return fmt.format(fold, weight, b_val, num_comps[i])

        itr = enumerate(cv_gen)
        if verbose:
            itr = tqdm(itr, total=len(cv_gen), leave=True)
            
        for j, (train, test) in itr:
            best = 0 if weight.lower()=='accuracy' else 100
            for i, nc in enumerate(num_comps):
                if i == 0:
                    # This keyword argument construction should work
                    # for any built-in svd-xyz classifiers
                    best_lda = self.new_classifier(n_components=nc).fit(
                        samples[train], classes[train]
                        )
                else:
                    best_lda.named_steps['compress'].reset_comps(nc)
                    best_lda.fit( samples[train], classes[train] )
                var_pct[i, j] = best_lda.named_steps['compress'].explained_variance_ratio_.sum()

                if weight.lower() == 'accuracy':
                    cv_scores[i, j] = best_lda.score(
                        samples[test], classes[test]
                        )
                else:
                    cm = confusion_matrix(best_lda.predict(samples[test]),
                                          classes[test])
                    cv_scores[i,j] = decoding_distance(cm.astype('d'))
                if not break_early:
                    continue
                best = best_fn(cv_scores[i, j], best)
                #mn = min(cv_scores[i, j], mn)
                t = best / 1.5 if weight.lower() == 'accuracy' else best * 1.5
                if i > 0 and comp_fn(cv_scores[i, j], cv_scores[i-1, j]) \
                  and comp_fn(cv_scores[i-1, j], t):
                    cv_scores[i+1:, j] = cv_scores[i, j]
                    break
            if verbose:
                itr.set_description_str(_status(j))

        interp_kws.setdefault('k', 5)
        #interp_kws.setdefault('s', 0.5) # seems to be a good heuristic?
        beta = convex = weight.lower() == 'accuracy'
        # XXX: plotting should be somewhere else -- inappropriate here
        if plot:
            r = smoothed_cross_validation_curve(
                num_comps, cv_scores, ret_all=True, beta=beta, convex=convex,
                **interp_kws
                )
            sm_ln = r[0]
            ip, spl = sm_ln
            raw_ln = r[1]
            gamma_fits = r[2]
            from ecogana.anacode.plot_util import subplots
            f, (ax1, ax2) = subplots(2, 1, sharex=True)
            ax1.errorbar(
                raw_ln[0], raw_ln[1], yerr=1.0/raw_ln[2], fmt='none', ecolor='k'
                )
            ax1.plot(ip, spl, color='g')
            ax1.set_ylabel(weight)
            if weight.lower() == 'accuracy':
                ax2.plot(raw_ln[0], gamma_fits)
                ax2.legend( ('alpha', 'beta') )
                ax2.set_ylabel('Gamma dist params')
            ax2.set_xlabel('Number of components')
            
            f.tight_layout(pad=0)
            pk = ip[ spl.argmax() ] if convex else ip[ spl.argmin() ]
            return int(round(pk)), f
        components = smoothed_cross_validation_curve(num_comps, cv_scores, beta=beta, convex=convex, **interp_kws)
        if return_variance:
            avg_var = var_pct.mean(1)
            i = num_comps.searchsorted(components)
            c_hi = num_comps[i]
            v_hi = avg_var[i]
            v_lo = avg_var[i - 1]
            w_lo = (c_hi - components) / density
            return v_lo * w_lo + v_hi * (1 - w_lo)
        return int(components)
    
    @staticmethod
    def optimize_svd_modes(
            samples, classes, plot=False, density=10, 
            weight='accuracy', break_early=True, cv_gen=None, 
            classifier='svdlda', n_folds=6,
            verbose=False, return_variance=False, **interp_kws
            ):
        d = Decoder(classifier=classifier, n_folds=n_folds)
        return d.train_compression_step(
            samples, classes, plot=plot, density=density, weight=weight,
            break_early=break_early, cv_gen=cv_gen, verbose=verbose, return_variance=return_variance,
            **interp_kws
            )

    def kfold_decoding(self, samples, classes, cv_gen=None, **c_opts):
        if cv_gen is None:
            skf = StratifiedKFold(n_splits=self.n_folds)
            cv_gen = skf.split(samples, classes)
        clf = self.new_classifier(**c_opts)
        roc = roc_curve(
            clf, samples, classes, cv_gen=cv_gen, plot=False
            )
        roc.cv_gen = list(cv_gen)
        roc.classifier = clf
        roc.classes = classes
        return roc

    @staticmethod
    def decode(
            samples, classes, 
            n_folds=6, classifier='svdlda', cv_gen=None, **c_opts
            ):
        d = Decoder(classifier=classifier, n_folds=n_folds)
        return d.kfold_decoding(
            samples, classes, cv_gen=cv_gen, **c_opts
            )

    @staticmethod
    def unpack_decode(roc_obj):
        classes = roc_obj.classes
        if roc_obj.cls_score.ndim > 1:
            decode = np.argmax(roc_obj.cls_score, axis=1)
        else:
            decode = (roc_obj.cls_score >= 0).astype('i')
        cm = confusion_matrix(classes, decode)
        cm = cm.astype('d')
        error = decoding_distance(cm)
        chance = decoding_distance(np.ones(cm.shape, dtype='d'))
        classes = np.unique(roc_obj.classes)
        if roc_obj.cls_score.ndim > 1:
            groups = [np.sum(roc_obj.classes==c) for c in classes]
            class_scores = equalize_groups(roc_obj.cls_score, groups, axis=0)
            class_scores = class_scores.transpose(0, 2, 1)
            auc = auc_matrix(class_scores)
        else:
            class_scores = roc_obj.cls_score
            auc = None
        results = Bunch(decode=decode,
                        confusion=cm,
                        error=error,
                        chance_error=chance,
                        class_scores=class_scores,
                        auc_matrix=auc,
                        classifier=roc_obj.classifier)
                        
        return results
   
