"""
Tools for post-hoc decoding/decoder analysis.
"""

import numpy as np
import matplotlib as mpl
from matplotlib.colors import LinearSegmentedColormap, BoundaryNorm
from scipy.stats.distributions import chi2
import scipy.stats.distributions as dists
from scipy.special import logit
import pandas as pd
import statsmodels.api as sm
import statsmodels.formula.api as smf
from sklearn.pipeline import Pipeline
from sklearn.metrics import auc

from collections import namedtuple
ANODEResult = namedtuple('ANODEResult', ('LRT', 'pvalue'))

from ..seaborn_lite import color_palette, blend_palette, despine

from ecoglib.util import Bunch
from ..colormaps import composited_color_palette

from .predictions import confusion_to_prediction, decoding_distance

__all__ = ['combine_roc_curves', 'combine_roc_curves2', 'plot_roc',
           'plot_decision_regions', 'canonical_projections',
           'sort_samples', 'plot_sorted_samples',
           'video_sorted_samples', 'compare_decoding_accuracy',
           'compare_decoding_error']

def combine_roc_curves(fpr, tpr, thresh, bins=50):
    """Aggregate mean/stdev of ROC curves.

    Pool false positive/true positive rates over blocks of the
    decision variable. A set of true positives for a block is 
    defined as

    {t_i(c) : c(b) < c < c(b) + delta, i in input} 

    Returns
    -------
    fpr_avg, tpr_avg
        Point estimates of the x and y ROC coordinates
    fpr_err, tpr_err
        Standard dev of the x and y ROC coordinates
    
    """
    
    fpr, tpr, thresh = map(np.concatenate, (fpr, tpr, thresh))
    ix = np.argsort(thresh)[::-1]
    fpr, tpr, thresh = map(lambda x: x[ix], (fpr, tpr, thresh))
    #assert ( np.argsort(thresh)[::-1] == np.argsort(fpr) ).all()

    p_size = len(thresh) // bins
    ## quantiles = np.sort(thresh)[ np.arange(0, len(thresh), bins) ]
    ## quantiles = np.r_[ quantiles, thresh.max() + 1e-8 ][::-1]
    ## print quantiles
    fpr_avg = np.zeros(bins); fpr_err = np.zeros(bins)
    tpr_avg = np.zeros(bins); tpr_err = np.zeros(bins)
    thresh_avg = np.zeros(bins)
    for n in xrange(bins):
        #ix = ( thresh <  ) & ( thresh >= quantiles[n+1] )
        ix = slice( n * p_size, min( (n+1) * p_size, len(fpr) ) )
        fpr_avg[n] = fpr[ix].mean()
        fpr_err[n] = fpr[ix].std()
        tpr_avg[n] = tpr[ix].mean()
        tpr_err[n] = tpr[ix].std()
        thresh_avg[n] = thresh[ix].mean()

    # sometimes the average FPR jumps down rather than up ???
    # so make sure fpr is always increasing
    ix = np.argsort(fpr_avg)
    arrs = map(lambda x: x[ix], (fpr_avg, tpr_avg, fpr_err, tpr_err))
    arrs.append(thresh_avg)
    return arrs

def combine_roc_curves2(fpr, tpr, thresh, samps=50, err='beta'):
    """Aggregate mean/stdev of ROC curves from ranks of the parameter.

    Pool false positive/true positive rates over blocks of the
    decision variable. Curves are compared to each other based
    on ranks (percentiles) of their own parameterization variable.

    A set of true positives for a block is defined as

    {t_i(c_i) : c_i(b) < c_i < c_i(b+1), i in input} 

    Parameters
    ----------
    fpr, tpr, thresh
        Lists of false-positive, true-positive, and decision variable
        thresholds (may be different length).
    samps
        Number of levels to compute aggregates.
    err
        If err=='beta', error bars of the point estimates are the
        standard deviation of a beta distribution fit by
        maximum-likelihood of the input samples. Otherwise, error bars
        span the 15th to 85th percentile of the input samples. 
    
    Returns
    -------
    fpr_avg, tpr_avg
        Point estimates of the x and y ROC coordinates
    fpr_err, tpr_err
        Standard dev of the x and y ROC coordinates
    
    """

    s_fpr = []; s_tpr = []; s_thresh = []
    for fpr_, tpr_, thresh_ in zip(fpr, tpr, thresh):
        # make sure all are sorted according to threshold
        ix = np.argsort( np.unique(thresh_) )
        s_fpr.append( fpr_[ix] )
        s_tpr.append( tpr_[ix] )
        s_thresh.append( thresh_[ix] )
        
    ## start = samps//2
    ## skip = len(all_thresh) // samps
    ## t_samps = all_thresh[start::skip]
    n = np.argmin(map(len, s_fpr))
    skip = len(s_fpr[n])//samps
    start = skip//2
    tpr_avg = np.zeros(samps)
    fpr_avg = np.zeros(samps)
    tpr_err = np.zeros((2, samps))
    fpr_err = np.zeros((2, samps))
    #for n, t in enumerate(t_samps):
    for i in xrange(samps):
        ## ix = [t_.searchsorted(t) for t_ in s_thresh]
        ## fpr_samps = [fpr_[i] for fpr_, i in zip(s_fpr, ix)]
        ## tpr_samps = [tpr_[i] for tpr_, i in zip(s_tpr, ix)]
        n = start + i*skip
        fpr_samps = np.array( [fpr_[n] for fpr_ in s_fpr] )
        tpr_samps = np.array( [tpr_[n] for tpr_ in s_tpr] )

        if not fpr_samps.var() > 1e-8:
            q = [0, fpr_samps[0], 0]
        elif err=='beta':
            zm = fpr_samps == 0
            fpr_samps[zm] = 1e-6
            om = fpr_samps == 1
            fpr_samps[om] = 1 - 1e-6
            a, b = dists.beta.fit(fpr_samps, floc=0, fscale=1)[:2]
            m1 = dists.beta.moment(1, a, b)
            m2 = dists.beta.moment(2, a, b)
            var = m2 - m1**2
            q = [m1 - var**.5, m1, m1 + var**.5]
            #q = dists.beta.ppf(a, b, [0.975, 0.5, 0.025])
        else:
            q = np.percentile(fpr_samps, [15, 50, 85])
        fpr_avg[i] = q[1]
        fpr_err[0,i] = q[1] - q[0]
        fpr_err[1,i] = q[2] - q[1]

        if not tpr_samps.var() > 1e-8:
            q = [0, tpr_samps[0], 0]
        elif err=='beta':
            zm = tpr_samps == 0
            tpr_samps[zm] = 1e-6
            om = tpr_samps == 1
            tpr_samps[om] = 1 - 1e-6
            a, b = dists.beta.fit(tpr_samps, floc=0, fscale=1)[:2]
            m1 = dists.beta.moment(1, a, b)
            m2 = dists.beta.moment(2, a, b)
            var = m2 - m1**2
            q = [m1 - var**.5, m1, m1 + var**.5]
            #q = dists.beta.ppf(a, b, [0.975, 0.5, 0.025])
        else:
            q = np.percentile(tpr_samps, [15, 50, 85])
        tpr_avg[i] = q[1]
        tpr_err[0,i] = q[1] - q[0]
        tpr_err[1,i] = q[2] - q[1]

    return fpr_avg, tpr_avg, fpr_err, tpr_err

def plot_roc(
        fpr, tpr, fpr_err=(), tpr_err=(), 
        cls_labels=(), title='', colors='rainbow', 
        legend=True, num_err=10, ax=None, **line_kws
        ):
    """Plot one or more ROC curves with optional x- and y-error.
    """

    import distutils.version as vs
    import matplotlib as mpl
    fmt_string = None if vs.LooseVersion(mpl.__version__) < vs.LooseVersion('1.4.0') else 'none'
    
    if isinstance(fpr[0], (tuple, list, np.ndarray)) or \
      (isinstance(fpr, np.ndarray) and fpr.ndim > 1):

        n_class = len(fpr)
        ## if not cls_labels:
        ##     cls_labels = ['cls {0}'.format(i) for i in xrange(n_class)]

        try:
            roc_colors = color_palette( 
                colors, n_colors=n_class, desat=.7 
                )
            #roc_colors = np.array( roc_colors )
            err_colors = color_palette( 
                colors, n_colors=n_class, desat=.35 
                )
        except ValueError:
            roc_colors = [ colors ] * n_class
            err_colors = [ colors ] * n_class
    else:
        n_class = 1
        fpr = [ fpr ]
        tpr = [ tpr ]
        fpr_err = [ fpr_err ] if len(fpr_err) else ()
        tpr_err = [ tpr_err ] if len(tpr_err) else ()
        roc_colors = [ '#D95041' ]
        err_colors = [ 'black' ]


    if not ax:
        import matplotlib.pyplot as pp
        fig = pp.figure(figsize=(6,6))
        ax = fig.add_subplot(111)
    else:
        fig = ax.figure

    mx_yerr = 0
    mx_xerr = 0
    mn_yerr = 1
    mn_xerr = 1
    for n in xrange(n_class):
        incr = np.sign(np.diff(fpr[n])).sum() > 0
        if incr:
            fpr_ = np.r_[0, fpr[n], 1]
            tpr_ = np.r_[0, tpr[n], 1]
        else:
            fpr_ = np.r_[1, fpr[n], 0]
            tpr_ = np.r_[1, tpr[n], 0]
        auc_ = 'AUC {0:.2f}'.format(auc(fpr_, tpr_))
        if legend:
            if len(cls_labels):
                auc_ = ': '.join([cls_labels[n], auc_])
        else:
            auc_ = ''
        lns = ax.plot(
            fpr[n], tpr[n], label=auc_, color=roc_colors[n], **line_kws
            )
        if len(fpr_err):
            f_err = fpr_err[n]
            t_err = tpr_err[n]
            # sort everything according to fpr
            ix = np.argsort(fpr[n])
            f = fpr[n][ix]
            t = tpr[n][ix]
            f_err = f_err[..., ix]
            t_err = t_err[..., ix]
            # subsamp at num_err equally spaced points on the fpr-axis
            sx = f.searchsorted(
                np.linspace(0.5/num_err, 1 - 0.5/num_err, num_err)
                )
            # if there are multiple ROCs, then stagger these samples
            # by -(n_class-1) + 2*n
            sx += -(n_class-1) + 2*n
            sx[-1] = min(len(f)-1, sx[-1])
            sx = np.unique( sx[ sx < len(f) ] )
            f_err = f_err[..., sx]
            t_err = t_err[..., sx]
            t = t[sx]
            f = f[sx]

            ax.errorbar(
                f, t, yerr=t_err, xerr=f_err, fmt=fmt_string,
                capsize=0, ecolor=err_colors[n], 
                elinewidth=0.8 * lns[0].get_lw()
                )

            if t_err.ndim > 1:
                mx_yerr = max(mx_yerr, (t + t_err[1]).max())
                mn_yerr = min(mn_yerr, (t - t_err[0]).min())
                mx_xerr = max(mx_xerr, (f + f_err[1]).max())
                mn_xerr = min(mn_xerr, (f - f_err[0]).min())
            else:
                mx_yerr = max((t+t_err).max(), mx_yerr)
                mn_yerr = min((t-t_err).min(), mn_yerr)
                mx_xerr = max((f+f_err).max(), mx_xerr)
                mn_xerr = min((f-f_err).min(), mn_xerr)

    ax.plot([0, 1], [0, 1], color=(0.6, 0.6, 0.6), ls='--', lw=2)
    if legend:
        ax.legend(loc='lower right')

    ax.axis('image')
    xmin = min(-0.01, mn_xerr); xmax = max(1.01, mx_xerr)
    ymin = min(-0.01, mn_xerr); ymax = max(1.01, mx_yerr)
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    
    despine(trim=True, offset=5, ax=ax)
    ax.set_xlabel('false positive rate')
    ax.set_ylabel('detection rate')
    if title:
        ax.set_title(title)
        
    return fig

def canonical_projections(
        clf, samples, training=(), dims=(0, 1), 
        classes=(), plot=True, **pkwargs
        ):
    """Compute a linear discriminant surface for a LDA classifier.

    Either with a pre-trained classifier, or using training samples,
    compute the resulting maximum likelihood surfaces over the
    discriminant space for two given dimensions.

    Parameters
    ----------
    clf : classifier or Pipeline object
        Classifier that is pre-trained or will be trained by given
        training samples
    samples : (n_sample x n_feature)
        Sample points to plot in the likelihood map (if plot==True)
    training : (n_training x n_feature)
        Optional training samples
    dims : sequence
        Two discriminant dimensions over which to compute likelihood
        maps. 
    plot : bool
        Create a plot of the surface
    classes : (n_sample,)
        Class identification of the given samples
    
    Returns
    -------
    b : Bunch(xx=xx, yy=yy, p_mesh=posterior_mesh, samples=xf_samps)
        Struct of the computed surface etc.
    
    """
    
    if len(training):
        clf.fit(*training)
    cpred = clf.predict(samples)
        
    dims = list(dims)
    xf_samps = clf.transform(samples)[:, dims]

    gmin = xf_samps.min(); gmax = xf_samps.max()
    buf = 0.1 * (gmax - gmin)
    gmin -= buf; gmax += buf
    grid = np.linspace(gmin, gmax, 200)
    xx, yy = np.meshgrid( grid, grid )
    # predict the likelihood and posterior proba on this mesh,
    # sliced through the origin across the requested plane
    if isinstance(clf, Pipeline):
        clf = clf.steps[-1][-1]
    y_mesh = np.c_[xx.ravel(), yy.ravel()].dot(clf.coef_.T[dims])
    y_mesh += clf.intercept_

    likelihood = np.exp(y_mesh - y_mesh.max(axis=1)[:,None])
    # equally probable priors
    posterior_mesh = likelihood / likelihood.sum(axis=1)[:,None]

    dim_labels = ['LDA dim {0}'.format(d+1) for d in dims]
    if plot:
        return plot_decision_regions(
            xx, yy, posterior_mesh, xf_samps, cpred, 
            dim_labels=dim_labels, true_classes=classes, **pkwargs
            )
    else:
        return Bunch(
            xx=xx, yy=yy, p_mesh=posterior_mesh, samples=xf_samps
            )

def plot_decision_regions(
        xx, yy, p_mesh, samples, classes, 
        title='', colors='rainbow',
        dim_labels=(), cls_labels=(), mask_proba=0.5, 
        surf_desat=0.6, surf_alpha=0.5,
        true_classes=(), ax=None
        ):

    """
    Plots heatmap of posterior density for each class, and the decision
    boundaries.

    Parameters
    ----------
    xx, yy, p_mesh 
        The x-mesh, y-mesh and posterior density surface.
    samples
        Sample projections
    classes
        Class identifications of the samples
    colors 
        Can be a color scheme by colormap name, colormap instance, 
        or a sequence of colors    
    true_classes
        If given, then highlight misclassified samples with a bold
        outline. 
    mask_proba 
        Should be > 0 to aid blending the distinct posterior
        density heatmaps. The surfaces are nan-masked at levels below
        mask_proba
    surf_desat 
        Controls the blend-to-gray appearance of the surface colors
    surf_alpha 
        Controls the alpha compositing of the posterior surfaces
    ax
        Axes to plot into

    Returns
    -------
    f : matplotlib Figure
    
    """

    uclasses = np.unique(classes)
    if not cls_labels:
        cls_labels = [str(i) for i in uclasses.astype('i')]

    if not len(true_classes):
        accurate = np.ones_like(classes, dtype='?')
        accuracy = ''
    else:
        accurate = true_classes == classes
        accuracy = np.sum(accurate) / float(len(accurate))
        accuracy = '{0:.2f}% accuracy'.format(100*accuracy)
    title = title + '\n' + accuracy if title else accuracy
    ## dot_colors = np.array(color_palette(colors, n_colors=len(cls_labels)))
    dot_colors = composited_color_palette(
        palette=colors, n_colors=len(cls_labels), alpha=0.5 * (1 + surf_alpha)
        )
    dot_colors = LinearSegmentedColormap.from_list('dotcolors', dot_colors)
    surf_colors = color_palette(
        colors, n_colors=len(cls_labels), desat=surf_desat
        )

    xmin = xx.min(); xmax = xx.max()
    ymin = yy.min(); ymax = yy.max()

    if ax is None:
        import matplotlib.pyplot as pp
        f = pp.figure()
        ax = f.add_subplot(111)
    else:
        f = ax.figure
    
    for i in xrange(0, len(cls_labels)):
        pf = p_mesh[:,i].reshape(xx.shape)
        p_masked = np.ma.masked_less_equal(pf, mask_proba)
        ax.imshow(
            p_masked, norm=mpl.colors.Normalize(.2, 1), alpha=surf_alpha,
            cmap=blend_palette(['white', surf_colors[i]], as_cmap=True),
            extent=[xmin, xmax, ymin, ymax], origin='lower'
            )
        ax.contour(xx, yy, pf, [0.5], linewidths=2, colors='#808080')

    cls_boundaries = np.r_[uclasses, uclasses[-1] + 1]
    a_samples = samples[accurate]
    pc = ax.scatter(
        a_samples[:,0], a_samples[:,1], s=40, c=classes[accurate], 
        cmap=dot_colors, linewidths=.25, edgecolors='black',
        norm=BoundaryNorm(cls_boundaries, dot_colors.N)
        )        
    pc.set_zorder(10)
    if (~accurate).any():
        i_samples = samples[~accurate]
        pc_ = ax.scatter(
            i_samples[:,0], i_samples[:,1], s=40, c=classes[~accurate], 
            cmap=dot_colors, linewidths=1.25, edgecolors='black',
            norm=BoundaryNorm(cls_boundaries, dot_colors.N)
            )
        pc_.set_zorder(10)
    
    def _cbar(ref_ax=ax):
        bbox = ref_ax.get_position()
        ax_h = bbox.y1 - bbox.y0
        ax_l = bbox.x1
        cax_l = 0.9 * ax_l + 0.1
        cax = f.add_axes([cax_l, bbox.y0, 0.025, ax_h])
        cbar = f.colorbar(pc, cax=cax)
        #cbar = pp.colorbar(pc, ax=ref_ax, use_gridspec=True)
        cbar.set_ticks(0.5 * (cls_boundaries[:-1] + cls_boundaries[1:]) )
        cbar.set_ticklabels(cls_labels)
    f._cbar = _cbar
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    if not len(dim_labels):
        dim_labels = ['dim {0}'.format(i) for i in (1,2) ]
    ax.set_xlabel(dim_labels[0])
    ax.set_ylabel(dim_labels[1])
    if title:
        ax.set_title(accuracy)
    return f
    
def sort_samples(p, classes, samples, n_chan=None, tol=0):
    """Sort decoder features according to prediction success.

    Parameters
    ----------
    p : (n_trial,)
        Decoder predictions
    classes : (n_trial,)
        True class IDs
    samples : (n_trial, n_feature)
        Feature matrix used for predictions. 
    n_chan : int (optional)
        Each feature vector can be reshaped into a matrix with 
        shape (n_chan, -1) 
        
    Returns
    -------
    s_samps : Bunch
        A collection of correct and incorrect samples for each class.
    
    """

    if n_chan is not None:
        samples = samples.reshape( len(samples), n_chan, -1 )

    u_classes = np.unique(classes)
    s_samps = Bunch()
    # if binary categories, then tolerance must be zero error
    if len(np.unique(p)) == 2:
        tol = 0
    for n, c in enumerate(u_classes):
        cls_mask = classes == c
        corr_mask = np.abs(p[cls_mask] - c) <= tol
        
        correct = samples[cls_mask][corr_mask]
        incorrect = samples[cls_mask][~corr_mask]
        s_samps['cls_{0}'.format(c)] = Bunch(
            correct=correct, incorrect=incorrect
            )
    return  s_samps

def plot_sorted_samples(sdict):
    """Plots results from sort_samples (if n_chan was given)."""
    
    try:
        n_chan = sdict.values()[0].correct.shape[1]
    except:
        n_chan = sdict.values()[0].incorrect.shape[1]
    

    import matplotlib.pyplot as pp
    colors = pp.cm.YlGnBu( np.linspace(.1, .9, n_chan) )
    f, axs = pp.subplots(len(sdict), 2, figsize=(10, 1.0*len(sdict)))
    for n, k in enumerate(sdict.keys()):
        correct = sdict[k].correct
        incorrect = sdict[k].incorrect
        if correct.size:
            m = len(correct)
            axs[n, 0].set_color_cycle(colors)
            axs[n, 0].plot( correct.mean(0).T, lw=.5 )
            axs[n, 0].set_title(k + ' decoded (n={0})'.format(m), fontsize=10)
        if incorrect.size:
            m = len(incorrect)
            axs[n, 1].set_color_cycle(colors)
            axs[n, 1].plot( incorrect.mean(0).T, lw=.5 )
            axs[n, 1].set_title(
                k + ' misclassified (n={0})'.format(m), fontsize=10
                )
        yl1 = axs[n, 0].get_ylim()
        yl2 = axs[n, 1].get_ylim()
        yl = min( min(yl1), min(yl2) ), max( max(yl1), max(yl2) )
        axs[n, 0].set_ylim( yl )
        axs[n, 1].set_ylim( yl )

    f.tight_layout()
    return f

import matplotlib.animation as animation
def video_sorted_samples(
        sdict, chan_map, cls, fps=50, tx=(), cm='jet', **save_kws
        ):
    """Video of results from sort_samples (if n_chan was given)."""

    c = sdict['cls_{0}'.format(cls)]
    if not c.correct.size * c.incorrect.size:
        print(
            'correct/incorrect : {0}/{1}'.format(
                *map(len, (c.correct, c.incorrect))
                ) 
            )
        return
    nc = len(c.correct); correct = c.correct.mean(0)
    ni = len(c.incorrect); incorrect = c.incorrect.mean(0)
    mn = np.concatenate( (correct, incorrect), axis=0 ).min()
    mx = np.concatenate( (correct, incorrect), axis=0 ).max()
    cl = ( min(-mx, mn), max(mx, -mn) )
    correct = chan_map.embed(correct, axis=0)
    incorrect = chan_map.embed(incorrect, axis=0)
    
    import matplotlib.pyplot as pp
    f, axs = pp.subplots(1, 2, sharex=True, sharey=True, figsize=(8,4.2))
    im1 = axs[0].imshow(correct[..., 0], cmap=cm, clim=cl, origin='upper')
    axs[0].axis('off'); #axs[0].axis('image')
    t1 = axs[0].set_title('decoded (n={0})'.format(nc), fontsize=12)
    im2 = axs[1].imshow(incorrect[..., 0], cmap=cm, clim=cl, origin='upper')
    axs[1].axis('off'); #axs[1].axis('image')
    t2 = axs[1].set_title('misclassified (n={0})'.format(ni), fontsize=12)

    t1_pos = t1.get_transform().transform(t1.get_position())
    t2_pos = t2.get_transform().transform(t2.get_position())

    px = 0.5 * (t1_pos[0] + t2_pos[0]); py = t1_pos[1]
    t3_pos = np.array([px, py]) / (f.get_size_inches() * f.get_dpi())
    f.text(
        t3_pos[0], t3_pos[1], 'Class {0}'.format(cls), fontsize=12,
        va='baseline', ha='center'
        )
    if len(tx):
        time_txt = f.text(
            t3_pos[0], 1-t3_pos[1], 'Time: {0:0.2f}'.format(tx[0]),
            fontsize=12, ha='center'
            )
    else:
        time_txt = None

    f.tight_layout()
    def _step(num):
        im1.set_data(correct[..., num])
        im2.set_data(incorrect[..., num])
        if time_txt:
            time_txt.set_text('Time: {0:0.2f}'.format(tx[num]))
        return (im1, im2)

    ani = animation.FuncAnimation(
        f, _step, frames=correct.shape[-1], interval=1000.0/fps, blit=False
        )

    fname = save_kws.pop('fname', '')
    if len(fname):
        save_kws.setdefault('writer', 'ffmpeg')
        save_kws.setdefault('codec', 'libx264')
        save_kws.setdefault('extra_args', ['-pix_fmt', 'yuv420p', '-qp', '1'])
        save_kws.setdefault('dpi', 2*f.dpi)
        ani.save(fname, **save_kws)
        # assume that looping is not wanted now
        ani.repeat = False
    return ani


def _make_dataframe(samps, mode):
    split = [ confusion_to_prediction(s) for s in samps ]
    if mode.lower() == 'accuracy':
        c_samps = [ c==p for p, c in split ]
    elif mode.lower() == 'error':
        c_samps = [ c-p for p, c in split ]
    else:
        # pass through and hope for the best!
        c_samps = samps
        
    df = pd.DataFrame( 
        dict(DV=c_samps[0].astype('i'), cat=['0'] * len(c_samps[0])) 
        )
    for n, s in enumerate(c_samps[1:]):
        df = df.append( 
            pd.DataFrame( dict(DV=s.astype('i'), cat=[str(n+1)] * len(s)) ), 
            ignore_index=True 
            )
    return df


def compare_decoding_accuracy(*samps):
    """Use logistic regression to make an Analysis of Deviance 
    hypothesis test on equal means in the logit image of 
    classification success. 

    Parameters
    ----------

    samps : 
        One or more confusion matrices

    Returns
    -------
    LRT
        Likelihood ratio test value (difference of deviance)
    p-value
        Chi-squared p value of the LRT

    Note
    ----
    If only one sample is given, then a Wald test (t-test) is
    performed comparing the intercept with the chance accuracy value
    of logit( 1 / num_classes).
    """

    df = _make_dataframe(samps, 'accuracy')
    if len(samps) == 1:
        c = logit( 1.0 / len(samps[0]) )
        lm = smf.glm('DV ~ C(cat)', data=df, family=sm.families.Binomial()).fit()
        return lm.wald_test( ( [1], c ) )
        
    lm = smf.glm('DV ~ C(cat)', data=df, family=sm.families.Binomial()).fit()
    lm0 = smf.glm('DV ~ 1', data=df, family=sm.families.Binomial()).fit()
    ddf = lm0.df_resid - lm.df_resid
    LRT = lm0.deviance - lm.deviance
    
    return ANODEResult( LRT, chi2.sf(LRT, ddf) )


def compare_decoding_error(*samps):
    """Use Poisson regression to make an Analysis of Deviance 
    hypothesis test on equal means in the logit image of 
    classification success. 

    Parameters
    ----------

    samps : 
        Two or more confusion matrices
        Two or more arrays of integer classification errors (unitless
        whole number difference of predicted and actual classes).

    Returns
    -------
    LRT
        Likelihood ratio test value (difference of deviance)
    p-value
        Chi-squared p value of the LRT

    Note
    ----
    If only one sample is given, then a Wald test (t-test) is
    performed comparing the intercept with the log of the chance error
    value. 
    """

    df = _make_dataframe(samps, 'error')
    if len(samps) == 1:
        c = np.log( decoding_distance( np.ones_like(samps[0]) ) )
        lm = smf.glm('I(abs(DV)) ~ C(cat)', data=df,
                     family=sm.families.Poisson()).fit()
        return lm.wald_test( ( [1], c ) )

    lm0 = smf.glm(
        'I(abs(DV)) ~ 1', data=df, family=sm.families.Poisson()
        ).fit(scale='X2')
    lm = smf.glm(
        'I(abs(DV)) ~ C(cat)', data=df, family=sm.families.Poisson()
        ).fit(scale='X2')
    ddf = lm0.df_resid - lm.df_resid
    LRT = (lm0.deviance - lm.deviance) / lm0.scale
    
    return ANODEResult( LRT, chi2.sf(LRT, ddf) )
