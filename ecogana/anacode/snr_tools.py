"""
This module provides helper methods for the spectral analysis of SNR.
"""

import scipy.ndimage as ndimage
import scipy.interpolate as intrp
import os.path as p

from ecogana.anacode.signal_testing import block_psds, logged_estimators, \
     bad_channel_mask
import ecogana.anacode.plot_util as pu
import ecogana.anacode.colormaps as cmaps
from ecogana.devices.units import nice_unit_text

from ecoglib.filt.time import *
import ecoglib.numutil as nut
from ecoglib.util import Bunch, get_default_args
from ecoglib.data.h5utils import save_bunch, load_bunch

from sandbox.array_split import split_at

@split_at()
def para_spline(x, fx, bw, f0):
    shape = x.shape
    x = x.reshape( np.prod(shape[:-1]), shape[-1] )

    # get weight from distance to local median
    xm = ndimage.median_filter(x, size=(1, bw))
    ## w1 = 1.0 / np.maximum(1, np.abs((x - xm)/x)**3)
    ## s = np.sum((w1*(x - xm))**2, axis=1)
    ## print s
    #w = 1/x.std(axis=0)
    #w[ w1 < 1 ] = 0
    #w = np.ones_like(xm)
    xm = xm[..., f0:]
    #w1 = w1[..., f0:]
    #xsm = np.empty_like(x[..., f0:])
    xsm = np.empty_like(x)
    xsm[..., :f0] = x[..., :f0]
    fx = fx[f0:]
    for xi, xsmi, xmi in zip(x[:, f0:], xsm[:, f0:], xm):
        wi = 1.0 / np.maximum(1, np.abs( (xi - xmi)/xmi ))
        wi = np.convolve(wi, np.ones(bw//2)/(bw//2), mode='same')
        wi[:bw//2] = 1
        wi[-bw//2:] = 1
        wi[ wi < 1 ] = 0
        wi *= fx
        s = np.sum( (wi*(xi - xmi))**2 )
        #print s
        tck = intrp.splrep(fx, xi, w=wi, s=s)
        xsmi[:] = intrp.splev(fx, tck)
        #xsmi[:] = (wi*(xi - xmi))**2
    return xsm.reshape( shape[:-1] + (-1,) )

def estimate_psd(
        data, Fs, block_len=2.0, NW=3, pcar=(), spline=-1, mask=True,
        return_blocks=False, iqr=3.0
        ):
    """
    Estimate block-averaged power spectral densities (PSDs). After
    distinguishing inlier and outlier channels based on the power
    distribution, find average the PSD +/- 1-sigma for the set of
    channels. Mean and standard deviations are computed under a
    normalizing transform (i.e. logarithm) of the power.

    Line noise can be mitigated in two ways:

    subtraction a proportion of the common average reference (pcar)
        provide a sequence of frequencies at which to estimate the pcar

    smoothing spline
        If spline > 0, then a spline is fit to each spectrum. The
	error weighting is calculated inversely proportional to the
	spectrum's deviation from a local median. In this way,
	features such as harmonic noise are loosely fitted (smoothed
	over) while non-harmonic content is more closely fitted.

    Returns
    -------

    fx : freq axis
    psds : list of (chan means, set mean, set mean - stdev, set_mean + stdev),
           if return_blocks is True, the blockwise PSDs for all channels
    mask : the channel mask
    
    """
    
    # do a cheap PSD for power estimation
    fx, psds = block_psds(
        data, block_len, Fs, 
        NW=1.5, adaptive=False, jackknife=False
        )

    mn_psds = np.exp(np.log(psds).mean(1))
    pwr_est = np.log( mn_psds[..., fx<200].sum(-1) )

    ### Channel selection
    
    if data.shape[0] > 1 and mask:
        ## # first off, throw out anything pushing numerical precision
        ## m = pwr_est > np.log(1e-10)
        ## # automatically reject anything 3 orders of magnitude
        ## # lower than the median broadband power
        ## m[pwr_est < (np.median(pwr_est[m]) - np.log(1e3))] = False
        ## # now also apply a fairly wide outlier rejection
        ## msub = nut.fenced_out(pwr_est[m], low=True, thresh=4.0)
        ## m[m] = msub
        m = bad_channel_mask(pwr_est, iqr=iqr)
        data = data[m]
    else:
        m = np.ones( data.shape[0], dtype='?' )

    dsum = np.sum( data, axis=0 )
    if pcar:
        for line in pcar:
            fline = fx.searchsorted(line)
            # this is an estimator of a value proportional 
            # to the amplitude of the line component
            w = np.sqrt(mn_psds[m][:, fline])
            w /= w.sum()

            # estimate very narrowband component here
            dsum_n = notch_all(
                dsum, Fs, lines=(line,), inplace=False, nzo=3, nwid=1.5
                )
            nband = filter_array(
                dsum - dsum_n, inplace=False,
                design_kwargs=dict(lo=line-30, hi=line+30, Fs=Fs)
                )
            
            data -= nband * w[:,None]
        
    fx, psds = block_psds(
        data, block_len, Fs, 
        NW=NW, adaptive=False, jackknife=False
        )
    
    if not spline < 0: 
        psds = np.mean( np.log(psds), axis=1 )
        # smooth the spectra after the band specified by spline
        med_bw = (2 * 6 * NW / block_len) / (fx[1]-fx[0])
        f0 = fx.searchsorted(spline)
        xsm = para_spline(psds, fx, med_bw, f0)
        psds[..., f0:] = xsm

        p_mn = np.nanmean(psds, axis=0)
        p_sig = np.nanstd(psds, axis=0)

        if not return_blocks:
            psds = map(
                np.exp, (psds, p_mn, p_mn-p_sig, p_mn+p_sig)
                )
    else:
        if not return_blocks:
            psds = logged_estimators(psds, sem=False)
    
    return fx, psds, m

#@split_at(split_arg=1)
def notch_and_interp(fx, psd, notches, k=1):
    wfn = np.ones_like(fx)
    fx_mask = np.ones(fx.shape, dtype='?')
    for band in notches:
        #wfn[ (fx > band[0]) & (fx < band[1]) ] = 0
        fx_mask[ (fx > band[0]) & (fx < band[1]) ] = False
    p_shape = psd.shape
    psd = psd.reshape(-1, p_shape[-1])
    psd_i = np.empty_like(psd)
    if (psd[0] < 0).any():
        x_in = lambda x: x
        x_out = lambda x: x
    else:
        x_in = np.log
        x_out = np.exp
    for p, pi in zip(psd, psd_i):
        tck = intrp.splrep(fx[fx_mask], x_in(p[fx_mask]), k=k)
        pi[:] = intrp.splev(fx, tck)
    return x_out(psd_i).reshape(p_shape)

def safe_integrate(
        fx, pxx, cutoff, cut_below=1, bw=None, notches=(), running=False
        ):
    "Return bandpass power, with notches and extra low freqs cut out"
    set_nan = fx < cut_below

    if notches=='auto':
        n = 1
        notches = list()
        while n*60 < cutoff:
            notches.append(n*60)
            n += 1
    if not bw:
        params = get_default_args(estimate_psd)
        bw = np.ceil( float( params['NW'] ) / params['block_len'] )
    for notch in notches:
        set_nan = set_nan | ( (fx > notch - bw) & (fx < notch + bw) )

    pxx_nan = pxx.copy()
    pxx_nan[..., set_nan] = 0

    if running:
        return np.cumsum(pxx_nan[..., fx<cutoff], axis=-1) * fx[-1] / len(fx)
    return np.sum(pxx_nan[..., fx<cutoff], axis=-1) * fx[-1] / len(fx)

def snr_by_freq(fx, sg, nz, sg_mask, nz_mask, cutoff):

    # align channel masks
    sg_mask_aligned = nz_mask[sg_mask]
    nz_mask_aligned = sg_mask[nz_mask]
    sg = sg[sg_mask_aligned]
    nz = nz[nz_mask_aligned]

    # instantaneous SNR
    db_snr_a = 10 * ( np.log10(sg) - np.log10(nz) )
    db_snr_f_mu = db_snr_a.mean(0)
    db_snr_f_sig = db_snr_a.std(0)
    db_snr_f_all = db_snr_a[ ..., fx < cutoff ].copy()
    # restrict these
    db_snr_f_mu = db_snr_f_mu[ fx < cutoff ]
    db_snr_f_sig = db_snr_f_sig[ fx < cutoff ]

    # cumulative SNR
    sg_pwr = safe_integrate(fx, sg, cutoff, running=True)
    nz_pwr = safe_integrate(fx, nz, cutoff, running=True)

    db_snr_a = 10 * ( np.log10(sg_pwr) - np.log10(nz_pwr) )
    db_snr_c_mu = db_snr_a.mean(0)
    db_snr_c_sig = db_snr_a.std(0)
    db_snr_c_all = db_snr_a.copy()

    return (db_snr_f_mu, db_snr_f_sig), (db_snr_c_mu, db_snr_c_sig), \
      (db_snr_f_all, db_snr_c_all)

def plot_psds(
        fx, sg_mean, sg_itvl, nz_mean, nz_itvl, units, 
        sg_color='g', nz_color='k', ax=None
        ):
    if ax is None:
        import matplotlib.pyplot as pp
        f = pp.figure()
        ax = f.add_subplot(111)
    else:
        f = ax.figure

    f, sg_trace = pu.filled_interval(
        ax.semilogy, fx, sg_mean, sg_itvl, sg_color, ax=ax, lw=2
        )
    _, nz_trace = pu.filled_interval(
        ax.semilogy, fx, nz_mean, nz_itvl, nz_color, ax=ax, lw=2
        )

    #ax.grid(which='both')
    ax.legend( 
        (sg_trace[0], nz_trace[0]), ('Physio recording', 'Noise recording')
        )
    ax.set_xlabel('Frequency (Hz)')
    ax.set_ylabel('$%s^{2} /Hz$'%nice_unit_text(units).strip('$'))
    return f

def plot_psds_puc(
        fx, sg_mean, nz_mean, cutoff, units, notches=(),
        sg_color='g', nz_color='k', ax=None, f0=50, ft=None, bottom=None,
        textsize=12
        ):
    if ax is None:
        import matplotlib.pyplot as pp
        f = pp.figure()
        ax = f.add_subplot(111)
    else:
        f = ax.figure

    sg_trace = ax.semilogy(fx, sg_mean, color=sg_color, lw=2, zorder=20)
    ax.fill_between(
        fx, sg_mean, nz_mean, facecolor=sg_color, edgecolor='none', alpha=0.2,
        zorder=20
        )
    #nz_trace = ax.semilogy(fx, nz_mean, color=nz_color, lw=2)
    if not bottom:
        bottom = nz_mean[fx<cutoff].min()*1e-1
    ax.fill_between(
        fx, nz_mean, bottom, facecolor=nz_color, edgecolor='none',
        alpha=0.2, zorder=20
        )
    ax.set_xlabel('Frequency (Hz)')
    ax.set_ylabel('$%s^{2} /Hz$'%nice_unit_text(units).strip('$'))
    #ax.grid(which='both')
    # do text
    ix = fx.searchsorted(f0)
    y_sig = np.sqrt( sg_mean[ix] * nz_mean[ix] )
    y_nz = np.sqrt( nz_mean[ix] * bottom )

    sig_pwr = (safe_integrate(fx, sg_mean, cutoff, notches=()) ** 0.5)
    nz_pwr = (safe_integrate(fx, nz_mean, cutoff, notches=()) ** 0.5)
    if not ft:
        ft = f0
    t = ax.text(
            ft, y_sig, r'%1.1f %s (RMS)'%(sig_pwr, units), ha='left',
            fontsize=textsize
        )
    t = ax.text(
        ft, y_nz, r'%1.1f %s (RMS)'%(nz_pwr, units), ha='left',
        fontsize=textsize
        )
    ax.set_xlim(0, cutoff)
    yl = ax.get_ylim()
    top = 10 ** np.ceil( np.log10( 2 * sg_mean.max() ) )
    ax.set_ylim(bottom, top)
    return f
