if __name__=='__main__':
    ## import matplotlib.pyplot as pp
    ## from matplotlib.figure import Figure
    ## pyplot_figure = pp.figure
    ## pp.figure = Figure
    pass

from collections import OrderedDict

from traits.api import HasTraits, List, Instance, Button, Int, \
     Float, Enum, Bool, on_trait_change
from traitsui.api import View, Item, UItem, \
     VGroup, HGroup, spring, ListEditor, Label

from matplotlib.figure import Figure
import matplotlib.cm as cmaps

#import matplotlib.colors as colors
from functools import partial
import ecogana.anacode.colormaps as colormaps
from ecogana.anacode.ep_scoring import ptp, mahalanobis, \
     active_sites, nroots_average
from ecogana.devices.units import nice_unit_text

import ecoglib.trigger_fun as tfun
from ecogana.expconfig.exp_descr import StimulatedExperiment, ordered_epochs
import numpy as np
from ecoglib.vis.traitsui_bridge import MPLFigureEditor, PingPongStartup
from ecoglib.vis.gui_tools import *

from ecoglib.util import equalize_groups
from ecoglib.numutil import fenced_out

from .modules import AnalysisModule

__all__ = [ 'Analysis' ]

def _color_to_cmap(color):
    if isinstance(color, cmaps.colors.Colormap):
        return color


    colors = colormaps.composited_color_palette(
        palette=color, n_colors=cmaps.jet.N
        )
    colors = cmaps.colors.LinearSegmentedColormap.from_list(
        'anon_colors', colors
        )
    return colors

def _default_labels(exp):
    labels = dict()
    for t in exp.event_names:
        vals = np.unique(getattr(exp, t))
        labels[t] = ['{0}'.format(v) for v in vals]
    return labels

class Analysis(HasTraits):
    """Organizes an array recording by events in a StimulatedExperiment.

    Analysis objects are built from a generic 'Bunch' container
    representing the recording dataset. The three critical elements
    for an Analysis object are 

    * .data : a vector timeseries
    * .exp : a StimulatedExperiment describing events
    * .chan_map : a ChannelMap describing the geometry of channels

    Other attributes that are used

    * .Fs : sampling frequency
    * .units : units of the timeseries samples
    * .name : to identify the recording

    **Epochs**
    
    The Analysis object essentially organizes an array recording into
    trial epochs. Epochs can be queried based on chronological order,
    condition label and level, or with arbitrary masking. The epoch
    interval is set by real-time values (in seconds) 'epoch_start' and
    'epoch_end'.

    The StimulatedExperiment represents event timestamps and labels
    (i.e. experiment "conditions"). The Analysis uses this object to
    organize trials and scores (see below) into groups.  Although the
    number of trials may be different for each combination of
    condition levels, **it is necessary that all possible combinations
    occur in the experiment.** For example, if Condition A has levels
    {a, aa} and Condition B has levels {b, bb, bbb}, there must be at
    least one sample for each of the six level combinations.

    **Scoring**
    
    Scoring refers to putting a number to a response (or baseline)
    waveform. As such, a score is a functional mapping vectors to
    scalars. Examples include RMS voltage and peak-to-peak
    voltage. The Mahalanobis distance score requires baseline samples
    that are taken from inter-stimulus periods. The score functionals
    only operate on a subset of the present epoch interval set by
    'peak_min' and 'peak_max' attributes.

    **GUI**
    
    This object is centered around a functional map of the electrode
    array. The map can be explored by clicking on sites to reveal
    timeseries of the experiment's trial epochs.

    **Modules**
    
    This Analysis object is meant to be suplemented by a collection
    of modules that expose subcategories of analysis plots.

    """
    
    replot_map = Button('Update Map')

    # these are specified in seconds, relative to the tone onset
    epoch_start = Float(-0.050)
    epoch_end = Float(0.100)
    false_epoch_offset = Float(-0.200)

    peak_min = Float(0.005)
    peak_max = Float(0.025)

    # other flags and toggles
    score_avg_ep = Bool(False)
    epoch_outlier_threshold = Float(-1)

    plot_peaks = Bool(False)

    scoring = Enum( 'rms', ('pk-bl', 'ptp', 'rms', 'mahal', 'linelength') )

    # -- internal use
    modules = List( [AnalysisModule] )
    selected_site = Int(-1)
    _sites_inactive = Bool(False)

    exp = Instance(StimulatedExperiment)
    epoch_filter = Instance(object)
    
    def __init__(
            self, dataset, condition_labels=None, 
            condition_colors=None, channel_colors=None, 
            split_over=(), **traits
            ):
        """
        Parameters
        ----------
        dataset : Bunch
        condition_lables : dict
            Map from condition names to condition-level labels.
        condition_colors : dict
            Map from condition names to colormaps for condition-levels
        split_over : sequence
            Names of conditions, the levels of which should not be
            direcly compared to each other (e.g. for the purpose of
            statistical normalization) -- rarely used
        
        """

        if not condition_labels:
            self.condition_labels = _default_labels(dataset.exp)
        else:
            self.condition_labels = condition_labels
        ## HasTraits.__init__(self, **traits)

        self.dataset = dataset
        self._nice_unit = nice_unit_text(self.dataset.units)
        
        self.exp = self.dataset.exp
        self.split_over = split_over

        # colors 
        # -- default to rainbow for any condition and winter for all channels
        rb = cmaps.rainbow
        if condition_colors is None:
            condition_colors = dict()
        self._colors = OrderedDict(
            [ (t, _color_to_cmap(condition_colors.get(t, rb))) 
              for t in self.exp.enum_tables ]
              )
        self._colors['allchan'] = channel_colors or cmaps.winter

        # caches
        self._avg_ep_cache = dict()
        self._p2p_cache = dict()
        self._p2b_cache = dict()
        self._rms_cache = dict()
        self._linelength_cache = dict()
        self._mahal_cache = dict()
        self._false_score_cache = dict()
        self._rank_sum_z_cache = dict()
        
        self._pk_scatter = None

        HasTraits.__init__(self, **traits)
        self._attach_listed_modules()
        self._attach_array_plot()
        self._attach_ep_plot()

        self.sync_trait('selected_site', self.array_plot, mutual=True)
        self.sync_trait('peak_min', self.ep_plot, mutual=True)
        self.sync_trait('peak_max', self.ep_plot, mutual=True)

    def _attach_listed_modules(self):
        mods = self.modules[:]
        self.modules = list()
        for module in mods:
            traits = dict(
                [ (t, getattr(self, t)) for t in module.synced_traits ]
                )
            self._attach_module(
                module(self, condition_labels=self.condition_labels, **traits),
                )
    
    def _attach_module(self, mod):
        self.modules.append( mod )
        a_name = mod.name.replace(' ', '_')
        setattr(self, a_name, mod)
        
    def _epoch_window(self, samps=True, subresponse=False):
        """Compute the main epoch interval in peritrial samples or time"""
        if subresponse:
            pre = -self.peak_min
            post = self.peak_max
        else:
            pre = -self.epoch_start
            post = self.epoch_end
        if samps:
            len = np.round( (pre + post) * self.dataset.Fs )
            pre = np.sign(pre) * np.round( self.dataset.Fs * np.abs(pre) )
            #post = np.sign(post) * np.round( self.dataset.Fs * np.abs(post) )
            post = len - pre
        return pre, post

    def _false_epoch_window(self, samps=True):
        """Compute the baseline epoch interval in peritrial samples or time"""
        # false epoch offset will be a number like -200 ms (-0.2)
        pre = -self.false_epoch_offset
        post = -pre + (self.epoch_end - self.epoch_start)
        if samps:
            len = np.round( (pre + post) * self.dataset.Fs )
            pre = np.sign(pre) * np.round( self.dataset.Fs * np.abs(pre) )
            #post = np.sign(post) * np.round( self.dataset.Fs * np.abs(post) )
            post = len - pre
        return pre, post

    def _timebase(self):
        return self.timebase()
    
    def timebase(self):
        """The peritrial timebase of the present epoch"""
        Fs = self.dataset.Fs
        pre, post = self._epoch_window()
        n_samps = int( round(pre + post) )
        tx = np.arange(n_samps) / Fs + self.epoch_start
        return tx
    
    def _attach_array_plot(self):
        # this is a dummy analysis, just plot peak EP score
        st = not self.score_avg_ep
        ep_score = self.ep_score(reshape=False, single_trials=st)
        while ep_score.ndim > 2:
            ep_score = np.nanmean(ep_score, axis=-1)
        d_tab = self.exp.enum_tables[0]
        cmap = self._colors[d_tab]
        chan_map = self.dataset.chan_map
        if ep_score.shape[-1] > 1:
            ep_score, _ = nroots_average(ep_score)
            labels = self.condition_labels[d_tab]
            self.array_plot = ArrayMap(
                chan_map, labels=labels, vec=ep_score, cmap=cmap,
                clim=(0, len(labels)-1)
                )
            self.array_plot.ax.set_title(
                self.dataset.name + ' Best {0} EP Score'.format(d_tab)
                )
        else:
            ep_score = ep_score.ravel()
            clim = np.percentile( ep_score, [2, 98] )
            self.array_plot = ArrayMap(
                chan_map, vec=ep_score, cmap=cmap, clim=clim, 
                map_units='Score ({0})'.format(self.scoring)
                )
            self.array_plot.ax.set_title(
                self.dataset.name + ' Evoked Response Score'
                )
    
    def _attach_ep_plot(self):
        avg = self.ep_average(reshape=False)
        g_avg = np.nanmean(avg, axis=1)
        tx = self.timebase()
        self.ep_plot = EvokedPlot(
            tx, g_avg.T, t0=0, figsize=(4.67, 3.0),
            plot_line_props=dict(color=self._colors['allchan'], lw=0.5),
            mark_line_props=dict(color='k', lw=2, ls='--')
            )
        self.ep_plot.ax.set_ylabel(self._nice_unit)
        self._pk_scatter = None
        self._toggle_peaks()

    def ep_average(self, iqr_thresh=-1, baseline=False, reshape=True):
        """Return the average of trials per channel

        Trial-averages are organized by conditions and levels.

        Parameters
        ----------
        iqr_thresh : float
            Controls outlier detection for individual trials. Outliers
            are excluded from the mean.
        baseline : {True/False}
            Acts on baseline rather than response windows.
        reshape : {True/False}
            Organize the average waveforms by condition and level.
        
        Returns
        -------
        avg : ndarray (n_channel, ..., n_pt)
            Shape depends on 'reshape' parameter.

        """
        ## if iqr_thresh < 0:
        ##     iqr_thresh = self.epoch_outlier_threshold
        if baseline:
            pre, post = self._false_epoch_window(samps=True)
        else:
            pre, post = self._epoch_window(samps=True)
        key = (pre, post, iqr_thresh, self.epoch_filter)
        if key in self._avg_ep_cache:
            avg =  self._avg_ep_cache[ key ]
        else:
            # safer to extract epochs first and then average,
            # in case any epoch filtering is nonlinear
            e, g = self.epochs(arranged=True, mask_outliers=iqr_thresh)
            try:
                e = e.filled(np.nan)
            except AttributeError:
                pass
            nchan = e.shape[0]
            npt = e.shape[2]
            epochs = equalize_groups(e, g, axis=1)
            avg = np.nanmean(epochs, axis=-2).reshape(nchan, -1, npt)
            ## avg, navg = tfun.ep_trigger_avg(
            ##     self.dataset.data, self.exp, pre=pre, post=post,
            ##     iqr_thresh=iqr_thresh
            ##     )
            self._avg_ep_cache[ key ] = avg
        if reshape:
            rolled_shape = self.exp.rolled_conditions_shape()
            ep_shape = (avg.shape[0],) + rolled_shape + (avg.shape[-1],)
            return avg.reshape(ep_shape)
        return avg

    def _get_cached_score(
            self, scoring_fn, key, cache, 
            reshape=True, extra=False, single_trials=False
        ):
        """General functionality for cached scores.

        This method attempts to find a cached score based on the
        'key'. Failing that, it calls the 'scoring_fn' on timeseries
        returned by self.epochs (if single_trials==True) or
        self.ep_average (if single_trials==False). 

        'Flat' scores (not reshaped by condition / level) are then
        cached by the key, to be retrieved (and potentially reshaped)
        on later calls.
        
        Returns
        -------
        scores : ndarray (n_channel, ... )
            Waveform scores per channel. Shape depends on 'reshape'.
        
        """

        # !cache all results with unrolled shape!
        n_chan = self.dataset.data.shape[0]
        rolled_shape = (n_chan,) + self.exp.rolled_conditions_shape()
        if single_trials:
            rolled_shape = rolled_shape + (-1,)
        if key in cache:
            arrs = cache[key]
            if reshape:
                arrs = map(lambda x: x.reshape(rolled_shape), arrs)
            if not extra or len(arrs) == 1:
                arrs = arrs[0]
            return arrs

        # since key was not found, then compute and store the
        # unrolled scores
        if single_trials:
            to_score, g = self.epochs(arranged=True)
            if reshape:
                # must make sure that x can be reshaped
                to_score = equalize_groups(to_score, g, axis=1, reshape=False)
                
        else:
            to_score = self.ep_average(reshape=False)
        
        scores = scoring_fn(to_score)
        if not isinstance(scores, (list, tuple)):
            scores = (scores,)
        cache[key] = scores
        return self._get_cached_score(
            None, key, cache, reshape=reshape, extra=extra, 
            single_trials=single_trials
            )

    def peak_to_baseline(self, r=None, reshape=True, single_trials=False):
        """Peak-to-baseline waveform score

        In order to determine baseline, the present timebase must begin 
        before the trial (i.e. self.epoch_start < 0).

        If 'r' is given, score this array rather than the trials that
        would currently be returned.
        """

        tx = self.timebase()
        ep_mn = tx.searchsorted(self.peak_min)
        ep_mx = tx.searchsorted(self.peak_max)
        key = (self.peak_min, self.peak_max, single_trials, 
               self.epoch_filter, reshape)

        # get average baseline level for 25 samps before trigger
        i0 = tx.searchsorted(0)
        if i0 < 25:
            print(
                'warning: poor baseline estimation due '
                'to epoch window settings'
                )
            i0 = 25
        def score_fn(x):
            shp = x.shape
            x = x.reshape(shp[0], -1, shp[-1])
            # average this over time points and conditions
            baseline = x[..., i0-25:i0].mean(axis=-1).mean(axis=-1)
            peak = x[..., ep_mn:ep_mx].max(axis=-1)
            pk_bl_diff = peak - baseline[:,None]
            # must be positive!
            #pk_bl_diff -= pk_bl_diff.min(axis=-1)[:,None]
            pk_bl_diff[ pk_bl_diff < 0 ] = 0
            pk_bl_diff.shape = shp[:-1]
            return pk_bl_diff
        
        if r is not None:
            return score_fn(r)
        return self._get_cached_score(
            score_fn, key, self._p2b_cache, 
            reshape=reshape, single_trials=single_trials
            )
    
    def peak_to_peak(
            self, r=None, points=False, reshape=True, single_trials=False
            ):
        """Peak-to-peak waveform score

        If 'r' is given, score this array rather than the trials that
        would currently be returned.

        If points==True, also return the indices of the peaks.
        """

        tx = self.timebase()
        
        pk_dx_lo = tx.searchsorted(self.peak_min)
        pk_dx_hi = tx.searchsorted(self.peak_max)
        key = (self.peak_min, self.peak_max, points, 
               single_trials, self.epoch_filter, reshape)

        score_fn = lambda x: ptp(x, (pk_dx_lo, pk_dx_hi), points=points)
        if r is not None:
            return score_fn(r)
        ## return self._get_cached_score(
        ##     score_fn, key, self._p2p_cache, reshape=reshape, extra=points
        ##     )
        return self._get_cached_score(
            score_fn, key, {}, reshape=reshape, 
            extra=points, single_trials=single_trials
            )
        
    def rms(self, r=None, reshape=True, single_trials=False):
        """Root-mean-square waveform score

        Root-mean-square is defined here equivalently to the sample 
        standard deviation. That is, the overall mean is first subtracted.
        
        If 'r' is given, score this array rather than the trials that
        would currently be returned.
        """

        tx = self.timebase()
        ep_mn = tx.searchsorted(self.peak_min)
        ep_mx = tx.searchsorted(self.peak_max)

        key = (self.peak_min, self.peak_max, self.epoch_filter, 
               single_trials, reshape)
        score_fn = lambda x: x[..., ep_mn:ep_mx].std(axis=-1)
        if r is not None:
            return score_fn(r)
        return self._get_cached_score(
            score_fn, key, self._rms_cache, 
            reshape=reshape, single_trials=single_trials
            )
            
    def linelength(self, r=None, reshape=True, single_trials=False):
        """Line-length waveform score

        Line length is defined here as the incremental some of lengths
        between x(t) timeseries samples on the (t, x) Cartesian grid.

        sum_n ( (x(n) - x(n-1))^2 + (t(n) - t(n-1))^2 )^(1/2)

        If 'r' is given, score this array rather than the trials that
        would currently be returned.
        """

        tx = self.timebase()
        ep_mn = tx.searchsorted(self.peak_min)
        ep_mx = tx.searchsorted(self.peak_max)

        key = (self.peak_min, self.peak_max, self.epoch_filter, single_trials)
        def score_fn(x):
            del_t = np.diff(tx[ep_mn:ep_mx], axis=-1)
            del_x = np.diff(np.abs(x[..., ep_mn:ep_mx]), axis=-1)
            return np.sqrt(del_t**2 + del_x**2).sum(-1)
        if r is not None:
            return score_fn(r)
        return self._get_cached_score(
            score_fn, key, self._linelength_cache, 
            reshape=reshape, single_trials=single_trials
            )
    
    def mahalanobis(self, r=None, reshape=True, pn=0, single_trials=False):
        """Mahalanobis distance waveform score

        The center vector and covariance matrix are estimated from
        pre-trial baseline sampels (e.g. inter-stimulus). Separate
        baselines are estimated if the 'split_over' attribute is
        populated.

        For more consistent covariance estimation, it is possible to
        set the p-to-n ratio, where n is the number of samples used to
        estimate the p x p covariance matrix. If the number of
        inter-trial samples is less than n = p / pn, the baseline
        samples are searched for in the period before the first trial
        and after the final trial. 
        
        If 'r' is given, score this array rather than the trials that
        would currently be returned.
        """

        tx = self.timebase()
        ep_mn = tx.searchsorted(self.peak_min)
        ep_mx = tx.searchsorted(self.peak_max)
        
        # baseline period (only use equal amount as EP period)
        b_pre, _ = self._false_epoch_window(samps=True)
        b_post = -b_pre + (ep_mx - ep_mn)
        if pn > 0:
            # pn sets the p-to-n ratio, where n is the number of
            # samples used to estimate a (p x p) covariance
            # If pn is set to 0, then all available samples are used
            target_samples = int( (ep_mx - ep_mn) / pn )
            print target_samples
        else:
            target_samples = 0
        
        # Within the score function, look to see if the
        # epochs should be split up in any way (e.g. the
        # amplitude condition is held constant over one run in
        # some experiments, and so the appropriate baseline for 
        # the epochs in that run are baseline samples within the
        # same run).
        
        def score_fn(x, known_order=True):
            # by default, x is the unrolled ep_averages or the
            # unrolled single trials, in which cases the middle 
            # dimension can be organized by cshape

            # need to get the baseline samples through the epochs method,
            # in case there is any window filtering happening
            samps = self.epochs(baseline=True)
            samps = samps[..., :(ep_mx-ep_mn)]
            if known_order and len(self.split_over):
                cshape = self.exp.rolled_conditions_shape()
                try:
                    x = x.reshape( (len(x),) + cshape + (x.shape[-1],) )
                except:
                    _, groups = ordered_epochs(self.exp, (), group_sizes=True)
                    x = equalize_groups(x, groups, axis=1, reshape=True)
                    shp = x.shape
                    shp = shp[:1] + cshape + shp[2:]
                    x = x.reshape(shp)
                
                dists = np.empty(x.shape[:-1], dtype=x.dtype)
                for m, s in self.exp.iterate_for(self.split_over, c_slice=True):
                    d_sl = [slice(None)] + s
                    if x.ndim > len(cshape) + 2:
                        x_sl = [slice(None)] + s + \
                          [slice(None), slice(ep_mn, ep_mx)]
                    else:
                        x_sl = [slice(None)] + s + [slice(ep_mn, ep_mx)]
                    x_sl = x[x_sl]
                    shp = x_sl.shape
                    x_sl = x_sl.reshape( shp[:1] + (-1,) + shp[-1:] )
                    c_samps = samps[:, m, :]
                    if target_samples > c_samps.shape[1]:
                        print 'pulling more sampling'
                        ex_samps = self.baseline_samples(
                            n=target_samples-c_samps.shape[1], 
                            index=(b_pre, b_post), mode='pre,post'
                            )
                        c_samps = np.concatenate( (c_samps, ex_samps), axis=1)
                    x_dist = mahalanobis(c_samps, x_sl)
                    dists[d_sl] = x_dist.reshape(shp[:-1])
                dists.shape = len(dists), -1
                # why reduce this?
                if single_trials and not reshape:
                    valid = np.where(np.isfinite(dists.sum(0)))[0]
                    dists = dists.take(valid, axis=1)
            else:
                if target_samples > samps.shape[1]:
                    ex_samps = self.baseline_samples(
                        n=target_samples-samps.shape[1], 
                        index=(b_pre, b_post), mode='pre,post'
                        )
                    samps = np.concatenate( (samps, ex_samps), axis=1 )
                dists = mahalanobis(samps, x[..., ep_mn:ep_mx])
            return dists

        # this key should be based on the absolute (not relative) times!
        key = (self.peak_min, self.peak_max, b_pre, 
               target_samples, self.epoch_filter, single_trials, reshape)
        if r is not None:
            return score_fn(r, known_order=False)
        return self._get_cached_score(
            score_fn, key, self._mahal_cache, 
            reshape=reshape, single_trials=single_trials
            )

    @property
    def score_fn(self):
        if self.scoring == 'ptp':
            return partial(self.peak_to_peak, points=False)
        elif self.scoring == 'pk-bl':
            return self.peak_to_baseline
        elif self.scoring == 'rms':
            return self.rms
        elif self.scoring == 'mahal':
            return self.mahalanobis
        elif self.scoring == 'linelength':
            return self.linelength
    
    def ep_score(
            self, reshape=True, single_trials=False, avg_single_trials=False
            ):
        """Return 'evoked-potential' scores based on current score mode.

        Parameters
        ----------
        reshape : {True/False}
            Organize scores based on conditions and levels
        single_trials : {True/False}
            Score trial-by-trial responses or trial-averaged responses.
        avg_single_trials : {True/False}
            Return all trial scores, or average scores over trials

        Returns
        -------
        score : ndarray
            Shape depends on parameters

        """
        score = self.score_fn(reshape=reshape, single_trials=single_trials)
        if single_trials and avg_single_trials:
            return np.nanmean(score, axis=-1)
        return score

    def baseline_samples(self, n=None, mode='isi', index=()):
        """
        Gather baseline samples with optional specifications:

        Default: gather slices from each pre-stimulus period using
        the _false_epoch_window() method

        Modifications:

        Parameters
        ----------
        mode : str [ 'isi' | 'pre' | 'post' | 'pre,post' ]
            Sets whether to take inter-stimulus samples ('isi'), or
            samples before/after the experiment ('pre' and 'post' 
            or 'pre,post')
        index : integers (a, b)
            Sets the pre-stim and post-stim interval specification. If
            the mode is set for samples outside of the experiment,
            then the sum of the two numbers will be used to determine
            the sample length. By default, use the same sampling
            scheme as the current epoch window. 
        n : int
            Draw this many samples. If n is greater than the number of
            available samples (e.g. inter-stimulus periods), then the
            remaining samples will be drawn from before/after the
            experiment 

        Returns
        -------
        baseline : ndarray (n_chan, n, n_time)
        
        """

        if not len(index):
            pre_stim, post_stim = self._false_epoch_window()
        else:
            pre_stim, post_stim = index
        if not n:
            n = len(self.exp)
        else:
            n = int(n)

        data = self.dataset.data
        exp = self.exp

        # The default sequence is to get 'isi' samples, and then
        # supplement with pre/post samples if necessary.  If *all*
        # baseline samples are requested, then go through this same
        # procedure as if a very large # of samples are requested.
        raise_ = True
        if mode == 'all':
            mode = 'isi'
            n = data.shape[1]
            raise_ = False
        
        if mode == 'isi':
            baseline = tfun.extract_epochs(
                data, self.exp, pre=pre_stim, post=post_stim
                )
            if n < len(self.exp):
                idx = np.random.choice(np.arange(len(exp)), n, replace=False)
                baseline = baseline.take(idx, axis=1)
            if n > len(self.exp):
                mode = 'pre,post'
            else:
                if self.epoch_filter:
                    baseline = self.epoch_filter(baseline)
                return baseline
        else:
            baseline = ()

        modes = mode.split(',')
        extra_samples = list()
        n_pts = int(np.round(post_stim + pre_stim))
        pre_stim = int(pre_stim)
        if 'pre' in modes:
            cutoff = self.exp.time_stamps[0] - int(self.dataset.Fs)
            n_samps = cutoff // n_pts
            extra_samples.append( np.reshape(
                data[:, :n_samps*n_pts], (len(data), n_samps, n_pts)
                ) )
        if 'post' in modes:
            cutoff = self.exp.time_stamps[-1] + int(self.dataset.Fs)
            n_samps = (data.shape[-1] - cutoff) // n_pts
            extra_samples.append( np.reshape(
                data[:, cutoff:cutoff + n_samps*n_pts],
                (len(data), n_samps, n_pts)
                ) )
        
        extra_samples = np.concatenate(extra_samples, axis=1)

        m = n - baseline.shape[1] if len(baseline) else n
        try:
            idx = np.random.choice(
                np.arange(extra_samples.shape[1]), m, replace=False
                )
            extra_samples = extra_samples.take(idx, axis=1)
        except:
            if raise_:
                raise RuntimeError(
                    'There is not enough unstimulated recording to '
                    'generate {0} samples'.format(n)
                    )
        if len(baseline):
            extra_samples = np.concatenate((baseline, extra_samples), axis=1)
        if self.epoch_filter:
            extra_samples = self.epoch_filter(extra_samples)
        return extra_samples
        
    def epochs(
            self, epochs_mask=(), arranged=None, chan=-1,
            baseline=False, subresponse=False, mask_outliers=False
            ):
        """
        Return some or all experiment epochs, on all or one channel.

        Parameters
        ----------
        epochs_mask : ndarray
            Boolean mask or index-sequence specifying requested epochs
        arranged : bool or sequence
            If simply "True", then return epochs ordered by the 
            enumerated condition number. If a sequence, then that
            sequence specifies the (name, value) of any stim parameters
            that are to be fixed. The epochs that are returned correspond
            to the un-fixed parameters, in order of ascending value.
        chan : int
            Only returns data for the stated channel.
        baseline : bool
            If True, returns baseline epochs of equal length to the
            current epoch interval, but shifted according to the
            'false_epoch_offset' attribute.
        subresponse : bool
            If True, slice the epoch only to include the "scored" portion.
            This also affects baseline epochs.
        mask_outliers : bool or scalar
            If True and the 'epoch_outlier_threshold' is positive, mask 
            trials based on RMS signal power. If a scalar is given,
            the 'epoch_outlier_threshold' is over-ridden for this call.
            A MaskedArray is returned if 'mask_outliers' is used.

        Returns
        -------
        epochs : ndarray
            Queried epochs shaped (n_chan, n_trial, n_pts)
        group_sizes : list
            If 'arranged' is used, these are the number of trials
            belonging to each successive condition.
        """

        exp = self.exp
        
        if len(epochs_mask):
            # override whatever other arrangement requested
            arranged = None

        # Find out how to arrange the results, 
        # either by ordering events by a single stim parameter,
        # or by ordering all events by condition label
        if arranged:
            if not isinstance(arranged, (tuple, list)):
                # Use "slow" sorting of conditions, to
                # preserve trial order within each same-condition set
                arranged = ()
            epochs_mask, groups = ordered_epochs(
                exp, arranged, group_sizes=True
                )                    
        else:
            groups = None
        # Find out what data to use:
        # Can slice out channel if
        # * no epoch filter
        # * epoch filter does not need channels
        b = (self.epoch_filter is None or not self.epoch_filter.needs_channels)
        if chan >= 0 and b:
            d = self.dataset.data[chan]
        else:
            d = self.dataset.data

        #  Finally grab the slices
        if baseline:
            pre, post = self._false_epoch_window(samps=True)
        else:
            pre, post = self._epoch_window(samps=True)
        # epochs gain a dummy dimension if data strip is 1D
        epochs = tfun.extract_epochs(
            d, exp, selected=epochs_mask, pre=pre, post=post
            )
        epochs = epochs.squeeze()
        if self.epoch_filter is not None:
            epochs = self.epoch_filter(epochs)
            if chan >= 0 and epochs.ndim > 2:
                epochs = epochs[chan]
        if subresponse:
            s1, s2 = self.timebase().searchsorted( 
                (self.peak_min, self.peak_max) 
                )
            epochs = epochs[..., s1:s2]

        if mask_outliers:
            if isinstance(mask_outliers, bool):
                iqr_thresh = self.epoch_outlier_threshold
            else:
                iqr_thresh = mask_outliers
            if iqr_thresh > 0:
                e_shape = epochs.shape
                rms = epochs.std(-1).ravel()
                mask = fenced_out(rms, thresh=iqr_thresh).reshape(e_shape[:-1])
                mask = ~mask[...,None] * np.ones(e_shape[-1], dtype='?')
                epochs = np.ma.masked_array(epochs, mask)
        if groups is not None:
            return epochs, groups
        return epochs

    def rank_sum_z(self, n_boot=100, pcrit=1e-4, reshape=True):
        """Return z-scores for conditions and levels

        These z-scores are the standard approximation of rank-sum
        statistics between current response-waveform scores (e.g. rms,
        mahal, etc) and the same score made from baseline samples. The
        rank-sum statistic is computed over bootstrap resampling
        'n_boot' times to stabilize the standard approximation and to
        mitigate spurious scores due to multiple comparisons.

        Returns
        -------
        zscores : ndarray
            Shape depends on 'reshape' parameter, but single-trial 
            scores are not applicable

        """
        # this is similar enough to an EP score that it will use some
        # of the same machinery
        key = (self.scoring, self.peak_min, 
               self.peak_max, n_boot,
               self.false_epoch_offset)
        def score_fn(evoked):
            # evoked has samples listed in the unrolled conditions shape
            # groups has the correct number of samples per condition
            _, groups = ordered_epochs(self.exp, (), group_sizes=True)
            evoked = equalize_groups(evoked, groups, axis=1, reshape=False)
            evoked = self.score_fn(evoked).reshape(len(evoked), len(groups), -1)
            baseline = self.score_fn(self.epochs(baseline=True))
            survived, zscores = active_sites(
                baseline, evoked, pcrit=pcrit, n_boot=n_boot
                )
            ## if reshape:
            ##     cshape = self.exp.rolled_conditions_shape()
            ##     zscores.shape = (zscores.shape[0],) + cshape
            return zscores
        return self._get_cached_score(
            score_fn, key, self._rank_sum_z_cache, 
            reshape=reshape, single_trials=True
            )

    ### ---- UI Handlers ----
    def _replot_map_fired(self):
        self.change_map()

    def change_map(self, score=(), **aplot_extra):
        """
        Remap the array based on the current dominant condition score
        """
        st = not self.score_avg_ep
        score = score if len(score) else \
          self.ep_score(single_trials=st, reshape=True)
        while score.ndim > 2:
            score = np.nanmean(score, axis=-1)
        d_tab = self.exp.enum_tables[0]
        aplot_extra.setdefault('cmap', self._colors[d_tab])
        if score.shape[-1] > 1:
            best_category, _ = nroots_average(score)
            self.array_plot.update_map(best_category, **aplot_extra)
        else:
            score = score.ravel()
            aplot_extra.setdefault('clim', np.percentile(score, [2, 98]))
            c_label = 'Score ({0})'.format(self.scoring)
            aplot_extra.setdefault('c_label', c_label)
            self.array_plot.update_map(score, **aplot_extra)

    @on_trait_change('selected_site')
    def _change_ep_plot(self):
        if self._sites_inactive:
            return
        if self.selected_site < 0:
            # go back to grand averages
            plt_avg = np.nanmean(self.ep_average(reshape=False), axis=1).T
            colors = self._colors['allchan']
        else:
            # XXX: since the base object is ignorant of the semantics of
            # the rolled condition shape, it uses unrolled shape here
            plt_avg = self.ep_average(reshape=False)[self.selected_site]
            plt_avg = plt_avg.transpose()
            # use the dominant condition color??
            colors = self._colors.values()[0]
        self.change_ep_plot(plt_avg, colors)

    def change_ep_plot(self, traces, colors):
        """
        Updates the timeseries trace(s).

        * traces : 1- or 2-d array, plottable by Axes.plot
        * colors : list of colors for each trace
        """
        lines = self.ep_plot.static_artists[:]
        self.ep_plot.remove_static_artist(lines)
        # this is effectively removing any pk_scatter, so mark it as gone
        self._pk_scatter = None

        tx = np.arange(len(traces)) / self.dataset.Fs + self.epoch_start
        new_lines = self.ep_plot.create_fn_image(
            traces, t=tx, color=colors, lw=0.5
            )
        self.ep_plot.add_static_artist(new_lines)
        mn = np.nanmin(traces)
        mx = np.nanmax(traces)
        #ymin = 10 * (np.abs(mn)//10 + 1) * np.sign(mn)
        #ymax = 10 * (np.abs(mx)//10 + 1) * np.sign(mx)
        ymin, ymax = mn, mx
        self.ep_plot.ylim = (ymin, ymax)
        self.ep_plot.xlim = self.epoch_start, self.epoch_end
        self._toggle_peaks()
        self.ep_plot.draw()

    @on_trait_change('plot_peaks')
    def _toggle_peaks(self):
        if self.scoring != 'ptp':
            return
        if not (self.plot_peaks or self._pk_scatter):
            return
        if self.selected_site < 0:
            return
        if self._pk_scatter:
            self.ep_plot.remove_static_artist(self._pk_scatter)
            self._pk_scatter = None
            self.ep_plot.draw()
            return
        p2p, neg_pk, pos_pk = self.peak_to_peak(points=True, reshape=False)
        neg_pk = neg_pk[self.selected_site]
        pos_pk = pos_pk[self.selected_site]
        avg = self.ep_average(reshape=False)[self.selected_site]
        tx = np.arange(avg.shape[-1]) / self.dataset.Fs + self.epoch_start

        same = neg_pk == pos_pk
        same_pk = pos_pk[same]
        neg_pk = neg_pk[~same]
        pos_pk = pos_pk[~same]
        
        tn = np.take(tx, neg_pk)
        vn = avg[ (range(len(neg_pk)), neg_pk) ]
        tp = np.take(tx, pos_pk)
        vp = avg[ (range(len(pos_pk)), pos_pk) ]

        neg_scatter = self.ep_plot.ax.scatter(
            tn, vn, 20, (0,0,0), edgecolor='none'
            )
        pos_scatter = self.ep_plot.ax.scatter(
            tp, vp, 20, (0.6, 0.6, 0.6), edgecolor='none'
            )
        if len(same_pk):
            sp = np.take(tx, same_pk)
            vs = avg[ (range(len(same_pk)), same_pk) ]
            same_scatter = self.ep_plot.ax.scatter(
                sp, vs, 25, '#5BFA11', edgecolor='none'
                )
            self._pk_scatter = (neg_scatter, pos_scatter, same_scatter)
        else:
            self._pk_scatter = (neg_scatter, pos_scatter)
        
        self.ep_plot.add_static_artist(self._pk_scatter)
        self.ep_plot.draw()

    @on_trait_change('scoring')
    def _deactivate_peaks(self):
        if self.scoring != 'ptp' and self._pk_scatter:
            self.ep_plot.remove_static_artist(self._pk_scatter)
            self._pk_scatter = None
            self.ep_plot.draw()
        else:
            self._toggle_peaks()
                        
    # --- UI code ---    
    def _post_canvas_hook(self):
        self.array_plot.fig.canvas.mpl_connect(
            'button_press_event', self.array_plot.click_listen
            )
        self.ep_plot.connect_live_interaction()

    def default_traits_view(self):
        v = View(
            Item(
                'array_plot', editor=MPLFigureEditor(), 
                show_label=False, width=210, height=180,
                resizable=True
                ),
            Item('_'),
            Item(
                'ep_plot', editor=MPLFigureEditor(), 
                show_label=False, width=210, height=135,
                resizable=True
                ),
            HGroup(
                VGroup(
                    Item('scoring', label='EP Scoring'),
                    Item('false_epoch_offset', label='Baseline', style='simple')
                    ),
                VGroup(
                    Label('Epoch outlier thresh'),
                    UItem('epoch_outlier_threshold')
                    ),
                VGroup(
                    Item(
                        'plot_peaks', label='Plot peaks', 
                        enabled_when='selected_site >= 0 and scoring == "ptp"'
                        ),
                    Item('peak_min', show_label=False, 
                         style='readonly', format_str='%.4f'),
                    Item('peak_max', show_label=False, 
                         style='readonly', format_str='%.4f')
                    ),
                spring,
                Item('score_avg_ep', label='Score Avg EP'),
                Item('replot_map', show_label=False)
                ),
            Item('_'),
            VGroup(
                Item(
                    'modules', style='custom',
                    editor=ListEditor(
                        use_notebook=True, deletable=False, 
                        dock_style='tab', page_name='.name'
                        )
                    ),
                    show_labels=False,
                    show_border=True,
                    label='Modules'
                    ),
            resizable=True, title='Analysis',
            handler=PingPongStartup
            )
        return v

