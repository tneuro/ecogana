import numpy as np
from scipy.linalg import svd, toeplitz
from scipy.fftpack import fft, ifft

from traits.api import HasTraits, Enum, List, Instance, Button, Int, String, \
     Float, Bool, on_trait_change, Range, Tuple, property_depends_on, Property
from traitsui.api import View, Item, UItem, Handler, Label, Group, VGroup, HGroup

from .modules import AnalysisModule
from ecoglib.util import input_as_2d
from ecoglib.filt.time import slepian_projection, moving_projection, \
     harmonic_projection
from ecogana.anacode.ep_scoring import Mahalanobis
    
# -------- Define the actual filters here -------- #    
class FiltersArray(object):
    """Base class for callable objects that filter arrays."""
    
    name = 'Pass-Thru'
    needs_array = False
    needs_channels = False

    def __repr__(self):
        return self.name
    
    def __call__(self, array):
        return array

class CachedProjection(FiltersArray):
    """Performs "projection" filtering onto lowpass/bandpass subspaces.

    Note
    ----
    See ecoglib.filt.time.projection_filters.moving_projection docstring.
    
    """

    needs_array = False
    needs_channels = False
    name = 'Slepian Projection'
    
    def __init__(self, N, BW, taper=True, **kwargs):
        self._saved_dpss = None
        self._args = (N, BW)
        self._kwargs = kwargs
        if taper:
            self.taper = np.hanning(2*N)
        else:
            self.taper = ()
        
    def __call__(self, array):
        # just wrap the function call to always save dpss
        # and to provide the dpss stored here
        N = self._args[0]
        if len(self.taper):
            # pre-weight the ends of the signal to avoid
            # edge effects
            array[..., :N] *= self.taper[:N]
            array[..., -N:] *= self.taper[N:]
        cplx = self._kwargs['f0'] > 0 and self._kwargs['baseband']
        # if the timeseries length has changed up then clear the cache
        if self._saved_dpss is not None:
            if N != self._saved_dpss[0].shape[-1]:
                self._saved_dpss = None
        self._kwargs['dpss'] = self._saved_dpss
        self._kwargs['save_dpss'] = True
        args = (array,) + self._args
        y, dpss = moving_projection(*args, **self._kwargs)
        self._saved_dpss = dpss
        if cplx:
            y = np.abs(y)
        return y
        
        ## for i, sub_array in enumerate(array):
        ##     args = (sub_array,) + self._args
        ##     #f_array, dpss = slepian_projection(*args, **self._kwargs)
        ##     if cplx:
        ##         y, dpss = moving_projection(*args, **self._kwargs)
        ##         f_array[i] = np.abs(y)
        ##     else:
        ##         f_array[i], dpss = moving_projection(*args, **self._kwargs)
        ##     self._saved_dpss = dpss
        ## return f_array
    
class ForgetfulWhitening(FiltersArray):
    """Applies "causal" whitening of a signal with an eclipse time constant.

    The whitening transform is created from the Cholesky factors of
    a signal's covariance matrix. Because of the triangular form of
    the factors, a present sample in the output only depends on linear
    combinations of past samples (therefore it is "causal").
    
    """

    name = 'Forgetful Whitening'
    needs_array = False
    # needs the full array because one filter is learned per channel
    needs_channels = True
    
    def __init__(self, wfilters, means, forget_factor=-1.0):
        self.wfilters = np.array(wfilters)
        self.means = means
        if forget_factor > 0:
            x = np.arange( len(wfilters[0]), dtype='d' )
            W = toeplitz( x )
            W = np.exp( -0.5 * (W / forget_factor)**2 )
            self.wfilters *= W

    def __call__(self, x):
        # x is nchan, ntrial, npt
        xw = list()
        for x_, mu_, w_ in zip(x[..., ::-1], self.means, self.wfilters):
            xw_ = (x_-mu_).dot(w_)
            xw.append( xw_[..., ::-1] )
        return np.array(xw)

class MahalanobisExpansion(FiltersArray):
    """Expand covariance-normalized variates on the covariance eigenvectors.

    This filter takes the covariance-normalized samples from a
    Mahalanobis distance transformation and expands them in the
    sample's original timeseries space using the eigenvectors of the
    covariance matrix. Since this transformation is length-preserving,
    the size of the timeseries vector is equal to the Mahalanobis
    distance of the timeseries.

    """

    name = 'Mahalanobis Expansion'
    needs_array = False
    needs_channels = True
    
    def __init__(self, ana_obj):
        self.ana_obj = ana_obj
        self.__replace_mahal()

    def __replace_mahal(self):
        # hot swap!!
        self.ana_obj.epoch_filter = None
        baseln = self.ana_obj.epochs(baseline=True)
        self.ana_obj.epoch_filter = self
        self.mahal = Mahalanobis(baseln)

    def __call__(self, x):
        C = x.shape[0] if x.ndim > 2 else 1
        T = x.shape[-1]
        if ( C != len(self.mahal.n_mean) ) or ( T != self.mahal.vec_len ):
            self.__replace_mahal()
        return self.mahal.expand_projection(x)
    
class HarmonicProjection(FiltersArray):
    """Subtracts a robustly fit sinusoid from a short timeseries."""
    
    name = 'Harmonic subtraction'
    needs_array = False
    needs_channels = False

    def __init__(self, lines, stdevs=2):
        self._lines = lines
        self._stdevs = stdevs

    def __call__(self, array):
        return harmonic_projection(array, self._lines, stdevs=self._stdevs)

class SVDProjection(FiltersArray):
    """Projections from principal modes of the channel x time matrix"""
    
    name = 'SVD Projection'
    # Topos & Chronos modes are created by the channel x time matrix
    needs_array = True
    needs_channels = True

    def __init__(self, n_modes=1, skip_first=0, skip_last=0):
        #self.n_modes = n_modes
        self._mode_select = (n_modes, skip_first, skip_last)
        self._decomps = dict()

    def _make_slice(self, ashape):
        m, b, n = ashape
        rank = min(m, n)
        # given (P, Q, R) the possible ranges are
        # first P modes
        # all but first Q modes
        # all but last R modes (this case ignores P, Q)
        # first P modes after skipping first Q modes
        p, q, r = self._mode_select
        if r > 0: # (case iii)
            sl = ( slice(None), slice(0, rank-r) )
            sl_t = slice(0, rank-r)
            return sl, sl_t
        if q > 0: # (cases ii and iv)
            end = rank if p == 0 else q + p
            if end > rank:
                print 'Requested', end, 'modes, but only rank', rank, 'matrix'
                end = rank
            sl = ( slice(None), slice(q, end) )
            sl_t = slice(q, end)
            return sl, sl_t
        sl = ( slice(None), slice(0, p) )
        sl_t = slice(0, p)
        return sl, sl_t

    def __call__(self, array):
    
        u_slice, v_slice = self._make_slice(array.shape)
        d_proj = np.empty_like(array)
        a_hash = hash(array.tostring())
        decomps = self._decomps.get(a_hash, list())
        for b in xrange(array.shape[1]):
            if not a_hash in self._decomps:
                u, s, vt = svd(array[:, b, :], full_matrices=False)
                decomps.append( (u, s, vt) )
            else:
                u, s, vt = decomps[b]
            #dp = (u[:, :self.n_modes] * s[:self.n_modes]).dot(vt[:self.n_modes])
            dp = (u[u_slice] * s[v_slice]).dot(vt[v_slice])
            d_proj[:, b, :] = dp
        if not a_hash in self._decomps:
            self._decomps[a_hash] = decomps
        return d_proj        

class FourierProjection(SVDProjection):
    """SVD mode projection of channel x frequency matrices."""

    def __call__(self, array):
        d_proj = super(FourierProjection, self).__call__( fft(array) )
        return ifft(d_proj).real.copy()

# Note: Currently these "baseline-subtraction" methods only work 
#       as-intended with arranged epochs. For other epoch selections,
#       the baseline epoch will not match the response epoch.

class ResidualOfBaseline(SVDProjection):
    """Residual after projection onto baseline modes."""

    def __init__(self, baseline, **kwargs):
        super(ResidualOfBaseline, self).__init__(**kwargs)
        decomps = list()
        for b in xrange(baseline.shape[1]):
            u, s, vt = svd(baseline[:, b, :], full_matrices=False)
            decomps.append( (u, s, vt) )
        self._decomps = decomps
    
    def __call__(self, array):
        u_slice, v_slice = self._make_slice(array.shape)
        d_proj = array.copy()
        decomps = self._decomps
        for b in xrange(array.shape[1]):
            u, s, vt = decomps[b]
            dp = array[:, b, :].dot(vt[v_slice].T)
            d_proj[:, b, :] -= dp.dot(vt[v_slice])
        return d_proj

class FourierResidualOfBaseline(ResidualOfBaseline):
    """Residual after projection onto baseline frequency modes."""
    
    def __init__(self, baseline, **kwargs):
        m_base = np.abs(fft(baseline))
        super(FourierResidualOfBaseline, self).__init__(m_base, **kwargs)

    def __call__(self, array):
        array = fft(array)
        m_resp = np.abs(array)
        d_proj = super(FourierResidualOfBaseline, self).__call__( m_resp )
        d_proj = d_proj * np.exp(1j * np.angle(array))
        return ifft(d_proj).real.copy()
    
# ---- Define the objects that handle UI and building FiltersArray ---- # 
   
class BaseFilter(HasTraits):
    """Basic UI panel for filter creation."""
    manager = Instance(HasTraits)

    def create_filter(self):
        # Returns a callable function filt(data) that filters the data
        # array in the last dimension.
        return None

    def default_traits_view(self):
       return View(Group(label='No params'))
    
class ProjectionFilter(BaseFilter):
    """Creates a projection filter"""
    
    lo_freq = Float(0)
    block_len = Int(100)
    hi_freq = Float(-1)
    baseband = Bool(True)
    taper = Bool(True)
    min_bw = Property(Float)

    @property_depends_on('block_len')
    def _get_min_bw(self, *args):
        T = self.block_len / self.manager.ana.dataset.Fs 
        # TW > 0.5 --> W > 0.5 / T
        return 0.5 / T if T != 0 else np.inf
    
    def create_filter(self):
        # BW is half-bandwidth..
        # e.g. BW=50 for 50 Hz lowpass filter
        # e.g. BW=50 for 100-200 Hz bandpass filter
        if self.lo_freq > 0:
            BW = 0.5 * (self.hi_freq - self.lo_freq)
            f0 = 0.5 * (self.hi_freq + self.lo_freq)
        else:
            BW = self.hi_freq
            f0 = 0
        Fs = self.manager.ana.dataset.Fs
        fn = CachedProjection(self.block_len, BW, Fs=Fs, 
                              f0=f0, baseband=self.baseband, taper=self.taper)
        return fn

    def default_traits_view(self):
        v = View(
            VGroup(
                Item('lo_freq', label='Low Cutoff (0 for LPF)'),
                Item('hi_freq', label='High Cutoff'),
                Item('block_len', label='Block Length (Samples)'),
                Item('taper', label='Taper window ends'),
                Item('baseband', label='Baseband Recon.',
                     enabled_when='lo_freq>0'),
                Item('min_bw', label='Minimum BW', style='readonly'),
                label='Projection Filter Settings'
                )
            )
        return v

    
class MultivariateWhiten(BaseFilter):
    """Creates a multivariate whitening filter"""

    forget_time = Float(-1.0)
    
    def create_filter(self):
        ana = self.manager.ana
        return MahalanobisExpansion(ana)

class HarmonicFilter(BaseFilter):
    """Creates a harmonic filter"""
    
    h_freqs = Tuple
    stdevs = Float(2)
    entry = Float
    add_line = Button('Add notch')

    def _add_line_fired(self):
        self.h_freqs = self.h_freqs + (self.entry,)
    
    def create_filter(self):
        Fs = self.manager.ana.dataset.Fs
        lines = np.array(self.h_freqs, 'd') / Fs
        return HarmonicProjection(lines, self.stdevs)
        ## def _sub_freqs(x):
        ##     for f in map(float, self.h_freqs):
        ##         x = harmonic_projection(x, f/Fs)
        ##     return x
        ## return _sub_freqs

    def default_traits_view(self):
        v = View(
            Item('h_freqs', label='Notch Frequencies', style='readonly'),
            Item('stdevs', label='Mask threshold (stdevs)'),
            HGroup( UItem('entry'), UItem('add_line') )
            )
        return v

class SVDFilter(BaseFilter):
    """Creates a SVD projection filter"""

    n_modes = Int(1)
    skip_first = Int(0)
    skip_last = Int(0)

    proj_type = SVDProjection
    
    def create_filter(self):
        return self.proj_type(n_modes=self.n_modes, 
                             skip_first=self.skip_first,
                             skip_last=self.skip_last)

    def default_traits_view(self):
        v = View(
            Item('n_modes', label='Number of modes'),
            Item('skip_first', label='Skip first n modes'),
            Item('skip_last', label='Skip last n modes')
            )
        return v

class FourierSVDFilter(SVDFilter):
    """Creates a SVD frequency-domain projection filter"""
    
    proj_type = FourierProjection

class BaselineSVDFilter(SVDFilter):
    """Creates a SVD residual projection filter"""

    proj_type = ResidualOfBaseline
    
    def create_filter(self):
        filt = self.manager.ana.epoch_filter
        self.manager.ana.epoch_filter = None
        baseline = self.manager.ana.epochs(baseline=True)
        self.manager.ana.epoch_filter = filt
        return self.proj_type(baseline, n_modes=self.n_modes,
                              skip_first=self.skip_first,
                              skip_last=self.skip_last)

class FourierBaselineSVDFilter(BaselineSVDFilter):
    """Creates a SVD frequency-domain residual projection filter"""

    proj_type = FourierResidualOfBaseline
    
class ParamsHandler(Handler):
    params_lut = {
        'none' : BaseFilter,
        'projection' : ProjectionFilter,
        'whiten' : MultivariateWhiten,
        'harmonics' : HarmonicFilter,
        'svd' : SVDFilter,
        'fourier svd' : FourierSVDFilter,
        'baseline svd' : BaselineSVDFilter,
        'baseline fr svd' : FourierBaselineSVDFilter
        }
    
    def object_filter_name_changed(self, info):
        ptype = self.params_lut[ info.object.filter_name.lower() ]
        info.object.filt = ptype(manager=info.object)

class Filters(AnalysisModule):
    """Module to create filters that act on the present Analysis epochs.

    This module sets the 'epoch_filter' attribute of the Analysis
    object. Filters are created through the BaseFilter types defined
    in this module. BaseFilter objects in turn create callable
    FiltersArray objects that process array timeseries generated by
    the Analysis object.

    """
    
    name = 'Filters'
    filter_name = Enum('None', ('None', 'Projection', 
                                'Whiten', 'Harmonics', 'SVD',
                                'Fourier SVD', 'Baseline SVD',
                                'Baseline Fr SVD'))
    filt = Instance(BaseFilter())
    send_filter = Button('Apply Filter')
    current_filter = Property(String)

    @property_depends_on('ana:epoch_filter')
    def _get_current_filter(self, *args):
        return str(self.ana.epoch_filter)
        
    def _send_filter_fired(self):
        self.ana.epoch_filter = self.filt.create_filter()

    def default_traits_view(self):
        v = View(
            HGroup(
                VGroup(
                    Item('filter_name', label='Filter'), 
                    UItem('send_filter'), 
                    Label('Current filter:'),
                    UItem('current_filter', style='readonly'),
                    show_border=True
                    ),
                Group(
                    Item('filt', style='custom'),
                    show_border=True, show_labels=False
                    )
                ),
            handler=ParamsHandler(),
            resizable=True
            )
        return v
