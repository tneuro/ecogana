import os
import os.path as osp

import gc
import numpy as np

from traits.api import HasTraits, Str, Enum, List, Instance, Button, Int, \
     Float, Array, Enum, Bool, on_trait_change, Range, File, Tuple
from traitsui.api import View, Item, UItem, Handler, EnumEditor, \
     TextEditor, SetEditor, Include, Group, VGroup, HGroup, \
     CheckListEditor, CustomEditor, spring, ListEditor, RangeEditor

from .base import Analysis

from ecogana.devices.data_util import load_experiment_auto, join_datasets
from ecogana.devices import current_tools as ct
from ecogana.anacode.signal_testing import safe_avg_power, bad_channel_mask
from ecoglib.filt.transforms import lowpass_envelope

fake_sessions = ('-no sessions, demo only-',)

class AnalysisSetup(HasTraits):
    """
    This object organizes sessions and their recordings, and
    launches Analysis windows.
    """

    # this is purely nominal and must be overloaded in subclasses
    session = Enum(fake_sessions[0], fake_sessions)

    proc_runs = List
    save = Bool(False)
    units = Str('uV')
    auto_prune = Bool(False)
    manu_prune = Bool(False)
    normalize = Bool(False)
    car = Bool(False)
    integrate_amps = Bool(False)
    
    proc_bandpass = Tuple(2.0, 150.0)
    proc_notches = Str
    proc = Button('Analyze')

    scroll_len = Float(2.0)
    scroll = Button('Scroller')
    _can_use_scroller = Bool(False)

    ana_class = Analysis

    def __init__(self, **traits):
        # a length-1 working set of data
        self._dset_cache = (None, None)
        super(AnalysisSetup, self).__init__(**traits)
        from ecoglib.vis.data_scroll import ChannelScroller
        if ChannelScroller is None:
            self._can_use_scroller = False
        else:
            self._can_use_scroller = True

            
    def _get_dataset_params(self):
        """
        Returns "common" dataset loading parameters:

        * session (non-optional)
        * list-of-runs (non-optional)
        * keyword dictionary:
          + bandpass settings
          + notch filter freqs
          + save (t/f)
          + snip_transient (t/f)
          + units (string code)

        Downstream objects can add additional keywords to the dictionary
        """
        
        runs = [r.split(',')[0] for r in self.proc_runs]
        if self.proc_notches:
            notches = map(float, self.proc_notches.split(','))
        else:
            notches = ()
        # string together the args and kwargs used to load the data,
        # and compare it against the working memory set

        args = (self.session, runs)
        kwargs = dict(
            bandpass=self.proc_bandpass, notches=notches,
            save=self.save, snip_transient=True, units=self.units
            )
        post_proc = dict(
            auto_prune=self.auto_prune, manu_prune=self.manu_prune,
            car=self.car, normalize=self.normalize,
            integrate_amps=self.integrate_amps
            )
        key = args + (kwargs, post_proc)
        return key

    def ana_opts(self):
        return dict()

    def _get_dataset(self):
        params = self._get_dataset_params()

        if params == self._dset_cache[0]:
            return self._dset_cache[1]
        
        session, runs, kwargs, post_proc = params
        dsets = [load_experiment_auto(session, r, **kwargs) for r in runs]
        for d in dsets:
            if self.auto_prune:
                rms = safe_avg_power(d.data, int(d.Fs), iqr_thresh=8)
                # slightly conservative 4x IQR
                mask = bad_channel_mask(np.log(rms), iqr=4)
                d.data = d.data[mask]
                d.chan_map = d.chan_map.subset(mask.nonzero()[0])
            elif self.manu_prune:
                from ecogana.devices.channel_picker import interactive_mask
                mask = interactive_mask(d)
                d.data = d.data[mask]
                d.chan_map = d.chan_map.subset(mask.nonzero()[0])
        if self.integrate_amps:
            for d in dsets:
                session = d.name.split('.')[0]
                d.data = ct.convert_amps_to_volts(d, to_unit='uV').data
                # highpass filter again
                ## bp = kwargs['bandpass']
                ## d.data = filter_array(
                ##     d.data, inplace=False,
                ##     design_kwargs=dict(lo=bp[0], hi=bp[1], Fs=d.Fs)
                ##     )
                d.units = 'uV'
        if self.normalize:
            for d in dsets:
                rms = safe_avg_power(d.data, int(d.Fs))
                d.data /= rms[:,None]
                d.units = 'sigma'
        dataset = join_datasets(dsets)
        if self.car:
            # this is primitive
            # dataset.data -= dataset.data.mean(0)
            # this is better
            from ecoglib.filt.time import common_average_regression
            common_average_regression(dataset.data)
        if self.lowpass_env:
            # hilbert transform the data, and then lowpass at the
            # width of the set bandpass
            print 'computing lowpass envelope'
            lp = self.proc_bandpass[1] - self.proc_bandpass[0]
            Fs = dataset.Fs
            ## dataset.data = lowpass_envelope(
            ##     dataset.data, int(2*Fs), 2 * lp / Fs
            ##     )
            dataset.data = lowpass_envelope(
                dataset.data, int(2*Fs), 2.5 * lp / Fs
                )
        self._dset_cache = (params, dataset)
        gc.collect()
        return dataset

    def _scroll_fired(self):
        from ecoglib.vis.data_scroll import ChannelScroller
        if ChannelScroller is None:
            print 'ChannelScroller is unavailable', self._can_use_scroller
            return
        dataset = self._get_dataset()
        scr = ChannelScroller.from_dataset_bunch(dataset, self.scroll_len)
        scr.configure_traits()
        return scr
    
    def _proc_fired(self):
        dataset = self._get_dataset()
        ana = self.ana_class(dataset, **self.ana_opts())
        ana.configure_traits()
        return ana
    
    view = View(
        HGroup(Item(name='session')),
        Item('_'),
        Item( name='proc_runs', show_label=False,
              editor=SetEditor(
                 name='handler.available_runs',
                 left_column_title='session runs',
                 right_column_title='to process'
                 ) ),
        Item('_'),
        HGroup(
            VGroup(
                Item('proc_bandpass', style='simple', label='Bandpass Edges'),
                Item(
                    'proc_notches', style='simple', label='Notch freqs',
                    help='comma-separated list of notches'
                    )
                ),
            spring,
            VGroup(
                Item('units', width=3),
                Item('save', label='Saving')
                ),
            spring,
            VGroup(
                Item(name='proc', label='Tonotopy'),
                Item('_'),
                HGroup(
                    Item('scroll_len', width=3, label='Page sz'),
                    Item('scroll', enabled_when='_can_use_scroller')
                    )
                )
            ),
        title='Analysis Helper',
        resizable=True
        )
