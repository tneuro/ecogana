import itertools
import numpy as np
from matplotlib.collections import LineCollection
import statsmodels.api as sm
import statsmodels.formula.api as smf
import networkx as nx
from ecogana.anacode.colormaps import diverging_cm
import seaborn_lite as sns
sns.reset_orig()

from scipy.optimize import fmin_tnc, minimize, minimize_scalar, \
     curve_fit
import scipy.special as spfn
import math
import scipy.interpolate as interp


from ecoglib.estimation.spatial_variance import binned_variance, binned_variance_aggregate
from ecoglib.util import ChannelMap


def cxx_to_pairs(cxx, chan_map, **kwargs):
    if 'pitch' in kwargs:
        print 'pitch no longer used in cxx_to_pairs'
    if cxx.ndim < 3:
        cxx = cxx[np.newaxis, :, :]
    chan_combs = chan_map.site_combinations
    pairs = zip(chan_combs.p1, chan_combs.p2)
    ix = [x for x,y in sorted(enumerate(pairs), key = lambda x: x[1])]
    idx1 = chan_combs.idx1[ix]; idx2 = chan_combs.idx2[ix]
    dist = chan_combs.dist[ix]
    tri_x = np.triu_indices( cxx.shape[1], k=1 )
    cxx_pairs = np.array([ c_[ tri_x ] for c_ in cxx ])
    return dist, cxx_pairs.squeeze()


# legacy
def binned_obs(x, y, binsize=None):
    return binned_variance(x, y, binsize=binsize)


# legacy
def binned_obs_error(xb, y_tab, type='sem'):
    return binned_variance_aggregate(xb, y_tab, mid_type='mean', scale_type='sem')


def subsample_bins(xb, yb, min_bin=-1, max_bin=-1):
    """
    Sub-sample bins to equalize the group sizes. The minimum group
    size can be specified by min_bin, or will be set by the minimum
    of the provided group sizes. Bins with sizes smaller than the
    minimum will be dropped.
    """

    group_sizes = map(len, yb)
    if min_bin < 0:
        min_bin = min( group_sizes )

    if any( [g < min_bin for g in group_sizes] ):
        nb = len(group_sizes)
        xb = [ xb[i] for i in xrange(nb) if group_sizes[i] >= min_bin ]
        yb = [ yb[i] for i in xrange(nb) if group_sizes[i] >= min_bin ]

    if max_bin > 0:
        min_bin = max_bin

    yb_r = [ np.random.choice(y_, min_bin, replace=False) for y_ in yb ]
    return xb, yb_r
    
def resample_bins(xb, yb, min_bin=10, beta=0.5):
    """
    Do a bootstrap resample within the len(xb) bins in the list yb.
    Only resample if there are at least min_bin elements in a bin,
    otherwise reject the entire bin with probability (1-beta).
    """
    xb = np.asarray(xb)
    bin_size = np.array( map(len, yb) )
    b_mask = bin_size >= min_bin
    yb_r = [ y_[ np.random.randint(0, len(y_), len(y_)) ]
                 for y_, b in zip(yb, b_mask) if b ]
    yb_sm = [ y_ for y_, b in zip(yb, b_mask) if not b ]
    xb_r = xb[ b_mask ]
    keep_small = np.random.rand( len(xb) - b_mask.sum() ) < beta
    xb_r = np.r_[xb_r, xb[ ~b_mask ][ keep_small ]]
    yb_r.extend( [y_ for y_, k in zip(yb_sm, keep_small) if k] )
    return xb_r, yb_r

def concat_bins(xb, yb):
    sizes = map(len, yb)
    x = np.concatenate( [ [x_] * g for x_, g in zip(xb, sizes) ] )
    y = np.concatenate(yb)
    return x, y

def make_line_collection(x, y, cen_fn=np.mean, len_fn=np.std, **lc_kwargs):
    print 'Use covar_to_lines()'
    return covar_to_lines(x, y, cen_fn=cen_fn, len_fn=len_fn, **lc_kwargs)

def covar_to_lines(x, y, cen_fn=np.mean, len_fn=np.std, binsize=None, **lc_kwargs):
    xb, yb = binned_obs(x, y, binsize=binsize)
    y_c = np.array( [ cen_fn(_y) for _y in yb ] )
    y_l = np.array( [ len_fn(_y) for _y in yb ] )

    bin_lines = [ [(xi, yi-li), (xi, yi+li)]
                  for xi, yi, li in zip(xb, y_c, y_l) ]
    return (xb, y_c), LineCollection(bin_lines, **lc_kwargs)

def covar_to_iqr_lines(x, y, binsize=None, **lc_kwargs):
    xb, yb = binned_obs(x, y, binsize=binsize)
    iqr = [np.percentile(y_, [25, 50, 75]) for y_ in yb]
    bin_lines = [ [(x, y[0]), (x, y[2])] for (x, y) in zip(xb, iqr) ]
    lc = LineCollection(bin_lines, **lc_kwargs)
    med = [y[1] for y in iqr]
    return (xb, med), lc

def robust_semivar(x, y, return_x=False):
    # each sample of y is an estimate E{ (s_i - s_j)^2 }
    # convert to | e(s_i) - e(s_j) | ^ 1/2 
    # by taking 4th root?
    #
    xb, yb = binned_obs(x, y)

    rsv = np.zeros_like(xb)
    for n in xrange(len(xb)):
        Nb = len(yb[n])

        num = np.power( yb[n], 0.25 ).mean()
        rsv[n] = num ** 4.0 / (0.914 + 0.988/Nb)
    return (xb, rsv) if return_x else rsv

def exp_eval(x, alpha, x0):
    return np.exp( alpha * (x - x0) )

def wls_fit(x, y, correct_offset=(), logy=False, alpha=95.0):
    # make a grid for predicting
    if len(correct_offset):
        correct_offset = np.array(correct_offset)
        xp = np.linspace(correct_offset[0], np.ceil(x.max()), 200)
    else:
        xp = np.linspace(x.min()/4.0, np.ceil(x.max()), 200)


    def reg_fun(x, y):
        xb, y_tab = binned_obs(x, y)

        if logy:
            y_tab = [np.log(np.abs(_y)) for _y in y_tab]
            ## if len(correct_offset):
            ##     correct_offset[1] = np.log(correct_offset[1])


        # only accept samples with more than 1 observation!!
        y_vals, y_var = map(
            np.array, zip( *[ (_y.mean(), _y.var()) 
                              for _y in y_tab if len(_y) > 1] )
            )
        # s.e.m. weight
        y_var_ = y_var / np.array([len(_y) for _y in y_tab if len(_y) > 1])

        xb = np.array( [xb[i] for i in xrange(len(xb)) if len(y_tab[i]) > 1] )
        # save an unweighted, unshifted copy
        _y_vals = y_vals.copy()
        _xb = xb.copy()

        if len(correct_offset):
            _y_vals -= correct_offset[1]
            _y_vals /= np.sqrt(y_var_)
            _xb -= correct_offset[0]
            _xb /= np.sqrt(y_var_)
            ## reg_func = lambda _x, _y: np.array(
            ##     [0, np.sum(_x[:,1]*_y) / np.sum(_x[:,1]**2)]
            ##     )
            c0 = 0
            c1 = np.sum(_xb*_y_vals) / np.sum(_xb**2)
            return np.array([c0, c1])
        else:
            _y_vals /= np.sqrt(y_var_)
            _x = sm.add_constant(_xb)
            _x /= np.sqrt(y_var_)[:,None]
            ## reg_func = lambda _x, _y: np.linalg.pinv(_x).dot(_y)
            return np.linalg.pinv(_x).dot(_y_vals)

    ## xx = sm.add_constant(_xb)
    ## xx /= np.sqrt(y_var_)[:,None]
    ## coef = reg_func(xx, _y_vals)
    if logy and len(correct_offset):
        correct_offset[1] = np.log(correct_offset[1])
    coef = reg_fun(x, y)
    yp = sm.add_constant(xp).dot(coef)

    ## beta_boots = sns.algorithms.bootstrap(
    ##     xx, _y_vals, func=reg_func, n_boot=250
    ##     )
    beta_boots = sns.algorithms.bootstrap(
        x, y, func=reg_fun, n_boot=250
        )
    beta_boots = beta_boots[ np.isfinite(np.sum(beta_boots, 1)) ]
    coef_ci = sns.utils.ci(beta_boots, alpha, axis=0)
    y_boots = sm.add_constant(xp).dot(coef_ci.T).T

    xb, y_tab = binned_obs(x, y)
    y_vals, y_var = map(
        np.array, zip( *[ (_y.mean(), _y.var()) for _y in y_tab ] )
        )

    if len(correct_offset):
        # reintroduce the correct offset
        yo = correct_offset[0] * coef[1] + coef[0]
        yp -= (yo - correct_offset[1])
        y_boots -= (yo - correct_offset[1])
        coef[0] = correct_offset[1] - coef[1]*correct_offset[0]
        beta_boots[:,0] = correct_offset[1] - beta_boots[:,1]*correct_offset[0]

    if logy:
        # recompute y-variance in the natural space
        yp, y_boots = map(np.exp, (yp, y_boots))

    ## y_boots[0] = yp - y_boots[0]
    ## y_boots[1] = y_boots[1] - yp
    return coef, (xp, yp, y_boots), (xb, y_vals, y_var), beta_boots

def wls_fit_simp(x, y, correct_offset=(), logy=False, alpha=95.0):
    # make a grid for predicting
    if len(correct_offset):
        correct_offset = np.array(correct_offset)
        xp = np.linspace(correct_offset[0], np.ceil(x.max()), 200)
    else:
        xp = np.linspace(x.min()/4.0, np.ceil(x.max()), 200)


    def reg_fun(x, y):
        xb, y_tab = binned_obs(x, y)

        if logy:
            y_tab = [np.log(np.abs(_y)) for _y in y_tab]
            ## if len(correct_offset):
            ##     correct_offset[1] = np.log(correct_offset[1])


        # only accept samples with more than 1 observation!!
        y_vals, y_var = map(
            np.array, zip( *[ (_y.mean(), _y.var()) 
                              for _y in y_tab if len(_y) > 1] )
            )
        # s.e.m. weight
        y_var_ = y_var / np.array([len(_y) for _y in y_tab if len(_y) > 1])

        xb = np.array( [xb[i] for i in xrange(len(xb)) if len(y_tab[i]) > 1] )
        # save an unweighted, unshifted copy
        _y_vals = y_vals.copy()
        _xb = xb.copy()

        if len(correct_offset):
            _y_vals -= correct_offset[1]
            _y_vals /= np.sqrt(y_var_)
            _xb -= correct_offset[0]
            _xb /= np.sqrt(y_var_)
            ## reg_func = lambda _x, _y: np.array(
            ##     [0, np.sum(_x[:,1]*_y) / np.sum(_x[:,1]**2)]
            ##     )
            c0 = 0
            c1 = np.sum(_xb*_y_vals) / np.sum(_xb**2)
            return np.array([c0, c1])
        else:
            _y_vals /= np.sqrt(y_var_)
            _x = sm.add_constant(_xb)
            _x /= np.sqrt(y_var_)[:,None]
            ## reg_func = lambda _x, _y: np.linalg.pinv(_x).dot(_y)
            return np.linalg.pinv(_x).dot(_y_vals)

    if logy and len(correct_offset):
        correct_offset[1] = np.log(correct_offset[1])
    coef = reg_fun(x, y)

    if alpha <= 0:
        return coef
    
    beta_boots = sns.algorithms.bootstrap(
        x, y, func=reg_fun, n_boot=250
        )
    beta_boots = beta_boots[ np.isfinite(np.sum(beta_boots, 1)) ]
    coef_ci = sns.utils.ci(beta_boots, alpha, axis=0)

    return coef, coef_ci, beta_boots


def exponential_fit(
        cxx, chan_map, pitch=1.0, bin_lim=10, Tv=np.log, lsq='linear', 
        nugget=False, sill=False, cov=False
        ):
    if isinstance(chan_map, ChannelMap):
        ## combs = channel_combinations(chan_map)
        ## pairs = zip(chan_combs.p1, chan_combs.p2)
        ## ix = [x for x,y in sorted(enumerate(pairs), key = lambda x: x[1])]
        ## idx1 = chan_combs.idx1[ix]; idx2 = chan_combs.idx2[ix]
        ## dist = chan_combs.dist[ix] * pitch
        ## cxx_pairs = cxx[ np.triu_indices(len(cxx), k=1) ]
        dist, cxx_pairs = cxx_to_pairs(cxx, chan_map)
    else:
        cxx_pairs = cxx
        dist = chan_map

    xb, yb = binned_obs(dist, cxx_pairs)
    #T = np.log
    if Tv is None:
        Tv = lambda x: x
    yv = np.array([np.var(Tv(y_)) if len(y_) > bin_lim else -1 for y_ in yb])
    yv[ yv < 0 ] = np.max(yv)
    # need to put weights and data in the same order
    yv, _ = concat_bins(yv, yb)
    x, y = concat_bins(xb, yb)
    if lsq == 'linear':
        # model (exponential) covariance has full range (0, 1)
        # covariance measures with noise & redundancy have:
        # minimum (floor): 1-sill
        # maximum: 1-nugget
        # So scale measurements by 
        # 1) subtracting (1-sill)
        # 2) expanding by (1-nugget) - (1 - sill) = 1 / (sill - nugget)
        nugget = float(nugget)
        if isinstance(sill, float):
            sill = float(sill)
        else:
            sill = 1.0
        yt = (y - 1 + sill) / ( sill - nugget )
        d_obj = dict(dist=x, cxx=yt)
        wls = smf.wls(
            'I(-np.log(np.abs(cxx))) ~ 0 + dist', d_obj, weights=1/yv
            ).fit()
        lam = 1.0/float(wls.params.dist)
        return (lam, wls) if cov else lam
    else:
        ## def _expdecay(x_, lam_):
        ##     return np.exp(-x_ / lam_)
        def _expdecay(x_, lam_, nug_=0, sill_=1):
            nug_ = nug_ if est_nugget else fixed_nugget
            sill_ = sill_ if est_sill else fixed_sill
            if lam_ < 1e-2 or (est_nugget and nug_ < 1e-6) or (est_sill and sill > 1):
                return np.ones_like(x_) * 1e5
            #lam_ = p[0]
            return (sill_ - nug_) * np.exp(-x_ / lam_) + (1-sill_)
        p0 = [1.0]
        if isinstance(nugget, bool) and nugget:
            est_nugget = True
            #p0 = [1.0, 0.5]
            p0.append(0.5)
        elif isinstance(nugget, float):
            est_nugget = False
            #p0 = [1.0]
            fixed_nugget = nugget
        else:
            est_nugget = False
            #p0 = [1.0]
            fixed_nugget = 0
        if isinstance(sill, bool) and sill:
            est_sill = True
            p0.append(1.0)
        elif isinstance(sill, float):
            est_sill = False
            fixed_sill = sill
        else:
            est_sill = False
            fixed_sill = 1.0
            print
        p, pcov = curve_fit(
            _expdecay, x, y, p0=p0, sigma=np.sqrt(yv),
            absolute_sigma=True
            )
        lam = p[0]
        return (p, pcov) if cov else p

def power_law_eval(x, p):
    a, g = p
    y = (1 + a*x) ** g
    return y

def power_law_fit(x, y):

    # find parameters alpha and gamma
    bnds = [ (0, 5), (-1, -.1) ]

    def _costfn(p, xx, yy):
        errs = yy - power_law_eval(xx, p)
        return np.sum( errs**2 )

    p0 = np.array([1, -1])
    r = fmin_tnc(
        _costfn, p0, args=(x, y), approx_grad=1, bounds=bnds,
        messages=5, maxfun=1000
        )
    return r

def matern_correlation2(
        x, theta=1.0, nu=0.5, sill=1.0, nugget=0.0
        ):
    """
    Parameters are defined in terms of the normalized semivariogram,
    which grows from a minimum of nugget to a maximum of sill.

    y(x, *p) = (sill - nugget) * (1 - MF(x, theta, nu)) + nugget
    
    """
    
    t = np.sqrt(2*nu) * np.abs(x) / theta
    n = 2.0**(1.0-nu) / math.gamma(nu)
    MF = n * np.power(t, nu) * spfn.kv(nu, t)
    y = (sill - nugget) * (1 - MF) + nugget
    return 1 - y

def matern_correlation_fit(
        x, y, sill=1.0, nugget=0.0, bin_limit=30, dist_limit=0.75,
        weights='var'
        ):

    xb, yb = binned_obs(x, y)
    xb, yb = zip( 
        *[ (x_, y_) for x_, y_ in zip(xb, yb) if len(y_) > bin_limit ]
        )
    x, y = concat_bins(xb, yb)
    
    weights = weights.lower()
    if weights == 'textbook':
        raise NotImplementedError('Iterative weights not ready yet')
    elif weights == 'var':
        yv = map(np.var, yb)
        w, _ = concat_bins(yv, yb)
    elif weights == 'none':
        w = np.ones_like(x)
    else:
        raise ValueError('Not a known error type')

    w *= 1 / np.linalg.norm(w)

    def _loss(p):
        kw = dict()
        kw['theta'], kw['nu'] = p[:2]
        if sill < 1.0 and nugget > 0.0:
            kw['nugget'], kw['sill'] = p[2:]
        elif sill < 1.0 and nugget == 0.0:
            kw['sill'] = p[2]
            kw['nugget'] = 0.0
        elif sill == 1.0 and nugget > 0.0:
            kw['nugget'] = p[2]
            kw['sill'] = 1.0
        
        y_est = matern_correlation2(x, **kw)
        return np.sum( w * (y - y_est)**2 )
    
    # constraints:
    # 1. all positive
    cons = ( dict(type='ineq', fun = lambda p: np.array(p) - 1e-3), )
    p = [1.0, 0.5]
    if sill < 1.0 and nugget > 0.0:
        # 2. nugget < sill --> sill - nugget >= 0
        cons = cons + ( dict(type='ineq', fun=lambda p: p[3] - p[2]), )
        p.extend( [nugget, sill] )
    elif sill < 1.0 and nugget == 0.0:
        mx = y.max() * .9
        cons = cons + ( dict(type='ineq', fun=lambda p: mx - p[2]), )
        p.append(sill)
    elif sill == 1.0 and nugget > 0.0:
        mx = y.max() / 2.0
        cons = cons + ( dict(type='ineq', fun=lambda p: mx - p[2]), )
        p.append(nugget)
    r = minimize(_loss, np.array(p), constraints=cons)
    return r.x, r


# handcock & wallis def (also Rasmussen & Williams 2006 ch4)
def matern_correlation(x, theta=1.0, nu=0.5, d=1.0, **kwargs):
    theta, nu, d = map(float, (theta, nu, d))
    if 'nugget' in kwargs or 'sill' in kwargs:
        print 'The matern_correlation() method no longer scales past [0, 1]'
    if theta < 1e-6 or nu < 1e-2:
        # argument error, return null vector
        return np.zeros_like(x)
    def _comp(x_):
        scl =  2 ** (1.0-nu) / math.gamma(nu)
        z = (2 * nu)**0.5 * x_ / theta
        return scl * (z ** nu) * spfn.kv(nu, z)
    cf = _comp(x)
    if np.iterable(cf):
        cf[ np.isnan(cf) ] = 1.0
    elif np.isnan(cf):
        cf = 1.0
    return cf


# from Rasmussen & Williams 2006 MIT press ch4
def matern_spectrum(k, theta=1.0, nu=0.5, d=1.0):
    const = 2 ** d * np.pi ** (d/2.) * math.gamma(nu + d/2.) * (2*nu)**nu
    const = const / math.gamma(nu) / (theta ** (2*nu))
    spec = (2 * nu / theta**2 + 4 * np.pi**2 * k**2) ** (nu + d/2.0)
    return const / spec

def matern_covariance_matrix(chan_map, channel_variance=(), **matern_params):
    """Returns a model covariance for sites from a ChannelMap

    If channel_variance is given, probably a good idea that
    sill + nugget + kappa = 1 (which is default)

    If channel_variance is not given, then the diagonal will be sill+kappa
    """
    n_chan = len(chan_map)
    combs = chan_map.site_combinations
    # estimated correlation matrix with nuisance terms
    prm = matern_params.copy()
    nugget = prm.pop('nugget', 0)
    sill = prm.pop('sill', 1)
    kappa = prm.pop('kappa', 0)
    udist = np.unique(combs.dist)
    covar_values = (sill - nugget) * matern_correlation(udist, **prm) + kappa
    udist = np.round(udist, decimals=3)
    dist_hash = dict(zip(udist, covar_values))
    Kg_flat = [dist_hash[d] for d in np.round(combs.dist, decimals=3)]
    # Kg_flat = (sill - nugget) * matern_correlation(combs.dist, **prm) + kappa
    Kg = np.zeros( (n_chan, n_chan) )
    Kg.flat[0::n_chan+1] = (sill + kappa) / 2.0
    Kg[ np.triu_indices(n_chan, k=1) ] = Kg_flat
    Kg = Kg + Kg.T
    if len(channel_variance):
        cv = np.sqrt(channel_variance / sill)
        Kg = Kg * np.outer( cv, cv )
    return Kg

def simulate_matern_process(
        extent, dx, theta=1.0, nu=0.5, nugget=0, sill=1,
        kappa=0, nr=1, mu=(), cxx=False
        ):
    """
    (Dense) simulation of spatial fields using Matern covariance.
    The terminology of sill is slightly adapted from typical usage.
    
    * The covariance function shape is parameterized by (nu, theta)
    * "nugget" codes absolute noise strength, and not a proportion
    * "kappa" is optional and represents a minimum covariance > 0
    * "sill" is slightly different than typical usage. Sill refers to
      marginal site variance (including nugget + kappa components)
    
    """
    # extent -- if number then generate samples along an axis spaced by dx
    #        -- if a ChannelMap, then generate correlated timeseries
    #           samples on the grid with spacing of dx
    from scipy.linalg import toeplitz
    if isinstance(extent, ChannelMap):
        chan_combs = extent.site_combinations
        h = chan_combs.dist
        nrow, ncol = extent.geometry
        nchan = len(extent)
        cx = np.zeros((nchan, nchan), 'd')
        upper = np.triu_indices(len(cx), k=1)
        ## mx = matern_correlation(h, theta, nu)
        ## # this seems wrong??
        ## #mx = (sill - nugget) * mx + nugget
        ## mx = (sill - nugget) * mx + kappa
        ## #mx = (sill - nugget - kappa) * mx + kappa
        ## cx[upper] = mx
        ## cx = cx + cx.T
        ## cx.flat[::len(cx)+1] = sill + kappa
        cx = matern_covariance_matrix(
            extent, theta=theta, nu=nu,
            sill=sill, nugget=nugget, kappa=kappa
            )
        x = np.linspace(0, ncol-1, ncol) - ncol/2.0
        y = np.linspace(0, nrow-1, nrow) - nrow/2.0
        x = (x*dx, y*dx)
        if not len(mu):
            mu = np.zeros( (nchan,) )
    else:
        extent_ = round(extent / dx)
        x = np.arange(-extent_, extent_+1, dtype='d') * dx
        rx = np.abs(x[0] - x)
        #print rx
        mask = rx < 1e-3
        ## mx = matern_semivariogram(rx, theta, nu, nugget, sill)
        ## mx[mask] = 0
        ## cx = (sill - nugget) * (1 - mx) + (1 - sill)
        cm = matern_correlation(rx, theta=theta, nu=nu)
        cx = (sill - nugget) * cm + kappa        
        cx[mask] = sill + kappa
        cx = toeplitz(cx)
        if not len(mu):
            mu = np.zeros_like(x)
    if cxx:
        return cx
    try:
        samps = np.random.multivariate_normal(mu, cx, nr)
    except np.linalg.LinAlgError:
        # perturb matrix slightly
        n = len(cx)
        cx.flat[::n+1] += 1e-5
        samps = np.random.multivariate_normal(mu, cx, nr)
    if isinstance(extent, ChannelMap):
        samps = extent.embed(samps, axis=1)
    return x, samps.squeeze()

# these def'd from chapter 5 of handbook of modern geostats
# eq 5.12 (with sigma=1), and also Handcock & Wallis 1994
## def matern_correlation(x, theta=1.0, nu=0.5):
##     theta, nu = map(float, (theta, nu))
##     scalar = 1 / 2.0 ** (nu-1) / math.gamma(nu)
##     return scalar * \
##       np.power(x * np.sqrt(nu) * 2 / theta, nu) * \
##       spfn.kv(nu, x * np.sqrt(nu) * 2 / theta)

# This seems consistent for d=1 (the integral is 1). Don't know
# what's happening with d>1
## def matern_spectrum(k, theta=1.0, nu=0.5, d=1.0, onesided=True):
##     theta, nu, d = map(float, (theta, nu, d))
##     g = math.gamma(nu + d/2.0) * np.power(4*nu, nu)
##     g = g / math.gamma(nu) / np.power(np.pi, d/2.0) / np.power(theta, 2*nu)
##     fk = g / np.power(4*nu/theta**2 + (2*np.pi*k)**2, nu + d/2.0)
##     return 2*fk if onesided else fk

def make_matern_label(**params):
    """Helper function for plot labels."""
    label = u''
    if 'nu' in params:
        label = label + u'\u03BD {nu:.1f} '
    if 'theta' in params:
        label = label + u'\u03B8 {theta:.1f} (mm) '
    if 'nugget' in params:
        label = label + u'\u03C3 {nugget:.2f} (uV^2) '
    if 'sill' in params:
        label = label + u'\u03B6 {sill:.2f} (uV^2) '
    
    return label.format(**params)

# BUG -- there is still bias when fitting a cloud with fit_mean=False using
# IRLS weights. It looks like the 1 / semivar weighting pushes the
# estimate very high (at least pushes the nugget high, if it is a free
# variable).
def matern_semivariogram(
        x, theta=1.0, nu=1.0, nugget=0, sill=None, y=(),
        free=('theta', 'nu'), wls_mode='irls', fit_mean=False, binsize=None,
        dist_limit=None, bin_limit=None, bounds=None, weights=(),
        fraction_nugget=False, **kwargs
        ):

    if 'ci' in kwargs.keys():
        print 'Conf intervals not supported anymore'
    if 'wls' in kwargs.keys():
        print "Use wls='none' to turn off weighted least squares"

    # change default sill value depending on whether this is
    # a fitting or an evaluation
    if sill is None:
        if not len(y):
            sill = 1.0
        else:
            sill = y.max()

    if not len(y):
        mf = matern_correlation(x, theta, nu)
        if fraction_nugget:
            nugget = sill * nugget
        return (sill - nugget) * (1 - mf) + nugget

    if bounds is None:
        bounds = {}
    if dist_limit is not None:
        keep = x < dist_limit * x.max()
        x = x[keep]
        y = y[keep]

    if wls_mode is None:
        wls_mode = 'none'
    if wls_mode.lower() == 'textbook':
        wls_mode = 'irls'

    # Set up data -- always "fit" only at unique x points, but
    # calculate error at
    # * all points, if fit_mean == False
    # * mean points, if fit_mean == True

    # First bin data (if given a cloud) or create pseudo-bins otherwise
    if len(x) > len(np.unique(x)):
        xbinned, ybinned = binned_obs(x, y, binsize=binsize)
    else:
        fit_mean = True
        xbinned = x.copy()
        ybinned = [np.array([yi]) for yi in y]
        # I don't think it makes sense to replicate the mean value Ni times..
        # we already have the correct bin weights with IRLS
        # if wls_mode.lower() == 'irls' and len(weights):
        #     ybinned = [ np.array([yi] * Ni) for yi, Ni in zip(y, weights) ]
        # else:
        #     ybinned = [np.array([yi]) for yi in y]
      
    # If weights are supplied in IRLS mode, then they are
    # the bin counts of the semivariance estimates.
    if wls_mode.lower() == 'irls' and len(weights):
        Nh = weights
        if dist_limit is not None:
            Nh = Nh[keep]
    else:
        # else get the observed bin counts (even if ones)
        Nh = np.array( map(len, ybinned), dtype='d' )
    # If weights are *not* supplied in inverse-variance weighted mode,
    # then define weights
    if wls_mode.lower() == 'var' and not len(weights):
        weights = np.array( map(np.var, ybinned) )
        # this corrects for single-entry bins
        weights[ weights == 0 ] = max(1, weights.max())
    elif wls_mode.lower() == 'none':
        norm_weight = 1 / y.var()
        weights = [norm_weight] * len(xbinned)
    # This condition seems redundant with the else of "len(x) > len(np.unique(x))" from above
    if len(xbinned) == len(x):
        fit_mean = True
           
    if bin_limit is not None:
        keep = Nh > bin_limit
        xbinned = xbinned[keep]
        ybinned_ = [ybinned[n] for n in xrange(len(keep)) if keep[n]]
        ybinned = ybinned_
        Nh = Nh[keep]

    if fit_mean:
        x_, y_ = xbinned, np.array( map(np.mean, ybinned) )
    else:
        #x_, y_ = concat_bins(xbinned, ybinned)
        _, y_ = concat_bins(xbinned, ybinned)
        x_ = xbinned
        # now bin-counts are redundant
        #Nh = np.ones_like(y_)
        Nh = np.ones_like(x_)

    # Set up bounds defaults
    # maximum value for nu -- the variation in chaning nu past 10 is < 1ppm
    bounds.setdefault('nu', (.1, 10))

    # ensure theta > small
    bounds.setdefault('theta', (1e-6, None))

    if fraction_nugget:
        # nugget will be fit as a proportion [0, 1)
        bounds.setdefault('nugget', (0, 1))
    else:
        bounds.setdefault('nugget', (0, None))

    # sill must be positive
    bounds.setdefault('sill', (y_.min(), None))

    cons = [ dict(type='ineq', fun = lambda p: np.array(p)) ]
    solver_bounds = [ bounds.get(f, (None, None)) for f in free ]
    #print zip(free, solver_bounds)
    cons = tuple(cons)

        
    def _split_free_fixed(params):
        pd = dict( zip(free, params) )
        # fixed is globally set
        pd.update(fixed)
        return pd

    def calc_weight(x, y):
        if wls_mode == 'irls':
            if (y==0).all():
                w = Nh
            else:
                y = np.clip(y, y[y>0].min(), y.max())
                w = Nh / y**2
        elif wls_mode == 'var':
            # not iterative (since zero variance for model values)
            # w = 1 / np.array( map(np.var, ybinned) )
            w = weights
        else:
            #w = np.ones_like(y)
            w = weights
        #w /= ( w.sum() / len(w) )
        return w        
    
    def err(p):
        mf_params = _split_free_fixed(p)
        if fraction_nugget:
            if 'nugget' in free:
                # if optimizing for nugget, then convert to units here
                nz_prop = mf_params['nugget']
                mf_params['nugget'] = mf_params['sill'] * nz_prop
                mf_params['fraction_nugget'] = False
            else:
                # else preserve the fraction info
                mf_params['fraction_nugget'] = True
        y_est = matern_semivariogram(x_, **mf_params)
        w = calc_weight(x_, y_est)
        if not fit_mean:
            # reproduce weights and estimates
            w, _ = concat_bins(w, ybinned)
            y_est, _ = concat_bins(y_est, ybinned)
        return np.sqrt(w) * (y_ - y_est)

    def loss(p):
        mf_params = _split_free_fixed(p)
        # if nugget is in real units, then
        # return a large loss if it is larger than the sill
        if 'nugget' in free and not fraction_nugget:
            if mf_params['nugget'] >= mf_params['sill']:
                return 1e20
        resid = err(p)
        return np.sum(resid**2)

    if 'nugget' in free and nugget > 1 and fraction_nugget:
        #nugget = 0.01
        nugget /= sill

    fixed = dict(theta=theta, nu=nu, nugget=nugget, sill=sill)
    p0 = [ fixed.pop(f) for f in free ]

    r = minimize(
        loss, p0, constraints=cons, method='SLSQP', bounds=solver_bounds
        )
    params = dict( zip( free, r.x ) )
    if 'nugget' in params and fraction_nugget:
        sill = params.get('sill', sill)
        nz_prop = params['nugget']
        params['nugget'] = sill * nz_prop
    
    return params
    
import scipy.stats.distributions as distros
def matern_semivariogram_orig(
        x, theta=1.0, nu=0.5, nugget=0, sill=None, y=(), 
        priors=False, free=('theta', 'nu'), wls_mode='textbook', 
        fit_mean=False, dist_limit=None, bin_limit=None, bounds=dict(),
        **kwargs
        ):

    if 'ci' in kwargs.keys():
        print 'Conf intervals not supported anymore'
    if 'wls' in kwargs.keys():
        print "Use wls='none' to turn off weighted least squares"

    # change default sill value depending on whether this is
    # a fitting or an evaluation
    if sill is None:
        if not len(y):
            sill = 1.0
        else:
            sill = y.max()

    if not len(y):
        mf = matern_correlation(x, theta, nu)
        return (sill - nugget) * (1 - mf) + nugget

    if dist_limit is not None:
        keep = x < dist_limit * x.max()
        x = x[keep]
        y = y[keep]

    def _split_free_fixed(params):
        pd = dict( zip(free, params) )
        # fixed is globally set
        pd.update(fixed)
        return pd

    def calc_weight(x, y):
        if wls_mode == 'none':
            return np.ones_like(y)
        #xb, yb = binned_obs(x, y)
        if wls_mode == 'textbook':
            if fit_mean:
                yf = y
            else:
                _, yb = binned_obs(x, y)
                yf = np.array(map(np.mean, yb))
            #n_obs = np.array( map(len, yb), dtype='d' )
            with np.errstate(divide='ignore'):
                w = Nh / yf**2
                w[yf==0] = Nh[yf==0]
            ## # re-order inputs into blocks
            if not fit_mean:
                w, _ = concat_bins(w, yb)
            ## x, _ = concat_bins(xb, yb)
            ## y = np.concatenate(yb)
        elif wls_mode == 'var':
            # not iterative (since zero variance for model values)
            w = 1 / np.array( map(np.var, ybinned) )
            if not fit_mean:
                w, _ = concat_bins(w, ybinned)
        else:
            w = np.ones_like(y)
        w /= ( w.sum() / len(w) )
        return w        
    
    def err(p):
        mf_params = _split_free_fixed(p)
        y_est = matern_semivariogram_orig(x_, **mf_params)
        w = calc_weight(x_, y_est)
        return np.sqrt(w) * (y_ - y_est)


    # maximum value for nu -- the variation in chaning nu past 10 is < 1ppm
    # nu_max = 10
    bounds.setdefault('nu', (.1, 10))
    nu_max = bounds['nu'][1]

    # ensure theta > small
    bounds.setdefault('theta', (1e-6, None))

    # ensure nugget < sill
    bounds.setdefault('nugget', (0, sill))
    
    # Get beta distribution paremters for nu and nugget. Using beta
    # because then we put everything on a [0, 1] scale.
    nu_mode = 0.5 / nu_max
    b_nu = 6
    a_nu = (nu_mode * b_nu - 2 * nu_mode + 1) / (1 - nu_mode)

    # This gives a mode of 0
    b_nug = 4.0
    a_nug = 1.0
    
    def loss(p):
        mf_params = _split_free_fixed(p)
        Lp = 0
        # hard enforcement of positivity req'd for downstream maths
        ## if 'theta' in free:
        ##     theta_ = mf_params['theta']
        ##     lims = bounds.get('theta', (1e-4, 4*x.max()))
        ##     if (theta_ < lims[0]) or (theta_ > lims[1]):
        ##         return 1e6
        if 'nu' in free:
            nu_ = mf_params['nu']
            ## lims = bounds['nu']
            ## if (nu_ < lims[0]) or (nu_ >= lims[1]):
            ##     # Lp += -1e6
            ##     # just short-circuit the cost calculation 
            ##     # and return something yuge
            ##     return 1e6
            if priors:
                nn = nu_/nu_max
                Lp += np.log(distros.beta.pdf(nn, a_nu, b_nu))

        if 'nugget' in free:
            nug = mf_params['nugget']
            ## lims = bounds.get('nugget', (0, mf_params['sill']))
            ## if (nug < lims[0]) or (nug > lims[1]):
            ##     return 1e6
            if priors:
                Lp += np.log(distros.beta.pdf(nug, a_nug, b_nug))

        ## if ('sill' in bounds) and ('sill' in free):
        ##     lims = bounds['sill']
        ##     sill_ = mf_params['sill']
        ##     if (sill_ < lims[0]) or (sill_ > lims[1]):
        ##         return 1e6
                
        resid = err(p)
        Ll = -0.5 * np.sum(resid**2)
        #print p, 'Loss: {0:.2f}, {1:.2f}'.format(-Ll, -Lp)
        return (-Ll + Lp)

    xbinned, ybinned = binned_obs(x, y)
    Nh = np.array( map(len, ybinned), dtype='d' )
    if fit_mean:
        if bin_limit is not None:
            keep = Nh > bin_limit
            xbinned = xbinned[keep]
            ybinned_ = [ybinned[n] for n in xrange(len(keep)) if keep[n]]
            ybinned = ybinned_
            Nh = Nh[keep]
        x_, y_ = xbinned, np.array( map(np.mean, ybinned) )
    else:
        x_, y_ = concat_bins(xbinned, ybinned)

    # basic: all positive contraint
    cons = [ dict(type='ineq', fun = lambda p: np.array(p)) ]
    ## # implement bounds constraints
    ## for n, f in enumerate(free):
    ##     if f in bounds:
    ##         lims = bounds[f]
    ##         if lims[0] is not None:
    ##             cons.append( dict(type='ineq', fun=lambda p: p[n] - lims[0]) )
    ##         if lims[1] is not None:
    ##             cons.append( dict(type='ineq', fun=lambda p: lims[1] - p[n]) )
    solver_bounds = [ bounds.get(f, (None, None)) for f in free ]
    #print zip(free, solver_bounds)
    cons = tuple(cons)

    # these two parameters covary like crazy -- do iterative fitting
    # to pin them down
        
    if 'nu' in free and 'nugget' in free:
        kws = dict(priors=priors, wls_mode=wls_mode, 
                   fit_mean=fit_mean, y=y, bounds=bounds)
        free_ = list(free)
        free_.remove('nu')
        free_.remove('nugget')
        nu0 = nu
        nug0 = nugget
        cvg = False
        it = 0
        while not cvg and it < 50:
            fr = free_ + ['nugget']
            #print 'fitting ', fr,
            mf = matern_semivariogram_orig(
                x, theta=theta, nu=nu0, nugget=nug0, sill=sill, free=fr, **kws
                )
            if 'theta' in fr:
                theta = mf.x[fr.index('theta')]
            if 'sill' in fr:
                sill = mf.x[fr.index('sill')]
            nug = mf.x[-1]
            #print theta, sill, nu, nug
            fr = free_ + ['nu']
            #print 'fitting ', fr, 
            mf = matern_semivariogram_orig(
                x, theta=theta, nu=nu0, nugget=nug, sill=sill, free=fr, **kws
                )
            if 'theta' in fr:
                theta = mf.x[fr.index('theta')]
            if 'sill' in fr:
                sill = mf.x[fr.index('sill')]
            nu = mf.x[-1]
            #print theta, sill, nu, nug
            cvg = (np.abs(nug - nug0) < 1e-3) and (np.abs(nu - nu0) < 1e-3)
            nug0 = nug
            nu0 = nu
            it += 1
        params = dict( zip( fr + ['nugget'], np.r_[mf.x[:-1], nu, nug] ) )
        mf.x = np.array( [params[k] for k in free] )
        return mf
    
    # determine free and fixed parameters
    if 'nugget' in free:
        nugget = 0.1
    fixed = dict(theta=theta, nu=nu, nugget=nugget, sill=sill)
    p0 = [ fixed.pop(f) for f in free ]

    r = minimize(loss, p0, constraints=cons, method='SLSQP', bounds=solver_bounds) #method='COBYLA')
    #y_est = matern_semivariogram(x_, **_split_free_fixed(r.x))
    return r#, x, y_est, calc_weight(x, y_est)

# fit the full covariance model in parts
def matern_fit_start(x, y, n0=1e-3, s0=None, **kwargs):
    kwargs.setdefault('fit_mean', True)
    # fix nu to start
    mf = matern_semivariogram(
        x, y=y, theta=1, nu=1.5, nugget=n0, sill=s0,
        free=('theta', 'nugget', 'sill'), **kwargs
        )
    theta, nugget, sill = [ mf[f] for f in ('theta', 'nugget', 'sill') ]
    #theta, nugget, sill = mf.x
    print mf
    
    mf = matern_semivariogram(
        x, y=y, theta=theta, nu=1.5, sill=sill, nugget=nugget,
        free=('theta', 'nu'), **kwargs
        )
    theta, nu = [ mf[f] for f in ('theta', 'nu') ]
    print mf

    mf = matern_semivariogram(
        x, y=y, theta=theta, nu=nu, sill=sill, nugget=nugget,
        free=('nu', 'nugget'), **kwargs
        )
    nu, nugget = [ mf[f] for f in ('nu', 'nugget') ]
    print mf

    return theta, nu, nugget, sill

def matern_fit_start_orig(x, y, finish=False, n0=0.01, s0=None, **kwargs):
    kwargs.setdefault('fit_mean', True)
    # fit theta and sill and nu
    mf = matern_semivariogram_orig(
        x, y=y, theta=1, nu=1.5, nugget=n0, sill=s0,
        free=('theta', 'nu', 'sill'), **kwargs
        )
    theta, nu, sill = mf.x
    print mf.x
    #sill = min(1.0, sill)
    # fit for nugget with theta and nu floating
    mf = matern_semivariogram_orig(
        x, y=y, theta=theta, nu=nu, sill=sill, nugget=n0,
        free=('theta', 'nu', 'nugget'), **kwargs
        )
    theta, nu, nugget = mf.x
    if finish:
        mf = matern_semivariogram_orig(
            x, y=y, theta=1.0, nu=1.0, sill=sill, nugget=nugget,
            free=('theta', 'nu'), **kwargs
            )
        theta, nu = mf.x
    return theta, nu, nugget, sill

def effective_range(p, mx):
    theta = p[0]
    x0 = 2*theta
    fx = lambda x: (matern_semivariogram(x, *p) - mx)**2
    return minimize_scalar(fx).x

def kriging_error(G, X, g0, x0):
    Gi = np.linalg.inv(G)
    r = np.atleast_1d( X.T.dot( Gi.dot(g0) ) )
    s = np.atleast_1d( g0.T.dot( Gi.dot(g0) ) )
    XG = np.atleast_2d( X.T.dot( Gi.dot(X) ) )
    (r - x0).T.dot( np.linalg.solve(XG, r - x0) )
    return  s - (r - x0).T.dot( np.linalg.solve(XG, r - x0) )

def grid_kriging_error(
        g, theta, nu, nugget=0, sill=None, dx=1.0, dy=1.0,
        offset=0.0
        ):
    xx, yy = map(np.ravel, np.meshgrid(np.arange(g[1])*dx, np.arange(g[0])*dy))
    N = g[0] * g[1]
    d = [ np.sqrt( (xx[m] - xx[n])**2 + (yy[m] - yy[n])**2 )
          for m, n in itertools.combinations( xrange(N), 2 ) ]
    d = np.array(d)
    gu = matern_semivariogram( d, theta, nu, nugget, sill )
    G = np.zeros( (N, N) )
    G[np.triu_indices(N, k=1)] = gu
    G = G + G.T
    #G[np.diag_indices(N)] = 0 if nugget is None else nugget

    offset = np.complex(offset)
    if not np.real(offset) or np.imag(offset):
        err = list()
        X = np.ones(N-1)
        for i in xrange(N):
            Gs = np.column_stack( (G[:, :i], G[:, i+1:]) )
            Gs = np.row_stack( (Gs[:i], Gs[i+1:]) )
            g0 = np.r_[ G[:i, i], G[i+1:, i] ]
            err.append( kriging_error(Gs, X, g0, 1) )
        return np.concatenate(err)
            

    sy = yy + np.imag(offset)
    sx = xx + np.real(offset)
    # sh is the N x N' table of distances
    sh = np.sqrt( (xx[:,None] - sx)**2 + (yy[:,None] - sy)**2 )
    g0 = matern_semivariogram(sh.ravel(), theta, nu, nugget, sill)
    g0 = g0.reshape(N, N)
    X = np.ones(N)
    return np.concatenate( [kriging_error(G, X, g_, 1) for g_ in g0.T] )
    
        
## def matern_spectrum(k, theta, nu, d=2.0):
##     #scl = 2**d * np.pi**(d/2.0) * spfn.gamma(nu+d/2.0) * (2*nu)**nu
##     #scl /= ( spfn.gamma(nu) * theta**(2*nu) )
##     scl = spfn.gamma(nu + d/2.0) * (4*nu)**nu
##     scl /= ( np.pi**(d/2.0) * theta**(2*nu) * spfn.gamma(nu) )
##     fk = (4*nu/theta**2 + (2*np.pi*k)**2) ** (nu + d/2.0)
##     return scl / fk

def matern_spectrum2(theta, nu, dx=0.05):
    import matplotlib.pyplot as pp
    import seaborn as sns
    xx = np.arange( int(2.5*theta/dx) ) * dx
    xx[0] = 1e-3
    cx = matern_correlation(xx, theta=theta, nu=nu)
    Ck = np.fft.fft( np.r_[cx[1:][::-1], cx] )
    k = np.arange(len(Ck)) * 1.0 / dx / len(Ck)
    nk = len(Ck) // 2 + 1
    print nk
    with sns.plotting_context('notebook'), sns.axes_style('dark'), \
      sns.color_palette('muted'):
      f, axs = pp.subplots(2, 1)
      axs[0].plot(xx, cx)
      axs[1].semilogy(k[:nk], np.abs(Ck[:nk])/np.abs(Ck[0]))
      axs[1].set_xlim(0, 4*theta)
    return f, np.r_[cx[1:][::-1], cx]
    

def matern_demo(
        nus=[0.2, 0.5, 1, 2], thetas=[1, 2, 3, 4], spectrum=False, paired=False,
        reparam=False, context='notebook', figsize=None, semivar=False
        ):
    import matplotlib.pyplot as pp
    import seaborn as sns
    #sns.set_style('dark')
    #sns.set_palette('muted')
    xx = np.linspace(0, 2, 200) if spectrum else np.linspace(0.001, 5, 200)
    sns_st = {u'font.sans-serif': [u'Helvetica', u'Arial']}
    with sns.plotting_context(context), sns.axes_style('dark', rc=sns_st), \
      sns.color_palette('muted'):
        if paired:
            f, ax = pp.subplots()
            axs = [ax]
        else:
            f, axs = pp.subplots(2, 1, sharex=True, sharey=True, figsize=figsize)
        ls = itertools.cycle( ('-', '--', '-.', ':') )
        if paired:
            for nu, theta in zip(nus, thetas):
                label=r'$\theta$={1:.1f}, $\nu$={0:.1f}'.format(nu, theta)
                if spectrum:
                    y = matern_spectrum(xx, theta=theta, nu=nu)
                    ax.plot(
                        xx, y/y[0], 
                        label=label, ls=ls.next()
                        )
                else:
                    if reparam:
                        #theta = theta * (2*nu)**0.5
                        theta = theta / (2*nu)**0.5
                        #label=r'$\theta$={1:.1f}, $\nu$={0:.1f}'.format(nu, theta)
                    y = matern_correlation(xx, nu=nu, theta=theta)
                    if semivar:
                        y = 1 - y
                    ax.plot(
                        xx, y,
                        label=label, ls=ls.next()
                        )
            ax.legend(loc='upper right')
            axs = [ax]
        else:
            for nu in nus:
                label=r'$\theta$=2, $\nu$={0:.1f}'.format(nu)
                if spectrum:
                    y = matern_spectrum(xx, theta=1.0, nu=nu)
                    axs[0].plot(
                        xx, y/y[0], 
                        label=label, ls=ls.next()
                        )
                else:
                    theta = 2.0
                    if reparam:
                        #theta = (2*nu)**0.5
                        theta = theta / (2*nu)**0.5
                        #label=r'$\theta$=1, $\nu$={0:.1f}'.format(nu)
                    y = matern_correlation(xx, nu=nu, theta=theta)
                    if semivar:
                        y = 1 - y
                    axs[0].plot(
                        xx, y, 
                        label=label, ls=ls.next()
                        )
            ls = itertools.cycle( ('-', '--', '-.', ':') )
            for theta in thetas:
                label=r'$\theta$={0}, $\nu$=1'.format(theta)
                if spectrum:
                    y = matern_spectrum(xx, theta=theta, nu=1.0)
                    axs[1].plot(
                        xx, y/y[0],
                        label=label, ls=ls.next()
                        )
                else:
                    if reparam:
                        #theta = theta * 2**0.5
                        theta = theta / (2*nu)**0.5
                        #label=r'$\theta$={0}, $\nu$=1'.format(theta)
                    y = matern_correlation(xx, nu=nu, theta=theta)
                    if semivar:
                        y = 1 - y
                    axs[1].plot(
                        xx, y, 
                        label=label,
                        ls=ls.next()
                        )

        for ax in axs:
            ax.legend(loc='upper right')
            if not spectrum:
                ax.set_yticks([0, 0.5, 1.0])
        if spectrum:
            axs[-1].set_xlabel('Spatial freq (mm$^{-1}$)')
        else:
            axs[-1].set_xlabel('Distance (mm)')
        for ax in axs:
            ax.set_ylabel('power (normalized)' if spectrum else 'Correlation')
        f.tight_layout()
    return f
    
def matern_diffs(max_nu=50):
    import matplotlib.pyplot as pp
    import seaborn as sns
    nus = np.linspace(0.5, max_nu, 200)
    xx = np.linspace(0.001, 5, 500)
    materns = np.array( [matern_correlation(xx, nu=nu) for nu in nus] )
    materns_err = np.diff(materns, axis=0)
    np.power(materns_err, 2, materns_err)
    np.power(materns, 2, materns)
    pwr = np.sum(materns, axis=-1)
    err = np.sum(materns_err, axis=-1) / pwr[:-1]
    with sns.plotting_context('notebook'), sns.axes_style('dark'), \
      sns.color_palette('muted'):
        pp.figure(); pp.semilogy(nus[1:], err)
        pp.ylabel('relative error')
        pp.xlabel('value of nu')
        pp.tight_layout()

def spline_fit(x, y, x_fit=(), **fit_kwargs):
    xb, yb = binned_obs(x, y)
    y_mn = np.array( [_y.mean() for _y in yb] )
    y_sg = np.array( [_y.var() for _y in yb] )
    y_sz = np.array( [len(_y) for _y in yb] )

    w = ( y_sg / y_sz ) ** -1

    tck = interp.splrep(xb, y_mn, w=w, **fit_kwargs)
    if not len(x_fit):
        x_fit = np.linspace(xb.min(), xb.max(), 100)
    return interp.splev(x_fit, tck)

def barycentric_fit(x, y, x_fit=()):
    xb, yb = binned_obs(x, y)
    y_mn = np.array( [_y.mean() for _y in yb] )

    if not len(x_fit):
        x_fit = np.linspace(xb.min(), xb.max(), 100)
    return interp.barycentric_interpolate(xb, y_mn, x_fit)
    
    
def plot_electrode_graph(
        graph, chan_map, scale='auto', edge_colors=('black', 'red'), ax=None, stagger_x=False, stagger_y=False
        ):

    import matplotlib.pyplot as pp
    if not ax:
        figsize = np.array(chan_map.geometry)
        if scale == 'auto':
            scale = 8.0 / max(figsize)
        figsize = tuple( scale * figsize )
        f = pp.figure(figsize=figsize)
        ax = f.add_subplot(111)
    else:
        f = ax.figure
    
    ii, jj = chan_map.to_mat()
    if stagger_x:
        jj = jj + (np.random.rand(len(jj)) * 0.7 + 0.1)
    if stagger_y:
        ii = ii + (np.random.rand(len(ii)) * 0.7 + 0.1)
    n = len(graph)
    # plot in (x, y) coords, which is (jj, ii)
    pos = dict(enumerate(zip(jj, ii)))
    graph = graph.copy()
    graph.flat[::n+1] = 0
    graph[ np.isnan(graph) ] = 1e-3
    rank = np.nansum(np.abs(graph), axis=0) / n
    # make node size somewhere between 40-80 pts
    nsize = 80 * rank + 40 * (1-rank)
    G = nx.Graph(graph)
    ew = graph[ np.triu_indices(n, k=1) ]

    cm = diverging_cm(-1, 1, edge_colors)
    nx.draw(
        G, pos, edge_color=ew, edge_cmap=cm, with_labels=False,
        node_size=nsize, node_color=rank, cmap=pp.cm.Blues,
        width=np.abs(ew)**4, linewidths=0, 
        ax=ax, edge_vmin=-1, edge_vmax=1
        )
    
    ax.axis('equal')
    ax.set_ylim(chan_map.geometry[0]-0.5, -0.5)
    ax.set_xlim(-0.5, chan_map.geometry[1]-0.5)
    cbar = pp.colorbar(ax.collections[0], ax=ax, use_gridspec=True)
    cbar.set_label('graph avg rank')
    return f


## from sympy import sqrt, besselk, power, gamma, symbols
## from sympy.printing.theanocode import theano_function
## def sympy_matern_fn():
##     from sympy.abc import t, h
##     nu, theta = symbols('nu,theta')
##     #nu = 0.5
##     #theta = 1
##     #h = 1
##     t = sqrt(2.0 * nu) * h / theta
##     f1 = besselk(nu, t)
##     f2 = t ** nu #power( t, nu )
##     f3 = (2 ** (1-nu) ) / gamma(nu) #power(2.0, 1.0-nu) / gamma(nu)

##     mf = f1 * f2 * f3
##     #return mf
##     return theano_function([h, theta, nu], [mf], dims={h:1, theta:1, nu:1})

from scipy.stats import spearmanr
def dissimilarity_range(x, y):
    xb, yb = binned_obs(x, y)
    r = list()
    p = list()
    for d in xb[:-1]:
        # pick ~ 100 pairs of (x,y) combinations such that x > d
        m = x > d
        idx = np.random.permutation(m.sum())[0:min(100, m.sum())]
        x_ = x[m][idx]
        y_ = y[m][idx]
        dst = [np.abs(x1-x2) for x1, x2 in itertools.combinations(x_, 2)]
        err = [np.abs(y1-y2) for y1, y2 in itertools.combinations(y_, 2)]
        r_, p_ = spearmanr(dst, err)
        r.append(r_)
        p.append(p_)
    return xb[:-1], r, p
        
