# basic evoked potential scoring functions
from __future__ import division
from contextlib import closing
import ecoglib.mproc as mp
import numpy as np

import scipy.ndimage as ndimage
import scipy.linalg as la
from scipy.interpolate import interp1d, RegularGridInterpolator, \
     RectBivariateSpline

from sklearn import covariance

from .stat_tests import rayleigh_test

def ptp(arr, xwin, neg_width = 3, points=False):
    oshape = arr.shape
    arr = arr.reshape(-1, oshape[-1])

    arr_pos = arr[..., xwin[0]:xwin[1]]
    pos_pk = np.argmax(arr_pos, axis=-1) + xwin[0]

    neg_pk = np.empty_like(pos_pk)
    win_len = neg_width * (xwin[1] - xwin[0])
    for n in xrange(arr.shape[0]):
        post_slice = slice(pos_pk[n], min(oshape[-1], pos_pk[n] + win_len))
        post_trace = arr[n, post_slice]
        neg_pk[n] =  pos_pk[n] + np.argmin(post_trace)

    seq = np.arange(arr.shape[0])
    ptp = arr[ (seq, pos_pk) ] - arr[ (seq, neg_pk) ]

    if points:
        return map(lambda x: x.reshape(oshape[:-1]), (ptp, neg_pk, pos_pk))
    else:
        return ptp.reshape(oshape[:-1])

def rms(arr, axis=-1):
    return arr.std(axis=axis)

def peak_lags(avg_ep, ref_ep=None, interp=4): 
    if interp > 1:
        n_pts = avg_ep.shape[-1]
        ix = np.arange(n_pts)
        interp_fun = interp1d(ix, avg_ep, kind='cubic', axis=-1)
        ix_ = np.linspace(ix[0], ix[-1], (n_pts-1)*interp + 1)
        avg_ep = interp_fun(ix_)
        if ref_ep is not None:
            ref_ep = interp1d(ix, ref_ep, kind='cubic', axis=-1)(ix_)

    if ref_ep is None:
        # choose mean ep as template
        ref_ep = avg_ep.mean(axis=0)

    if not avg_ep.shape[-1] % 2:
        avg_ep = np.column_stack( ( avg_ep, avg_ep[:,-1] ) )
        ref_ep = np.r_[ref_ep, ref_ep[-1]]
    L = len(ref_ep)
    xcorr = ndimage.convolve1d(avg_ep, ref_ep[::-1], mode='constant', axis=-1)
    xcorr_lags = (np.argmax(xcorr, axis=-1).astype('d') - L//2) / float(interp)
    xcorr_peaks = xcorr.max(axis=-1)
    return xcorr_lags, xcorr_peaks

class RegCovariance(covariance.EmpiricalCovariance):

    def fit(self, X, y=None):
        from sklearn.utils import check_array
        X = check_array(X)
        if self.assume_centered:
            self.location_ = np.zeros(X.shape[1])
        else:
            self.location_ = X.mean(0)
        cov = covariance.empirical_covariance(X, assume_centered=self.assume_centered)
        lam = np.linalg.eigvalsh(cov)
        mn_lam = max(1e-2, 1e-6 * lam.max())
        n = len(cov)
        cov.flat[::n+1] += mn_lam
        self._set_covariance(cov)
        return self

class Mahalanobis(object):

    def __init__(self, noise_samps, cov_est='lw_shrink'):
        if noise_samps.ndim < 3:
            noise_samps = noise_samps.reshape( (1,) + noise_samps.shape )
    
        self.n_mean = np.nanmean(noise_samps, axis=1)
        self.vec_len = noise_samps.shape[-1]

        # build estimators for each channel
        kwargs = dict(
            assume_centered=True, store_precision=True
            )
        if cov_est == 'lw_shrink':
            estimator = covariance.LedoitWolf
            kwargs['block_size'] = self.vec_len
        elif cov_est == 'oas_shrink':
            estimator = covariance.OAS
        elif cov_est == 'mincovdet':
            estimator = covariance.MinCovDet
        elif cov_est == 'empirical':
            #estimator = covariance.EmpiricalCovariance
            estimator = RegCovariance
        
        cov_estimators = list()
        for samps, mn in zip(noise_samps, self.n_mean):
            cxx = estimator(**kwargs)
            # use 1st sample as an indicator 
            s_mask = np.all( np.isfinite(samps), axis=1 )
            samps = samps[s_mask]
            cov_estimators.append( cxx.fit(samps - mn) )
        self._estimators = cov_estimators
        self._temporal_wfilters = ()
        self._wfilters = ()

    def _calc_whitening_filters(self, temporal=True):
        """
        Calculate filters for covariance-normalization. Optionally
        transform normalized coefficients with the eigenvectors.
        Filters are applied y = x.dot(f), where the last dimension of
        x has the variates to be normalized.
        
        """

        if temporal and len(self._temporal_wfilters):
                return self._temporal_wfilters
        if not temporal and len(self._wfilters):
            return self._wfilters
        filters = []
        for est in self._estimators:
            # Whitened with eigenvectors just gives a vector
            # of "inverse" principal components in no sequential order
            w, v = np.linalg.eigh( est.covariance_ )
            f = v / np.sqrt(w)
            if temporal:
                f = f.dot(v.T)
            filters.append(f)
        if temporal:
            self._temporal_wfilters = filters
        else:
            self._wfilters = filters
        return filters

    def _prep_shape(self, eps):
        ep_shape = eps.shape
        if eps.ndim == 2:
            if ep_shape[0] == len(self._estimators):
                eps = eps.reshape(ep_shape[0], 1, ep_shape[1])
            else:
                eps = eps[None, :, :]
        if eps.ndim > 3:
            dim = int(np.prod( ep_shape[1:-1] ))
            eps = eps.reshape( ep_shape[0], dim, ep_shape[-1] )
        if eps.shape[-1] != self.vec_len:
            ep_len = eps.shape[-1]
            raise ValueError(
                'Response vectors are length-%d, but must be length-%d'%(ep_len, self.vec_len)
                )
        if eps.shape[0] != len(self._estimators):
            raise ValueError(
                'This object is trained for %d response channels'%len(self._estimators)
                )
        return eps, ep_shape


    def __call__(self, eps):
        eps, ep_shape = self._prep_shape(eps)        
        dists = list()
        for responses, mn, cxx in zip(eps, self.n_mean, self._estimators):
            d = cxx.mahalanobis(responses-mn) ** 0.5
            dists.append( d )
        return np.array(dists).reshape(ep_shape[:-1])

    def whiten(self, X, temporal=False):
        """Calculate covariance-normalized variates.

        Parameters
        ----------
        X : ndarray
            shape ([n_chan], n_sample, n_pt)
        temporal : bool {False}
            Calculate the a-temporal normalized variates
	    (default). Otherwise, apply length-preserving transform 
            back to timeseries space. 

        Returns
        -------
        Xw : ndarray, shape of X
            
        Note
        ----
        See expand_projections for temporal transformation.
        
        """
        
        X, X_shape = self._prep_shape(X)
        wf = self._calc_whitening_filters(temporal=temporal)
        xw = np.empty_like(X)
        for n, (x, mu, f) in enumerate(zip(X, self.n_mean, wf)):
            xw[n] = (x-mu).dot(f)
        return xw.reshape(X_shape)

    def expand_projection(self, X, preserve_input_variance=False):
        """Expansion of the Mahalanobis projection in the timeseries space.

        Make a linear combination of the projection of X back into the
        space of X using the basis vectors found from the covariance
        matrix.

        Parameters
        ----------
        X : ndarray
            shape ([n_chan], n_sample, n_pt)
        preserve_input_variance : bool {True | False}
            If True, then scale the output vectors to match the
            variance of the input vectors. 

        Returns
        -------
        Xw : ndarray
            same shape as X

        """
        
        X, X_shape = self._prep_shape(X)
        Xw = np.zeros_like(X)
        wf = self._calc_whitening_filters(temporal=True)
        for xw_, x_, mn, P in zip(Xw, X, self.n_mean, wf):
            xw_[:] = (x_-mn).dot(P)
            if preserve_input_variance:
                scale = x_.std(-1) / xw_.std(-1)
                xw_[:] *= scale[...,None]
        return Xw.reshape(X_shape)

def mahalanobis(noise_samps, eps=None):
    """
    Scores EPs by the Mahalanobis distance.

    Parameters
    ----------

    noise_samps : ndarray ([n_chans], n_samps, n_pts)
        A sample of non-evoked EEG. For robust covariance estimation,
        the number of samples should roughly match the length of the
        response timeseries.

    eps : optional, ndarray ([n_chans], [n_responses], n_pts)
        A number of evoked responses to score against the noise population.
        If not provided, then this method returns a callable object that
        computes the Mahalanobis distance based on the estimated noise
        distribution.

    Returns
    -------
    
    dists : ndarray ([n_chans], [n_responses])
        A Mahalanobis distance scored for each evoked response.

    mahal_obj : Mahalanobis object
        If eps parameter is not given, return the scoring object
        
    """

    mahal_obj = Mahalanobis(noise_samps)
    if eps is not None:
        return mahal_obj(eps)
    return mahal_obj

def evoked_snr(baseline, response, groups=(), correct_bias=True, db=True,
               median=True, return_trials=False, return_group=False):
    """
    Calculate evoked response over baseline signal to noise ratio (ESNR)

    Parameters
    ----------
    baseline : array
        Scores of baseline signal from one or more channels.
    response : array
        A set of response scores from one or more channels, same
        size as baseline.
    groups : sequence
        Optional partition for the responses. ESNR is determined
        for the group with the strongest responses. In this case,
        baseline samples are also partitioned and the maximal-group
        is used in the SNR denominator.
    db : Bool
        Return ratio in decibels, in which case scores are
        assumed to be analogous to "power" units.
    median : Bool
        Use median versus mean estimator for sample middles.
    return_trials : Bool
        By default, return a "middle" value of SNR per channel.
        Alternately, return ratios for all trials in the maximal-group.

    Returns
    -------
    esnr : array
        The bias-corrected evoked SNR values
    group : array
        Indices per channel of the highest scoring group
        (used to compute SNR). Only returned if return_group==True.
    """

    # TODO -- think about jack-knifing the db or ratio estimator
    from ecoglib.util import equalize_groups
    baseline = np.atleast_2d(baseline)
    response = np.atleast_2d(response)
    if response.ndim == 3:
        # can infer groups if response is already grouped
        G, T = response.shape[1:]
        groups = [T] * G
        response = response.reshape(-1, G*T)
    if baseline.ndim > 2 or response.ndim > 2:
        raise ValueError('only 2D arrays')
    if baseline.shape != response.shape:
        raise ValueError('baseline and response arrays must match in shape')

    C, M = response.shape
    if not len(groups):
        groups = [M]

    baseline = equalize_groups(baseline, groups, axis=1)
    response = equalize_groups(response, groups, axis=1)

    mid_est = np.nanmedian if median else np.nanmean

    b_score = mid_est(baseline, axis=2)
    r_score = mid_est(response, axis=2)

    # Calculate best groups
    b_best = np.argmax(b_score, axis=1)
    r_best = np.argmax(r_score, axis=1)

    if correct_bias:
        # keep best baseline middles
        b_middle = b_score[ zip(*enumerate(b_best)) ]
    else:
        b_middle = mid_est( baseline.reshape(C, -1), axis=1 )

    # keep best response trials / middles
    if return_trials:
        # return a Channel x Trial matrix
        numer = response[ zip(*enumerate(r_best)) ]
        b_middle = b_middle[:, np.newaxis]
    else:
        # Return a scalar for each channel
        numer = r_score[ zip(*enumerate(r_best)) ]

    if db:
        snr = 10 * (np.log10(numer) - np.log10(b_middle))
    else:
        snr = numer / b_middle
    if return_group:
        return snr, r_best
    return snr

def _init_pool(pool_args):
    for kw in pool_args.keys():
        globals()[kw] = pool_args[kw]

def _permutation_sampler(s_range):
    N = len(s_range)
    # raw samples defined in pool init
    M, C = raw_samples.shape
    M = M // 2
    samples = np.empty( (N,) + raw_samples.shape, raw_samples.dtype )
    for i, x in enumerate(s_range):
        samples[i] = np.random.permutation(raw_samples)

    r1 = samples[:, :M]
    r2 = samples[:, M:]
    # groups and correct_bias defined in pool init
    pstat = evoked_snr(r1.transpose(0, 2, 1).reshape(N*C, -1), 
                       r2.transpose(0, 2, 1).reshape(N*C, -1),
                       groups=groups, correct_bias=correct_bias)
    pstat.shape = (N, C)
    # pctile defined in pool init
    if pctile==100:
        pstat = np.nanmax(pstat, axis=1)
    else:
        pstat = np.nanpercentile(pstat, pctile, axis=1)
    return pstat

def permuted_snr(base, resp, groups=(), N=2000, pctile=100, correct_bias=True):
    """Sample permutation statistic distribution by swapping
    response / baseline labels and re-computing SNR. By default,
    using maximum-SNR to control for multiple comparisons.
    """
    C, M = base.shape
    x = np.c_[base, resp].T.copy()

    mem_size = N * 2 * M * C * base.dtype.itemsize
    clip_size = 50e6
    num_chunks = int(mem_size / clip_size)

    pool_args = dict(raw_samples=x, groups=groups, pctile=pctile,
                     correct_bias=correct_bias)
    with closing(mp.Pool(processes=None, initializer=_init_pool,
                         initargs=(pool_args,))) as p:
        rng = np.arange(N)
        num_chunks = max(num_chunks, p._processes)
        splits = np.array_split(rng, num_chunks)
        res = p.map_async(_permutation_sampler, splits)

    p.join()
    if res.successful():
        pstat = np.concatenate(res.get())
    else:
        res.get()

    stat = evoked_snr(base, resp, groups=groups, correct_bias=correct_bias)
    # find empirical p-value of true stat
    pstat = np.sort( pstat[ np.isfinite(pstat) ] )
    return stat, 1 - pstat.searchsorted( stat ) / float(len(pstat)), pstat

def calc_msnr(noise_samps, sig_samps, thresh_distance=0.5):
    """
    Report the average multivariate distance of evoked response
    signals from background activity. Use the maximum of average
    distances over conditions as the SNR.

    Parameters
    ----------

    noise_samps : ndarray (n_chan, n_samps, n_pts)

    sig_samps : ndarray (n_chan, n_conds, n_reps, n_pts)
    
    Returns
    -------

    max_dist : ndarray (n_chan,)
        The best (maximum) of average Mahalanobis distances.

    max_base : ndarray (n_chan,)
        If b_max is true, then this is he same maximal statistic 
        computed on a partition of the noise sample. Otherwise it is
        the median self-distance of baseline samples to the population.
    """

    scorer = mahalanobis(noise_samps)
    n_chan, n_conds, n_reps, n_pts = sig_samps.shape
    dists = scorer(sig_samps.reshape(n_chan, n_conds*n_reps, n_pts))
    dists /= np.sqrt(n_pts)
    dists.shape = (n_chan, n_conds, n_reps)
    ## dists = mahalanobis(noise_samps, sig_samps)
    ## dists.shape = (dset.data.shape[0], n_tones, -1)
    base_dists = scorer(noise_samps).reshape(dists.shape)
    base_dists /= np.sqrt(n_pts)

    max_dist = np.max( np.nanmedian( dists, axis=-1 ), axis=-1 )

    #return dists, base_dists
    return max_dist, np.percentile(base_dist, thresh_distance)

def coding_error(baseline, response, beta_p=1e-3):
    """ 
    Compute the theoretical coding cost suffered when approximating
    the response from the baseline distribution. This cost is calculated
    by the Kullback-Leibler divergence for two Gaussians.

    baseline: ndarray (n_chan, m1)
        covariance will be estimated as (B-mu)(B-mu)^T

    response: ndarray, ( [n_samps], n_chan, m2 )
        covariance for each sample will be estimated by (R-mu)(R-mu)^T

    """

    N = len(baseline)
    baseline = baseline - baseline.mean(0)
    # u is Cbb's eigenvectors, diag(s)^2 is Cbb's eigenvalues
    u, s, vt = la.svd(baseline, 0)

    beta_b = s[0]**2 * beta_p
    det_b = np.sum( np.log( s**2 + beta_b ) )

    if response.ndim < 3:
        response = response.reshape( (np.newaxis,) + response.shape )
    kl_costs = list()
    for R in response:
        R = R - R.mean(0)
        Crr = R.dot(R.T) / R.shape[-1]
        ## Crr = Crr.dot(u)
        ## vt_C_v = (u * Crr).sum(0)
        ## tr_CC = (vt_C_v / (beta_b + s**2)).sum()
        Crr = u.T.dot(Crr)
        Crr = ( u / (s**2 + beta_b) ).dot(Crr)
        tr_CC = np.trace(Crr)

        # pad the diagonal to ensure positive-definite stability
        Rs = la.svdvals(R)
        beta = Rs[0]**2 * beta_p
        det_r = np.sum( np.log( Rs**2 + beta ) )
        
        print det_b, det_r, tr_CC

        kl_costs.append( 0.5 * (det_b + tr_CC - det_r - N) )
    return np.array(kl_costs)

def simple_kld(baseline, response):
    # compute KL(r | b)
    N = len(baseline)
    baseline = baseline - baseline.mean(0)
    # u is Cbb's eigenvectors, diag(s)^2 is Cbb's eigenvalues
    b = la.svdvals(baseline, 0)
    b /= b.sum()
    if response.ndim < 3:
        response = response.reshape( (np.newaxis,) + response.shape )
    kl_costs = list()
    for R in response:
        R = R - R.mean(0)
        Rs = la.svdvals(R)
        Rs /= Rs.sum()

        kl_costs.append( np.sum( Rs[:-1] * (np.log(Rs[:-1]) - np.log(b[:-1])) ) )

    return np.array(kl_costs)
    

import scipy.stats as stats
import scipy.stats.distributions as distributions
import random
from sandbox.array_split import split_at

def avg_resp_zscore(
        responses, baseline, score_fn='rms', n_samps=1000, lognorm=False
        ):
    """
    Determines whether anything at all is happening at a recording site
    based on a very loose test comparing the response averaged over 
    repetitions to many bootstrap-resampled averages of the baseline.
        
    * responses is shaped: n_chan, n_cond, n_rep, n_pts (may be nan-filled)
    * baseline is shaped n_chan, n_samp, n_pts
    * score_fn is a functional that scoress a vector of n_pts
    
    """
    
    # this would assume that all channels have seen equal repetitions,
    # but each condition may have had unequal repititions.
    valid = np.isfinite(responses[0, :, :, 0])
    groups = np.sum(valid, axis=1)
    max_rep = max(groups)
    n_cond = len(groups)

    avg_responses = np.nanmean(responses, axis=-2)

    if isinstance(score_fn, str):
        if score_fn == 'rms':
            score_fn = lambda x: x.std(-1)
        elif score_fn == 'mahal':
            score_fn = mahalanobis(baseline)
        else:
            raise ValueError('cannot score based on {0}'.format(score_fn))
    # else assume score fn is callable

    # resample each channel's baseline responses to get n_samps groups
    # of max_rep baseline vectors
    n_cond = 1
    resample = np.random.randint(
        0, baseline.shape[1], n_samps * n_cond * max_rep
        )
    samp_epochs = baseline.take(resample, axis=1)
    samp_epochs.shape = ( len(baseline), n_samps, n_cond, max_rep, -1 )
    # mask the sample counts to match the condition x reps
    if n_cond > 1 and not valid.all():
        samp_mask = np.zeros( samp_epochs.shape, dtype='?' )
        samp_mask *= (~valid)[None, None, :, :, None]
        samp_epochs = np.masked_array(samp_epochs, samp_mask)
    samp_epochs = np.ma.masked_invalid(samp_epochs)
    # make n_samps "means" and score them as if they're mean evoked-responses
    samp_epochs = samp_epochs.mean(-2)

    # get n_samps false scores for each channel -- (n_chans, n_samps)
    samp_scores = score_fn(samp_epochs)

    # z-score the true responses based on the mean and stdev of the
    # bootstrapped sample -- these are shaped (n_chan, n_cond)
    if lognorm:
        # Note: since the baseline RMS should be very low, they
        # are modeled as log-normal rather than normal
        samp_mu = np.log(samp_scores).mean(1)
        samp_stdev = np.log(samp_scores).std(1)
        #print samp_mu, samp_stdev
        true_score = np.log(score_fn(avg_responses))
    else:
        samp_mu = samp_scores.mean(1)
        samp_stdev = samp_scores.std(1)
        true_score = score_fn(avg_responses)
    
    z_score = ( (true_score - samp_mu) / samp_stdev )
    if hasattr(z_score, 'mask'):
        z_score = np.array(z_score.filled(0))
    return z_score
    
def avg_resp_zscore2(
        responses, baseline, score_fn='rms', n_samps=1000, lognorm=False
        ):
    """
    Determines whether anything at all is happening at a recording site
    based on a very loose test comparing the response averaged over 
    repetitions to many bootstrap-resampled averages of the baseline.
        
    * responses is shaped: n_chan, n_cond, n_rep, n_pts (may be nan-filled)
    * baseline is shaped n_chan, n_samp, n_pts
    * score_fn is a functional that scoress a vector of n_pts
    
    """
    
    # this would assume that all channels have seen equal repetitions,
    # but each condition may have had unequal repititions.
    valid = np.isfinite(responses[0, :, :, 0])
    groups = np.sum(valid, axis=1)
    max_rep = max(groups)
    n_cond = len(groups)

    avg_responses = np.nanmean(responses, axis=-2)

    if isinstance(score_fn, str):
        if score_fn == 'rms':
            score_fn = lambda x: x.std(-1)
        elif score_fn == 'mahal':
            score_fn = mahalanobis(baseline)
        else:
            raise ValueError('cannot score based on {0}'.format(score_fn))
    # else assume score fn is callable

    # resample each channel's baseline responses to get n_samps groups
    # of max_rep baseline vectors
    resample = np.random.randint(
        0, baseline.shape[1], n_samps * max_rep
        )
    samp_epochs = baseline.take(resample, axis=1)
    samp_epochs.shape = ( len(baseline), n_samps, max_rep, -1 )
    samp_epochs = np.ma.masked_invalid(samp_epochs)
    # make n_samps "means" and score them as if they're mean evoked-responses
    samp_epochs = samp_epochs.mean(-2)

    # get n_samps false scores for each channel -- (n_chans, n_samps)
    samp_scores = score_fn(samp_epochs)

    samp_scores = np.sort(samp_scores, axis=-1)
    true_scores = score_fn(avg_responses)
    chan_p = [ samp.searchsorted(true.ravel()) 
               for samp, true in zip(samp_scores, true_scores) ]
    chan_p = np.array(chan_p).reshape(true_scores.shape)
    return 1 - chan_p/n_samps
    
    ## # z-score the true responses based on the mean and stdev of the
    ## # bootstrapped sample -- these are shaped (n_chan, n_cond)
    ## if lognorm:
    ##     # Note: since the baseline RMS should be very low, they
    ##     # are modeled as log-normal rather than normal
    ##     samp_mu = np.log(samp_scores).mean(1)
    ##     samp_stdev = np.log(samp_scores).std(1)
    ##     #print samp_mu, samp_stdev
    ##     true_score = np.log(score_fn(avg_responses))
    ## else:
    ##     samp_mu = samp_scores.mean(1)
    ##     samp_stdev = samp_scores.std(1)
    ##     true_score = score_fn(avg_responses)
    
    ## z_score = ( (true_score - samp_mu) / samp_stdev )
    ## return z_score
    
## def avg_resp_zscore(ana_obj, n_samps=1000):
##     # response & scoring windows have already been set in the ana_obj
##     # -- meaning this should be an internal class method
##     conds, _ = ana_obj.exp.enumerate_conditions()
##     groups = [np.sum(conds==i) for i in np.unique(conds)]
##     if ana_obj.scoring == 'mahal':
##         # mahalanobis scoring is quirky.. the sample size
##         # needs to match 
##         n_samps = np.sum(groups)
##     max_rep = max(groups)
##     base_epochs = ana_obj.epochs(baseline=True)
##     # resample each channel's baseline responses to get n_samps groups
##     # of max_rep baseline vectors
##     resample = np.random.randint(0, base_epochs.shape[1], n_samps * max_rep)
##     samp_epochs = base_epochs.take(resample, axis=1)
##     samp_epochs.shape = ( len(base_epochs), n_samps, max_rep, -1 )
##     # make n_samps "means" and score them as if they're mean evoked-responses
##     samp_epochs = samp_epochs.mean(-2)
##     # get n_samps false scores for each channel -- (n_chans, n_samps)
##     samp_scores = ana_obj.score_fn(samp_epochs)

##     # z-score the true responses based on the mean and stdev of the
##     # bootstrapped sample
##     samp_mu = samp_scores.mean(1)
##     samp_stdev = samp_scores.std(1)

##     # this beast is shaped (nchan, nc1, [nc2, [[nc3, ...]] ])
##     # so... z-score each different condition based on the same 
##     # null hypothesis distribution estimates?
##     true_score = ana_obj.ep_score().transpose() # put channel dimension last

##     z_score = ( (true_score - samp_mu) / samp_stdev ).transpose()
##     return z_score

# XXX: this method requires the 1st two arguments to be split
#      equally on the first axis.. need to tweak split_at to accomplish this
@split_at(split_arg=(0,1), splice_at=(0,1))
def active_sites(baseline, evoked, pcrit=1e-4, n_boot=100):
    zscores = np.array(
        [ rank_sum_scores(baseline[n], evoked[n], bootstrapped=n_boot)[0]
          for n in xrange(len(evoked)) ]
          )

    chi2_scores = np.sum( zscores**2, axis=1 )
    thresh = distributions.chi2.isf( pcrit, evoked.shape[1]-1 )
    #return chi2_scores > thresh, chi2_scores
    return chi2_scores > thresh, zscores

def rank_sum_scores(baseline, evoked, bootstrapped=40):
    """

    evoked : list of arrays
        Array of n x k_n values (n classes to score by k_n samples in class n).
        If evoked is padded with nans, then these will be stripped prior to
        testing to avoid spurious ranks
    baseline : ndarray, 1D
        very large sample of baseline scores (M >> k) over which to bootstrap
        resample

    Use scipy's ranksums() test, which returns the z-value of the first sample
    
    """
    M = baseline.shape[0]
    if not np.isfinite(evoked).all():
        evoked = [ev[np.isfinite(ev)] for ev in evoked]
    kn = map(len, evoked)
    if bootstrapped:
        fn = stats.ranksums
        # arrange this so that z is positive for significant evoked
        r = [ [fn(evoked[j], baseline[random.sample(xrange(M), kn[j])])
               for i in xrange(bootstrapped)] for j in xrange(len(evoked)) ]
        z = np.array(r).mean(1)[:, 0]

        # one-sided test for epochs-score > baseline-score
        prob = distributions.norm.sf(z)
        return z, prob

    # if not bootstrapped, then compare each class sample against
    # the full baseline sample and convert to normal approximated z
    fn = stats.ranksums
    r = [fn(evoked[j], baseline) for j in xrange(len(evoked))]
    ## z = [ (r[n][0] - kn[n]*M/2.0) / np.sqrt(kn[n] *M * (kn[n]+M+1) / 12.0)
    ##       for n in xrange(len(epochs)) ]
    r = np.array(r)
    z = r[:,0]
    # return one-sided test for evoked > baseline
    prob = distributions.norm.sf(z)
    return z, prob

def nroots_average(scores):
    """
    Make a weighted average of the n roots of unity. The number of
    root n is taken by the number of scores to balance, and the weight
    of each root is the score itself.

    Parameters
    ----------

    scores : sequence
        A series of positive scores over a monotonic and evenly
	    spaced field of categories

    Returns
    -------

    category, strength : average direction (category) and strength of preference
    
    """

    
    # make a weighted average of the n-roots of unity based on the
    # given scores.
    scores = np.atleast_2d(scores)
    n_angles = scores.shape[-1]
    # make n_angles equally spaced vectors around the circle, with none
    # located at theta = 0
    d_phi = 2*np.pi / n_angles
    p_angles = np.linspace(d_phi/2, 2*np.pi - d_phi/2, n_angles)
    phasors = np.exp(1j * p_angles )
    wphasors = (scores * phasors).sum(axis=-1)
    wphasors /= scores.sum(axis=-1)
    phs = np.angle(wphasors)
    nphs = phs[ phs < 0 ]
    phs[ phs < 0 ] = nphs + 2*np.pi
    # Map the angle from [d_phi/2, 2PI-d_phi/2) back to a 
    # value within [0, n_angle].  Values outside this range 
    # ( i.e. [0, d_phi/2) U (2PI-d_phi/2, 2PI] ) are indeterminate,
    # but peg them to the edge of the output range
    phs = np.clip(phs, d_phi/2, 2*np.pi - d_phi/2) - d_phi/2
    category = ( phs / (2 * np.pi - d_phi) ) * n_angles
    return category.squeeze(), np.abs(wphasors).squeeze()

@split_at()
def shuffled_nroots_average(scores, n=1000):
    p = np.zeros(len(scores))
    _, r = nroots_average(scores)
    for k, s in enumerate(scores):
        s_shuffled = np.array(
            [np.random.choice(s, size=len(s), replace=False) 
             for i in xrange(n)]
            )
        _, sr = nroots_average(s_shuffled)
        p[k] = 1 - np.sort(sr).searchsorted(r[k]) / float(n)
    return p

### These routines score characteristic frequency from an FRA
#@split_at()
def array_cf(trial_scores, **kwargs):
    kwargs['plot'] = False
    cf = np.zeros(len(trial_scores))
    for n in xrange(len(cf)):
        print n
        cf[n] = bootstrapped_cf(trial_scores[n])
    return cf

def bootstrapped_cf(trial_scores, plot=False, smoothing=1):
    ### Note: this stands for 'characteristic frequency'
    ### I believe this was hacked circa SFN 2014
    # trials scores should be (n_tones, n_amps, n_trials)
    from ecoglib.numutil import bootstrap_stat
    from sklearn.mixture import GMM
    from scipy.ndimage import gaussian_filter1d
    from matplotlib import _cntr as cntr
    import matplotlib.pyplot as pp
    from seaborn.algorithms import bootstrap
    
    b_scores = bootstrap_stat(
        trial_scores.transpose(2, 0, 1), axis=0, 
        func=np.nanmean, n_boot=250
        )
    ## b_scores = bootstrap(
    ##     trial_scores.transpose(2, 0, 1), axis=0, 
    ##     func=np.nanmean, n_boot=250
    ##     )
    gmm_ = GMM(n_components=2).fit(b_scores.ravel())
    gmm0_ = GMM(n_components=1).fit(b_scores.ravel())
    scores = b_scores.mean(0)
    if gmm0_.bic(scores.ravel()) < gmm_.bic(scores.ravel()):
        #print 'two distribution model not supported'
        return -1
    # ignorant way of finding threshold
    range = np.linspace(trial_scores.min(), trial_scores.max(), 1000)
    low_class = np.argmin(gmm_.means_)
    null_posterior = gmm_.predict_proba(range)[:,low_class]
    x = np.where(null_posterior > 0.5)[0][-1]
    thresh = range[x + 1]

    if smoothing > 0:
        sig = smoothing
        scores = gaussian_filter1d(
            gaussian_filter1d(scores, sig, axis=-1), sig, axis=-2
            )
    tones, amps = scores.shape

    yy, xx = np.mgrid[:tones, :amps]
    c = cntr.Cntr(xx, yy, scores)
    res = c.trace(thresh)[:-1]
    if not res:
        #print 'no contour at the threshold level'
        return -1
    i = np.argmax( map(len, res) )
    res = res[i]
    min_amp = np.argmin(res[:,0])
    cf = res[min_amp, 1]
    if plot:
        f = pp.figure()
        pp.imshow(scores, cmap=pp.cm.gray_r)
        pp.colorbar()
        pp.xlabel('amps'); pp.ylabel('tones')
        pp.contour(xx, yy, scores, [thresh], colors='r')
        return cf, f
    return cf

class _ndinterp(object):
    """
    A wrapper of RectBivariateSpline that interpolates 2D vector fields.
    """
    def __init__(self, x, y, z, **kwargs):
        # z is (nx, ny, p)
        self.bbox = [x.min(), x.max(), y.min(), y.max()]
        self._interps = [ 
            RectBivariateSpline(x, y, z[..., n], **kwargs)
            for n in xrange(z.shape[-1])
            ]
    def __call__(self, coords, masked=False, mask_tol=0):
        arr = [ bvs(*coords.T, grid=False) for bvs in self._interps ]
        if masked:
            bbox = self.bbox
            dx = dy = mask_tol
            m = (coords[:,0] <= bbox[0] - dx) | (coords[:,0] >= bbox[1] + dx) |\
                (coords[:,1] <= bbox[2] - dy) | (coords[:,1] >= bbox[3] + dy)
            m = np.tile(m[:,None], (1, len(arr)))
            ma = np.ma.masked_array( np.column_stack(arr), mask=m )
            return ma
        return np.column_stack(arr)

def semi_affine_interp(field, a, i0, j0, **kwargs):
    ny, nx = field.shape[:2]
    jj, ii = np.meshgrid(np.arange(nx) - (nx-1)/2., np.arange(ny) - (ny-1)/2.)
    coords = np.c_[ii.ravel(), jj.ravel()]

    R = np.array([ [np.cos(a), -np.sin(a)], [np.sin(a), np.cos(a)] ])
    off = np.array( [j0, i0] )
    tcoords = (coords[..., ::-1].dot(R.T) + off)[..., ::-1]

    f_interp = _ndinterp(
        np.arange(ny)-(ny-1)/2., np.arange(nx)-(nx-1)/2., field
        )
    f_interp = f_interp(tcoords, **kwargs).reshape(ny, nx, -1)
    return f_interp

import scipy.optimize as opt
def coregister_fields(
        f1, f2, align='spatial', bounds=None, noisy=False, normed_cov=True
        ):
    assert f1.shape[:2] == f2.shape[:2], 'fields must be sampled equally'
    ny, nx = f1.shape[:2] # assume normal matrix ordering: (i,j) --> (y,x)

    jj, ii = np.meshgrid(np.arange(nx) - (nx-1)/2., np.arange(ny) - (ny-1)/2.)
    coords = np.c_[ii.ravel(), jj.ravel()]

    f1_flat = f1.reshape(nx*ny, -1)
    if align == 'spatial':
        f1_flat = f1_flat - f1_flat.mean(0)
    else:
        f1_flat = f1_flat - f1_flat.mean(1)[:,None]

    # interpolator works in transpose coordinates, so transpose input matrices
    ## f2_interp = RegularGridInterpolator( 
    ##     (np.arange(ny)-(ny-1)/2., np.arange(nx)-(nx-1)/2.), 
    ##     f2, #f2.transpose(1, 0, 2), 
    ##     fill_value=np.nan, bounds_error=False
    ##     )
    f2_interp = _ndinterp(
        np.arange(ny)-(ny-1)/2., np.arange(nx)-(nx-1)/2., f2
        )
    def _affine(a, x0, y0):
        # returns a function that transforms an N x 2 array of coordinates
        R = np.array([ [np.cos(a), -np.sin(a)], [np.sin(a), np.cos(a)] ])
        off = np.array( [x0, y0] )
        return lambda coords: (coords - off).dot(R.T) + off
    ## def _semi_affine(a, x0, y0):
    ##     # returns a function that transforms an N x 2 array of coordinates
    ##     # in a semi-affine transformation that rotates and then translates
    ##     R = np.array([ [np.cos(a), -np.sin(a)], [np.sin(a), np.cos(a)] ])
    ##     off = np.array( [x0, y0] )
    ##     return lambda coords: coords.dot(R.T) + off
    def _semi_affine(a, i0, j0):
        # returns a function that transforms an N x 2 array of coordinates
        # in a semi-affine transformation that rotates and then translates
        R = np.array([ [np.cos(a), -np.sin(a)], [np.sin(a), np.cos(a)] ])
        off = np.array( [j0, i0] )
        return lambda coords: (coords[..., ::-1].dot(R.T) + off)[..., ::-1]

    def loss_fn(p, normed_cov=True):
        T = _semi_affine(*p)
        Tcoords = T(coords)
        f2_p = f2_interp(Tcoords)
        if align == 'spatial':
            # subtract 1st moment across space
            f2_p = f2_p - np.nanmean(f2_p, axis=0)
        else:
            # subtract 1st moment across the field measurements
            f2_p = f2_p - np.nanmean(f2_p, axis=1)[:,None]
        if normed_cov:
            axis = 0 if align == 'spatial' else 1
            f1_cov = f1_flat.var(axis=axis)
            f2_p = np.ma.masked_invalid(f2_p)
            f2_cov = f2_p.var(axis=axis)
            # covariance should *not* be compensated by masked values:
            # masked values should result in less-than-optimal covariance
            cov = ( f2_p * f1_flat ).sum(axis=axis) / f2_p.shape[axis]
            # to be convervative, within-field variance should be
            # computed over un-masked values
            norm = np.sqrt(f1_cov * f2_cov)
            cov /= norm
            cov = cov.sum() / len(cov)
        else:
            cov = np.nanmean(f2_p * f1_flat)
        if noisy:
            print p, cov
        return -cov

    p0 = np.array( [0, 0, 0], 'd' )
    #r = opt.fmin_bfgs(loss_fn, p0, full_output=1)
    #r = opt.minimize(loss_fn, p0, method='BFGS')
    if bounds:
        method = 'SLSQP'
        method = 'TNC'
    else:
        method = 'Nelder-Mead'
    method = None
    print 'init:', np.nanmean(f1_flat * f2.reshape(nx*ny, -1))
    r = opt.minimize(
        loss_fn, p0, method=method, bounds=bounds, args=(normed_cov,)
        )

    # compute normalized cross-correlation of fields
    cov = -loss_fn(r.x, normed_cov=normed_cov)
    return r, _semi_affine(*r.x), cov
        
### Depends on the Analysis type in the anatool package
def get_best_ep(
        ana_obj, site_pctile=0.9, indexing=False, 
        scoring='mahal', sigma_penalty=0
        ):
    resps, groups = ana_obj.epochs(arranged=True)
    ana_obj.scoring = scoring
    # assuming group sizes are all equal
    n_groups = len(groups); group_sz = groups[0]

    # score based on the mean single trial response
    ## scores = ana_obj.score_fn(r=resps).reshape(-1, n_groups, group_sz)
    ## scores = scores.mean(-1)

    # score based on the difference of mean-to-sigma of single-trial responses
    scores = ana_obj.score_fn(r=resps).reshape(-1, n_groups, group_sz)
    scores = scores.mean(-1) - sigma_penalty * scores.std(-1)
    
    # try just scoring the mean evoked response
    ## scores = ana_obj.ep_score(reshape=False)
    
    best_stim = np.argmax(scores, axis=-1)
    best_score = scores[ zip(*enumerate(best_stim)) ]

    resps.shape = (len(resps), n_groups, group_sz, -1)
    if site_pctile >= 0:
        # choose one channel
        n = int( np.floor( site_pctile * len(best_score) ) )
        best_chan = np.argsort(best_score)[n]
        best_stim = best_stim[best_chan]
        if indexing:
            # return how to index the responses, 
            # rather than the actual responses
            return ( best_chan, 
                     slice(best_stim*group_sz, (best_stim+1)*group_sz) )
        eps = resps[best_chan, best_stim]

        false_eps = ana_obj.epochs(chan=best_chan, baseline=True).squeeze()
        samps = random.sample(range(false_eps.shape[0]), len(eps))
        return eps, false_eps[samps]

    if indexing:
        return zip(
            *enumerate( [slice(bs*group_sz, (bs+1)*group_sz) 
                         for bs in best_stim] )
            )
    eps = resps[ zip(*enumerate(best_stim)) ]
    false_eps = ana_obj.epochs(baseline=True)
    r = range(false_eps.shape[1])
    f_eps = np.array( [f_ep[random.sample(r, group_sz)]
                       for f_ep in false_eps] )
    # roll all samples into 1st axis
    eps.shape = (-1, eps.shape[-1])
    f_eps.shape = (-1, f_eps.shape[-1])
    return eps, f_eps

import nitime.algorithms as ntalg
import ecoglib.numutil as nut
def score_by_harmonic_stim(dataset, NW=7, mnfreq=-1, mxfreq=150):
    exp = dataset.exp
    Fs = dataset.Fs
    t0 = dataset.exp.time_stamps[0]
    tf = dataset.exp.time_stamps[-1]
    N = tf - t0
    NFFT = nut.nextpow2(N)
    dpss, eigs = ntalg.dpss_windows(N, NW, 2*NW)
    k = eigs > .98
    dpss = dpss[k]
    eigs = eigs[k]
    fx = np.linspace(0, Fs / 2, NFFT / 2 + 1)
    f0 = Fs / np.diff(dataset.exp.time_stamps).mean()
    if mnfreq < 0:
        mn_freq = f0
    h = np.arange(np.floor(mnfreq/f0), np.ceil(mxfreq/f0)) * f0
    ix = fx.searchsorted(h)
    scores = np.zeros( len(dataset.data) )
    for n, chan in enumerate(dataset.data):
        yk = ntalg.tapered_spectra(chan[t0:tf], dpss, NFFT=NFFT)
        pxx = ntalg.mtm_cross_spectrum(yk, yk, eigs[:,None], sides='onesided')
        scores[n] = pxx[ix].sum()
        print n,
    return scores
        
        

def middle_of_three(samps, quantile=100, repeat=10):
    scores = mahalanobis(samps, samps)
    middle_scores = np.zeros(samps.shape[0])
    stats = np.zeros(repeat)
    for s, site_scores in enumerate(scores):
        n_groups = len(site_scores) // 3
        p = n_groups * 3
        for n in xrange(repeat):
            idx = np.arange(len(site_scores))
            np.random.shuffle(idx)
            idx = idx[:p]
            mids = np.sort(site_scores[idx].reshape(3,-1), axis=0)[1]
            stats[n] = np.percentile(mids, quantile)
        middle_scores[s] = stats.mean()
    return middle_scores

def roc_test(
        vec_ts, baseline, event, discovery_rate=False, discovery_points=5,
        prop_active=0.1, limits=(), n_steps=100
        ):
    # vec_ts : shape (n_time, n_chan)
    # baseline : shape (n_samps, n_chan)
    # event : an event mask shape (n_time,) (value == 1 during events),
    #         or a length-2 list: [pos_edges, neg_edges]
    #
    # Steps:
    # 
    # Score the Mahalanobis distance of the vector timeseries at
    # each time step.  Mahalanobis distance is calculated based on
    # the given baseline sample
    #
    # Swing a threshold from low to high and calculate the detection
    # rate and the false alarm rate. The marginal probabilities of
    # active versus baseline states are assumed different, so these rates 
    # are defined slightly differently. Detection is counted as the
    # score exceeding threshold for some proportion of the given active
    # period and detection rate is the proportion of detected events.
    # False alarm rate is defined as the proportion of times that 
    # the score rises above threshold outside of the event periods.
    #
    # Summary:
    #
    # tp (True Positive Rate):
    #   P{ Detection | Response Window }
    #
    # fa (False Alarm Rate):
    #   P{ Detection | ~Response Window }
    #
    # If discovery_rate is set to True, then the "true positive" 
    # and "false alarm" statistics are interpreted as:
    #
    # tp (True Discovery Rate):
    #   proportion of detections falling under events 
    #
    # fa (False Discovery Rate): 
    #  proportion of detections falling outside of events 
    #      
    # Furthermore, discovery outside of the response window is limited
    # to occasions where at least "discovery_period" consecutive samples
    # are super-threshold.

    md = mahalanobis(baseline, vec_ts)
    if not len(limits):
        limits = md.min(), md.max()
    #t = np.linspace(limits[0], limits[1], n_steps)
    t = np.logspace(np.log10(limits[0]), np.log10(limits[1]), n_steps)

    if len(event) == 2:
        pos_edges, neg_edges = event
        event = np.zeros( len(vec_ts), dtype='?' )
        for i1, i2 in zip(pos_edges, neg_edges):
            event[i1:i2] = True
    elif len(event) == len(vec_ts):
        pos_edges = np.where( np.diff( event.astype('i') ) > 0 )[0] + 1
        neg_edges = np.where( np.diff( event.astype('i') ) < 0 )[0] + 1
        event = event.astype('?')
    else:
        raise ValueError('unknown event coding')
    non_event = md[~event]

    num_event = len(pos_edges)
    max_event = (neg_edges - pos_edges).max()
    event_scores = np.empty( (num_event, max_event), 'd' )
    detect_cutoff = np.zeros(num_event)
    event_scores.fill(np.nan)
    for n in xrange(num_event):
        pe = pos_edges[n]
        ne = neg_edges[n]
        event_scores[n, :(ne-pe)] = md[pe:ne]
        detect_cutoff[n] = prop_active * (ne - pe)
    event_scores = np.ma.masked_invalid(event_scores)

    tp = np.zeros_like(t)
    fa = np.zeros_like(t)
    detection_window = np.ones(discovery_points)
    for n in xrange(n_steps):
        detect = (event_scores > t[n]).sum(axis=1) > detect_cutoff
        if discovery_rate:
            false_detect = np.convolve( 
                (non_event > t[n]).astype('i'), detection_window, mode='same'
                )
            S = detect.sum()
            V = (false_detect >= discovery_points).sum()
            R = float( S + V )
            tp[n] = S / R
            fa[n] = V / R
        else:
            tp[n] = detect.sum() / float(num_event)
            fa[n] = ( non_event > t[n] ).sum() / float(non_event.size)

    # clean up
    mask = np.isfinite(fa) if discovery_rate else ( tp > 0 )
    return tp[mask], fa[mask], t[mask]
