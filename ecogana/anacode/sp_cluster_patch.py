from scipy.cluster.hierarchy import *
import numpy as np

class ClusterNode:
    """
A tree node class for representing a cluster.

Leaf nodes correspond to original observations, while non-leaf nodes
correspond to non-singleton clusters.

The to_tree function converts a matrix returned by the linkage
function into an easy-to-use tree representation.

See Also
--------
to_tree : for converting a linkage matrix ``Z`` into a tree object.

"""

    def __init__(self, id, left=None, right=None, dist=0, count=1):
        if id < 0:
            raise ValueError('The id must be non-negative.')
        if dist < 0:
            raise ValueError('The distance must be non-negative.')
        if (left is None and right is not None) or \
           (left is not None and right is None):
            raise ValueError('Only full or proper binary trees are permitted.'
                             ' This node has one child.')
        if count < 1:
            raise ValueError('A cluster must contain at least one original '
                             'observation.')
        self.id = id
        self.left = left
        self.right = right
        self.dist = dist
        if self.left is None:
            self.count = count
        else:
            self.count = left.count + right.count

    def get_id(self):
        """
The identifier of the target node.

For ``0 <= i < n``, `i` corresponds to original observation i.
For ``n <= i < 2n-1``, `i` corresponds to non-singleton cluster formed
at iteration ``i-n``.

Returns
-------
id : int
The identifier of the target node.

"""
        return self.id

    def get_count(self):
        """
The number of leaf nodes (original observations) belonging to
the cluster node nd. If the target node is a leaf, 1 is
returned.

Returns
-------
get_count : int
The number of leaf nodes below the target node.

"""
        return self.count

    def get_left(self):
        """
Return a reference to the left child tree object.

Returns
-------
left : ClusterNode
The left child of the target node. If the node is a leaf,
None is returned.

"""
        return self.left

    def get_right(self):
        """
Returns a reference to the right child tree object.

Returns
-------
right : ClusterNode
The left child of the target node. If the node is a leaf,
None is returned.

"""
        return self.right

    def is_leaf(self):
        """
Returns True if the target node is a leaf.

Returns
-------
leafness : bool
True if the target node is a leaf node.

"""
        return self.left is None

    def pre_order(self, func=(lambda x: x.id)):
        """
Performs pre-order traversal without recursive function calls.

When a leaf node is first encountered, ``func`` is called with
the leaf node as its argument, and its result is appended to
the list.

For example, the statement::

ids = root.pre_order(lambda x: x.id)

returns a list of the node ids corresponding to the leaf nodes
of the tree as they appear from left to right.

Parameters
----------
func : function
Applied to each leaf ClusterNode object in the pre-order traversal.
Given the i'th leaf node in the pre-ordeR traversal ``n[i]``, the
result of func(n[i]) is stored in L[i]. If not provided, the index
of the original observation to which the node corresponds is used.

Returns
-------
L : list
The pre-order traversal.

"""

        # Do a preorder traversal, caching the result. To avoid having to do
        # recursion, we'll store the previous index we've visited in a vector.
        n = self.count

        curNode = [None] * (2 * n)
        lvisited = set()
        rvisited = set()
        curNode[0] = self
        k = 0
        preorder = []
        while k >= 0:
            nd = curNode[k]
            ndid = nd.id
            if nd.is_leaf():
                preorder.append(func(nd))
                k = k - 1
            else:
                if ndid not in lvisited:
                    curNode[k + 1] = nd.left
                    lvisited.add(ndid)
                    k = k + 1
                elif ndid not in rvisited:
                    curNode[k + 1] = nd.right
                    rvisited.add(ndid)
                    k = k + 1
                # If we've visited the left and right of this non-leaf
                # node already, go up in the tree.
                else:
                    k = k - 1

        return preorder

_cnode_bare = ClusterNode(0)
_cnode_type = type(ClusterNode)


def to_tree(Z, rd=False):
    """
Converts a hierarchical clustering encoded in the matrix ``Z`` (by
linkage) into an easy-to-use tree object.

The reference r to the root ClusterNode object is returned.

Each ClusterNode object has a left, right, dist, id, and count
attribute. The left and right attributes point to ClusterNode objects
that were combined to generate the cluster. If both are None then
the ClusterNode object is a leaf node, its count must be 1, and its
distance is meaningless but set to 0.

Note: This function is provided for the convenience of the library
user. ClusterNodes are not used as input to any of the functions in this
library.

Parameters
----------
Z : ndarray
The linkage matrix in proper form (see the ``linkage``
function documentation).

rd : bool, optional
When False, a reference to the root ClusterNode object is
returned. Otherwise, a tuple (r,d) is returned. ``r`` is a
reference to the root node while ``d`` is a dictionary
mapping cluster ids to ClusterNode references. If a cluster id is
less than n, then it corresponds to a singleton cluster
(leaf node). See ``linkage`` for more information on the
assignment of cluster ids to clusters.

Returns
-------
L : list
The pre-order traversal.

"""

    Z = np.asarray(Z, order='c')

    is_valid_linkage(Z, throw=True, name='Z')

    # The number of original objects is equal to the number of rows minus
    # 1.
    n = Z.shape[0] + 1

    # Create a list full of None's to store the node objects
    d = [None] * (n * 2 - 1)

    # Create the nodes corresponding to the n original objects.
    for i in xrange(0, n):
        d[i] = ClusterNode(i)

    nd = None

    for i in xrange(0, n - 1):
        fi = int(Z[i, 0])
        fj = int(Z[i, 1])
        if fi > i + n:
            raise ValueError(('Corrupt matrix Z. Index to derivative cluster '
                              'is used before it is formed. See row %d, '
                              'column 0') % fi)
        if fj > i + n:
            raise ValueError(('Corrupt matrix Z. Index to derivative cluster '
                              'is used before it is formed. See row %d, '
                              'column 1') % fj)
        nd = ClusterNode(i + n, d[fi], d[fj], Z[i, 2])
        # ^ id ^ left ^ right ^ dist
        if Z[i, 3] != nd.count:
            raise ValueError(('Corrupt matrix Z. The count Z[%d,3] is '
                              'incorrect.') % i)
        d[n + i] = nd

    if rd:
        return (nd, d)
    else:
        return nd

