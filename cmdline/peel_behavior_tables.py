#!/usr/bin/env python
import os
import numpy as np
import contextlib
import matplotlib.pyplot as pp
from ecogana.devices.load.open_ephys import load_open_ephys_channels
from scipy.special import erfcinv

def dprime(correct, target):
    n_cr = (~target & correct).sum()
    n_fa = (~target & ~correct).sum()
    n_hits = (target & correct).sum()
    n_miss = target.sum() - n_hits
    z = lambda x: -np.sqrt(2) * erfcinv(x*2)
    return z(float(n_cr) / (n_cr + n_fa)) - \
      z(float(n_miss) / (n_hits + n_miss))

def write_table(fname, seq):
    with contextlib.closing( open(fname, 'w') ) as f:
        lines = [str(a) for a in seq]
        f.writelines('\n'.join(lines))

def get_tables(dir, rec, prefix):
    ch = load_open_ephys_channels(
        dir, rec, save_downsamp=False, target_Fs=2000
        )

    del ch.chdata
    adc = ch.adc
    make_tables(adc,dir,prefix)

def make_tables(adc,dir,prefix,print_d=True):
    trials1 = (adc[0] > 2).astype('i')
    pos_edge_target = np.where( np.diff(trials1) > 0 )[0] + 1

    trials2 = (adc[1] > 2).astype('i')
    pos_edge_foil = np.where(np.diff(trials2) > 0)[0] + 1

    pos_edge = np.union1d( pos_edge_target, pos_edge_foil )

    target_trials = np.array([ pe in pos_edge_target for pe in pos_edge ])
    fn = os.path.join(dir, prefix + '_target_tab.txt')
    write_table(fn, target_trials.astype('i'))

    # adc[0] target indicator
    # adc[1] foil indicator
    # adc[2] audio
    # adc[3] hit (high) / miss (low)
    # adc[4] fa (high) / cr (low)
    # adc[5] raw nosepoke

    correct = np.zeros_like(target_trials)
    for i in xrange(len(pos_edge)):
        t_start = pos_edge[i]
        t_end = pos_edge[i+1] if i < len(pos_edge) - 1 else -1
        if target_trials[i]:
            correct[i] = (adc[3, t_start : t_end] > 2).any()
        else:
            correct[i] = (adc[4, t_start : t_end] < 2).all()

    fn = os.path.join(dir, prefix + '_choice_tab.txt')
    write_table(fn, correct.astype('i'))
    if print_d:
        print 'D-prime:', dprime(correct, target_trials)
    
if __name__=='__main__':
    import sys
    import os
    rec, prefix = sys.argv[1:3]
    dir, rec = os.path.split(rec)
    get_tables(dir, rec, prefix)
    
