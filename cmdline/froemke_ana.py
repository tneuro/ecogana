#!/usr/bin/env python

if __name__=='__main__':

    import matplotlib.pyplot as pp
    from matplotlib.figure import Figure
    pyplot_figure = pp.figure
    pp.figure = Figure

    from ecogana.froemke.code.exp_view import TonotopyAnalysisSetup
    
    ast = TonotopyAnalysisSetup()
    ast.configure_traits()
