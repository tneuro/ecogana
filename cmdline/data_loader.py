#!/usr/bin/env python
import argparse

if __name__=='__main__':

    dtext = """
    A GUI menu-based tool to find and load micro-ECoG recordings.
    """
    
    prs = argparse.ArgumentParser(description=dtext)
    prs.add_argument('-m', '--mode', help='Starting mode: manual/automatic',
                     type=str, default='manual')

    args = prs.parse_args()
    mode = args.mode.lower()
    mode = mode[:1].upper() + mode[1:]
    
    from ecogana.devices.data_view import DataLoadSetup
    dls = DataLoadSetup(loadtype=mode)
    dls.configure_traits()
    
