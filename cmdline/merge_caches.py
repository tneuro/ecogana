#!/usr/bin/env python

import os
import sys
import subprocess
import argparse
from glob import glob
import h5py
from tqdm import tqdm

# fr.copy_node('/s16007', group, name='s2017_03_13_160072017_03_13_10_24_56_009_2017_03_13_10_31_56_010')

# rat_node = fr['s16007']
# rat_node.copy('s2016_10_25_160072016_10_25_11_54_58_008_2016_10_25_12_01_20_009', fw['s16007'])


def find_matching_files(source_location, dest_location):

    source_files = glob( os.path.join(source_location, '*.h5') )
    dest_files = glob( os.path.join(dest_location, '*.h5') )

    s_names = [ os.path.split(f)[1] for f in source_files ]
    d_names = [ os.path.split(f)[1] for f in dest_files ]
    
    missing = sorted( list( set(s_names) - set(d_names) ) )
    common = sorted( list( set(s_names).intersection( set(d_names) ) ) )
    return common, missing

def merge(source, dest, fake=False):

    s_tables = filter(lambda s: isinstance(source[s], h5py.Group),
                      source.keys())
    d_tables = filter(lambda s: isinstance(dest[s], h5py.Group),
                      dest.keys())
    ## print s_tables
    ## print d_tables

    if not s_tables:
        return

    missing = sorted( list( set(s_tables) - set(d_tables) ) )
    existing = sorted( list( set(s_tables).intersection( set(d_tables) ) ) )

    print 'In level'
    print '\t', source.name
    print '\t', dest.name
    print 'Groups to copy whole:', missing
    print 'Groups to descent into:', existing

    if missing and not fake:

        for group in missing: #tqdm(missing, desc='Copying'):
            print 'copying'
            source.copy(group, dest)

    for group in existing:
        merge(source[group], dest[group])
              
def call_rsync(source, dest, fake=False):

    args = ['rsync', '-auv', '--info', 'progress2', source, dest]
    if fake:
        print 'Calling', ' '.join(args)
        return
    r = subprocess.call(args)
    return r

def main():

    parser = argparse.ArgumentParser(description='merges HDF5 records')

    parser.add_argument('source', help='source file/directory')
    parser.add_argument('dest', help='destination file/directory')

    parser.add_argument('-d', '--dry-run',
                        help='dry run, only list changes',
                        action='store_true')
    parser.add_argument('-b', '--batch',
                        action='store_true',
                        help='merge all HDF5 files found in common')
    parser.add_argument('-c', '--copy-new',
                        action='store_true',
                        help='copy HDF5 from source that are not in dest')

    args = parser.parse_args()

    source = args.source
    dest = args.dest

    #args.dry_run = True

    if args.batch:
        common, missing = find_matching_files(source, dest)
        sources = [ os.path.join(source, f) for f in common ]
        dests = [ os.path.join(dest, f) for f in common ]
    else:
        sources = [ source ]
        dests = [ dest ]
        missing = []

    print sources, dests
        
    for s, d in zip(sources, dests):
        try:
            with h5py.File(s, 'r') as fr, h5py.File(d, 'a') as fw:
                merge(fr, fw, fake=args.dry_run)
        except:
            print 'File exception', sys.exc_info()[0]

    if missing and args.copy_new:
        for f in missing:
            sf = os.path.join(source, f)
            df = os.path.join(dest, f)
            try:
                call_rsync(sf, df, fake=args.dry_run)
            except:
                print 'rsync exception', sys.exc_info()[0]
                continue

if __name__ == '__main__':
    main()
