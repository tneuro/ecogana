#!/usr/bin/env python

"""

USAGE HELP

$ ./impedance_plot.py -h
usage: impedance_plot.py [-h] [-x MAX] [-n MIN] [-c CMAP] [-s SAVE_DIR]
                         [-f SAVE_FORMATS [SAVE_FORMATS ...]] [-p]
                         impedance_dirs [impedance_dirs ...] electrode

Plot open-ephys impedance magnitude

positional arguments:
  impedance_dirs        Recording names (directories) of the impedance
                        measurement
  electrode             Name of electrode LUT

optional arguments:
  -h, --help            show this help message and exit
  -x MAX, --max MAX     Maximum impedance (open circuit) in kOhm
  -n MIN, --min MIN     Minimum impedance (short circuit) in kOhm
  -c CMAP, --cmap CMAP  Name of colormap
  -s SAVE_DIR, --save-dir SAVE_DIR
                        Save plots to directory
  -f SAVE_FORMATS [SAVE_FORMATS ...], --save-formats SAVE_FORMATS [SAVE_FORMATS ...]
                        Image formats for saved plots
  -p, --phase           Plot impedance phase

EXAMPLE USAGE

$ impedance_plot.py /path/to/2016-06-27_12-33-45_004_Saline1 \
                    /path/to/2016-06-27/2016-06-27_12-34-49_005_Saline2/ \
                    psv_61_intan -f pdf eps -s 'zplots'

Will plot two magnitude impedance maps and save the plots in two
image formats (EPS and PDF)

...
saving zplots/pdf/2016-06-27_12-33-45_004_Saline1_mag.pdf
saving zplots/eps/2016-06-27_12-33-45_004_Saline1_mag.eps
saving zplots/pdf/2016-06-27_12-34-49_005_Saline2_mag.pdf
saving zplots/eps/2016-06-27_12-34-49_005_Saline2_mag.eps


"""

import os.path as osp
import numpy as np
import matplotlib.pyplot as pp
from ecogana.devices.load.open_ephys import load_open_ephys_impedance, plot_Z
from ecogana.anacode.colormaps import nancmap
from ecoglib.vis.script_plotting import ScriptPlotterMaker    

if __name__ == '__main__':
    import argparse

    prs = argparse.ArgumentParser(
        description='Plot open-ephys impedance magnitude'
        )
    prs.add_argument(
        'impedance_dirs', nargs = '+',
        help='Recording names (directories) of the impedance measurement', 
        type=str
        )
    prs.add_argument(
        'electrode', nargs = 1,
        help='Name of electrode LUT', type=str
        )
    prs.add_argument(
        '-a', '--avg', help='Average multiple impedance measurements',
        action='store_true'
        )
    prs.add_argument(
        '-x', '--max', help='Maximum impedance (open circuit) in kOhm',
        type=float, default=1e3
        )
    prs.add_argument(
        '-n', '--min', help='Minimum impedance (short circuit) in kOhm',
        type=float, default=1
        )
    prs.add_argument(
        '-c', '--cmap', help='Name of colormap', type=str, default='winter'
        )
    prs.add_argument(
        '-s', '--save-dir', help='Save plots to directory',
        type=str, default=''
        )
    prs.add_argument(
        '-f', '--save-formats', help='Image formats for saved plots', 
        nargs='+', default=['pdf']
        )
    prs.add_argument(
        '-p', '--phase', help='Plot impedance phase', action='store_true'
        )
    
    args = prs.parse_args()

    elec = args.electrode[0]
    mx = args.max
    mn = args.min
    cm = args.cmap
    sv = args.save_dir
    if sv:
        spm = ScriptPlotterMaker('.', sv, formats=args.save_formats)
    else:
        spm = None

    if args.avg:
        Zs = list()
        tests = list()
        
    for d in args.impedance_dirs:
        while d[-1] == '/':
            d = d[:-1]
        if args.avg:
            path, test = osp.split(osp.abspath(d))
            Z, _ = load_open_ephys_impedance(path, test, elec, magphs=False)
            Zs.append(Z)
            tests.append(test)
        else:
            f = plot_Z(d, elec, mn, mx, cm, phs=args.phase)
            if spm:
                with spm.new_plotter() as splot:
                    fname = osp.split(d)[-1] + ('_phs' if args.phase else '_mag')
                    splot.savefig(f, fname)
    if args.avg:
        Z = np.array(Zs).mean(0)
        Z = np.angle(Z) * 180 / np.pi if args.phase else np.abs(Z)
        f = plot_Z(Z, elec, mn, mx, cm, phs=args.phase, title='Avg Z')
        if spm:
            with spm.new_plotter() as splot:
                fname = '+'.join(tests) + ('_phs' if args.phase else '_mag')
                splot.savefig(f, fname)

    pp.show()
