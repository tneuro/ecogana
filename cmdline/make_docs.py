#!/usr/bin/env python

from subprocess import check_output, STDOUT, call, CalledProcessError
import os
import errno
from decorator import decorator
import shutil

TEMP='tmp'

### From SO:
### http://stackoverflow.com/questions/600268/mkdir-p-functionality-in-python
def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

def exec_in_path(path):
    path = os.path.abspath(path)
    @decorator
    def run(func, *args, **kwargs):
        # get the destination and return paths at run-time
        cpath = os.path.abspath(os.curdir)
        mkdir_p(path)
        os.chdir(path)
        try:
            r = func(*args, **kwargs)
        except CalledProcessError as e:
            print 'exception in {0}'.format(func.__name__)
            print e.output
            r = None
            raise e
        finally:
            os.chdir(cpath)
        return r
    return run
            
def _sed_cmd(root, find, replace):
    uname = os.uname()[0].lower()
    f_cmd = ['find', root, '-type f', '-name "*.py"']
    if uname == 'linux':
        s_cmd = ['sed', '-i', "'s/{0}/{1}/g'".format(find, replace)]

        cmd = f_cmd + ['-print', '|', 'xargs'] + s_cmd
    elif uname == 'darwin':
        s_cmd = ['sed', "-i ''", "'s/{0}/{1}/g'".format(find, replace)]
        cmd = f_cmd + ['-exec'] + s_cmd + ['{} +']
    else:
        raise RuntimeError('Unknown OS '+uname)

    return cmd

@exec_in_path(TEMP)
def setup_repos(src_path):
    src_path = os.path.abspath(src_path)
    for repo in ('ecogana', 'ecoglib'):
        cmd = ['git', 'clone', os.path.join(src_path, repo), repo]
        check_output(cmd)

@exec_in_path(os.path.join(TEMP, 'ecoglib'))
def build_cython():
    check_output('python setup.py build_ext --inplace', shell=True)
    

@exec_in_path(TEMP)
def massage_repos():

    # make ecoglib into a false "subpackage" of ecogana
    check_output('cp -r ecoglib/ecoglib ecogana/ecogana', shell=True)
    # put sandbox into the sphinx building path
    check_output('cp -r ecoglib/sandbox ecogana/', shell=True)

    # replace ecoglib in ecogana/ecoglib
    fr = _sed_cmd('ecogana/ecogana', 'ecoglib', 'ecogana.ecoglib')
    check_output(' '.join(fr), shell=True)
    # replace ecoglib in ecogana/sandbox
    fr = _sed_cmd('ecogana/sandbox', 'ecoglib', 'ecogana.ecoglib')
    check_output(' '.join(fr), shell=True)

@exec_in_path(os.path.join(TEMP, 'ecogana', 'docs', 'sphinx'))
def make_docs(target='full'):
    print 'making docs {0} in {1}'.format(target, os.path.abspath(os.curdir))
    check_output('make {0}'.format(target), shell=True)

def clean():
    shutil.rmtree(TEMP)


if __name__ == '__main__':
    import sys
    import argparse
    parser = argparse.ArgumentParser(description='dirty doc-build script')
    parser.add_argument(
        'src_path', help='path to existing repositories', type=str
        )
    parser.add_argument(
        '-t', '--target', help='make target (e.g. full)', default='full'
        )
    parser.add_argument(
        '-c', '--clean', help='delete tmp dir (default False)',
        action='store_true'
        )
    args = parser.parse_args()
    src_path = os.path.abspath(args.src_path)
    print src_path
    setup_repos(src_path)
    build_cython()
    massage_repos()
    make_docs(args.target)
    if args.clean:
        clean()
    
