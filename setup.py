import os.path as path
from setuptools import setup, find_packages
from glob import glob


with open('requirements.txt') as f:
    reqs = f.readlines()
    reqs = map(str.strip, reqs)
    def _is_repo(s):
        return s.lower().startswith('-e git')
    def _del_soft_reqs(s):
        if 'mayavi' in s.lower():
            return False
        if 'pyside' in s.lower():
            return False
        return not _is_repo(s)
    reqs = filter(_del_soft_reqs, reqs)
    links = filter(_is_repo, reqs)

setup(
    name='ecogana',
    version='0.1',
    packages=find_packages(exclude=['extra_tools', 'docs', 'cmdline']),
    scripts=glob(path.join('cmdline', '*.py')),
    install_requires=reqs,
    dependency_links=links
    )
